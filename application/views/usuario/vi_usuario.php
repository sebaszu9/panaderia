<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>  Usuarios </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Usuario</a></li>
      <li class="active">Registros</li>
    </ol>
  </section>


  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">

          <div class="box-header with-border">
            <h1 class="box-title">Todos los Usuarios</h1>
            <a href="<?= base_url('Controller_usuario/form')?>" class="btn  btn-primary ">Nuevo +</a>
            <div id="result"></div>
            <div id="mensaje"></div>
          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Nombre</th>
                <th>Role</th>
                <th>correo</th>
                <th>sede</th>
                <th>estado</th>
                <th>Acción</th>
              </tr>
              </thead>
              <tbody>
              </tr>

                <?php foreach($info as $datos) { ?>
                <tr>
                  <td><?php echo $datos->usr_nombre;?></td>
                  <td><?php echo $datos->tipus_tipo_usuario;?> </td>
                  <td><?php echo $datos->usr_correo;?> </td>
                  <td><?php echo $datos->bod_nombre;?> </td>
                  <?php if ($datos->usr_estado==1) { ?>
                      <td><button class="btn btn-success change-state" value="<?= $datos->usr_id; ?>" data-polo="0"> Activado </button></td>
                  <?php }elseif ($datos->usr_estado==0) { ?>
                    <td><button class="btn btn-danger change-state" value="<?= $datos->usr_id; ?>" data-polo="1"> Desactivado </button></td>
                  <?php } ?>
                  <td><a class="btn  btn-warning" href="<?php echo base_url('Controller_usuario/edit')."/".$datos->usr_id; ?>"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                  <button class="btn delete-user btn-danger" value="<?= $datos->usr_id; ?>" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
                </tr>
                <?php  }  ?>
              </tbody>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  <!-- /.content -->
  <script>

  $( ".change-state" ).click(function() {
    var state = $(this).data("polo");
    var code = $(this).val();
    var parametros = {  "safety" : true,  "user" : code, "status": state};
    $.ajax({
       data:  parametros,
       url:' <?= base_url('Controller_usuario/change_state')?>',
       type:  'post',
       beforeSend: function () {$("#mensaje").html("<h1>Cargando, espere por favor...</h1>");},
       success:  function (response) {
        const resp = JSON.parse(response);
        if (resp.success==true)
        {
          $("#mensaje").html("");
          alert(resp.message);
          location.reload();
        }
        else
        {
          $("#mensaje").html("");
          alert(resp.message)
        }

       }
     }).fail( function(error) {
       alert(error.status+" Error!! No se pudo eliminar. ");
       $("#mensaje").html(error.status+" "+error.statusText);
    })
  });

  $( ".delete-user" ).click(function() {

    var codigo = $(this).val();
    var mensaje = confirm("¿Confirmar eliminar ?");
    //Detectamos si el usuario acepto el mensaje
    if (mensaje) {
      var parametros = {  "safety" : true,  "codigo" : codigo};
      $.ajax({
         data:  parametros,
         url:' <?= base_url('Controller_usuario/delete')?>',
         type:  'post',
         beforeSend: function () {$("#mensaje").html("<h1>Enviando, espere por favor...</h1>");},
         success:  function (response) {
          const resp = JSON.parse(response);
          if (resp.success==true)
          {
            $("#mensaje").html("");
            alert(resp.message);
          }
          else
          {
            $("#mensaje").html("");
            alert(resp.message)
          }

         }
       }).fail( function(error) {

         alert(error.status+" Error!! No se pudo eliminar. Causa: Puede ser que esta en uso.");
         $("#mensaje").html(error.status+" "+error.statusText);
        //  alert( 'Error!! No se puede eplominar '+JSON.stringify(error));
        //alert(error.statusText)
      })
    }

  });
  </script>

</div>
<!-- /.content-wrapper -->
