
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Usuarios
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= base_url('Controller_usuario/index')?>"><i class="fa fa-dashboard"></i> Usuario</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <?php foreach($info as $datos) { ?>
    <!-- Main content -->
    <section class="content">

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Editar Usuario</h3>
              <a class=" btn  btn-primary pull-right" href="<?php echo base_url('Controller_usuario/index');?>"> Atrás <i class="fa fa-chevron-right"></i> </a>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?= base_url('Controller_usuario/update')?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">

                <?php if ($this->session->flashdata('mensaje'))  { ?>
                  <div class="alert alert-success alert-dismissible" id="hidemiss">
                    <p><?= $this->session->flashdata('mensaje'); ?></p>
                  </div>
                  <script>$( "#hidemiss" ).fadeOut( 5000, "linear" );</script>
                <?php } ?>
                <div class="form-group">
                  <label>Nombre </label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" name="nombre" placeholder="Escribe el nombre del Usuario" value="<?php echo $datos->usr_nombre;?>">
                    <input type="hidden" class="form-control" name="code"  value="<?php echo $datos->usr_id;?>">
                  </div>
                </div>

                <div class="form-group">
                  <label>Correo electronico</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="email" name="email" class="form-control"  placeholder="Escribe el correo del Usuario" value="<?php echo $datos->usr_correo;?>">
                  </div>
                </div>


                <div class="form-group">
                  <label>Tipo de Usuario</label>
                  <div class="input-group">
                    <select class="form-control" name="tipo" >
                      <option value="<?php echo $datos->usr_tipo_usuario;?>"><?php echo $datos->tipus_tipo_usuario;?></option>
                      <option value="1">Administador de SmartBake</option>
                      <option value="3">Administador de Produccion</option>
                      <option value="2">Panadero</option>
                    </select>
                  </div>
                </div>
                <br>

              <div class="box-footer">
                <input type="hidden" value="true" name="create">
                <input type="submit" class="btn btn-primary " value="Actualizar">
              </div>

          </div>
          </form>
          <!-- /.box -->
        </div>

        </div>

        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cambiar Contraseña</h3>
              <div id="mensaje"></div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">

                <input type="hidden" class="form-control" name="code" id="code"  value="<?php echo $datos->usr_id;?>">



                <div class="form-group">
                  <label>Contraseña</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" name="key" class="form-control"  placeholder="Escribe el correo del Usuario" id="key" value="<?php echo set_value('key'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label>Repetir Contraseña</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" name="key2" class="form-control"  placeholder="Escribe el correo del Usuario" id="key2" value="<?php echo set_value('key2'); ?>">
                  </div>
                </div>

              <div class="box-footer">
                <input type="hidden" value="true" name="create">
                <input type="submit" class="btn btn-primary update_password" value="Actualizar Contraseña">
              </div>

          </div>
          <!-- /.box -->
        </div>

        </div>

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
    <?php  }  ?>
  </div>
  <!-- /.content-wrapper -->

  <script>
  $( ".update_password" ).click(function() {

    var code = $("#code").val();
    var key = $("#key").val();
    var key2 = $("#key2").val();

    var parametros = {  "safety":true,"key":key,"key2":key2,"code":code};
    $.ajax({
       data:  parametros,
       url:' <?= base_url('Controller_usuario/update_password')?>',
       type:  'post',
       beforeSend: function () {$("#mensaje").html("<h2>Enviando, espere por favor...</h2>");},
       success:  function (response) {
        const resp = JSON.parse(response);
        if (resp.success==true)
        {
          $("#mensaje").html("<h3 style='color:green;'>"+resp.message+"</h3>");
        }
        else
        {
          $("#mensaje").html("<h3 style='color:red;'>"+resp.message+"</h3>");
        }

       }
     }).fail( function(error) {

       alert(error.status+" Error!! No se pudo eliminar. Causa: Puede ser que esta en uso.");
       $("#mensaje").html(error.status+" "+error.statusText);
      //  alert( 'Error!! No se puede eplominar '+JSON.stringify(error));
      //alert(error.statusText)
    })

  });
  </script>
