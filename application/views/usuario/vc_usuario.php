
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Usuarios
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= base_url('Controller_usuario/index')?>"><i class="fa fa-dashboard"></i> Usuario</a></li>
        <li class="active">nuevo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Crear nuevo usuario</h3>
              <a class=" btn  btn-primary pull-right" href="<?php echo base_url('Controller_usuario/index');?>"> Atrás <i class="fa fa-chevron-right"></i> </a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?= base_url('Controller_usuario/create')?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">

                <?php if ($this->session->flashdata('mensaje'))  { ?>
                  <div class="alert alert-success alert-dismissible" id="hidemiss">
                    <p><?= $this->session->flashdata('mensaje'); ?></p>
                  </div>
                  <script>$( "#hidemiss" ).fadeOut( 5000, "linear" );</script>
                <?php } ?>

                <?php echo validation_errors(); ?>
                <div class="form-group">
                  <label>Nombre </label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" class="form-control" name="nombre" placeholder="Escribe el nombre del Usuario" value="<?php echo set_value('nombre'); ?>">
                </div>
                </div>

                <div class="form-group">
                  <label>Correo electronico</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="email" name="email" class="form-control"  placeholder="Escribe el correo del Usuario" value="<?php echo set_value('email'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label>Contraseña</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" name="key" class="form-control"  placeholder="Escribe el correo del Usuario" value="<?php echo set_value('key'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label>Repetir Contraseña</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" name="key2" class="form-control"  placeholder="Escribe el correo del Usuario" value="<?php echo set_value('key2'); ?>">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <label>Panadería</label>
                  <div class="input-group">
                  <select class="form-control" name="panaderia">
                    <?php foreach($info as $datosd) { ?>
                      <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
                    <?php  }  ?>
                  </select>
                  </div>
                </div>

                <div class="form-group">
                  <label>Tipo de Usuario</label>
                  <div class="input-group">
                    <select class="form-control" name="tipo" >
                      <option value="1">Administador de SmartBake</option>
                      <option value="3">Administador de Produccion</option>
                      <option value="2">Panadero</option>
                      <option value="4">Vendedor</option>
                    </select>
                  </div>
                </div>


              <div class="box-footer">
                <input type="hidden" value="true" name="create">
                <input type="submit" class="btn btn-primary " value="Guardar">
              </div>

          </div>
          </form>
          <!-- /.box -->

        </div>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
