<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado de Clientes.
    </h1>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Clientes/form')?>" class="btn  btn-primary">Nuevo +</a>

          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Nit/Cédula</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Dirección</th>
                <th>Ruta</th>
                <th>Acción</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($info as $datos) { ?>
                <tr>
                    <td><?php echo $datos->clt_cedula;?> </td>
                    <td><?php echo $datos->clt_nombre;?> </td>
                    <td><?php echo $datos->clt_telefono;?> </td>
                    <td><?php echo $datos->name;?> </td>
                    <td><?php echo $datos->clt_direccion;?> </td>
                    <td><a href="<?php echo base_url('Clientes/edit')."/".$datos->clt_id; ?>" class="btn  btn-warning"title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="<?php echo base_url('Clientes/get')."/".$datos->clt_id; ?>" class="btn  btn-danger"title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Nit/Cédula</th>
                  <th>Nombre</th>
                  <th>Teléfono</th>
                  <th>Dirección</th>
                  <th>Acción</th>
                </tr>
              </tfoot>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
