<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Editar
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Clientes</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box-header">

        <a class=" btn btn-primary" href="<?php echo base_url('Clientes/index');?>">Volver</a>
      </div>
      <div class="row">
        <div class="col-md-6">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Editar Departamento Productos</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php foreach($datos as $test) { ?>

    <form role="form" action="<?php echo base_url('Clientes/update/'); ?><?= $test->clt_id; ?>" method="POST" enctype="multipart/form-data">
      <div class="box-body">

        <?php if(isset($mensaje)):?>
                <div class="alert <?= $alert; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?= $mensaje; ?>
                </div>
        <?php endif; ?>
        <?php echo validation_errors(); ?>
        <input type="hidden" name="id"  class="form-control" value="<?= $test->clt_id; ?>" placeholder="Ingrese el nit o cédula del cliente">
        <div class="form-group">
          <label for="exampleInputEmail1">Nit/Cédula</label>
          <input type="number" name="nit"  class="form-control" value="<?= $test->clt_cedula; ?>" placeholder="Ingrese el nit o cédula del cliente">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" name="name"  class="form-control"  value="<?= $test->clt_nombre; ?>" placeholder="Ingrese el nombre del cliente o razón social">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Dirección</label>
          <input type="text" name="dir"  class="form-control" value="<?= $test->clt_direccion; ?>" placeholder="Ingrese la Dirección del cliente">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Teléfono</label>
          <input type="number" name="tel"  class="form-control" value="<?= $test->clt_telefono; ?>"  placeholder="Ingrese el teléfono del Cliente">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Teléfono 2</label>
          <input type="number" name="tel2"  class="form-control"  value="<?= $test->clt_telefono2; ?>" placeholder="Ingrese el teléfono alternativo del Cliente">
        </div>

        <div class="form-group col-md-12">
          <label>Vendedor</label>
          <select class="form-control select2" name="vendedor" >
            <option value="<?= $test->clt_ruta; ?>">Ruta actual</option>
            <?php foreach($vendedores as $datos2) { ?>
                <option value="<?php echo  $datos2->id;?>"><?php echo $datos2->name;?></option>
            <?php  }  ?>
          </select>
        </div>


        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
        </div>
        </div>
        </form>
        <?php  }  ?>

      </div>
      <!-- /.box-body -->



</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
