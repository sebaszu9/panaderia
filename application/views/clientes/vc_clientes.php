<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formulario de Creación de Clientes
      <small>Control panel</small>
    </h1>
  </section>


  <!-- Main content -->
  <section class="content">
    <div class="box-header">

      <a class=" btn  btn-primary" href="<?php echo base_url('Clientes/index');?>">Volver</a>
    </div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Crear nuevo cliente</h3>
          </div>

    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="<?php echo base_url(); ?>Clientes/create" method="POST">
      <?php if(isset($mensaje)):?>
              <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
              </div>
      <?php endif; ?>
      <?php echo validation_errors(); ?>

      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nit/Cédula</label>
          <input type="number" name="nit"  class="form-control" placeholder="Ingrese el nit o cédula del cliente">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" name="name"  class="form-control"  placeholder="Ingrese el nombre del cliente o razón social">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Dirección</label>
          <input type="text" name="dir"  class="form-control" placeholder="Ingrese la Dirección del cliente">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Teléfono</label>
          <input type="number" name="tel"  class="form-control"  placeholder="Ingrese el teléfono del Cliente">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Teléfono 2</label>
          <input type="number" name="tel2"  class="form-control"  placeholder="Ingrese el teléfono alternativo del Cliente">
        </div>

        <div class="form-group col-md-12">
          <label>Ruta</label>
          <select class="form-control select2" name="vendedor" >
            <?php foreach($vendedores as $data) { ?>
                <option value="<?php echo  $data->id;?>"><?php echo $data->name;?></option>
            <?php  }  ?>
          </select>
        </div>

        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Guardar</button>
        </div>
        </div>
      </div>
      <!-- /.box-body -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
