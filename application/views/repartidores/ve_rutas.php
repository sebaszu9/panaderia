<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Editar
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Repartidor</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box-header">

        <a class=" btn btn-primary" href="<?php echo base_url('Rutas/index');?>">Volver</a>
      </div>
      <div class="row">
        <div class="col-md-6">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Editar repartidores</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php foreach($datos as $test) { ?>

    <form role="form" action="<?php echo base_url('Rutas/update/'); ?><?= $test->id; ?>" method="POST" enctype="multipart/form-data">
      <div class="box-body">
        <?php echo validation_errors(); ?>
        <input type="hidden" name="id"  class="form-control" value="<?= $test->id; ?>" placeholder="Ingrese el nit o cédula del cliente">

        <div class="form-group">
          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" name="name"  class="form-control"  value="<?= $test->nombre_repartidor; ?>" placeholder="Ingrese el nombre">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Ruta</label>
          <input type="text" name="ruta"  class="form-control"  value="<?= $test->name; ?>" placeholder="Ingrese ruta">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Celular</label>
          <input type="number" name="cel"  class="form-control" value="<?= $test->celular_repartidor; ?>" placeholder="Ingrese Celular">
        </div>

        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
        </div>
        </div>
        </form>
        <?php  }  ?>

      </div>
      <!-- /.box-body -->



</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
