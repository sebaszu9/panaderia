
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>

        <!-- <li><a href="<?php echo base_url(); ?>/Pos/index"><i class="fa fa-laptop"></i> <span>POS</span></a></li> -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart "></i> <span>Productos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Productos/index"><i class="fa fa-circle-o"></i> Listado de productos</a></li>
            <li><a href="<?php echo base_url(); ?>Categoriasprod/index"><i class="fa fa-circle-o"></i> Departamentos</a></li>
            <li><a href="<?php echo base_url(); ?>Impuestos/index"><i class="fa fa-circle-o"></i> Impuestos de productos</a></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-archive"></i> <span>Materias Primas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Materiasprimas/index"><i class="fa fa-circle-o"></i> Listado de materias</a></li>
            <li><a href="<?php echo base_url(); ?>Categoriasmat/index"><i class="fa fa-circle-o"></i> Departamentos Materias</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="<?php echo base_url(); ?>Clientes/index">
            <i class="fa fa-user"></i> <span>Clientes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <li><a href="<?php echo base_url(); ?>Rutas/index"><i class="fa fa-circle-o"></i> Repartidores</a></li>
          <!-- <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Categoriasmat/index"><i class="fa fa-circle-o"></i> Departamentos Materias</a></li>
          </ul> -->
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-birthday-cake"></i> <span>Recetas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Recetas/index"><i class="fa fa-circle-o"></i>Recetas de Productos</a></li>
            <li><a href="<?php echo base_url(); ?>Concept_ing/index"><i class="fa fa-circle-o"></i>Conceptos de Ingredientes</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-truck "></i> <span>Proveedores</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Proveedores/index"><i class="fa fa-circle-o"></i> Listado de Proveedores</a></li>
            <li><a href="<?php echo base_url(); ?>Proveedores/form"><i class="fa fa-circle-o"></i> Agregar Proveedor</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-truck "></i> <span>Pedidos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Pedidos/hoy"><i class="fa fa-circle-o"></i> Día</a></li>
            <li><a href="<?php echo base_url(); ?>Pedidos/formulario"><i class="fa fa-circle-o"></i> Histórico</a></li>
            <li><a href="<?php echo base_url(); ?>Pedidos/form_item"><i class="fa fa-circle-o"></i> Conteo unidades</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="<?php echo base_url(); ?>Produccion/index_admin">
            <i class="fa fa-industry"></i> <span>Producción</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Produccion/index_admin"><i class="fa fa-circle-o"></i>Producción día</a></li>
            <li><a href="<?php echo base_url(); ?>/Calculos/"><i class="fa fa-circle-o"></i>Registrar Produccion</a></li>
            <li><a href="<?php echo base_url(); ?>/Orden/produccion/index"><i class="fa fa-circle-o"></i> <span>Ordenes Produccion</span></a></li>
            <li><a href="<?php echo base_url(); ?>/Produccion/sede/form"><i class="fa fa-circle-o"></i> <span>Comparativos sedes</span></a></li>
          </ul>
        </li>

        <li class="treeview">
          <a >
            <i class="fa fa-users"></i> <span>Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Usuario/index"><i class="fa fa-circle-o"></i> Listado Usuario</a></li>
            <li><a href="<?php echo base_url(); ?>Usuario/form"><i class="fa fa-circle-o"></i> Agregar Usuario</a></li>
          </ul>

        </li>

        <li class="treeview">
          <a >
            <i class="fa fa-cubes"></i> <span>Inventario</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>Orden/producto/index"><i class="fa fa-circle-o"></i> Ordenes de compra</a></li>
                <li><a href="<?php echo base_url(); ?>Inventario/stock/materia"><i class="fa fa-circle-o"></i> Materia prima</a></li>
                <li><a href="<?php echo base_url(); ?>Inventario/stock/productos"><i class="fa fa-circle-o"></i> Productos</a></li>
                <li><a href="<?php echo base_url(); ?>Inventario/movimiento/index"><i class="fa fa-circle-o"></i> Movimiento</a></li>
              </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>Reportes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Reportes/materias/form"><i class="fa fa-circle-o"></i>Materias</a></li>
            <li><a href="<?php echo base_url(); ?>Reportes/produccion/form"><i class="fa fa-circle-o"></i>Productos</a></li>
            <li><a href="<?php echo base_url(); ?>Ventas/index"><i class="fa fa-circle-o"></i>Reporte de ventas</a></li>
          </ul>
          <ul class="treeview-menu">
          </ul>
        </li>

        <li class="treeview">
          <a href="<?php echo base_url(); ?>Calculos/mensaje">
            <i class="fa fa-bar-chat"></i> <span>Mensaje</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
        </li>
        <!--
        <li class="treeview">
          <a href="#">
            <i class="fa fa-coffee"></i> <span>Mesas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>Mesas/zonas/form"><i class="fa fa-circle-o"></i>Zona</a></li>
            <li><a href="<?php echo base_url(); ?>Mesas/mesas/form"><i class="fa fa-circle-o"></i>Mesas</a></li>
          </ul>
          <ul class="treeview-menu">
          </ul>
        </li>
      -->

      </ul>
    </section>
  </aside>
