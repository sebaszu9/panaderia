<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Inicio
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> SmartBake</a></li>
        <li class="active">Principal</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div class="col-md-12">
          <center>
            <img src="<?php echo base_url(); ?>image/sb.png" style="width:500px">
            <hr>
          </center>
        </div>

        <a href="<?php echo base_url('Calculos/produccion'); ?>" class="col-md-6">
          <div class="box box-default">
            <div class="box-body">
              <img  class=""  src="<?php echo base_url(); ?>image/mobile/menu2.png" style="width:100%">
              <h1 style="text-align:center;">Registro Producción</h1>
            </div>
            <!-- /.box-body -->
          </div>
        </a>

        <a href="<?php echo base_url(); ?>/Calculos" class="col-md-6">
          <div class="box box-default">
            <div class="box-body">
              <img  class=""  src="<?php echo base_url(); ?>image/mobile/menu1.png" style="width:100%">
              <h1 style="text-align:center;">Recetas</h1>
            </div>
            <!-- /.box-body -->
          </div>
        </a>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->
