<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Importación Masiva
    </h1>
  </section>


  <!-- Main content -->
  <section class="content">

    <div class="row">
          <div class="col-md-6">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">Selecciona los datos a importar</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="box-group" id="accordion">
                  <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                  <div class="panel box box-primary">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" style="color:black" data-parent="#accordion" href="#collapseOne">
                          Categorias Materias
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                      <div class="box-body">
                        <form role="form" action="<?= base_url()?>Controller_import/cat_mat" method="POST" enctype="multipart/form-data">
                          <div class="form-group col-md-8">
                            <div class="input-group">
                              <input type="file" class="form-control" name="file" >
                              <div class="input-group-addon">
                                <i class="fa fa-file"></i>
                              </div>
                            </div>
                            <!-- /.input group -->
                          </div>
                          <div class="form-group col-md-8">
                            <label style="color:white;">.</label><br/>
                            <input type="submit" class="btn btn-primary" value="importar">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="panel box box-danger">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" style="color:black" data-parent="#accordion" href="#collapseTwo">
                          Materias Primas
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                      <div class="box-body">
                        <form role="form" action="<?= base_url()?>Import/mat" method="POST" enctype="multipart/form-data">
                          <div class="form-group col-md-8">
                            <div class="input-group">
                              <input type="file" class="form-control" name="file" >
                              <div class="input-group-addon">
                                <i class="fa fa-file"></i>
                              </div>
                            </div>
                            <!-- /.input group -->
                          </div>
                          <div class="form-group col-md-8">
                            <label style="color:white;">.</label><br/>
                            <input type="submit" class="btn btn-primary" value="importar">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="panel box box-success">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" style="color:black" data-parent="#accordion" href="#collapseThree">
                          Categoria Productos
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                      <div class="box-body">
                        <form role="form" action="<?= base_url()?>Import/cat_prod" method="POST" enctype="multipart/form-data">
                          <div class="form-group col-md-8">
                            <div class="input-group">
                              <input type="file" class="form-control" name="file" >
                              <div class="input-group-addon">
                                <i class="fa fa-file"></i>
                              </div>
                            </div>
                            <!-- /.input group -->
                          </div>
                          <div class="form-group col-md-8">
                            <label style="color:white;">.</label><br/>
                            <input type="submit" class="btn btn-primary" value="importar">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="panel box box-success">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" style="color:black" data-parent="#accordion" href="#collapseThree">
                          Productos
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                      <div class="box-body">
                        <form role="form" action="<?= base_url()?>Import/prod" method="POST" enctype="multipart/form-data">
                          <div class="form-group col-md-8">
                            <div class="input-group">
                              <input type="file" class="form-control" name="file" >
                              <div class="input-group-addon">
                                <i class="fa fa-file"></i>
                              </div>
                            </div>
                            <!-- /.input group -->
                          </div>
                          <div class="form-group col-md-8">
                            <label style="color:white;">.</label><br/>
                            <input type="submit" class="btn btn-primary" value="importar">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="panel box box-success">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" style="color:black" style="color:black" data-parent="#accordion" href="#collapseThree">
                          Inventario Productos
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                      <div class="box-body">
                        <form role="form" action="<?= base_url()?>Import/inv_prod" method="POST" enctype="multipart/form-data">
                          <div class="form-group col-md-8">
                            <div class="input-group">
                              <input type="file" class="form-control" name="file" >
                              <div class="input-group-addon">
                                <i class="fa fa-file"></i>
                              </div>
                            </div>
                            <!-- /.input group -->
                          </div>
                          <div class="form-group col-md-8">
                            <label style="color:white;">.</label><br/>
                            <input type="submit" class="btn btn-primary" value="importar">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="panel box box-success">
                    <div class="box-header with-border">
                      <h4 class="box-title">
                        <a data-toggle="collapse" style="color:black" data-parent="#accordion" href="#collapseThree">
                          Inventario Materias
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                      <div class="box-body">
                        <form role="form" action="<?= base_url()?>Import/inv_mat" method="POST" enctype="multipart/form-data">
                          <div class="form-group col-md-8">
                            <div class="input-group">
                              <input type="file" class="form-control" name="file" >
                              <div class="input-group-addon">
                                <i class="fa fa-file"></i>
                              </div>
                            </div>
                            <!-- /.input group -->
                          </div>
                          <div class="form-group col-md-8">
                            <label style="color:white;">.</label><br/>
                            <input type="submit" class="btn btn-primary" value="importar">
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->

  </section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
