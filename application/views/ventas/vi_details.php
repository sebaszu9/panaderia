<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Informacion de la compra </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <?php foreach($datos as $test) { ?>
        <div class="col-md-6">
          <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Reportes de Venta</h3>
          </div>
              <!-- form start -->
                <div class="box-body">
                  <table id="table-alla-order" class="table table-bordered table-striped">
                    <tbody>
                      <tr>
                        <th>ID</th>
                        <td><?php echo $test->pvi_id;?></td>
                      </tr>
                      <tr>
                        <th>Cliente</th>
                        <td><?php echo $test->clt_nombre;?></td>
                      </tr>
                      <tr>
                        <th>Vendedor</th>
                        <td><?php echo $test->usr_nombre;?></td>
                      </tr>

                      <tr>
                        <th>Fecha</th>
                        <td><?php echo $test->pvi_fecha?></td>
                      </tr>
                      <tr>
                        <th>Hora</th>
                        <td><?php echo $test->pvi_hora;?></td>
                      </tr>
                      <tr>
                        <th>Monto</th>
                        <td><?php echo number_format($test->pvi_total)?></td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
      <!-- /.box-body -->
      </div>

      <?php  }  ?>



        <div class="col-md-6">
          <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detalles del la Compra</h3>
          </div>
              <!-- form start -->
                <div class="box-body">
                  <table id="table-alla-order" class="table table-bordered table-striped">
                    <tbody>
                      <?php foreach($details as $testx) { ?>
                        <tr id="tr<?php echo $testx->pvd_id;?>">
                          <td><?php echo $testx->prod_nombre;?></td>
                          <td><?php echo $testx->pvd_cant;?></td>
                          <td>$<?php echo number_format($testx->pvd_cant);?></td>
                          <td>$<?php echo number_format($testx->pvd_venta);?></td>
                          <td><button class="btn btn-defautl delitem" value="<?php echo $testx->pvd_id;?>">Eliminar</button></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
            </div>
        </div>
      <!-- /.box-body -->
      </div>



    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready(function(){
    $(".delitem").click(function() {
      var del = $(this).val();

      var parametros = { "delete"  : true, "posicion" : del};

       $.ajax({ data: parametros, url:' <?= base_url('Ventas/delete/item')?>',type:  'post',
         beforeSend: function () {$('#tr'+del).html("<tr><td colspan='5'>Enviando, espere por favor...</td></tr>");},
         success:  function (response) {

           var response = JSON.parse(response);
           if (response.success==true)
           {
             $('#tr'+del).hide(2000);
           }
           else
           {
             alert(JSON.stringify(response))
           }
         }
       }).fail( function(error) {
          alert(JSON.stringify(error))
       })
    })
  })
</script>
