<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Informe de venta </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Vendedor</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Impuesto</th>
                <th>Descuento</th>
                <th>Monto</th>
                <th colspan="2"></th>
              </tr>
              </thead>
              <tbody>
                <?php foreach($datos as $test) { ?>
                <tr id="tr<?php echo $test->pvi_id;?>">
                  <td><?php echo $test->pvi_id;?></td>
                  <td><?php echo $test->clt_nombre;?></td>
                  <td><?php echo $test->usr_nombre;?></td>
                  <td><?php echo $test->pvi_fecha?></td>
                  <td><?php echo $test->pvi_hora	;?></td>
                  <td><?php echo number_format($test->pvi_descuento)?></td>
                  <td><?php echo number_format($test->pvi_impuestos)?></td>
                  <td><?php echo number_format($test->pvi_total)?></td>
                  <td><button class="btn btn-default show-details" data-toggle="modal" data-target="#myModal" value="<?php echo $test->pvi_id?>">Ver detalles</button></td>
                </tr>
                <?php  }  ?>
              </tbody>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalles</h4>
      </div>
      <div class="modal-body" id="data-detail">

        <table class="table">
          <thead>
            <tr>
              <th>Producto</th>
              <th>Valor</th>
              <th>Cantidad</th>
              <th>Subtotal</th>
            </tr>
          </thead>
          <tbody  id="det-prod">
          </tbody>
        </table>
        <hr>
        <table class="table">
          <thead>
            <tr>
              <th>Subtotal</th>
              <th>Descuento</th>
              <th>Impuesto</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr id="det-fact">
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>
