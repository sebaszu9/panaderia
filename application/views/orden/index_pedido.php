<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Orden de compra </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Orden</a></li>
      <li class="active">Inicio</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">


        <form action="<?= base_url('Orden/producto/buscar') ?>" method="POST" >

        <div class="col-md-4">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Consultar</h3>
              <?php echo validation_errors(); ?>

              <?php if ($this->session->flashdata('mensaje'))  { ?>
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p><i class="icon fa fa-exclamation-circle"></i> <?= $this->session->flashdata('mensaje') ?></p>
                </div>
              <?php } ?>

              <a href="<?php echo base_url('Orden/producto/form') ?>" class="btn btn-info pull-right" >Nuevo</a>

            </div>

            <div class="box-body">

              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Fecha Inicio</label>
                <input type="hidden" name="search" value="true" >
                <input type="text" name="fecha1"  autocomplete="off" id="datepicker" class="form-control">
              </div>

              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Fecha Final</label>
                <input type="text" name="fecha2" autocomplete="off"  id="datepicker2" class="form-control">
              </div>

              <div class="form-group col-md-12">
                <label>Panaderia</label>
                <select class="form-control" name="panaderia">
                  <option value="">Todos</option>
                <?php foreach($panaderias as $datosd) { ?>
                  <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-12">
                <label>Proveedor</label>
                <select class="form-control select2" name="proveedor">
                <option value="">Todos</option>
                <?php foreach($proveedor as $datos) { ?>
                  <option value="<?= $datos->prov_id;?>"><?= $datos->prov_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>
            </div>

            <div class="box-footer with-border">

              <div class="form-group col-md-12">
                <div class="form-group ">
                  <input type="hidden" value="1" name="parte">
                  <button type="submit" class="btn btn-block btn-info" ><i class="fa fa-check-circle"></i> Buscar</button>
                </div>
              </div>

            </div>

          </div>
        </div>
      </form>

        <div class="col-md-8">
          <div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Productos</h3>
            </div>

            <div class="box-footer with-border">

              <div class="col-md-12">

                <table class="table table-bordered order-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Fechas</th>
                      <th>Proveedores</th>
                      <th>Sede</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="loadcarttable">
                    <?php
                          $i = 1;
                        foreach ($orden as $value) {
                          echo "<tr>";
                          echo "<td> ".$i." </td>";
                          echo "<td> ".$value->op_fecha." </td>";
                          echo "<td> ".$value->prov_nombre." </td>";
                          echo "<td> ".$value->bod_nombre." </td>";
                          echo '<td><a class="btn btn-success" href="'.base_url('Orden/producto/detalles/'.$value->op_id).'">Detalles</a></td>';
                          echo "<tr>";
                          $i++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>




          </div>
        </div>



    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->


<script>

$(document).on('click', '.addcart', function(){

  var code  = $("#selecprod").val();
  var cantd = $("#cantidad").val();
  var costo = $("#precio").val();
  var name  = $("#selecprod").find(':selected').data("cat");

  if (costo==='') {   costo = 0;  }

    var carrito = {
      "insert"   : true,
      "pedido"   : <?php echo $this->uri->segment(4); ?>,
      "producto" : code,
      "cantidad" : cantd,
      "costo"    : costo
    };

    $.ajax({ data:  carrito, url:' <?= base_url('Orden/producto/item/add')?>',type:  'post',
       beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
       success:  function (response) {
         var response = JSON.parse(response);
         if (response.success==true)
         { location.reload(); }
         else
         { alert(response.message) }
       }
     })

});

$( "#selecttipo" ).change(function() {
  var select = $(this).find(':selected').data("tipo");
  $("#tipomov").val(select);
});

$(document).on('click', '.deletet', function(){
  var code  = $(this).val();
  var carrito = {
    "delete"   : true,
    "posicion"   : code,
  };

  $.ajax({ data:  carrito, url:' <?= base_url('Orden/producto/item/del')?>',type:  'post',
     beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
     success:  function (response) {
       var response = JSON.parse(response);
       if (response.success==true)
       { location.reload(); }
       else
       { alert(response.message) }
     }
   })

});

</script>
