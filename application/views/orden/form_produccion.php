<script src="<?php echo base_url(); ?>/assets/jquery-1.9.1.js"></script>

<script src="<?php echo base_url(); ?>/assets/jquery-ui.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Ordenes de produccion</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orden</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">


          <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Crear una nueva orden.</h3>
                <a class="btn btn-default pull-right" href="<?php echo base_url('Orden/produccion/index/'); ?>">Volver</a>
              </div>

              <div class="box-body">

                <form method="post" action="<?= base_url('/Orden/produccion/crear')?>" >

                <div class="form-group col-md-12">
                  <label>Sede de Panaderia</label>
                  <select class="form-control" name="sede" readonly="">
                    <?php foreach($sede as $datos) { ?>
                      <option value="<?php echo  $datos->bod_id;?>"><?php echo  $datos->bod_nombre;?></option>
                    <?php  }  ?>
                  </select>
                </div>

                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Fecha de Produccion</label>
                  <input type="text" name="fecha" class="form-control pull-right" id="datepicker1" value=""  autocomplete="off">
                </div>


                <div class="form-group col-md-12">
                  <label style="color:white;">.</label><br/>
                  <input type="submit" class="btn btn-primary" value="Guardar">
                </div>

                <input type="hidden" name="search" value="true">
              </form>

              </div>
            </div>
        </div>



        <!-- /.Left col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
      $(document).ready(function() {
          //Date picker
          $('#datepicker1').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker2').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });
      });
  </script>
