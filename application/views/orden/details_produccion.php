
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Orden de produccion </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url('Orden/produccion/index')?>"><i class="fa fa-dashboard"></i> Orden</a></li>
        <li class="active">Detalles</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">


      <?php foreach ($orden as $datos) { ?>

        <div class="col-md-12">

          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Detalles de la orden.</h3>
                <a class="btn btn-default pull-right" href="<?php echo base_url('Orden/produccion/index/'); ?>">Volver</a><br><br>
              </div>

              <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Orden</th>
                        <th>Sede</th>
                        <th>Fecha</th>
                        <th></th>
                    </thead>
                    <tbody>
                      <tr>
                        <td>#<?php echo  $datos->orp_id;?></td>
                        <td><?php echo  $datos->bod_nombre;?></td>
                        <td><?php echo  $datos->orp_fecha;?></td>
                        <td><button type="submit" class="btn btn-danger delete-orden pull-right" value="<?php echo  $datos->orp_id;?>">Eliminar</button></td>
                      </tr>
                    </tbody>
                </table>

              </div>

        <?php $producto = array(); ?>


            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th>Receta</th>
                  <th>Producto</th>
                  <th>Unidades</th>
                  <th>Estado</th>
                  <th></th>
                </tr>
                <?php foreach($detalles as $detl) { ?>
                  <tr>
                  <td><?php echo $detl->rec_nombre; ?></td>
                  <td><?php echo $detl->prod_nombre; ?></td>
                  <td><?php echo $detl->pp_cant; ?></td>
                  <td>
                    <?php

                    if($detl->pp_estado==0)
                    {
                      echo '<span class="badge bg-green"> Pendiente </span>';
                    }
                    else
                    {
                      echo '<span class="badge bg-orange"> Realizado </span>';
                    }

                    ?>
                  </td>
                  <td><a href="<?php echo base_url('Orden/produccion/imprimir/'.$detl->pp_id)?>">Imprimir receta</a></td>
                  <td><button class="btn btn-danger delete-prods" value="<?php echo $detl->pp_id; ?>">Eliminar</button></td>
                </tr>
                <?php  }  ?>
              </table>
              <hr>
              <table class="table table-bordered">
                <tr>
                  <td> Receta </td>
                  <td>
                    <select class="form-control" name="receta" id="select-receta" >
                      <?php foreach($recetas as $recdata) { ?>
                        <option value="<?php echo  $recdata->rec_id;?>" ><?php echo  $recdata->rec_nombre;?></option>
                      <?php  }  ?>
                    </select>
                  </td>
                </tr>

                <tr>
                  <td> Producto </td>
                  <td>
                    <select class="form-control" name="producto" required="true" id="producselx">
                      <?php foreach($producto as $datosx) { ?>
                        <option value="<?php echo  $datosx->prod_id;?>" datac-cat="<?php echo $datosx->prod_id;?>"><?php echo  $datosx->prod_nombre;?></option>
                      <?php  }  ?>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    Tipo
                  </td>
                  <td colspan="2">
                    <select class="form-control" name="tipo" required="true" id="tipo_unidx">
                        <option value="2" >Ingrediente Base.</option>
                        <option value="1" >Unidades.</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <th>Cantidad</th>
                  <td>
                    <div class="form-group">
                      <input type="text" id="cantd" class="form-control" value="" autocomplete="off">
                    </div>
                  </td>
                </tr>

                <tr>
                  <td colspan="2">
                    <input type="submit" class="btn btn-primary btn-block save-prod" value="Adicionar"></td>
                  </td>
                </tr>

              </table>
            </div>
          </div>
        </div>

      </div>





      <?php } ?>

        <!-- /.Left col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


  <script>
  $( ".save-prod" ).click(function() {

    var select = $( "#producselx" ).val();
    var cantd = $( "#cantd" ).val();
    var orden = $( "#ordens" ).val();
    var parametros = {
      "save" : true,
      "receta" :  $( "#select-receta" ).val(),
      "tipo" : $("#tipo_unidx").val(),
      "producto" : select,
      "cantidad": cantd,
      "orden": <?php echo $this->uri->segment(4);?>
    };

    console.log(parametros)

    $.ajax({ data: parametros, url:' <?= base_url('Orden/produccion/producto/add')?>',type:'post',
       beforeSend: function () {$("#resoptsav").html("Enviando, espere por favor...");},
       success:  function (response) {

         console.log(response);

         var response = JSON.parse(response);
         if (response.success==true)
         {
           //alert(response.message)
           location.reload();
         }
         else
         {
           alert(response.message)
         }
       }
    }).fail( function(error) {
      alert(JSON.stringify(error))
   })

  });

  $( ".delete-prods" ).click(function() {

    var value = $(this).val();
    var parametros = { "delete" : true, "value" : $(this).val()  };

    $.ajax({ data: parametros, url:' <?= base_url('Orden/produccion/producto/del')?>',type:'post',
       beforeSend: function () {$("#resoptsav").html("Enviando, espere por favor...");},
       success:  function (response) {
         var response = JSON.parse(response);
         if (response.success==true)
         { location.reload(); }
         else
         { alert(response.message) }
       }
    }).fail( function(error) {
      alert(JSON.stringify(error))
   })

  });

  $( ".delete-orden" ).click(function() {


    if (confirm("Esta seguro desea eliminar?")) {

      var value = $(this).val();
      var parametros = { "delete" : true, "value" : $(this).val()  };

      $.ajax({ data: parametros, url:' <?= base_url('Orden/produccion/eliminar')?>',type:'post',
         beforeSend: function () {$("#resoptsav").html("Enviando, espere por favor...");},
         success:  function (response) {
           var response = JSON.parse(response);
           if (response.success==true)
           {
             window.location.href ="<?php echo base_url('Orden/produccion/index'); ?>"
           }
           else
           { alert(response.message) }
         }
      }).fail( function(error) { alert(JSON.stringify(error)) })

    }
  });


  $(".selectrseceta").on('change', function() {
      alert( this.value );
  });

    $( "#select-receta" ).change(function() {

      //alert("hola")
      var select = $(this).val();
      var parametros = {  "safety" : true,  "categoria" : select};
      $.ajax({ data:  parametros, url:' <?= base_url('Controller_order_prod/loadrecetaprod/')?>'+select,type:  'post',
          beforeSend: function () {
            $("#producselx").html("<option>Cargando, espere por favor...</option>");
            },
          success:  function (response) {
            //alert(response)
            console.log(response)

            $("#producselx").html(response);

            /*
            response.forEach(function(element,key){

              //$("#producselx").append("<option>"+element.prod_nombre+"</option>");
            })
            */
            //$("#subcategoria").html(response);
          }
        })

    });

  </script>

  <script type="text/javascript">
      $(document).ready(function() {
          //Date picker
          $('#datepicker1').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker2').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });
      });
  </script>
