<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Orden de compra </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Orden</a></li>
      <li class="active">Producto</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">


        <?php foreach ($orden as $key): ?>

        <div class="col-md-4">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informacion</h3>
              <?php echo validation_errors(); ?>

              <?php if ($this->session->flashdata('mensaje'))  { ?>
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p><i class="icon fa fa-exclamation-circle"></i> <?= $this->session->flashdata('mensaje') ?></p>
                </div>
              <?php } ?>

              <button type="submit" class="btn btn-danger pull-right delete-order" ><i class="fa fa-check-circle"></i> Eliminar</button>

            </div>

            <form action="<?= base_url('Orden/producto/update') ?>" method="POST" >

            <div class="box-body">

              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Fecha</label>
                <input type="hidden" name="pedido" value="<?php echo $this->uri->segment(4); ?>" >
                <input type="hidden" name="update" value="true" >
                <input type="text" name="fecha"  id="datepicker" value="<?php echo $key->op_fecha; ?>" class="form-control">
              </div>

              <div class="form-group col-md-12">
                <label>Panaderia</label>
                <select class="form-control" name="panaderia">
                  <option value="<?php echo $key->bod_id; ?>"><?php echo $key->bod_nombre; ?></option>
                <?php foreach($panaderias as $datosd) { ?>
                  <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-12">
                <label>Proveedor</label>
                <select class="form-control select2" name="proveedor">
                    <option value="<?php echo $key->prov_id; ?>"><?php echo $key->prov_nombre; ?></option>
                <?php foreach($proveedor as $datos) { ?>
                  <option value="<?= $datos->prov_id;?>"><?= $datos->prov_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>

            </div>

            <div class="box-footer with-border">

              <div class="form-group col-md-12">
                <div class="form-group ">
                  <input type="hidden" value="1" name="parte">
                  <button type="submit" class="btn btn-block btn-info" ><i class="fa fa-check-circle"></i> Guardar</button>
                  <br>
                  <a href="<?php echo base_url('Orden/producto/index') ?>" class="btn btn-default" >Atrás</a>
                </div>
              </div>

            </div>

          </form>

          </div>
        </div>

      <?php endforeach; ?>

        <div class="col-md-8">
          <div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Productos</h3>
            </div>

            <div class="box-footer with-border">

              <div class="form-group col-md-3">
                <label>Materia</label>
                <select class="form-control select2" name="producto" id="selecprod">
                <?php foreach($materias as $datosq) { ?>
                  <option value="<?= $datosq->mat_id;?>" data-cat="<?= $datosq->mat_nombre;?>"><?= $datosq->mat_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label for="exampleInputEmail1">Cantidad</label>
                <input type="number" name="cantidad" id="cantidad" class="form-control" value="1">
              </div>

              <div class="form-group col-md-3">
                <label for="exampleInputEmail1">Precio</label>
                <input type="number" name="precio" id="precio" class="form-control">
              </div>

              <div class="form-group col-md-3">
                <label style="color:white">.</label>
                <button type="button" class="btn btn-block btn-success addcart"><i class="fa fa-cart-plus"></i></button>
              </div>

              <div class="col-md-12"><hr></div>

              <div class="col-md-12">

                <table class="table table-bordered order-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Producto</th>
                      <th>Cant.</th>
                      <th>Costo</th>
                      <th>Eliminar</th>
                    </tr>
                  </thead>
                  <tbody id="loadcarttable">
                    <?php
                          $i = 1;
                        foreach ($materia as $value) {
                          echo "<tr>";
                          echo "<td> ".$i." </td>";
                          echo "<td> ".$value->mat_nombre." </td>";
                          echo "<td> ".$value->ipo_cantidad." </td>";
                          echo "<td> ".$value->ipo_precio." </td>";
                          echo "<td> <button class='btn btn-default deletet' value='".$value->ipo_id."'><i class='fa fa-trash'></i></button> </td>";
                          echo "<tr>";
                          $i++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>




          </div>
        </div>



    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->


<script>

$(document).on('click', '.addcart', function(){

  var code  = $("#selecprod").val();
  var cantd = $("#cantidad").val();
  var costo = $("#precio").val();
  var name  = $("#selecprod").find(':selected').data("cat");

  if (costo==='') {   costo = 0;  }

    var carrito = {
      "insert"   : true,
      "pedido"   : <?php echo $this->uri->segment(4); ?>,
      "producto" : code,
      "cantidad" : cantd,
      "costo"    : costo
    };

    $.ajax({ data:  carrito, url:' <?= base_url('Orden/producto/item/add')?>',type:  'post',
       beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
       success:  function (response) {
         var response = JSON.parse(response);
         if (response.success==true)
         { location.reload(); }
         else
         { alert(response.message) }
       }
     })

});

$( "#selecttipo" ).change(function() {
  var select = $(this).find(':selected').data("tipo");
  $("#tipomov").val(select);
});

$(document).on('click', '.deletet', function(){
  var code  = $(this).val();
  var carrito = {
    "delete"   : true,
    "posicion"   : code,
  };

  $.ajax({ data:  carrito, url:' <?= base_url('Orden/producto/item/del')?>',type:  'post',
     beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
     success:  function (response) {
       var response = JSON.parse(response);
       if (response.success==true)
       { location.reload(); }
       else
       { alert(response.message) }
     }
   })

});

$(document).on('click', '.delete-order', function(){

  if (confirm("Esta seguro desea eliminar?")) {

    var carrito = { "delete": true, "id": <?php echo $this->uri->segment(4); ?> };

    $.ajax({ data:  carrito, url:' <?= base_url('Orden/producto/delete')?>',type:  'post',
       beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
       success:  function (response) {
         $("#loadcarttable").html(response);
         var response = JSON.parse(response);
         if (response.success==true)
         {
           window.location.href ="<?php echo base_url('Orden/producto/index'); ?>"
         }
         else
         { alert(response.message) }
       }
     })

  }

});

</script>
