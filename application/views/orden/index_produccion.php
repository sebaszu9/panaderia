<script src="<?php echo base_url(); ?>/assets/jquery-1.9.1.js"></script>

<script src="<?php echo base_url(); ?>/assets/jquery-ui.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Ordenes de produccion</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orden</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">




          <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Consultar</h3>

                <?php echo validation_errors(); ?>
              </div>

              <div class="box-body">

                <form method="post" action="<?= base_url('/Orden/produccion/buscar')?>" >

                <div class="form-group col-md-12">
                  <label>Sede de Panaderia</label>
                  <select class="form-control" name="sede" readonly="">
                    <option value="0"></option>
                    <?php foreach($sede as $datos) { ?>
                      <option value="<?php echo  $datos->bod_id;?>"><?php echo  $datos->bod_nombre;?></option>
                    <?php  }  ?>
                  </select>
                </div>

                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Fecha de Produccion</label>
                  <input type="text" name="fecha" class="form-control pull-right" id="datepicker1" value=""  autocomplete="off">
                </div>

                <div class="form-group col-md-12">
                  <label style="color:white;">.</label><br/>
                  <input type="submit" class="btn btn-primary" value="Buscar">
                </div>

                <input type="hidden" name="search" value="true">

              </form>

              </div>
            </div>
        </div>

        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Historial</h3>
              <a class="btn btn-primary pull-right" href="<?php echo base_url('Orden/produccion/nuevo/'); ?>">Nuevo +</a>
            </div>

            <div class="box-body">

              <table class="table table-bordered">
                <tr>
                  <th>Sedes</th>
                  <th>Fechas</th>
                  <th>Recetas</th>
                  <th></th>
                </tr>
                <?php foreach ($orden as $value) {                  
                   $dia =  date('d', strtotime($value->orp_fecha));
                   $mes = $this->model_panaderia->convert_text_date($value->orp_fecha);
                ?>

                  <tr>
                    <td><?php echo  $value->bod_nombre;?></td>
                    <td><?php echo "$dia de $mes";?></td>
                    <td><a class="btn btn-success" href="<?php echo base_url('Orden/produccion/detalles/'.$value->orp_id); ?>">Detalles</a></td>
                  </tr>
                <?php } ?>

              </table>
            </div>
          </div>
        </div>




        <!-- /.Left col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
      $(document).ready(function() {
          //Date picker
          $('#datepicker1').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker2').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });
      });
  </script>
