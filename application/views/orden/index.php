<script src="<?php echo base_url(); ?>/assets/jquery-1.9.1.js"></script>

<script src="<?php echo base_url(); ?>/assets/jquery-ui.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Reporte cosumo Materias prima </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reportes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">

          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Consulta</h3>
                <?php echo validation_errors(); ?>
              </div>

              <div class="box-body">

                <form method="post" action="<?= base_url('/Reportes/materias/search')?>" >

                <div class="form-group col-md-3">
                  <label>Sede de Panaderia</label>
                  <select class="form-control" name="tipo" readonly="">
                    <option value="0">Todos</option>
                    <?php foreach($sede as $datos) { ?>
                      <option value="<?php echo  $datos->bod_id;?>"><?php echo  $datos->bod_nombre;?></option>
                    <?php  }  ?>
                  </select>
                </div>


                <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Seleccionar Fecha Inicial</label>
                  <input type="text" name="fecha1" class="form-control pull-right" id="datepicker1" value=""  autocomplete="off">
                </div>

                <div class="form-group col-md-3">
                  <label for="exampleInputEmail1">Seleccionar Fecha Final</label>
                  <input type="text" name="fecha2" class="form-control pull-right" id="datepicker2" value=""  autocomplete="off">
                </div>

                <div class="form-group col-md-3">
                  <label>Materia Prima</label>
                  <select class="form-control" name="materia" >
                    <option value="">Seleccione</option>
                    <?php foreach($materias as $datosx) { ?>
                      <option value="<?php echo  $datosx->mat_id;?>"><?php echo  $datosx->mat_nombre;?></option>
                    <?php  }  ?>
                  </select>
                </div>

                <div class="form-group col-md-3">
                  <label style="color:white;">.</label><br/>
                  <input type="submit" class="btn btn-primary" value="Consultar">
                </div>

                <input type="hidden" name="search" value="true">

              </form>

              </div>
            </div>
        </div>

        <!-- /.Left col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
      $(document).ready(function() {
          //Date picker
          $('#datepicker1').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker2').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });
      });
  </script>
