<div class="content-wrapper" style="min-height: 1126px;">

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border" style="text-align:center">
          <h3 class="box-title"style="font-size:40px" >Buscar Recetas</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

          <div class="col-lg-6 col-xs-6" style="margin: 0px auto;">
            <input type="text" class="form-control" id="datasearch" placeholder="Buscar recetas ...">
          </div>
          <div class="col-lg-3 col-xs-6" style="margin: 20px auto; ">
            <button type="submit" class="btn btn-info btn-block pull-right">Buscar</button>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer with-border">
          <p id="consultado"></p>
        </div>
      </div>
      <!-- /.box -->

      <div class="row" id="box-reset">

        <a href="http://192.168.1.5/pos-php/pan/Recetas/seleccionar_ing/1" class="col-md-6 col-lg-6 col-xs-6"> <div class="small-box bg-yellow"><div class="inner"><h3>Moje para Pan de Sal</h3><h4>- Este es el mimo moje del pan de 500</h4></div><div class="icon"><i class="ion ion-ios-cloud"></i></div><div href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i> </div></div></a>

      </div>

    </section>
    <!-- /.content -->
  </div>

  <script>
    $(document).ready(function(){
        $("input").keydown(function(){
            $("input").css("background-color", "yellow");
        });
        $("input").keyup(function(){
            $("input").css("background-color", "white");
            var data = $("#datasearch").val();

            var parametros = {
              "safety" : true,
              "data" : data
            };
            $.ajax({ data:  parametros, url:' <?= base_url('Calculos/search')?>',type:  'post',
               beforeSend: function () {$("#box-reset").html("Enviando, espere por favor...");},
               success:  function (response) {$("#box-reset").html(response); }
            }).fail( function(error) {
              alert(JSON.stringify(error))
              //alert(error.status+" Error!! No se pudo eliminar. Causa: Puede ser que esta en uso.");
              //$("#mensaje").html(error.status+" "+error.statusText);
           })
        });
    });
  </script>
