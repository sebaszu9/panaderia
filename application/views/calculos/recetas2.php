<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Cálculo de receta </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Alimenteq</a></li>
        <li class="active">Calculo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-4">
          <div class="box box-default">
            <div class="box-header with-border">

              <h2 class="box-titles"> <i class="fa fa-sticky-note-o"></i>  Productos</h2>
            </div>
            <!-- /.box-header -->
            <div class="box-body">



              <?php $x = 0;
                    foreach($productos as $datos) {
                    $x = $x + ($datos->pc_cant/$datos->pc_cant_rec);
              ?>
                  <h3 id='<?php echo $datos->pc_id;?>' class="card-title cantidad" style="    border-radius: 10px;padding: 10px;background-color: #337ab7; color: white;">  <?php echo $datos->prod_nombre;?> <span class=" badge bg-yellow" style="font-size:25px; " ><?php echo $datos->pc_cant;?></span></h3>
              <?php } ?>

              <?php
                    foreach($materias as $mat) {
                    $x = $x + ($mat->mc_cant/$mat->mc_cant_rec)
              ?>
              <button type="button " id='<?php echo $mat->mc_id;?>' class="btn btn-block btn-default btn-lg cantidad-mat">
                  <h3 class="card-title" style="color:black;"><?php echo $mat->mat_nombre;?> <span class=" badge bg-yellow" style="font-size:25px" ><?php echo $mat->mc_cant/$mat->unidad_escala;;?> <?php echo $mat->unidad_nombre;?>s</span>
              </button>
              <?php
                    }
              ?>
              <a href="#" class="btn btn-block btn-success btn-lg produccion"> <i class="fa fa-edit"></i>  Registrar Producción </a>
              <a href="<?= base_url('Calculos/')?>" class="btn btn-block btn-danger btn-lg"> <i class="fa fa-book"></i>  Nueva Receta </a>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-8">
          <div class="box box-default">
            <div class="box-header with-border">
              <h2 class="box-titles"> <i class="fa fa-navicon"></i>  Ingredientes</h2>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <ul class="list-group list-group-flush">

                <li class="list-group-item pantcolors" style="background-color: #d3eae8;" >
                   <span style="font-size:20px; color:black;"  >Ingrediente Base</span>
                </li>
                <?php foreach($ing as $ingredientes) { ?>
                <li class="list-group-item pantcolors licol<?php echo $ingredientes->mat_id;?>"  data-id="<?php echo $ingredientes->mat_id;?>">
                   <span class=" badge bg-yellow" style="font-size:25px;margin:0px 10px;float: left;" ><?php echo $ingredientes->cr_nombre; ?></span><span style="font-size:20px; color:black;" > <?php echo $ingredientes->mat_nombre;?> </span><span class=" badge bg-blue" style="font-size:25px; float: right;" ><?php if ($ingredientes->mat_un == "1"){echo number_format($x*$ingredientes->ing_cant/$ingredientes->unidad_escala/$ingredientes->mat_ue)."U -";} ?>
                     <?php echo number_format($x*$ingredientes->ing_cant/$ingredientes->unidad_escala); ?> <?php echo $ingredientes->unidad_nombre; ?>s</span>
                </li>
                <?php

                $ingrediente = $this->model_materias->load_ing_comp_per_mat($ingredientes->mat_id);

                foreach ($ingrediente as $ing) {
                  $gramo = $ing->inc_cantidad/$ing->unidad_escala;
                  $gramisimo = (($x*$ingredientes->ing_cant/$ingredientes->unidad_escala)*$gramo)/$ingredientes->mat_ue;

                  echo '<li class="list-group-item pantcolorsub licol'.$ing->mat_id .'sub subing'.$ingredientes->mat_id .'"  data-id="'.$ing->mat_id .'" style="display: none;">
                     <span class=" badge bg-gray" style="font-size:25px;margin:0px 10px;float: left;" > '.$ingredientes->mat_nombre.' </span>
                     <span style="font-size:20px; color:black;" > '.$ing->mat_nombre .'</span>
                     <span class=" badge bg-blue" style="font-size:25px; float: right;" >'.number_format($gramisimo).'  '.$ingredientes->unidad_nombre.'s</span> </li>';



                }

                ?>
                <?php } ?>

                <?php foreach($productos as $data) { ?>

                <?php  foreach($productos_rec as $details) {

                       if ($details->prod_id == $data->prod_id) { ?>

                     <li class="list-group-item pantcolors " style="background-color: #d3eae8;" >
                       <span style="font-size:20px; color:black;"  ><B>Subingrediente</B> <?php echo $data->prod_nombre;  ?></span>
                     </li>
                     <li class="list-group-item pantcolors " style="background-color: #d3eae8;" >
                       <span style="font-size:20px; color:black;"  ><B> Detalles: </B><?php echo $details->pr_detalles;?></span>
                     </li>

                <?php } } ?>

                <?php

                    $x =($data->pc_cant/$data->pc_cant_rec);

                    foreach($ingredientes_prod as $datos7) {

                       if ($datos7->prod_id==$data->prod_id){ ?>
                    <li class="list-group-item pantcolors licol<?php echo $datos7->ing_pr_id;?>" data-id="<?php echo $datos7->ing_pr_id;?>">
                       <span class=" badge bg-yellow" style="font-size:25px;margin:0px 10px;float: left;" >
                         <?php echo $datos7->cr_nombre; ?></span>
                         <span style="font-size:20px; color:black;"  ><?php echo $datos7->mat_nombre;?></span>
                         <span class=" badge bg-blue" style="font-size:25px; float:right;" >
                        <?php if ($datos7->mat_un == "1"){echo $x*$datos7->ing_pr_cant/$datos7->unidad_escala/$datos7->mat_ue."U -";} ?>
                        <?php echo number_format($datos7->ing_pr_cant*$x/$datos7->unidad_escala);?>
                        <?php echo $datos7->unidad_nombre; ?>s
                      </span>
                    </li>
                  <?php
                        }
                      }


                    }// cierre de productos

                    ?>

              </ul>


            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>

        </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->


<!-- Modal -->
<div class="modal fade" id="materia-rec" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
          <h2 class="modal-title">Ingrese cantidad de Materia</h2>
      </div>
      <div class="modal-body">
        <form action="#" id="form-mat" class="form-horizontal">
          <h4 class="modal-title" id="exampleModalLabel">Seleccionar tipo</h4>
          <select name="tipo" class="form-control" style="width: 300px;">
            <option value="2">Ingrediente Base</option>
            <option value="1">Cantidad</option>
          </select>

          <h4 class="modal-title" id="exampleModalLabel">Cantidad</h4>
          <input name="cantidad" placeholder="cantidad" class="form-control" type="number">
          <input name="calculo"  class="form-control" type="hidden" value="<?php echo $calculo;?>">
          <input name="receta"  class="form-control" type="hidden" value="<?php echo $ingredientes->ing_rec;?>">

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnMat"  class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="materiaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
          <h2 class="modal-title">Ingrese cantidad de producto terminado</h2>
      </div>
      <div class="modal-body">
        <form action="#" id="form-materia" class="form-horizontal">
          <h4 class="modal-title" id="exampleModalLabel">Seleccionar tipo</h4>
          <select name="tipo" class="form-control" style="width: 300px;">
            <option value="2">Ingrediente Base</option>
            <option value="1">Unidades</option>
          </select>
          <h4>Cantidad</h4>
          <input name="cantidad" placeholder="cantidad" class="form-control" type="number">
          <input name="calculo"  class="form-control" type="hidden" value="<?php echo $calculo;?>">
          <input name="receta"  class="form-control" type="hidden" value="<?php echo $ingredientes->ing_rec;?>">

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnSave"  class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

<!-- Modal PRODUCCION-->
<div class="modal fade" id="produccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h2 class="modal-title">Registro de Producción</h2>
      </div>
      <div class="modal-body">
        <form action="#" id="form-produccion" class="form-horizontal">
          <h4>Panadero</h4>
          <div class="form-group">
              <div class="col-md-9">
                <select name="panadero" class="form-control" style="width: 300px;">
                  <?php foreach($panadero as $panaderos) { ?>
                    <option value="<?=$panaderos->usr_id; ?>"><?=$panaderos->usr_nombre; ?></option>
                     <?php } ?>
                </select>
              </div>
          </div>
            <h4>Observaciones</h4>
            <input name="observaciones" placeholder="observaciones" class="form-control" type="text">
            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Fecha</label>
              <input type="text" name="fecha"  id="datepicker" class="form-control"  readonly>
            </div>
            <input name="calculo" class="form-control" type="hidden" value="<?php echo $calculo;?>">
            <input name="receta"  class="form-control"  type="hidden" value="<?php echo $ingredientes->ing_rec;?>">
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnproduccion"  class="btn btn-primary">Guardar</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->


<script type="text/javascript">
// Ajax post
$(document).ready(function()
{
  var save_method; //for save method string
  var id;

///MOSTRAR form producto produccion
$(".cantidad").click(function(){
  id = $(this).attr("id");
  $('#form-materia')[0].reset();
  $('#materiaModal').modal('show');
});
///MOSTRAR form producto produccion
$(".cantidad-mat").click(function(){
  id = $(this).attr("id");
  $('#form-mat')[0].reset();
  $('#materia-rec').modal('show');
});
////MOSTRAR MODAL PRODUCCION

$(".produccion").click(function(){
  $('#produccion').modal('show');
});


 // //AJAX INSERTAR OPCION
  $("#btnSave").click(function(){
    $.ajax({
        url : "<?php echo site_url('Recetas/prod_term/')?>"+id,
        type: "POST",
        data: $('#form-materia').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          $('#materiaModal').modal('hide');
         location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown)
         {
           alert('error');
        }
    });
  });

  // //AJAX INSERTAR OPCION
   $("#btnMat").click(function(){
     $.ajax({
         url : "<?php echo site_url('Recetas/mat_term/')?>"+id,
         type: "POST",
         data: $('#form-mat').serialize(),
         dataType: "JSON",
         success: function(data)
         {
           $('#materia-rec').modal('hide');
          location.reload();
         },
         error: function (jqXHR, textStatus, errorThrown)
          {
            alert('error');
         }
     });
   });

  // //AJAX INSERTAR OPCION
   $("#btnproduccion").click(function(){
     $.ajax({
         url : "<?php echo site_url('Produccion/create/')?>",
         type: "POST",
         data: $('#form-produccion').serialize(),
         dataType: "JSON",
         success: function(data)
         {
           $('#materiaModal').modal('hide');
           if (data.status==true) {
             //alert(data.idmp)
             window.location.href = '<?php echo base_url()?>Produccion/details/'+data.idmp+'/'+data.idcm;
           }
           //alert(JSON.stringify(data));
           //ACA
         },
         error: function (jqXHR, textStatus, errorThrown)
          {
            alert('error');
         }
     });
   });

   $(".pantcolors").click(function(){
     var id = $(this).data("id")
     //alert('pitnando '+id)
     //$(".licol"+id).toggleClass("green");
     //$(".licol"+id).css("background-color","#28a745");

     $(".subing"+id).toggle();

     if($(".licol"+id).css('background-color')=='rgb(0, 204, 0)')
     {
        $(".licol"+id).css('background-color', '#fff');
     }
     else {
      $(".licol"+id).css('background-color', '#00cc00');
    }

   });

   $(".pantcolorsub").click(function(){
     var id = $(this).data("id")
     //alert('pitnando '+id)
     //$(".licol"+id).toggleClass("green");
     //$(".licol"+id).css("background-color","#28a745");

     if($(".licol"+id+'sub').css('background-color')=='rgb(0, 204, 0)')
     {
        $(".licol"+id+'sub').css('background-color', '#fff');
     }
     else {
      $(".licol"+id+'sub').css('background-color', '#00cc00');
    }

   });


});
</script>
