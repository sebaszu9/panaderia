<?php $k = 0; foreach($productos as $datos) {   $k =$k+1;   } ?>
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Producción del Día </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> SmartBake</a></li>
        <li class="active">Producción</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <i style="font-size:28px" class="fa fa-lemon-o" ></i>
              <h1 style="font-size:28px" class="box-title" >Productos</h1>
            </div>
            <div class="box-body">
              <div class="card">
                <ul class="list-group list-group-flush">
                  <?php  if ($k > 0) {
                     foreach($productos as $datos) {
                    ?>
                  <li class="list-group-item">
                    <a href="<?php echo base_url('Produccion/details')."/".intval($datos->cm_id)."/".intval($datos->cm_id-1); ?>">
                    <h1 style="color:black;"><?php echo $datos->prod_nombre;?><span class=" badge bg-blue" style="font-size:25px;    float: right;" ><?php echo $datos->mp_cant;?> Unidades</span></h1>
                    </a>
                  </li>
                  <?php }}  else { ?>
                    <li class="list-group-item">
                      <h4 style="color:black;">No hay Produccion registrada </h4>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <i style="font-size:28px" class="fa fa-cloud" ></i>
              <h1 style="font-size:28px" class="box-title" >Materias</h1>
            </div>
            <div class="box-body">
              <div class="card">
                <ul class="list-group list-group-flush">
                  <?php  
                  
                  if ($k > 0) {
                     foreach($materias as $datos1) {  ?>
                  <li class="list-group-item">
                    <a href="<?php echo base_url('Produccion/details')."/".intval($datos->cm_id)."/".intval($datos->cm_id-1); ?>">
                    <h1 style="color:black;"><?php echo $datos1->mat_nombre;?><span class=" badge bg-blue" style="font-size:25px;    float: right;" ><?php echo number_format($datos1->mm_cant/$datos1->unidad_escala);?> <?php echo $datos1->unidad_nombre; ?>s</span></h1>
                    </a>
                  </li>
                  <?php }}  else { ?>
                    <li class="list-group-item">
                      <h4 style="color:black;">No hay Produccion registrada </h4>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->
