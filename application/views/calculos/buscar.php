<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Recetas
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> SmartBake</a></li>
        <li class="active">Buscar Recetas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-body">
              <center>
              <i class="fa fa-search" style="font-size:40px;"></i>
              <h1 style="text-align:center;">Buscar Recetas</h1>
              </center>
              <div style="margin: 20px 20%; ">
                <input type="text" class="form-control" id="datasearch" placeholder="Digite ...">
              </div>

              <div style="margin: 20px 20%; ">
                <button type="submit" class="btn btn-block btn-info btn-lg ">Buscar</button>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="row" id="box-reset">

        </div>



    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->


<script>
$(document).ready(function(){
    $("input").keydown(function(){
        $("input").css("background-color", "yellow");
    });
    $("input").keyup(function(){
        $("input").css("background-color", "white");
        var data = $("#datasearch").val();

        var parametros = {
          "safety" : true,
          "data" : data
        };
        $.ajax({ data:  parametros, url:' <?= base_url('Calculos/search')?>',type:  'post',
           beforeSend: function () {$("#box-reset").html("Enviando, espere por favor...");},
           success:  function (response) {$("#box-reset").html(response); }
        }).fail( function(error) {
          alert(JSON.stringify(error))
          //alert(error.status+" Error!! No se pudo eliminar. Causa: Puede ser que esta en uso.");
          //$("#mensaje").html(error.status+" "+error.statusText);
       })
    });
});
</script>
