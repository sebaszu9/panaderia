<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        

</head>
<body>
    <h1>hola esto es material design </h1>

     <div class="collection">
        <a href="#!" class="collection-item"><span class="badge">1</span>Alan</a>
        <a href="#!" class="collection-item"><span class="new badge">4</span>Alan</a>
        <a href="#!" class="collection-item">Alan</a>
        <a href="#!" class="collection-item"><span class="badge">14</span>Alan</a>
    </div>


    <a class="waves-effect waves-light btn">button</a>
<a class="waves-effect waves-light btn"><i class="material-icons left">cloud</i>button</a>
<a class="waves-effect waves-light btn"><i class="material-icons right">cloud</i>button</a>
    
    
</body>
</html>