<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="row">
          <!-- Main Row -->
        <div class="col-md-12">
        <div class="box-header with-border">
          <h3 class="box-title">Detalles del la Orden del Producción</h3>
        </div>
        <div class="box-body">
        <table class="table table-bordered table-striped">
            <tbody>
              <?php foreach($produccion as $prod) {  ?>
              <tr>
                <td>Ubicación</td>
                <td><?php echo $prod->bod_nombre;?></td>
              </tr>
              <tr>
                <td>Fecha</td>
                <td><?php echo $prod->cm_fecha;?></td>
              </tr>
              <tr>
                <td>Panadero</td>
                <td><?php echo $prod->usr_nombre;?></td>
              </tr>
              <tr>
                <td>Observaciones</td>
                <td><?php echo $prod->cm_obs;?></td>
              </tr>

            <?php } ?>
            </tbody>
        </table>
        <div class="box-header with-border">
          <h3 class="box-title">Productos de la Producción</h3>
        </div>
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>ID Nombre</th>
              <th>Cantidad</th>
              <th>Costo</th>
              <th>Precio</th>
              <th>Utilidad</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($producto as $datos) {  ?>
              <tr>
                <td><?php echo $datos->prod_id;?> <?php echo $datos->prod_nombre;?></td>
                <td><?= $datos->mp_cant;?></td>
                <td>$<?php echo $datos->mp_costo;?></td>
                <td><?= $datos->prod_precio;?></td>
                <td><?php echo number_format(($datos->prod_precio-$datos->mp_costo)/$datos->prod_precio*100)."%";?> </td>
              </tr>
              <?php } ?>
              <?php foreach($materias as $datos2) {  ?>
                <tr>
                  <td><?php echo $datos2->mat_id;?></td>
                  <td>Materia prima</td>
                  <td><?= number_format($datos2->mm_cant/$datos2->unidad_escala); ?> <?= $datos2->unidad_nombre; ?>s</td>
                  <td><?php echo $datos2->mat_nombre;?></td>
                  <td>$<?php echo $datos2->mm_costo;?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <br>
        <a href="<?php echo base_url('Produccion/index_admin')?>" class="btn btn-success" >Volver</a>
        </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-8">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Materias usadas en la producción</h3>
      </div>
      <div class="box-body">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Costo</th>
          <!--  <th>Costo</th> -->
          </tr>
          </thead>
          <tbody>
          </tr>
          <?php $x=0;
          foreach($ing as $ingredientes) {  ?>
            <tr>
              <td><?php echo $ingredientes->mat_id;?></td>
              <td><?php echo $ingredientes->mat_nombre;?></td>
              <td><?= number_format($ingredientes->mm_cant/$ingredientes->unidad_escala); ?> <?= $ingredientes->unidad_nombre; ?>s</td>
             <td><?php  echo "$".number_format($ingredientes->mm_costo);?></td>
            </tr>
            <?php $x = $ingredientes->mm_costo + $x;} ?>
          </tbody>
      </table>
      </div>
      <div class="box-footer with-border">
        <h3 class="box-title">Costo del Moje:<?php  echo "$".number_format($x);?> </h3>
      </div>
  </div>
</div>
  </div>
</section>
</div>
