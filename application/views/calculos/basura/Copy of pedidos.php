
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 id="resoptsav"> Pedidos</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> SmartBake</a></li>
        <li class="active">Pedidos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <i style="font-size:28px" class="fa fa-lemon-o" ></i>
              <h1 style="font-size:28px" class="box-title" >Ordenes de produccion</h1>
            </div>
            <div class="box-body">

                <?php foreach ($orden as $value) { ?>
                  <table class="table table-bordered">
                  <tr>
                    <td>Recetas:<h1><?php echo  $value->rec_nombre;?></h1></td>
                    <td>Fecha:<h2><?php echo $value->orp_fecha;?></h2></td>

                  </tr>
                  <?php $detalles = $this->Model_orden_prod->read_prod_producto($value->orp_id); ?>
                  <?php foreach($detalles as $det) { ?>
                    <tr>
                      <th>Producto:<h3><?php echo $det->prod_nombre; ?></h3></th>
                      <th>Cantidad:<h3><span class=" badge bg-blue" style="font-size:25px;"><?php echo $det->pp_cant; ?> Unidades.</span></h3></th>
                      <th>

                        <?php
                          $buttonstate = "btn-default";
                          $status = 1;
                          if ($det->pp_estado==1) {
                            $buttonstate = "btn-success";
                            $status = 0;
                          }
                        ?>

                        <button type="button" class="btn btn-block <?php echo $buttonstate; ?> btn-lg update-val" value="<?php echo $det->pp_id; ?>" data-state="<?php echo $status; ?>">Hecho</button>
                    </tr>
                  <?php } ?>
                </table>
                <hr>
                <?php } ?>



            </div>
            <!-- /.box-body -->
          </div>
        </div>



    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<script>
$( ".update-val" ).click(function() {

  var value = $(this).val();
  var state = $(this).data("state");

  var parametros = { "update" : true, "value" : value, "state" : state};

  $.ajax({ data: parametros, url:' <?= base_url('Orden/produccion/producto/update-state')?>',type:'post',
     beforeSend: function () {$("#resoptsav").html("Enviando, espere por favor...");},
     success:  function (response) {
       var response = JSON.parse(response);
       if (response.success==true)
       { location.reload(); }
       else
       { alert(response.message) }
     }
  }).fail( function(error) {
    alert(JSON.stringify(error))
 })


});
</script>
