@if(Auth::user())

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script >
      $(function () {
        $('#table').DataTable( {
          "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
          },
          "iDisplayLength": 100,
          dom: 'Bfrtip',
          buttons: [
            'excel', 'pdf'
          ]
        } );
      });
    </script>
  </head>
  <body>
    <div id="chart-div"></div>
    <div class=" bg-white" style="border: 1px solid #ced4da; width:100%; height:70px; margin: 0px; position:solid; text-align:right;" >
      <a class="boton btn btn-info" style="width:70px; float:left; margin:10px;" href="javascript:history.go(-1)">Átras</a>
      <a class="boton btn btn-info" style="width:70px; float:left; margin:10px;" href="{{action('ControllerInicio@listado_registrados')}}">Inicio</a>
      <img style="height:65px; width:175px; margin: 0px 25%;" src="{{asset('/logo camara/camara de comercio.jpeg')}}">
      <div class="btn-group" role="group">
        <button style="margin:10px;" id="btnGroupDrop1" type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Menú
        </button>
        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="background-color:#17a2b8; padding:0px;">
          <a class="boton btn btn-info"  href="{{action('ControllerInicio@consult_QR')}}">CONSULTAR QR</a>
          <a class="boton btn btn-info"  href="{{action('ControllerInicio@inicio')}}">REGISTRAR</a>
          <a class="boton btn btn-info"  href="{{action('ControllerInicio@enviar_qr')}}">RECUPERAR QR</a>
          <a class="boton btn btn-info"  href="{{action('ControllerInicio@form_evento')}}">CREAR EVENTO</a>
          <a class="boton btn btn-info"  href="{{action('ControllerInicio@listado_eventos')}}">LISTADO DE EVENTOS</a>
          <a class="boton btn btn-info"  href="{{action('ControllerInicio@listado_registrados')}}">LISTADO DE USUARIOS</a>
        </div>
      </div>
      <a style="margin:10px;" class="btn btn-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        CERRAR SESIÓN
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST">
        @csrf
      </form>
    </div>

    <div class="shadow bg-white" style="width:94%; margin:30px 3%; padding: 10px; border-radius: 6px;">
      <div class="col-md-12" style="text-align:center;">
        <h2>Listado de usuarios</h2>
      </div>
      <div class="row " style=" overflow-x: scroll !important; margin:60px; padding:0px;">
        <table style="width:112%; border: 1px solid #ced4da; border-radius:5px;" id="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>Documento</th>
              <th>Celular</th>
              <th>Email</th>

              <th></th>
            </tr>
          </thead>
          <tbody style="border-left: 1px solid #ced4da;">
            @foreach ($Usuarios as $key)
              <tr>

                <td>{{str_pad(($key->id),5,"0",STR_PAD_LEFT)}}</td>

                <td>{{$key->nombres}}</td>
                <td>{{$key->apellido1." ".$key->apellido2}}</td>
                <td>{{$key->documento}}</td>
                <td>{{$key->celular}}</td>
                <td>{{$key->email}}</td>

                <td><a class="btn btn-info" href="{{action('ControllerInicio@datos_user',$key->id)}}">Ver..</a> </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/af-2.3.2/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/b-1.5.4/b-html5-1.5.4/datatables.min.js"></script>

  </body>
</html>
<style media="screen">
  body{
    padding: 0px;
    background-color: #eef;
  }
  .boton{
    text-align: left;
    width:100%;
  }

</style>
@else
  <script type='text/javascript'>window.location.href = '/'; </script>
@endif
