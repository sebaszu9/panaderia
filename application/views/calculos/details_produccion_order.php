<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Registro del orden del producción
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> SmartBake</a></li>
        <li><a href="#"><i class="fa fa-dashboard"></i> Producción del Día</a></li>
        <li class="active">Registro de producción</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h1 style="font-size:28px" class="box-title" >Productos</h1>
              <a href="<?php echo base_url('Calculos/pedidos');?>" class="btn btn-default pull-right">Volver</a>
            </div>

            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <?php

                $x = 0;

                foreach($producto as $datos) {

                  $cantd_rect = 0;
                  $prodrec = $this->model_recetas->load_prod_per_prod($datos->prod_id);

                  foreach ($prodrec as  $prodrecs)
                  {
                    $cantd_rect = $prodrecs->pr_cant;
                  }

                  $x = $x + ($datos->pp_cant/$cantd_rect);

                ?>

                <div class="col-md-6">
                  <h4><?php echo $datos->prod_nombre;?></h4>
                </div>

                <div class="col-md-3">
                  <?php

                    if($datos->pp_estado==0)
                    {
                      echo '<span class="badge bg-green update-status" style="font-size:22px;margin:10px;"> Pendiente</span>';
                    }
                    else
                    {
                      echo '<span class="badge bg-orange" style="font-size:22px;">Realizado</span>';
                    }

                  ?>
                  <input type="hidden" value="<?php echo $datos->pp_id; ?>" id="valprod">
                </div>

                <div class="col-md-3">
                  <span class="pull-right badge bg-yellow " style="font-size:22px;"><?php echo $datos->pp_cant;?></span>
                </div>
                <?php } ?>
              </ul>

            </div>

          </div>
        </div>

        <?php foreach ($producto as $datos) { ?>

        <?php $ing = $this->model_recetas->display_ing($datos->rec_id); ?>

        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h1 style="font-size:28px" class="box-title" >Ingredientes</h1>
            </div>
            <div class="box-body">
              <ul class="list-group list-group-flush">
                <?php foreach($ing as $ingredientes) { ?>
                <li class="list-group-item">
                   <span style="font-size:20px; color:black;" >
                     <?php echo $ingredientes->mat_nombre; ?>
                   </span>

                   <span class=" badge bg-blue" style="font-size:25px; float: right;" >
                     <?php if ($ingredientes->mat_un == "1"){echo number_format($x*$ingredientes->ing_cant/$ingredientes->unidad_escala/$ingredientes->mat_ue)."U -";} ?>
                     <?php echo number_format($x*$ingredientes->ing_cant/$ingredientes->unidad_escala); ?> <?php echo $ingredientes->unidad_nombre; ?>s
                   </span>


                </li>
                <?php } ?>
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <?php } ?>



      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

  <script>
  $( ".update-status" ).click(function() {


    var r = confirm("Confirmar?");
    if (r == true)
    {

      var id = $("#valprod").val();
      var parametros = { "update" : true, "value" : id, "status": 1 };

       $.ajax({
         url : "<?php echo site_url('Orden/produccion/update/status')?>",
         type: "POST",
         data: parametros,
         dataType: "JSON",
         success: function(response)
         {
            location.reload();
         }
     }).fail( function(error) {
        alert(JSON.stringify(error))
    });


    }



  });
</script>
