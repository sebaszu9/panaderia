<?php $k = 0; foreach($productos as $datos) {   $k =$k+1;   } ?>
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Empaque del Día </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Alimenteq</a></li>
        <li class="active">Producción</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <i style="font-size:28px" class="fa fa-lemon-o" ></i>
              <h1 style="font-size:28px" class="box-title" >Productos</h1>
            </div>
            <div class="box-body">
              <div class="card">
                <ul class="list-group list-group-flush">
                  <?php  if ($k > 0) {
                     foreach($productos as $datos) {
                    ?>
                  <li class="list-group-item empaque" id="<?php echo $datos->emp_id;?>">
                    <h1 style="color:black;"><?php echo $datos->prod_nombre;?><span class=" badge bg-blue" style="font-size:25px; float: right;" ><?php echo $datos->mp_cant;?> Produccion</span>
                    <span class=" badge bg-red" style="font-size:25px;    float: right;" ><?php echo $datos->emp_cant;?> Empaque</span></h1>
                  </li>
                  <?php }}  else { ?>
                    <li class="list-group-item">
                      <h4 style="color:black;">No hay Produccion registrada </h4>
                    </li>
                  <?php } ?>
                </ul>
                <a class="btn-lg btn-danger" href="<?php echo site_url('Calculos/empaque2/')?>/">ANTEAYER</a>
                <a class="btn-lg btn-primary" href="<?php echo site_url('Calculos/empaque1/')?>/">AYER</a>
                <a class="btn-lg btn-success" href="<?php echo site_url('Calculos/empaque/')?>/">HOY</a>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="modal fade" id="modal_empaque" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h3 class="modal-title">MODAL</h3>
                  </div>
                  <div class="modal-body form">
                   <form action="#" id="form_empaque" class="form-horizontal">
                      <div class="form-body">
                        <div class="form-group">
                           <label class="control-label col-md-3">Cantidad</label>
                           <div class="col-md-9">
                               <input name="cantidad" class="form-control" type="number">
                               <span class="help-block"></span>
                           </div>
                        </div>
                    </div>
                   </form>
                 </div>
                  <div class="modal-footer">
                      <button type="button" id="guardar-empaque"  class="btn btn-primary">Guardar</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                  </div>
                </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
// Ajax post
$(document).ready(function()
{
var id;

 $(".empaque").click(function(){
   $('#modal_empaque').modal('show');
   id = $(this).attr("id");
   $('#form_empaque')[0].reset();
  });

 $("#guardar-empaque").click(function(){
   //Ajax Load data from ajax
   $.ajax({
       url : "<?php echo site_url('Calculos/empaque_update/')?>/" + id,
       type: "POST",
       data: $('#form_empaque').serialize(),
       dataType: "JSON",
       success: function(data)
       {
         $('#modal_empaque').modal('hide');
         location.reload();
       },
       error: function (jqXHR, textStatus, errorThrown){}
  });
  });


});
</script>
