<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Listado de pedidos hoy </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped" style="font-size: 26px;">
              <thead>
              <tr>
                <th>No.</th>
                <th>Cliente</th>
                <!-- <th>Vendedor</th> -->
                <th>Fecha - Hora</th>
                <!-- <th>Subtotal</th>
                <th>Impuesto</th>
                <th>Descuento</th> -->
                <!-- <th>Total</th> -->
                <th>Estado</th>
                <th colspan="2">Detalles</th>
              </tr>
              </thead>
              <tbody>
                <?php foreach($pedidos as $test) { ?>
                <tr id="tr<?php echo $test->pp_id;?>">
                  <td><?php echo $test->pp_id;?></td>
                  <td><?php echo $test->clt_nombre;?></td>
                  <!-- <td><?php echo $test->usr_nombre;?></td> -->
                  <td><?php echo $test->pp_fecha?></td>
                  <?php
                  if ($test->pp_estado==0) {
                    ?><td><button class="btn btn-danger click-id" data-toggle="modal" data-target="#myModal-state" id="<?php echo $test->pp_id?>">Sin confirmar</button></td>
                    <?php
                  }
                  elseif ($test->pp_estado==1) {
                    ?><td><button class="btn btn-warning click-id" data-toggle="modal" data-target="#myModal-state" id="<?php echo $test->pp_id?>">Empaque</button></td>
                    <?php
                  }
                  elseif ($test->pp_estado==2) {
                    ?><td><button class="btn btn-primary click-id" data-toggle="modal" data-target="#myModal-state" id="<?php echo $test->pp_id?>">Reparto</button></td>
                    <?php
                  }
                  elseif ($test->pp_estado==4) {
                    ?><td><button class="btn btn-danger click-id" data-toggle="modal" data-target="#state" id="<?php echo $test->pp_id?>">Eliminado</button></td>
                    <?php
                  }
                  elseif ($test->pp_estado==3) {
                    ?><td><button class="btn btn-success click-id" data-toggle="modal" data-target="#" id="<?php echo $test->pp_id?>">Entregado</button></td>
                    <?php
                  }
                  ?>
                  <!-- <td><button class="btn btn-danger show-details" data-toggle="modal" data-target="#myModal" value="<?php echo $test->pp_id?>">Pendiente</button></td> -->
                  <td><button class="btn btn-default show-details" data-toggle="modal" data-target="#myModal" value="<?php echo $test->pp_id?>">Ver detalles</button>
                  <a class="btn btn-default" href="http://pidomi.com/Alexpecial/admin/Controller_pedidos/print_pedido/<?php echo $test->pp_id?>" >Imprimir</a></td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
              <tr>
                <!-- <th>No.</th>
                <th>Cliente</th>
                <th>Vendedor</th>
                <th>Fecha - Hora</th> -->
                <!-- <th>Subtotal</th>
                <th>Impuesto</th>
                <th>Descuento</th> -->
                <!-- <th>Total</th>
                <th>Estado</th>
                <th colspan="2">Detalles</th> -->
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalles</h4>
      </div>
      <div class="modal-body" id="data-header">

        <table class="table" style="    font-size: 36px;">
          <thead>
            <tr>
              <th>Producto</th>
              <!-- <th>Precio</th> -->
              <th>Cantidad</th>
              <th>Concepto</th>
              <!-- <th>Subtotal</th> -->
              <!-- <th>Total</th> -->
            </tr>
          </thead>
          <tbody  id="det-prod">
          </tbody>
        </table>
        <table class="table">
          <thead>
            <tr>
              <!-- <th>Id</th>
              <th>Cliente</th> -->
              <!-- <th>Vendedor</th> -->
              <!-- <th>Telefono</th> -->
              <!-- <th>Subtotal</th> -->
              <!-- <th>Descuento</th> -->
              <!-- <th>Total</th> -->
            </tr>
          </thead>
          <tbody  id="det-head" style="font-size: 26px;">
          </tbody>
        </table>
        <hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<div id="myModal-state" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar de Estado el Pedido</h4>
      </div>
      <div class="modal-body" >
          <!-- <td><button class="btn btn-danger show-details" >Eliminar</button></td><br>
          <hr>
        <td><button class="btn btn-secondary show-details" id="sin-confirmar">Sin confirmar</button></td><br>
        <hr> -->
        <!-- <td><button class="btn btn-warning show-details" id="empaque" >Empaque</button></td><br>
        <hr> -->
        <td><button class="btn btn-info show-details" id="reparto">LISTO</button></td><br>
        <hr>
        <!-- <td><button class="btn btn-success show-details" id="entregado" >Entregado</button></td><br>
        <hr> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script>

  $(document).ready(function(){
    var id;
    $(".show-details").click(function() {

      var del = $(this).val();
      var parametros = { "call"  : true, "posicion" : del};

       $.ajax({ data: parametros, url:' <?= base_url('Controller_pedidos/details_pedido_json/')?>'+del,type:  'post',
         beforeSend: function () {

         },
         success:  function (response) {
           $('#det-prod').empty();
           $('#det-total').empty();

           var response = JSON.parse(response);
           if (response.success==true)
           {
             $('#det-head').html("");
             // $('#det-fact').html("");
             const info = response.datos[0];
             $('#det-head').append( "<td>PEDIDO #"+info.pp_id+"</td>");
             $('#det-head').append("<td>CLIENTE:"+info.clt_nombre+"</td>");
             // $('#det-head').append("<td>"+info.clt_direccion+"</td>");
             // $('#det-head').append("<td>"+info.clt_telefono+"</td>");
             // $('#det-head').append("<td>"+info.pp_subtotal+"</td>");
             // $('#det-head').append("<td>"+info.pp_descuento+"</td>");
             // $('#det-head').append("<td>"+info.pp_total+"</td>");
             // $('#det-fact').append("<td>id</td><td>"+info.pp_id+"</td>");
             // $('#det-fact').append("<td>$"+info.ppi_subtotal+"</td>");
             // $('#det-fact').append("<td>$"+info.ppi_descuento+"</td>");
             // $('#det-fact').append("<td>$"+info.ppi_impuestos+"</td>");
             // $('#det-fact').append("<td>$"+info.ppi_total+"</td>");

             const itemsArray = response.details

             itemsArray.forEach(function(element,key){

               var div = "<tr>";
               div = div + "<td>"+element.prod_nombre+"</td>";
               // div = div + "<td>$"+element.ppi_precio_unitario+"</td>";
               div = div + "<td>"+element.ppi_cantidad+"</td>";
               // div = div + "<td>$"+element.ppi_desc+" ";
               div = div + "<td>"+element.dc_nombre+"</td>";
               // div = div + "<td>$"+element.ppi_subt+"</td>";
               // div = div + "<td>$"+element.ppi_total+"</td>";
               div = div + "</tr>";
               $('#det-prod').append(div);
             })


             //alert(JSON.stringify(response.datos))
             //$('#tr'+del).hide(2000);
           }
           else
           {
             alert(JSON.stringify(response))
           }
         }
       }).fail( function(error) {
          alert(JSON.stringify(error))
       })
    })
    $(".click-id").click(function(){
      id = $(this).attr("id");
      console.log(id);
    });

    $("#sin-confirmar").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/0')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#empaque").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/1')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#reparto").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/2')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#entregado").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/3')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

  })

  // $(document).ready(function(){
  //   $(".calldel").click(function() {
  //     var del = $(this).val();
  //
  //     var parametros = { "delete"  : true, "posicion" : del};
  //
  //      $.ajax({ data: parametros, url:' <?= base_url('Ventas/delete')?>',type:  'post',
  //        beforeSend: function () {$('#tr'+del).html("<tr><td colspan='5'>Enviando, espere por favor...</td></tr>");},
  //        success:  function (response) {
  //
  //          var response = JSON.parse(response);
  //          if (response.success==true)
  //          {
  //            $('#tr'+del).hide(2000);
  //          }
  //          else
  //          {
  //            alert(JSON.stringify(response))
  //          }
  //        }
  //      }).fail( function(error) {
  //         alert(JSON.stringify(error))
  //      })
  //   })
  // })
</script>
