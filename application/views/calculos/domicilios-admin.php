<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Listado de pedidos hoy </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Vendedor</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Impuesto</th>
                <th>Descuento</th>
                <th>Monto</th>
                <th>Estado</th>
                <th colspan="2"></th>
              </tr>
              </thead>
              <tbody>
                <?php foreach($orden as $test) { ?>
                <tr id="tr<?php echo $test->pvi_id;?>">
                  <td><?php echo $test->pvi_id;?></td>
                  <td><?php echo $test->clt_nombre;?></td>
                  <td><?php echo $test->usr_nombre;?></td>
                  <td><?php echo $test->pvi_fecha?></td>
                  <td><?php echo $test->pvi_hora	;?></td>
                  <td><?php echo number_format($test->pvi_descuento)?></td>
                  <td><?php echo number_format($test->pvi_impuestos)?></td>
                  <td><?php echo number_format($test->pvi_total)?></td>
                  <td><button class="btn btn-danger show-details" data-toggle="modal" data-target="#myModal" value="<?php echo $test->pvi_id?>">PENDIENTE</button></td>
                  <td><button class="btn btn-default show-details" data-toggle="modal" data-target="#myModal" value="<?php echo $test->pvi_id?>">Ver detalles</button></td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
              <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Vendedor</th>
                <th>Fecha</th>
                <th>Hora</th>
                <!-- <th>Impuesto</th> -->
                <!-- <th>Descuento</th> -->
                <th>Monto</th>
                <th>Estado</th>
                <th colspan="2"></th>
              </tr>
              </tfoot>
            </table>


          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalles</h4>
      </div>
      <div class="modal-body" id="data-detail">

        <table class="table">
          <thead>
            <tr>
              <th>Producto</th>
              <th>Valor</th>
              <th>Cantidad</th>
              <th>Subtotal</th>
            </tr>
          </thead>
          <tbody  id="det-prod">
          </tbody>
        </table>
        <hr>
        <table class="table">
          <thead>
            <tr>
              <th>Subtotal</th>
              <th>Descuento</th>
              <th>Impuesto</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr id="det-fact">
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<script>

  $(document).ready(function(){
    $(".show-details").click(function() {

      var del = $(this).val();
      var parametros = { "call"  : true, "posicion" : del};

       $.ajax({ data: parametros, url:' <?= base_url('Controller_pos/json_detail_venta/')?>'+del,type:  'post',
         beforeSend: function () {

         },
         success:  function (response) {

           var response = JSON.parse(response);
           if (response.success==true)
           {
             $('#det-fact').html("");
             const info = response.datos[0];
             $('#det-fact').append("<td>$"+info.pvi_subtotal+"</td>");
             $('#det-fact').append("<td>$"+info.pvi_descuento+"</td>");
             $('#det-fact').append("<td>$"+info.pvi_impuestos+"</td>");
             $('#det-fact').append("<td>$"+info.pvi_total+"</td>");

             const itemsArray = response.details

             itemsArray.forEach(function(element,key){

               var div = "<tr>";
               div = div + "<td>"+element.prod_nombre+"</td>";
               div = div + "<td>$"+element.prod_precio+"</td>";
               div = div + "<td>$"+element.pvd_desc+"</td>";
               div = div + "<td>$"+element.pvd_subt+"</td>";
               div = div + "</tr>";
               $('#det-prod').append(div);
             })


             //alert(JSON.stringify(response.datos))
             //$('#tr'+del).hide(2000);
           }
           else
           {
             alert(JSON.stringify(response))
           }
         }
       }).fail( function(error) {
          alert(JSON.stringify(error))
       })
    })
  })

  $(document).ready(function(){
    $(".calldel").click(function() {
      var del = $(this).val();

      var parametros = { "delete"  : true, "posicion" : del};

       $.ajax({ data: parametros, url:' <?= base_url('Ventas/delete')?>',type:  'post',
         beforeSend: function () {$('#tr'+del).html("<tr><td colspan='5'>Enviando, espere por favor...</td></tr>");},
         success:  function (response) {

           var response = JSON.parse(response);
           if (response.success==true)
           {
             $('#tr'+del).hide(2000);
           }
           else
           {
             alert(JSON.stringify(response))
           }
         }
       }).fail( function(error) {
          alert(JSON.stringify(error))
       })
    })
  })
</script>
