<?php foreach($ing as $data2) { $y = $data2->mm_id_concepto; } foreach($producto as $data1) {   $x = $data1->mp_id_concepto; }?>


<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Registro de producción
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> SmartBake</a></li>
        <li><a href="#"><i class="fa fa-dashboard"></i> Producción del Día</a></li>
        <li class="active">Registro de producción</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h1 style="font-size:28px" class="box-title" >Productos</h1>
            </div>

            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <?php foreach($producto as $datos) {  ?>
                  <li class="movimiento" id="<?php echo $datos->mp_id;?>" ><a href="#" style="font-size:38;" ><?php echo $datos->prod_nombre;?> <span class="pull-right badge bg-yellow" style="font-size:28;"><?php echo $datos->mp_cant;?></span></a></li>
                <?php } ?>
                <?php foreach($materias as $datos2) {  ?>
                  <li><a href="#" style="font-size:28;" ><?php echo $datos2->mat_nombre;?> <span class="pull-right badge bg-yellow" style="font-size:28;"><?php echo number_format($datos2->mm_cant/$datos2->unidad_escala); ?> <?php echo $datos2->unidad_nombre; ?></span></a></li>
                <?php } ?>

              </ul>
            </div>

          </div>
        </div>


        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h1 style="font-size:28px" class="box-title" >Ingredientes</h1>
            </div>
            <div class="box-body">
              <ul class="list-group list-group-flush">
                <?php foreach($ing as $ingredientes) { ?>
                <li class="list-group-item">
                   <span style="font-size:20px; color:black;" ><?php echo $ingredientes->mat_nombre;?>    </span><span class=" badge bg-blue" style="font-size:25px;    float: right;" ><?php echo number_format($ingredientes->mm_cant/$ingredientes->unidad_escala); ?> <?php echo $ingredientes->unidad_nombre; ?>s</span>
                </li>
                <?php } ?>
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div  class="col-md-12">
          <form role="form" action="<?php echo base_url(); ?>Controller_produccion/delete" method="POST">
            <input type="hidden" name="out" value="<?php echo $y; ?>" class="form-control"  >
            <input type="hidden" name="in" value="<?php echo $x; ?>" class="form-control"  >
            <input type="hidden" name="x" value="2"class="form-control"  >
            <button class="btn btn-danger btn-lg"> Eliminar Producción</button>
          </form>
        </div>

      </div>

      <!-- Modal -->
      <div class="modal fade" id="Modalprod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                <h2 class="modal-title">Ingrese cantidad de producto terminado</h2>
            </div>
            <div class="modal-body">
              <form action="#" id="form" class="form-horizontal">

                <h4>Cantidad</h4>
                <input name="cantidad" placeholder="cantidad" class="form-control" type="number">
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
              <button type="button" id="btnSave"  class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
$(document).ready(function()
{
  var id;

///MOSTRAR form producto produccion
$(".movimiento").click(function(){
  id = $(this).attr("id");
  $('#form')[0].reset();
  $('#Modalprod').modal('show');
});

// //AJAX INSERTAR OPCION
 $("#btnSave").click(function(){
   $.ajax({
       url : "<?php echo site_url('Controller_produccion/edit_mov_prod/')?>"+id,
       type: "POST",
       data: $('#form').serialize(),
       dataType: "JSON",
       success: function(data)
       {
         $('#Modalprod').modal('hide');
        location.reload();
       },
       error: function (jqXHR, textStatus, errorThrown)
        {
          alert('error');
       }
   });
 });

});
</script>
