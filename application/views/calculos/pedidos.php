
<!-- Full Width Column -->
<div class="content-wrapper">
  <div class="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 id="resoptsav"> Pedidos</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> SmartBake</a></li>
        <li class="active">Pedidos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <div  class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <i style="font-size:28px" class="fa fa-lemon-o" ></i>
              <h1 style="font-size:28px" class="box-title" >Ordenes de produccion</h1>
            </div>
            <div class="box-body">

                <?php foreach ($orden as $value) { ?>

                  <?php $detalles = $this->Model_orden_prod->read_prod_producto($value->orp_id); ?>
                  <?php foreach($detalles as $det) { ?>

                 	 <?php $buttonstate = "btn-default"; $status = 1;
                      if ($det->pp_estado==1) { $buttonstate = "btn-success";  $status = 0; }
                    ?>

                      <li class="list-group-item">
                        <a href="<?php echo base_url('Produccion/orden/detalles/'.$value->orp_id.'/'.$det->pp_id); ?>">
                        <?php echo $det->prod_nombre;?>
                        </a>
                        <?php 
                        
                        if($det->pp_estado==0)
                        {
                          echo '<span class="badge bg-green"> Pendiente</span>';
                        }
                        else
                        {
                          echo '<span class="badge bg-orange">Realizado</span>';
                        }

                        ?>
                        
                        <span class=" badge bg-blue"   ><?php echo $det->pp_cant; ?> Unidades</span>
                      </li>



                  <?php } ?>
                <hr>
                <?php } ?>



            </div>
            <!-- /.box-body -->
          </div>
        </div>



    </section>
    <!-- /.content -->
  </div>
  <!-- /.container -->
</div>
<!-- /.content-wrapper -->

<script>
$( ".update-val" ).click(function() {

  var value = $(this).val();
  var state = $(this).data("state");

  var parametros = { "update" : true, "value" : value, "state" : state};

  $.ajax({ data: parametros, url:' <?= base_url('Orden/produccion/producto/update-state')?>',type:'post',
     beforeSend: function () {$("#resoptsav").html("Enviando, espere por favor...");},
     success:  function (response) {
       var response = JSON.parse(response);
       if (response.success==true)
       { location.reload(); }
       else
       { alert(response.message) }
     }
  }).fail( function(error) {
    alert(JSON.stringify(error))
 })


});
</script>
