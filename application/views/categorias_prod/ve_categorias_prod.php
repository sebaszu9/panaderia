<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Editar
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Productos</a></li>
        <li><a href="#">Departamentos</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box-header">

        <a class=" btn  btn-primary" href="<?php echo base_url('Categoriasprod/index');?>">Volver</a>
      </div>
      <div class="row">
        <div class="col-md-6">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Editar Departamento Productos</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php foreach($datos as $test) { ?>

    <form role="form" action="<?php echo base_url('Categoriasprod/update/'); ?><?= $test->cat_prod_id; ?>" method="POST" enctype="multipart/form-data">
      <div class="box-body">

        <?php if(isset($mensaje)):?>
                <div class="alert <?= $alert; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?= $mensaje; ?>
                </div>
        <?php endif; ?>
        <?php echo validation_errors(); ?>

        <div class="form-group">
          <label for="exampleInputEmail1">Código Departamento</label>
          <input type="text" name="code" class="form-control"  value="<?php echo $test->cat_prod_code; ?>">

          <label for="exampleInputEmail1">Nombre del Departamento</label>
          <input type="text" name="nombre"  class="form-control"  value="<?php echo $test->cat_prod_nombre; ?>">

          <label for="exampleInputEmail1">Descripción del Departamentos</label>
          <input type="text" name="descripcion"  class="form-control"  value="<?php echo $test->cat_prod_dsc; ?>">

          <div class="form-group">
            <input type="hidden" name="cat_id"  class="form-control" value="<?php echo $test->cat_prod_id; ?>">
          </div>
        </div>



        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
        </div>
        </div>
        </form>
        <?php  }  ?>

      </div>
      <!-- /.box-body -->



</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
