<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detalles Producto
    </h1>
    <a class=" btn  btn-primary" href="<?php echo base_url('Productos/index');?>">Volver</a>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <!-- Main Row -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Detalles del Producto</h3>
            </div>

    <?php foreach($datos as $test) { ?>
  <!-- /.box-header -->
  <div class="box-body">
      <table id="table-alla-order" class="table table-bordered table-striped">
        <tbody>
          <tr>
            <th>ID</th>
            <td><?php echo $test->prod_codigo;?></td>
          </tr>
          <tr>
            <th>Producto</th>
            <td>
              <?php echo $test->prod_nombre;?>
            </td>
          </tr>
          <tr>
            <th>Categoría </th>
            <td>
              <?php echo $test->cat_prod_nombre;   ?>
            </td>
          </tr>
          <tr>
            <th>Descripción</th>
            <td>
              <?php echo $test->prod_descripcion;   ?>
            </td>
          </tr>
          <tr>
            <th>Precio</th>
            <td>
              <?php echo $test->prod_precio;   ?>
            </td>
          </tr>
          <tr>
            <th>Costo</th>
            <td>
              <?php echo "$".$test->prod_costo;   ?>
            </td>
          </tr>
          <tr>
            <th>Utilidad</th>
            <td>
              <?php echo "$".($test->prod_precio - $test->prod_costo); ?>
            </td>
          </tr>
          <tr>
            <th>% Utilidad</th>
            <td>
              <?php echo intval(($test->prod_precio - $test->prod_costo)*100/$test->prod_precio)."%"; ?>
            </td>
          </tr>
          <tr>
            <th>Impuesto</th>
            <td>
              <?php echo $test->imp_porcentaje."%"; ?>
            </td>
          </tr>
          <tr>
            <th>Código de Barras</th>
            <td>
              <?php echo $test->prod_barcode;?>
            </td>
          </tr>

        </tbody>
      </table>
  </div>
  <!-- /.box-body -->
    <?php  }  ?>
</div>
<!-- /.box -->
</div>

<div class="col-md-6">
  <!-- general form elements -->
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">No inventario</h3>
  </div>
</div>
<!-- /.box -->
</div>

<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
