<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Eliminar Producto
    </h1>
    <a class=" btn  btn-primary" href="<?php echo base_url('Productos/index');?>">Volver</a>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <!-- Main Row -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Detalles del Producto a Eliminar</h3>
            </div>

    <?php foreach($datos as $test) { ?>
  <!-- /.box-header -->
  <div class="box-body">
    <form role="form" action="<?= base_url()?>Productos/delete/<?= $test->prod_id; ?>" method="POST" enctype="multipart/form-data">


      <div class="form-group">
          <label for="exampleInputEmail1">Código  del Producto</label>
          <input type="text" class="form-control" readonly="true" name="titulo" placeholder="No tiene código" value="<?php echo $test->prod_codigo;?>">
        </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Nombre  del Producto</label>
        <input type="text" class="form-control" readonly="true" name="titulo" placeholder="Escribe el nombre de su producto" value="<?php echo $test->prod_nombre;?>">
      </div>

      <div class="form-group">
          <label for="exampleInputEmail1">Categoría del producto</label>
          <input type="text" class="form-control" readonly="true" name="titulo" placeholder="Escribe el nombre de su producto" value="<?php echo $test->cat_prod_nombre;?>">
        </div>
      <div class="form-group">
          <label for="exampleInputEmail1">Descripción</label>
          <input type="text" class="form-control" readonly="true" name="titulo" placeholder="No tiene descripcion" value="<?php echo $test->prod_descripcion;?>">
      </div>

      <div class="form-group">
          <label for="exampleInputEmail1">Precio</label>
          <input type="text" class="form-control" readonly="true" name="titulo" placeholder="No tiene precio" value="<?php echo $test->prod_precio;?>">
      </div>
      <div class="form-group">
          <label for="exampleInputEmail1">Impuesto</label>
          <input type="text" class="form-control" readonly="true" name="titulo" placeholder="No tiene impuesto" value="<?php echo $test->imp_nombre." del ".$test->imp_porcentaje;?>">
      </div>
      <div class="form-group">
          <label for="exampleInputEmail1">Código de Barras</label>
          <input type="text" class="form-control" readonly="true" name="titulo"  value="<?php echo $test->prod_barcode;?>">
      </div>

<input type="submit" class="btn btn-danger btn-sm" value="Confirmar Eliminar">
    </form>
  </div>
  <!-- /.box-body -->
    <?php  }  ?>
</div>
<!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
