  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Formulario de creación de Productos
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Producto</a></li>
        <li class="active">Formulario Productos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box-header">

        <a class=" btn  btn-primary" href="<?php echo base_url('Productos/index');?>">Volver</a>
      </div>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Crear nuevo producto</h3>
            </div>

            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="<?= base_url()?>Productos/create" method="POST" enctype="multipart/form-data">
              <div class="box-body">

                <?php if(isset($mensaje)):?>
                        <div class="alert <?= $alert; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?= $mensaje; ?>
                        </div>
                <?php endif; ?>
                <?php echo validation_errors(); ?>
                <div class="form-group">
                  <label for="exampleInputEmail1">Código de Producto</label>
                  <input type="text" class="form-control" name="id" placeholder="Escribe el código de su producto" value="<?php echo set_value('id'); ?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Nombre de Producto</label>
                  <input type="text" class="form-control" name="nombre" placeholder="Escribe el nombre de su producto" value="<?php echo set_value('nombre'); ?>">
                </div>
                <div class="form-group">
                  <label>Categoría</label>
                  <select class="form-control" name="categoria">
                    <?php foreach($categorias as $datos) { ?>
                      <option value="<?= $datos->cat_prod_id;?>"> <?= $datos->cat_prod_nombre." Código ".$datos->cat_prod_id;?></option>
                      <?php  }  ?>
                    </select>
                  </div>

                <div class="form-group">
                  <label>Inventariable</label>
                  <select class="form-control" name="inventario">
                      <option value="1">Si</option>
                      <option value="0">No</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Incluye vendaje</label>
                  <select class="form-control" name="vendaje">
                      <option value="1">Si</option>
                      <option value="0">No</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Tipo de Producto</label>
                  <select class="form-control" name="tipo">
                    <option value="0">Único</option>
                    <option value="1">Combo</option>
                    <option value="2">Receta</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Precio</label>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                    <input type="number" name="precio" class="form-control" value="<?php echo set_value('precio'); ?>">
                  </div>
                </div>

              <div class="form-group">
                <label>Código de barras</label>
                <input type="text" name="barras" class="form-control" value="<?php echo set_value('barras'); ?>">
              </div>
                <div class="form-group">
                  <label>Impuesto</label>
                  <select class="form-control" name="impuesto">
                  <?php foreach($impuestos as $datos) { ?>
                    <option value="<?= $datos->imp_id;?>"><?= $datos->imp_nombre." ".$datos->imp_porcentaje;?></option>
                  <?php  }  ?>
                  </select>
                </div>
                <br>



                <div class="form-group">
                  <label>Descripción</label>
                  <textarea class="form-control" name="desscripcion" rows="3" placeholder="Descipción breve del producto ..."><?php echo set_value('desscripcion'); ?></textarea>
                </div>

              <div class="box-footer">
                <input type="hidden" value="true" name="create">
                <input type="submit" class="btn btn-primary btn-sm" value="Guardar">
              </div>

          </div>
          </form>
          <!-- /.box -->

        </div>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
