<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formulario de edición de Productos
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box-header">
      <a class=" btn  btn-primary" href="<?php echo base_url('Productos/index');?>">Volver</a>
    </div>
    <!-- Small boxes (Stat box) -->
    <?php echo validation_errors(); ?>
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Editar producto</h3>
          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <?php foreach($datos as $test) { ?>
          <form role="form" action="<?= base_url()?>Productos/update/<?= $test->prod_id; ?>" method="POST" enctype="multipart/form-data">
            <div class="box-body">

              <?php if(isset($mensaje)):?>
                <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
                </div>
              <?php endif; ?>
              <input type="hidden" class="form-control" name="id" value="<?= $test->prod_id; ?>">
              <div class="form-group">
                <label for="exampleInputEmail1">Código de Producto</label>
                <input type="text" class="form-control" name="code" placeholder="Escribe el código de su producto" value="<?= $test->prod_codigo; ?>">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nombre de Producto</label>
                <input type="text" class="form-control" name="nombre" placeholder="Escribe el nombre de su " value="<?= $test->prod_nombre; ?>">
              </div>
              <div class="form-group">
                <label>Categoría</label>
                <select class="form-control" name="categoria">
                  <option value="<?php  echo $test->prod_cat;?>" selected><?php echo $test->cat_prod_nombre;?> (antiguo)</option>
                  <?php foreach($categorias as $datos) { ?>
                    <option value="<?= $datos->cat_prod_id;?>"><?= $datos->cat_prod_nombre." Codigo ".$datos->cat_prod_id;?></option>
                    <?php  }  ?>
                </select>
              </div>

              <div class="form-group">
                <label>Inventariable</label>
                <select class="form-control" name="inventario">
                    <option value="<?= $test->prod_inv;?>">
                      <?php if ($test->prod_inv == 1) {
                            echo "Si(Actual)";
                            }
                            else {
                              echo "No(Actual)";
                            }
                     ?></option>
                    <option value="1">Si</option>
                    <option value="0">No</option>
                </select>
              </div>

              <div class="form-group">
                <label>Incluye vendaje</label>
                <select class="form-control" name="vendaje">
                    <option value="<?= $test->prod_vendaje;?>">
                      <?php if ($test->prod_vendaje == 1) {
                            echo "Si(Actual)";
                            }
                            else {
                              echo "No(Actual)";
                            }
                     ?></option>
                    <option value="1">Si</option>
                    <option value="0">No</option>
                </select>
              </div>

              <div class="form-group">
                <label>Tipo</label>
                <select class="form-control" name="tipo">
                     <option value="<?= $test->prod_tipo;?>">
                      <?php if ($test->prod_tipo == 1) {
                            echo "Combo(Actual)";
                            }
                            else {
                              echo "Único(Actual)";
                            }?>
                     </option>
                     <option value="0">Único</option>
                     <option value="1">Combo</option>
                </select>
              </div>
              <div class="form-group">
                <label>Precio</label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                  <input type="number" name="precio" class="form-control" value="<?= $test->prod_precio; ?>">
                </div>
              </div>
            <div class="form-group">
              <label>Código de barras</label>
              <input type="number" name="barras" class="form-control" value="<?= $test->prod_barcode; ?>">
            </div>

              <div class="form-group">
                <label>Impuesto</label>
                <select class="form-control" name="impuesto">
                  <option value="<?php  echo $test->prod_imp;?>" selected><?php echo $test->imp_nombre;?> (antiguo)</option>
                <?php foreach($impuestos as $datos) { ?>
                  <option value="<?= $datos->imp_id;?>"><?= $datos->imp_nombre." ".$datos->imp_porcentaje;?></option>
                <?php  }  ?>
                </select>
              </div>
              <br>
              <div class="form-group">
                <label>Descripción</label>
                <textarea class="form-control" name="desscripcion" rows="3" placeholder="Descipcion breve del producto ..."><?php echo set_value('desscripcion'); ?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Foto(No es obligatorio)</label>
                <input type="file" id="exampleInputFile" name="photo">
                <p class="help-block">Por favor, subir la imagen de formato permitido jpg, png y jpeg.</p>
              </div>

            <div class="box-footer">
              <input type="hidden" value="true" name="create">
              <input type="submit" class="btn btn-primary btn-sm" value="Guardar">
            </div>
        </div>
        </form>
        <?php  }  ?>
        <!-- /.box -->
      </div>
      </div>

      <?php foreach ($receta as $darect ){ ?>

        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Receta del prodcucto</h3>
              <a id="add-ing" class="btn btn-primary btn-md pull-right" ><i class="fa fa-plus" aria-hidden="true"></i> Ingrediente</a>
             </div>

             <div class="box-body">
               <table class="table table-bordered table-striped">
                 <thead>
                   <tr>
                     <th>ID</th>
                     <th>Producto</th>
                     <th>Cantidad</th>
                     <th>Eliminar</th>
                   </tr>
                   </thead>
                   <tbody>
                     <?php  $acumulador = 0;  $peso = 0; $ingrediente = $this->model_recetas->display_ing($darect->rp_receta); ?>
                     <?php foreach ($ingrediente as $ing): ?>
                       <tr>
                         <td><?php echo $ing->mat_id;?> </td>
                         <td><?php echo $ing->cr_nombre;?> </td>
                         <td><?php echo $ing->mat_nombre;?> </td>
                         <td>
                           <?php
                             $costo =(($ing->ing_cant/$ing->unidad_escala)*($ing->mat_costo)/($ing->mat_ue));
                             $acumulador = $acumulador + $costo;
                             $peso = $peso + $ing->ing_cant/$ing->unidad_escala;
                             echo  "$".number_format($costo);
                           ?>
                         </td>
                       </tr>
                     <?php endforeach; ?>


                   </tbody>
               </table>

               <table class="table table-bordered table-striped">
                 <tbody>
                   <tr>
                     <th><h3>Costo Total Base </h3> <h3>$<?= number_format($acumulador); ?></h3></th>
                     <th><h3>Peso  </h3> <h3><?= number_format($peso); ?> grs</h3></th>
                   </tr>
                 </tbody>

               </table>

             </div><!-- /.box -->

            </div>
        </div>

        <div class="modal fade" id="modal_form_ing" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h3 class="modal-title">Adicionar Ingrediente Base</h3>
                  </div>
                  <div class="modal-body form">
                       <form action="#" id="form" class="form-horizontal">
                           <div class="form-body">
                             <div class="form-group">
                                 <label class="control-label col-md-3">Materia Prima</label>
                                 <div class="col-md-9">
                                   <select name="materia" class="form-control" style="width: 300px;">
                                     <?php foreach($materias as $datos3) { ?>
                                       <option value="<?=$datos3->mat_id; ?>"><?=$datos3->mat_nombre; ?> Unidad:<?=$datos3->unidad_nombre; ?></option>
                                        <?php } ?>
                                   </select>
                                 </div>
                             </div>
                             <input name="receta" value="<?php echo $darect->rp_receta;?>" class="form-control" type="hidden">
                             <div class="form-group">
                                 <label class="control-label col-md-3">Concepto</label>
                                 <div class="col-md-9">
                                   <select name="concepto" class="form-control" style="width: 300px;">
                                     <?php foreach($conceptos as $concepto) { ?>
                                       <option value="<?=$concepto->cr_id; ?>"><?=$concepto->cr_nombre; ?></option>
                                        <?php } ?>
                                   </select>
                                 </div>
                             </div>
                             <div class="form-group">
                               <label class="control-label col-md-3">Cantidad</label>
                               <div class="col-md-9">
                                   <input name="cantidad" placeholder="Cantidad" class="form-control" type="number">
                                   <span class="help-block"></span>
                               </div>
                            </div>
                        </div>
                       </form>
                    </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                      <button type="button" id="btnSave"  class="btn btn-primary">Guardar</button>
                  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


      <?php } ?>

      <?php  if ($test->prod_tipo == 1) { ?>
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Productos del Combo</h3>
               <div class="box-header with-border">
                <th><a id="add" class="btn btn-primary btn-md" ><i class="fa fa-plus" aria-hidden="true"></i> Producto</a>
               </div>
                 <div class="box-body">
                   <table class="table table-bordered table-striped">
                     <thead>
                       <tr>
                         <th>ID</th>
                         <th>Producto</th>
                         <th>Cantidad</th>
                         <th>Eliminar</th>
                       </tr>
                       </thead>
                       <tbody>
                       </tr>
                       <?php foreach($combo as $datos_combo) { ?>
                         <tr>
                             <td><?= $datos_combo->prod_id; ?></td>
                             <td><?= $datos_combo->prod_nombre; ?></td>
                             <td><?= $datos_combo->comb_cant; ?></td>
                             <td><button id="<?=$datos_combo->comb_id; ?>" class="btn btn-danger btn-sm delete" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
                         </tr>
                         <?php  }  ?>
                       </tbody>
                   </table>
                 </div><!-- /.box -->
           </div>
            <?php  }  ?>
          </div>
      </div>



      </div>
    </div>    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Producto Combo</h3>
            </div>
            <div class="modal-body form">
                 <form action="#" id="form" class="form-horizontal">
                     <div class="form-body">
                       <input name="padre" value="<?= $test->prod_id; ?>" class="form-control" type="hidden">
                           <label>Producto</label>
                           <select class="form-control select2" style="width:100%;" name="hijo">
                           <?php foreach($productos as $ing) { ?>
                             <option value="<?= $datos2->prod_id;?>"><?= $datos2->prod_nombre;?></option>
                           <?php  }  ?>
                           </select>
                           <label>Cantidad</label>
                           <br>
                           <input name="cantidad"  class="form-control" type="number">
                      </div>
                 </form>
            </div>
          <div class="modal-footer">
              <button type="button" id="guardar"  class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">ELIMINAR</h3>
            </div>
            <div class="modal-footer">
                <button type="button" id="delete"  class="btn btn-primary">Confirmar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
          </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
// Ajax post
$(document).ready(function()
{
  var id;
  ///MOSTRAR VARIACION
  $("#add").click(function(){ /// ADICIONAR INGREDIENTE
    $('#modal_form').modal('show');
   });

   $("#add-ing").click(function(){ /// ADICIONAR INGREDIENTE
     $('#modal_form_ing').modal('show');
    });

   $(".delete").click(function(){ /// ADICIONAR INGREDIENTE
     $('#modal_delete').modal('show');
     id = $(this).attr("id");
    });

  // //AJAX INSERTAR VARIACION
   $("#guardar").click(function(){
     $.ajax({
          url : "<?php echo site_url('Controller_productos/add_combo/')?>",
          type: "POST",
          data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {
           $('#modal_form').modal('hide');
           location.reload();
          },
          error: function (jqXHR, exception) {},
       });
     });

     // //AJAX INSERTAR VARIACION
     $("#btnSave").click(function(){
      $.ajax({
           url : "<?php echo base_url('Recetas/add_ing');?>",
           type: "POST",
           data: $('#form').serialize(),
           dataType: "JSON",
           success: function(data)
           {
            $('#modal_form_ing').modal('hide');
            location.reload();
           },
           error: function (jqXHR, exception) {},
        });
      });


    $("#delete").click(function(){
    $.ajax({
        url : "<?php echo site_url('Controller_productos/delete_combo/')?>"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

});
</script>
