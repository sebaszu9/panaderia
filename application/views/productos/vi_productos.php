<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado de Productos
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Productos/form')?>" class="btn  btn-primary">Nuevo +</a>

            <form class="pull-right" method="post" action="<?= base_url('/Productos/excel')?>">
              <input type="hidden" name="excel" value="true">
              <button class="btn btn-success">Exportar en Excel <i class="fa fa-file-excel-o"></i></button>
            </form>

            <form class="pull-right" method="post" action="<?= base_url('/Productos/pdf')?>">
              <input type="hidden" name="excel" value="true">
              <button class="btn btn-danger">Exportar en PDF <i class="fa fa-file-excel-o"></i></button>
            </form>

          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Código</th>
                <th>Categoria</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Impuesto</th>
                <th>Codigo Barras</th>
                <th>Acción</th>
              </tr>
              </thead>
              <tbody>
                <?php foreach($info as $datos) { ?>
                <tr>
                    <td><?php echo $datos->prod_codigo;?> </td>
                    <td><?php echo $datos->cat_prod_nombre;?> </td>
                    <td><?php echo $datos->prod_nombre;?> </td>
                    <td><?php echo $datos->prod_precio;?> </td>
                    <td><?php echo $datos->imp_nombre;?> </td>
                    <td><?php echo $datos->prod_barcode;?> </td>
                    <td><a href="<?php echo base_url('Productos/details')."/".$datos->prod_id; ?>" class="btn btn-success" title="Detalles"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a href="<?php echo base_url('Productos/edit')."/".$datos->prod_id; ?>" class="btn btn-warning" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="<?php echo base_url('Productos/get')."/".$datos->prod_id; ?>"  class="btn btn-danger" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Categoria</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Impuesto</th>
                    <th>Codigo Barras</th>
                    <th>Acción</th>
                </tr>
              </tfoot>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
