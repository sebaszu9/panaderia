<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formulario
    </h1>
  </section>



  <!-- Main content -->
  <section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detalles de la Salida</h3>
            <?php echo validation_errors(); ?>

            <?php if ($this->session->flashdata('mensaje'))  { ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p><i class="icon fa fa-exclamation-circle"></i> <?= $this->session->flashdata('mensaje') ?></p>
              </div>
            <?php } ?>

          </div>

          <div class="box-body">

            <form action="<?= base_url('Movimiento/producto/output/create') ?>" method="POST" >
            <div class="form-group col-md-3">
              <label>Panaderia</label>
              <select class="form-control" name="panaderia">
              <?php foreach($panaderias as $datosd) { ?>
                <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-3">
              <label>Concepto</label>
              <select class="form-control" name="concepto_cod">
              <?php foreach($concepto as $datos) { ?>
                <option value="<?= $datos->c_id;?>"><?= $datos->c_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label for="exampleInputEmail1">Fecha</label>
              <input type="text" name="fecha"  id="datepicker" value="<?php echo set_value('fecha'); ?>" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-6">
              <div class="form-group ">
                <label for="exampleInputEmail1">Observaciones</label>
                <input type="text" name="descripcion"  id="datepicker" v class="form-control" placeholder="Descripcion ..." value="<?php echo set_value('descripcion'); ?>">
              </div>
            </div>

            <div class="form-group col-md-2">
              <div class="form-group ">
                <label for="exampleInputEmail1">.</label>
                <button type="submit" class="btn btn-block btn-info" ><i class="fa fa-check-circle"></i> Realizar Salida</button>
              </div>
            </div>

            <input type="hidden" name="concepto" value="pedido" class="form-control"  >
            <input type="hidden" name="tipo" value="in" class="form-control"  >

          </form>
          </div>

        </div>

        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Materia de la Salida</h3>
          </div>
          <div class="box-body">
            <div class="col-md-2">
              <button type="button" class="btn btn-block btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i> Buscar productos</button>
            </div>

            <div class="modal-body col-md-12">

              <div class="col-md-12">

                <table class="table table-bordered order-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Producto</th>
                      <th>Cant empaques.</th>
                      <th>Costo</th>
                    </tr>
                  </thead>
                  <tbody id="loadcarttable">
                    <?php
                        if ($this->session->userdata('pedido_materia')) {

                          $i = 1;
                          $carritop = $this->session->userdata('pedido_materia');

                          foreach ($carritop as $value) {
                            $posicion = $i-1;
                            echo "<tr>";
                            echo "<td> ".$i." </td>";
                            echo "<td> ".$value['nombre']." </td>";
                            echo "<td> ".$value['cantidad']." </td>";
                            echo "<td> ".number_format($value['costo'])." </td>";
                            echo '<td> <button type="button" class="btn btn-info deletet" value="'.$posicion.'"><i class="fa fa-trash"></i></button></td>';
                            echo "<tr>";
                            $i++;
                          }
                        }
                    ?>
                  </tbody>
                </table>
              </div>

            </div>



          </div>
        </div>

      </div>
    </div>
      <!-- /.box-body -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Buscar productos</h4>
        </div>


        <div class="modal-body">
          <div class="col-md-12">

            <div class="form-group col-md-6">
              <div class="form-group ">
                <label for="exampleInputEmail1">Categoria</label>
                <select class="form-control" id="selectcatg">
                  <option value="">Seleccione</option>
                  <?php foreach($categoria as $datos) { ?>
                    <option data-cat="<?= $datos->cat_prod_id;?>"><?php echo $datos->cat_prod_nombre;?></option>
                  <?php  }  ?>
                </select>
              </div>
            </div>

            <div class="form-group col-md-6">
              <div class="form-group ">
                <label for="exampleInputEmail1">Filtrar</label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  <input type="search" class="light-table-filter form-control" data-table="order-table" placeholder="Buscar ...">
                </div>
              </div>
            </div>

            <div class="col-md-12">

              <table class="table table-bordered order-table">
                <thead>
                  <tr>
                    <th>Productos</th>
                    <th>Cant.</th>
                    <th>Costo</th>
                    <th>Agregar</th>
                  </tr>
                </thead>
                <tbody id="loadtable">

                </tbody>
              </table>
            </div>

          </div>
        </div>


        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
  </div>


  <script>
  $("#selectcatg").change(function() {
    var select = $(this).find(':selected').data("cat");
    var parametros = {  "safety" : true,  "categoria" : select};
    $.ajax({ data:  parametros, url:' <?= base_url('Controller_productos/load_prod_table')?>',type:  'post',
    	 beforeSend: function () {$("#loadtable").html("<tr><td>Cargando, espere por favor...</tr></td");},
    	 success:  function (response) {$("#loadtable").html(response); }
     })
  });

  $(document).on('click', '.addcar', function(){
    var code  = $(this).val();
    var cantd = $("#cant"+code).val();
    var costo = $("#costo"+code).val();
    var name  = $("#nombre"+code).val();

    if (costo<=50) {
      alert('Muy bajo costo');
    }
    else {
      var carrito = {
        "safety"   : true,
        "nombre"   : name,
        "producto" : code,
        "cantidad" : cantd,
        "costo"    : costo
      };

      $.ajax({ data:  carrito, url:' <?= base_url('Controller_productos/addcart_prod')?>',type:  'post',
      	 beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
      	 success:  function (response) {$("#loadcarttable").html(response); }
       })
    }
  });


  $(document).on('click', '.deletet', function(){
    var code  = $(this).val();
    var carrito = {
      "safety"   : true,
      "delete"   : 54,
      "posicion"   : code,
    };

    $.ajax({ data:  carrito, url:' <?= base_url('Controller_productos/delete_array')?>',type:  'post',
       beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
       success:  function (response) {$("#loadcarttable").html(response); }
     })

  });

  </script>
