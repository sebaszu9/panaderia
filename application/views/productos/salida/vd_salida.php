<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detalles
    </h1>
  </section>



  <!-- Main content -->
  <section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <?php foreach($entrada as $datosd) { ?>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Consulta entrada</h3>
          </div>

          <div class="box-body">

            <div class="form-group col-md-3">
              <label>Panaderia</label>
              <select class="form-control" name="panaderia" readonly>
                <option value="none"><?= $datosd->bod_nombre;?></option>
              </select>
            </div>

            <div class="form-group col-md-3">
              <label>Concepto</label>
              <select class="form-control " name="proveedor" readonly>
                <option value="none"><?= $datosd->c_nombre;?></option>
              </select>
            </div>

            <div class="form-group col-md-3">
              <label for="exampleInputEmail1">Fecha</label>
              <input type="text" name="fecha"  id="datepicker" value="<?= $datosd->cm_fecha;?>" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-6">
              <div class="form-group ">
                <label for="exampleInputEmail1">Observaciones</label>
                <input type="text" name="descripcion"  id="datepicker" class="form-control" value="<?= $datosd->cm_obs;?>" readonly="true">
              </div>
            </div>


            <div class="form-group col-md-2">
              <div class="form-group ">
                <label for="exampleInputEmail1" style="color:white">Pillao</label>
                <button type="submit" class="btn btn-block btn-danger deletas" value="<?= $datosd->cm_id;?>"> Eliminar entrada <i class="fa fa-trash"></i></button>
              </div>
            </div>

          </div>




        </div>

        <script>
          $(".deletas").click(function() {

            swal({
              title: 'Eliminar',
              text: " ¿Está  seguro que quiere eliminar este registro permanentemente de la base de datos?",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar.'
            }).then((result) => {
              if (result.value) {
                var code  = $(this).val();
                var carrito = { "safety"   : true, "concepto" : code };
                $.ajax({ data:  carrito, url:' <?= base_url('Movimiento/materia/output/delete')?>',type:  'post',
                   beforeSend: function () {$("#pololo").html("Cargando, espere por favor...");},
                   success:  function (response) { window.location.replace("<?= base_url('Movimiento/materia/output/index/') ?>"); }
                 })
              }
            })

          });
        </script>

        <?php  }  ?>

        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detalles del Pedido</h3>
          </div>
          <div class="box-body">


            <div class="modal-body col-md-12">

              <div class="col-md-12">

                <table class="table table-bordered order-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Producto</th>
                      <th>Cant empaques.</th>
                      <th>Costo</th>
                    </tr>
                  </thead>
                  <tbody id="loadcarttable">
                    <?php
                          $i = 1;
                        foreach ($materias as $value) {
                          echo "<tr>";
                          echo "<td> ".$i." </td>";
                          echo "<td> ".$value->mat_nombre." </td>";
                          echo "<td> ".$value->mm_cant." </td>";
                          echo "<td> ".number_format($value->mm_costo)." </td>";
                          echo "<tr>";
                          $i++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
      <!-- /.box-body -->
  </section>
  <!-- /.content -->



</div>
<!-- /.content-wrapper -->
