<script src="<?php echo base_url(); ?>/assets/jquery-1.9.1.js"></script>

<script src="<?php echo base_url(); ?>/assets/jquery-ui.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reporte Historico Pedidos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reportes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">

          <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <?php echo validation_errors(); ?>
              </div>

              <div class="box-body">
                <form method="post" action="<?= base_url('/Pedidos/historico/')?>" >
                <!-- <div class="form-group col-md-12">
                  <label>Sede de Panaderia</label>
                  <select class="form-control" name="tipo" readonly="">
                    <option value="0">Todos</option>
                    <?php foreach($sede as $datos) { ?>
                      <option value="<?php echo  $datos->bod_id;?>"><?php echo  $datos->bod_nombre;?></option>
                    <?php  }  ?>
                  </select>
                </div>

     -->

                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Seleccionar Fecha Inicial</label>
                  <input type="text" name="fecha1" class="form-control pull-right" id="datepicker1" value=""  autocomplete="off">
                </div>

                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Seleccionar Fecha Final</label>
                  <input type="text" name="fecha2" class="form-control pull-right" id="datepicker2" value=""  autocomplete="off">
                </div>

                <div class="form-group col-md-12">
                  <label>Clientes</label>
                  <select class="form-control select2" name="producto" >
                    <option value="0">Todos</option>
                    <?php foreach($clientes as $datos1) { ?>
                      <option value="<?php echo  $datos1->clt_id;?>"><?php echo $datos1->clt_nombre;?></option>
                    <?php  }  ?>
                  </select>
                </div>
                <div class="form-group col-md-12">
                  <label>Vendedor</label>
                  <select class="form-control select2" name="vendedor" >
                    <option value="0">Todos</option>
                    <?php foreach($vendedores as $datos2) { ?>
                        <option value="<?php echo  $datos2->id;?>"><?php echo $datos2->name;?></option>
                    <?php  }  ?>
                  </select>
                </div>
                <div class="form-group col-md-12">
                  <label>Tipo de reporte</label>
                  <select class="form-control" name="reporte" >
                    <option value="0">Imprimir</option>
                    <option value="1">Tabla</option>
                  </select>
                </div>

                <div class="form-group col-md-12">
                  <label style="color:white;">.</label><br/>
                  <input type="submit" class="btn btn-primary" value="Consultar">
                </div>

                <input type="hidden" name="search" value="true">

              </form>

              </div>
            </div>
        </div>

        <!-- /.Left col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
      $(document).ready(function() {
          //Date picker
          $('#datepicker1').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker2').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          $('#datepicker3').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker4').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          $('#datepicker5').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker6').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });
      });
  </script>
