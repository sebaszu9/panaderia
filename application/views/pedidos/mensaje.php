<script src="<?php echo base_url(); ?>/assets/jquery-1.9.1.js"></script>

<script src="<?php echo base_url(); ?>/assets/jquery-ui.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mensaje
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reportes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">

          <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <?php echo validation_errors(); ?>
              </div>

              <div class="box-body">
                <form method="post" action="<?= base_url('/Calculos/update_mensaje/')?>" >

                  <div class="form-group">
                    <?php foreach ($mensaje as $para): ?>
                    <label>Descripción</label>
                      <textarea class="form-control" name="texto" rows="3" placeholder="Descipción breve del producto ..."><?php echo $para->para_texto; ?></textarea>
                      <input type="hidden" name="id" value="<?php echo $para->para_int; ?>">
                      <label>Celular</label><br>
                      <input type="number" name="cel" value="<?php echo $para->para_cel; ?>"><br>
                      <label>Fijo</label><br>
                      <input type="text" name="fijo" value="<?php echo $para->para_fijo; ?>">
                    <?php endforeach; ?>
                  </div>
                <div class="form-group col-md-12">
                  <label style="color:white;">.</label><br/>
                  <input type="submit" class="btn btn-primary" value="Actualizar">
                </div>


              </form>

              </div>
            </div>
        </div>

        <!-- /.Left col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- <script type="text/javascript">
      $(document).ready(function() {
          //Date picker
          $('#datepicker1').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker2').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          $('#datepicker3').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker4').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          $('#datepicker5').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker6').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });
      });
  </script> -->
