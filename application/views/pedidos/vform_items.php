<script src="<?php echo base_url(); ?>/assets/jquery-1.9.1.js"></script>

<script src="<?php echo base_url(); ?>/assets/jquery-ui.js"></script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reporte Historico productos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reportes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">

          <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <?php echo validation_errors(); ?>
              </div>

              <div class="box-body">
                <form method="post" action="<?= base_url('/Pedidos/reporte/')?>" target="_blank">
                <!-- <div class="form-group col-md-12">
                  <label>Sede de Panaderia</label>
                  <select class="form-control" name="tipo" readonly="">
                    <option value="0">Todos</option>
                    <?php foreach($sede as $datos) { ?>
                      <option value="<?php echo  $datos->bod_id;?>"><?php echo  $datos->bod_nombre;?></option>
                    <?php  }  ?>
                  </select>
                </div>

     -->

                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Seleccionar Fecha Inicial</label>
                  <input type="text" name="fecha1" class="form-control pull-right" id="datepicker1" value=""  autocomplete="off">
                </div>

                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Seleccionar Fecha Final</label>
                  <input type="text" name="fecha2" class="form-control pull-right" id="datepicker2" value=""  autocomplete="off">
                </div>

                <div class="form-group">
                  <div class="col-md-9">
                    <label class="control-label col-md-3">Concepto</label>
                  <select class="form-control" name="concepto">
                      <option value="1">Venta + Vendaje</option>
                      <option value="2">Venta</option>
                      <option value="3">Venta incluye vendaje</option>
                      <option value="4">Vendaje</option>
                      <option value="5">Cambio</option>
                  </select>
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <br>
                  <label>Productos</label>
                  <select class="form-control select2" name="producto" >
                    <?php foreach($productos as $prod) { ?>
                      <option value="<?php echo  $prod->prod_id;?>"><?php echo $prod->prod_nombre;?></option>
                    <?php  }  ?>
                  </select>
                </div>
                <div class="form-group col-md-12">
                  <label style="color:white;">.</label><br/>
                  <input type="submit" class="btn btn-primary" value="Consultar" >
                </div>
                <input type="hidden" name="search" value="true">

              </form>

              </div>
            </div>
        </div>

        <!-- /.Left col -->
      </div>
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
      $(document).ready(function() {
          //Date picker
          $('#datepicker1').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker2').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          $('#datepicker3').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker4').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          $('#datepicker5').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });

          //Date picker
          $('#datepicker6').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd'
          });
      });
  </script>
