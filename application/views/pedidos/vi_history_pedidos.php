<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Listado Histórico Pedidos </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Vendedor</th>
                <th>Fecha - Hora</th>
                <th>Total</th>
                <th >Detalles</th>
                <th >Editar</th>
              </tr>
              </thead>
              <tbody>
                <?php  $x = 0  ?>
                <?php foreach($pedidos as $test) { ?>
                <tr id="tr<?php echo $test->pp_id;?>">
                  <td><?php echo $test->pp_id;?></td>
                  <td><?php echo $test->clt_nombre;?></td>
                  <td><?php echo $test->name;?></td>
                  <td><?php echo $test->pp_fecha?></td>
                  <!-- <td><?php echo $test->pp_subtotal;?></td> -->
                  <!-- <td><?php echo number_format($test->pp_descuento)?></td> -->
                  <!-- <td><?php echo number_format($test->pp_impuestos)?></td> -->
                  <td><?php echo number_format($test->pp_total)?></td>
                  <?php  $x = $x+$test->pp_total;  ?>
                  <!-- <td><button class="btn btn-danger show-details" data-toggle="modal" data-target="#myModal" value="<?php echo $test->pp_id?>">Pendiente</button></td> -->
                  <td><button class="btn btn-default show-details" data-toggle="modal" data-target="#myModal" value="<?php echo $test->pp_id?>">Ver detalles</button></td>
                  <td><a href="<?php echo base_url('Pedidos/edit')."/".$test->pp_id; ?>"  class="btn btn-info" title="Imprimir" target="_blank">Editar</a></td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>No.</th>
                  <th>Cliente</th>
                  <th>Vendedor</th>
                  <th>Fecha - Hora</th>
                  <th>Total</th>
                  <th >Detalles</th>
                  <th >Editar</th>
                </tr>
              </tfoot>
            </table>

            <h1>Total <?php  echo number_format($x);  ?> </h1>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalles</h4>
      </div>
      <div class="modal-body" id="data-header">

        <table class="table">
          <thead>
            <tr>
              <th>Producto</th>
              <th>Precio</th>
              <th>U</th>
              <th>Subtotal</th>
              <th>Descuento</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody  id="det-prod">
          </tbody>
        </table>
        <table class="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Cliente</th>
              <th>Direccion</th>
              <th>Telefono</th>
              <th>Subtotal</th>
              <th>Descuento</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody  id="det-head">
          </tbody>
        </table>
        <hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<div id="myModal-state" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar de Estado el Pedido</h4>
      </div>
      <div class="modal-body" >
          <td><button class="btn btn-danger show-details" >Eliminar</button></td><br>
          <hr>
        <td><button class="btn btn-secondary show-details" id="sin-confirmar">Sin confirmar</button></td><br>
        <hr>
        <td><button class="btn btn-warning show-details" id="empaque" >Empaque</button></td><br>
        <hr>
        <td><button class="btn btn-info show-details" id="reparto">Reparto</button></td><br>
        <hr>
        <td><button class="btn btn-success show-details" id="entregado" >Entregado</button></td><br>
        <hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<script>

  $(document).ready(function(){
    var id;
    $(".show-details").click(function() {

      var del = $(this).val();
      var parametros = { "call"  : true, "posicion" : del};

       $.ajax({ data: parametros, url:' <?= base_url('Controller_pedidos/details_pedido_json/')?>'+del,type:  'post',
         beforeSend: function () {

         },
         success:  function (response) {

           var response = JSON.parse(response);
           if (response.success==true)
           {
             $('#det-head').html("");
             // $('#det-fact').html("");
             const info = response.datos[0];
             $('#det-head').append( "<td>"+info.pp_id+"</td>");
             $('#det-head').append("<td>"+info.clt_nombre+"</td>");
             $('#det-head').append("<td>"+info.clt_direccion+"</td>");
             $('#det-head').append("<td>"+info.clt_telefono+"</td>");
             $('#det-head').append("<td>"+info.pp_subtotal+"</td>");
             $('#det-head').append("<td>"+info.pp_descuento+"</td>");
             $('#det-head').append("<td>"+info.pp_total+"</td>");
             // $('#det-fact').append("<td>id</td><td>"+info.pp_id+"</td>");
             // $('#det-fact').append("<td>$"+info.ppi_subtotal+"</td>");
             // $('#det-fact').append("<td>$"+info.ppi_descuento+"</td>");
             // $('#det-fact').append("<td>$"+info.ppi_impuestos+"</td>");
             // $('#det-fact').append("<td>$"+info.ppi_total+"</td>");

             const itemsArray = response.details

             itemsArray.forEach(function(element,key){

               var div = "<tr>";
               div = div + "<td>"+element.prod_nombre+"</td>";
               div = div + "<td>$"+element.ppi_precio_unitario+"</td>";
               div = div + "<td>"+element.ppi_cant+"</td>";
               div = div + "<td>$"+element.ppi_desc+" ";
               div = div + ""+element.dc_nombre+"</td>";
               div = div + "<td>$"+element.ppi_subt+"</td>";
               div = div + "<td>$"+element.ppi_total+"</td>";
               div = div + "</tr>";
               $('#det-prod').append(div);
             })


             //alert(JSON.stringify(response.datos))
             //$('#tr'+del).hide(2000);
           }
           else
           {
             alert(JSON.stringify(response))
           }
         }
       }).fail( function(error) {
          alert(JSON.stringify(error))
       })
    })
    $(".click-id").click(function(){
      id = $(this).attr("id");
      console.log(id);
    });

    $("#sin-confirmar").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/0')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#empaque").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/1')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#reparto").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/2')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#entregado").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/3')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

  })

  // $(document).ready(function(){
  //   $(".calldel").click(function() {
  //     var del = $(this).val();
  //
  //     var parametros = { "delete"  : true, "posicion" : del};
  //
  //      $.ajax({ data: parametros, url:' <?= base_url('Ventas/delete')?>',type:  'post',
  //        beforeSend: function () {$('#tr'+del).html("<tr><td colspan='5'>Enviando, espere por favor...</td></tr>");},
  //        success:  function (response) {
  //
  //          var response = JSON.parse(response);
  //          if (response.success==true)
  //          {
  //            $('#tr'+del).hide(2000);
  //          }
  //          else
  //          {
  //            alert(JSON.stringify(response))
  //          }
  //        }
  //      }).fail( function(error) {
  //         alert(JSON.stringify(error))
  //      })
  //   })
  // })
</script>
