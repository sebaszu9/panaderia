<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Listado de pedidos hoy </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Fecha</th>
                <th>Cantidad</th>
              </tr>
              </thead>
              <tbody>
                <?php
                  $total = 0;
                  foreach($pedidos as $key => $test) {
                ?>
                <td><?php echo $test['fecha'];?></td>
                  <td><?php echo $test['cant'];?></td>

                </tr>
                <?php
                  $total = $total + $test['cant'];
              }  ?>
              </tbody>
              <tfoot>
              <tr>
                <th>Fecha</th>
                <th>Cantidad</th>
              </tr>
              </tfoot>
            </table>

            <h1>Total <?php echo number_format($total) ?> Unidades</h1>
          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalles</h4>
      </div>
      <div class="modal-body" id="data-header">
        <table class="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Cliente</th>
              <th>Direccion</th>
              <th>Telefono</th>
              <!-- <th>Subtotal</th> -->
              <!-- <th>Descuento</th> -->
              <!-- <th>Total</th> -->
            </tr>
          </thead>
          <tbody  id="det-head">
          </tbody>
        </table>

        <table class="table">
          <thead>
            <tr>
              <th>Producto</th>
                <th>Vendaje</th>
              <th>cantidad</th>
              <th>Concepto</th>
              <!-- <th>Subtotal</th> -->
              <th>Total</th>
            </tr>
          </thead>
          <tbody  id="det-prod">
          </tbody>
        </table>

        <table class="table">
          <thead>
            <tr>
              <th>Descuento</th>
              <th>Vendaje</th>
              <th>Cambio</th>
              <th>Descuentos</th>
              <th>Total Pagar</th>
              <!-- <th>Descuento</th> -->
              <!-- <th>Total</th> -->
            </tr>
          </thead>
          <tbody  id="det-total">
          </tbody>
        </table>

        <hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<div id="myModal-state" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Cambiar de Estado el Pedido</h4>
      </div>
      <div class="modal-body" >
        <td><button class="btn btn-danger show-details" id="delete">Eliminar</button></td><br>
          <hr>
        <td><button class="btn btn-secondary show-details" id="sin-confirmar">Sin confirmar</button></td><br>
        <hr>
        <td><button class="btn btn-warning show-details" id="empaque" >Empaque</button></td><br>
        <hr>
        <td><button class="btn btn-info show-details" id="reparto">Reparto</button></td><br>
        <hr>
        <td><button class="btn btn-success show-details" id="entregado" >Entregado</button></td><br>
        <hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<script>

  $(document).ready(function(){
    var id;
    $(".show-details").click(function() {

      var del = $(this).val();
      var parametros = { "call"  : true, "posicion" : del};

       $.ajax({ data: parametros, url:' <?= base_url('Controller_pedidos/details_pedido_json/')?>'+del,type:  'post',
         beforeSend: function () {

         },
         success:  function (response) {

           var response = JSON.parse(response);

           $('#det-prod').empty();
           $('#det-total').empty();

           if (response.success==true)
           {
             $('#det-head').html("");
             // $('#det-fact').html("");
             var Vendaje = 0;
             var Cambio = 0;
             var Promocion = 0;

             const itemsArray = response.details

             itemsArray.forEach(function(element,key){
               if (element.ppi_tipo_desc==3) {
                 Cambio = parseInt(Cambio) + parseInt(element.ppi_total);
               }
               if (element.ppi_tipo_desc==2) {
                 Promocion = parseInt(Promocion) + parseInt(element.ppi_desc);
               }
               if (element.ppi_tipo_desc==3) {
                 Vendaje = parseInt(Vendaje) + parseInt(element.ppi_total);
               }
               var div = "<tr>";
               div = div + "<td>"+element.prod_nombre+"</td>";
               if(element.prod_vendaje==1){
                    div = div + "<td>SI</td>";
                    Vendaje = parseInt(Vendaje) + (0.2*parseInt(element.ppi_total));
                } else {
                    div = div + "<td>NO</td>";
                }
               div = div + "<td>"+element.ppi_cantidad+"</td>";
               div = div + "<td>"+element.dc_nombre+"</td>";
               // div = div + "<td>$"+element.ppi_desc+" ";
               // div = div + ""+element.dc_nombre+"</td>";
               // div = div + "<td>$"Cambio"</td>";
               div = div + "<td>$"+element.ppi_subtotal+"</td>";
               div = div + "</tr>";
               $('#det-prod').append(div);
             })
             console.log('Ventaje-'+Vendaje);
             console.log('Cambio-'+Cambio);
             console.log('Promocion-'+Promocion);
             var total_desc= parseInt(Vendaje+Cambio+Promocion);

             const info = response.datos[0];
             $('#det-head').append( "<td>PEDIDO:"+info.pp_id+"</td>");
             $('#det-head').append("<td>"+info.clt_nombre+"</td>");
             $('#det-head').append("<td>"+info.clt_direccion+"</td>");
             $('#det-head').append("<td>"+info.clt_telefono+"</td>");

             $('#det-total').append( "<td>$"+Promocion+"</td>");
             $('#det-total').append( "<td>$"+Vendaje+"</td>");
             $('#det-total').append( "<td>$"+Cambio+"</td>");
             $('#det-total').append( "<td>$"+total_desc+"</td>");
             $('#det-total').append( "<td>$"+info.pp_total+"</td>");
           }
           else
           {
             alert(JSON.stringify(response))
           }
         }
       }).fail( function(error) {
          alert(JSON.stringify(error))
       })
    })
    $(".click-id").click(function(){
      id = $(this).attr("id");
      console.log(id);
    });

    $("#sin-confirmar").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/0')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#empaque").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/1')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#reparto").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/2')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#entregado").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/3')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

    $("#delete").click(function(){
      console.log(id);
      url = "<?php echo site_url('Controller_pedidos/update_pedido_state/4')?>/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown){}
      });
    });

  })
</script>
