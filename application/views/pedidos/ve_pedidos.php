<!-- <?php foreach($ing as $data2) { $y = $data2->mm_id_concepto; } foreach($producto as $data1) {   $x1 = $data1->mp_id_concepto; }?> -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="row">
          <!-- Main Row -->
        <div class="col-md-12">
        <div class="box-header with-border">
          <h3 class="box-title">Detalles del Pedido</h3>
        </div>
        <div class="box-body">
        <table class="table table-bordered table-striped">
            <tbody>
              <?php foreach($pedido as $ped) {  ?>
              <tr>
                <td>ID Pedido</td>
                <td>#<?php echo $ped->pp_id;?></td>
              </tr>
              <tr>
                <td>Cliente</td>
                <td><?php echo $ped->clt_nombre;?>
                <a class="btn btn-danger cambiar_clt" >Editar</a>
              </td>
              </tr>
              <tr>
                <td>Dirección</td>
                <td><?php echo $ped->clt_direccion;?></td>
              </tr>
              <tr>
                <td>Teléfono</td>
                <td><?php echo $ped->clt_telefono;?></td>
              </tr>
              <tr>
                <td>Vendedor</td>
                <td><?php echo $ped->name;?>
                <a class="btn btn-danger cambiar_ven" >Editar</a>
              </td>
              </tr>
              <tr>
                <td>Subtotal</td>
                <td><?php echo $ped->pp_subtotal;?></td>
              </tr>

            <?php } ?>
            </tbody>
        </table>
        <br>
        <a href="<?php echo base_url('Pedidos/hoy')?>" class="btn btn-success" >Volver</a>
        </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-8">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Materias usadas en la producción</h3>
      </div>
      <div class="box-body">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Producto</th>
            <th>Concepto</th>
            <th>Cantidad</th>
            <th>Subtotal</th>
            <th>Acción</th>
          <!--  <th>Costo</th> -->
          </tr>
          </thead>
          <tbody>
          </tr>
          <?php $x=0;
          foreach($items as $it) {
            // $x=$x+$it->ppi_total;  ?>
            <tr>
              <td><?php echo $it->prod_nombre;?></td>
              <td><?php echo $it->dc_nombre;?></td>
              <td><?php echo $it->ppi_cantidad;?></td>
             <td><?php  echo "$".number_format($it->ppi_subtotal);?></td>
             <th><a id="<?php echo $it->ppi_id;?>" class="btn btn-danger delete_button" >Eliminar</a></th>
            </tr>
          <?php } ?>
          </tbody>
      </table>
      <br>
              <a id="add_button" class="btn btn-info add_prod" >Agregar producto</a>
      </div>
      <div class="box-footer with-border">
        <!-- <h3 class="box-title">Total:<?php  echo "$".number_format($x);?> </h3>
        <a id="add_button" class="btn btn-success" >Agregar producto</a> -->
        <!-- <div  class="col-md-12">
          <form role="form" action="<?php echo base_url(); ?>Controller_produccion/delete" method="POST">
            <input type="hidden" name="out" value="<?php echo $y; ?>" class="form-control"  >
            <input type="hidden" name="in" value="<?php echo $x1; ?>" class="form-control"  >
            <input type="hidden" name="x" value="1"class="form-control"  >
          </form>
        </div> -->
      </div>
  </div>
</div>
  </div>
</section>
</div>

<!-- Modal PRODUCTO EMPAQUE -->
<div class="modal fade" id="add_modal" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Agregar producto</h3>
          </div>
          <div class="modal-body form">
           <form action="#" id="form_producto" class="form-horizontal">
              <div class="form-body">

                  <div class="form-group">
                      <label class="control-label col-md-3">Productos</label>
                    <div class="col-md-9">
                    <select class="form-control select2" name="producto">
                      <?php foreach($productos as $pro) {  ?>
                        <option value="<?= $pro->prod_id;?>"> <?= $pro->prod_nombre ?></option>
                        <?php  }  ?>
                    </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Concepto</label>
                    <div class="col-md-9">
                    <select class="form-control" name="concepto">
                        <option value="1">Venta + Vendaje</option>
                        <option value="2">Venta</option>
                        <option value="3">Venta incluye vendaje</option>
                        <option value="4">Vendaje</option>
                        <option value="5">Cambio</option>
                    </select>
                    </div>
                  </div>

                  <div class="form-group">
                     <label class="control-label col-md-3">Subtotal</label>
                     <div class="col-md-9">
                         <input name="precio" class="form-control" type="number">
                         <span class="help-block"></span>
                     </div>
                  </div>

                <div class="form-group">
                   <label class="control-label col-md-3">Cantidad </label>
                   <div class="col-md-9">
                       <input name="cantidad" class="form-control" type="number">
                       <span class="help-block"></span>
                   </div>
                </div>

                <input name="pedido" class="form-control" type="hidden" value="<?php echo $ped->pp_id;?>">
            </div>
           </form>
         </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" id="save-prod"  class="btn btn-primary">Guardar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="delete_modal" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Eliminar producto del pedido</h3>
              <form action="#" id="form_del"class="form-horizontal">
              <input name="pedido" class="form-control" type="hidden" value="<?php echo $ped->pp_id;?>">

              </form>
          </div>
          <div class="modal-footer">
              <button type="button"  class="btn btn-primary confirm_delete">Eliminar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="cambiar_vendedor" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Cambiar vendedor</h3>
              <form action="#" id="form_ven"class="form-horizontal">
              <input name="pedido" class="form-control" type="hidden" value="<?php echo $ped->pp_id;?>">
              <br>
              <select class="form-control select2 " name="vendedor" style="width:200px">
                <?php foreach($vendedores as $ven) {  ?>
                  <option value="<?= $ven->id;?>"> <?= $ven->name ?></option>
                  <?php  }  ?>
              </select>

              </form>
          </div>
          <div class="modal-footer">
              <button type="button" id="save-ven" class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="cambiar_cliente" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Cambiar cliente</h3>
              <form action="#" id="form_clt"class="form-horizontal">
              <input name="pedido" class="form-control" type="hidden" value="<?php echo $ped->pp_id;?>">
              <br>
              <select class="form-control select2" name="cliente" style="width:200px">
                <?php foreach($clientes as $clt) {  ?>
                  <option value="<?= $clt->clt_id;?>"> <?= $clt->clt_nombre ?></option>
                  <?php  }  ?>
              </select>
              </form>
          </div>
          <div class="modal-footer">
              <button type="button" id="save-clt" class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<script type="text/javascript">
$(document).ready(function()
{
  var id;
///MOSTRAR form producto produccion
$("#add_button").click(function(){
  id = $(this).attr("id");
  $('#form_producto')[0].reset();
  $('#add_modal').modal('show');
});

$(".delete_button").click(function(){
  id = $(this).attr("id");
  $('#delete_modal').modal('show');
});

$(".cambiar_clt").click(function(){
  // id = $(this).attr("id");
  $('#cambiar_cliente').modal('show');
});

$(".cambiar_ven").click(function(){
  // id = $(this).attr("id");
  $('#cambiar_vendedor').modal('show');
});

 $(".confirm_delete").click(function(){
   //Ajax Load data from ajax
     $.ajax({
         url : "<?php echo site_url('Controller_pedidos/delete_item/')?>/" + id,
       type: "POST",
       data: $('#form_del').serialize(),
       dataType: "JSON",
       success: function(data)
       {
         // $('#modal_empaque').modal('hide');
         location.reload();
       },
       error: function (jqXHR, textStatus, errorThrown){}
  });
  });

  $("#save-prod").click(function(){
    $.ajax({
         url : "<?php echo site_url('Controller_pedidos/insert_item/')?>/",
         type: "POST",
         data: $('#form_producto').serialize(),
         dataType: "JSON",
         success: function(data)
         {
          $('#modal_cantidad ').modal('hide');
          location.reload();
         },
         error: function (jqXHR, exception) {},
    });
  });

  $("#save-clt").click(function(){
    $.ajax({
         url : "<?php echo site_url('Controller_pedidos/update_clt/')?>/",
         type: "POST",
         data: $('#form_clt').serialize(),
         dataType: "JSON",
         success: function(data)
         {
          $('#cambiar_cliente ').modal('hide');
          location.reload();
         },
         error: function (jqXHR, exception) {},
    });
  });

  $("#save-ven").click(function(){
    $.ajax({
         url : "<?php echo site_url('Controller_pedidos/update_ven/')?>/",
         type: "POST",
         data: $('#form_ven').serialize(),
         dataType: "JSON",
         success: function(data)
         {
          $('#cambiar_vendedor ').modal('hide');
          location.reload();
         },
         error: function (jqXHR, exception) {},
    });
  });

});
</script>
