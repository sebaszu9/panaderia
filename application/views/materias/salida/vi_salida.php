<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Salida
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Materias Primas</a></li>
      <li class="active">Listado de Salidas</li>
    </ol>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <div class="col-md-3">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Movimiento/materia/output/form')?>" class="btn  btn-primary">Nueva Salida</a>
          </div>
          <div class="box-header with-border">
            <h3 class="box-title">Detalles del Salida</h3>
          </div>

          <div class="box-body">

            <form action="<?= base_url('Movimiento/materia/output/search') ?>" method="POST" >
            <div class="form-group col-md-12">
              <label>Panaderia</label>
              <select class="form-control" name="panaderia">
                <option value="0"></option>
              <?php foreach($panaderias as $datosd) { ?>
                <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label>Concepto</label>
              <select class="form-control" name="concepto">
                <option value="0"></option>
              <?php foreach($concepto as $datos) { ?>
                <option value="<?= $datos->c_id;?>"><?= $datos->c_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Fecha inicial</label>
              <input type="text" name="fecha1"  id="datepicker" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Fecha final</label>
              <input type="text" name="fecha2"  id="datepicker2" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-12">
              <div class="form-group ">
                <label for="exampleInputEmail1"> </label>
                <button type="submit" class="btn btn-block btn-info" ><i class="fa fa-search"></i> Consultar</button>
              </div>
            </div>

          </form>
          </div>

        </div>
        </div>
        <div class="col-md-9">
        <?php if ($this->session->flashdata('search'))  { ?>

          <?php $info = $this->session->flashdata('search') ?>

          <div class="box box-primary">

            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Fecha</th>
                  <th>Sede</th>
                  <th>Usuario</th>
                  <th>Editar</th>
                  <th>Detalles</th>
                </tr>
                </thead>
                <tbody>
                </tr>
                  <?php foreach($info as $datos) { ?>
                  <tr>
                      <td><?php echo $datos->cm_id;?> </td>
                      <td><?php echo $datos->c_nombre;?> </td>
                      <td><?php echo $datos->cm_fecha;?> </td>
                      <td><?php echo $datos->bod_nombre;?> </td>
                      <td><?php echo $datos->usr_nombre;?> </td>
                      <td><a href="<?php echo base_url('Movimiento/materia/output/edit/')."/".$datos->cm_id; ?>" class="btn  btn-danger">Editar</a></td>
                      <td><a href="<?php echo base_url('Movimiento/materia/output/details/').$datos->cm_id; ?>" class="btn  btn-success">Detalles</a></td>
                  </tr>
                  <?php  }  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Proveedor</th>
                    <th>Fecha</th>
                    <th>Editar</th>
                    <th>Detalles</th>
                  </tr>
                </tfoot>
              </table>

            </div>

          </div>
        <?php } ?>

      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
