<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Formulario
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Materias</a></li>
        <li class="active">Form</li>
      </ol>
    </section>

    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box-header">

        <a class=" btn  btn-primary" href="<?php echo base_url('Materiasprimas/index');?>">Volver</a>
      </div>
      <div class="row">
        <div class="col-md-6">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Edicion de Materias Primas</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php if(isset($mensaje)):?>
            <div class="alert <?= $alert; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?= $mensaje; ?>
            </div>
    <?php endif; ?>
    <?php echo validation_errors(); ?>

    <?php foreach($datos as $test) { ?>

    <form role="form" action="<?php echo base_url('Materiasprimas/delete/'); ?><?= $test->mat_id; ?>" method="POST" enctype="multipart/form-data">
      <div class="box-body">

        <div class="form-group">
          <div class="form-group">
            <label for="exampleInputEmail1">Nombre</label>
            <input type="text" name="nombre"  readonly="true"  value="<?php echo $test->mat_nombre;?>"class="form-control" >
          </div>

          <div class="form-group">
            <label>Categoria</label>
            <select class="form-control" readonly="true" name="categoria">
            <option value="<?php  echo $test->mat_categoria;?>" selected><?php echo $test->cat_mat_nombre;?> (antiguo)</option>
            <?php foreach($categorias as $datos) { ?>
              <option value="<?= $datos->cat_mat_id;?>"><?= $datos->cat_mat_nombre." Codigo ".$datos->cat_mat_id;?></option>
            <?php  }  ?>
            </select>
          </div>

          <div class="form-group">
            <label>Descripcion</label>
            <textarea class="form-control" readonly="true" name="descripcion" rows="3" placeholder="Descipcion breve del producto ..."><?php echo $test->mat_descripcion;?></textarea>
          </div>

          <div class="form-group">
            <label>Unidad de Medicion </label>
            <select class="form-control" readonly="true" name="unidad_medida">
            <option value="<?php  echo $test->mat_um;?>" selected><?php echo $test->unidad_nombre;?> (antiguo)</option>
            <?php foreach($unidades as $datos) { ?>
              <option value="<?= $datos->unidad_id;?>"><?= $datos->unidad_nombre?> </option>
            <?php  }  ?>
            </select>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Unidad de Empaque</label>
            <input type="text" name="unidad_empaque"  readonly="true" value="<?php echo $test->mat_ue;?>"class="form-control"  placeholder="Si es un bulto de harina de 50 kilogramos el valor sera de 50.">
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Costo de la materia</label>
            <input type="number" name="costo"  readonly="true" value="<?php echo $test->mat_costo;?>"class="form-control" >
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Inventario deseado</label>
            <input type="number" name="inventario_deseado"  readonly="true" value="<?php echo $test->mat_inv_max;?>"class="form-control"  placeholder="Si es un bulto de harina de 50 kilogramos el valor sera de 50.">
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Codigo de barras</label>
            <input type="text" name="barcode"  readonly="true"  value="<?php echo $test->mat_barras;?>"class="form-control"  placeholder="Codigo de barras de la materia">
          </div>


        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-danger btn-sm">Eliminar</button>
        </div>
        </div>
        </form>
        <?php  }  ?>

      </div>
      <!-- /.box-body -->



</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
