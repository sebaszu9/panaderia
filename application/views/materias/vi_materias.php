<div class="content-wrapper">
  <!-- Main content -->
  <section class="content-header">
    <h1>
      Listado de Materias
    </h1>
  </section>
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Materiasprimas/form')?>" class="btn  btn-primary">+ Nueva</a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Categoria</th>
                <th>Tamaño Empaque</th>
                <th>Unidad de Medida</th>
                <th>Costo Empaque</th>
                <th>Costo Gramo</th>
                <th>Acción</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($info as $datos) { ?>
                <tr>
                    <td><?php echo $datos->mat_codigo;?> </td>
                    <td><?php echo $datos->mat_nombre;?> </td>
                    <td><?php echo $datos->cat_mat_nombre;?> </td>
                    <td><?php echo number_format($datos->mat_ue);?> </td>
                    <td><?php echo $datos->unidad_nombre;?> </td>
                    <td>$<?php echo number_format($datos->mat_costo);?> </td>
                    <td>$<?php echo number_format($datos->mat_costo/$datos->mat_ue , 2);?> </td>
                    <td><a href="<?php echo base_url('Materiasprimas/details')."/".$datos->mat_id; ?>" class="btn btn-success"title="Detalles"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a href="<?php echo base_url('Materiasprimas/edit')."/".$datos->mat_id; ?>" class="btn btn-warning" title="Editar" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="<?php echo base_url('Materiasprimas/get')."/".$datos->mat_id; ?>" class="btn btn-danger"title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  <th>Categoria</th>
                  <th>Descripcion</th>
                  <th>Acción</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
