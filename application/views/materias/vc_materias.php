<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formulario
      <small>Control panel</small>
    </h1>
  </section>


  <!-- Main content -->
  <section class="content">
    <div class="box-header">

      <a class=" btn  btn-primary" href="<?php echo base_url('Materiasprimas/index');?>"></i> Volver</a>
    </div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Crear nueva Materia Prima</h3>
          </div>

    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="<?php echo base_url(); ?>Materiasprimas/create" method="POST">
      <?php if(isset($mensaje)):?>
              <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
              </div>
      <?php endif; ?>
      <?php echo validation_errors(); ?>

      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Código Materia</label>
          <input type="text" name="id"  class="form-control" value="<?php echo set_value('id'); ?>" placeholder="Ingrese el codigo con el que va a identificar la Categoria">
        </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Nombre</label>
        <input type="text" name="nombre"   value="<?php echo set_value('nombre'); ?>"class="form-control"  placeholder="Ingrese el nombre de la Materia Prima">
      </div>

      <div class="form-group">
        <label>Categoria</label>
        <select class="form-control" name="categoria">
          <?php foreach($categorias as $datos) { ?>
            <option value="<?= $datos->cat_mat_id;?>"><?= $datos->cat_mat_nombre." Codigo ".$datos->cat_mat_id;?></option>
            <?php  }  ?>
          </select>
        </div>

        <div class="form-group">
          <label>Inventariable</label>
          <select class="form-control" name="inventario">
              <option value="1">Si</option>
              <option value="0">No</option>
          </select>
        </div>

        <div class="form-group">
          <label>Tipo</label>
          <select class="form-control" name="tipo">
              <option value="0">Materia prima</option>
              <option value="1">Ingrediente compuesto</option>
          </select>
        </div>

        <div class="form-group">
          <label>Descripcion</label>
          <textarea class="form-control" name="descripcion" rows="3" placeholder="Descipcion breve del producto ..."><?php echo set_value('desscripcion'); ?></textarea>
        </div>

        <div class="form-group">
          <label>Precio</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            <input type="number" name="costo" class="form-control" value="<?php echo set_value('costo'); ?>">
          </div>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Codigo de barras</label>
          <input type="text" name="barcode"   value="<?php echo set_value('barcode'); ?>"class="form-control"  placeholder="Codigo de barras de la materia">
        </div>

        <div class="form-group">
          <label>Unidad de Medicion (Unidad usada en las formulas y la producción)</label>
          <select class="form-control" name="unidad_medida">
          <?php foreach($unidades as $datos) { ?>
            <option value="<?= $datos->unidad_id;?>"><?= $datos->unidad_nombre?> </option>
          <?php  }  ?>
          </select>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Unidad de Empaque</label>
          <input type="text" name="unidad_empaque"   value="<?php echo set_value('unidad_empaque'); ?>"class="form-control"  placeholder="Si es un bulto de harina de 50 kilogramos el valor sera de 50.">
        </div>

        <div class="form-group">
          <label>Mostrar empaques en la fórmula</label>
          <select class="form-control" name="un">
              <option value="0">No</option>
              <option value="1">Si</option>
          </select>
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Inventario deseado</label>
          <input type="number" name="inventario_deseado"   value="<?php echo set_value('unidad_empaque'); ?>"class="form-control"  placeholder="Cuanto almacenas en promedio.">
        </div>



        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Guardar</button>
        </div>
        </div>
      </div>
      <!-- /.box-body -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
