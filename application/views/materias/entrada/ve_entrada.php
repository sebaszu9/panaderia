<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detalles
    </h1>
  </section>



  <!-- Main content -->
  <section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <?php foreach($entrada as $datosd) { ?>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Editar entrada</h3>
          </div>

          <div class="box-body">

          <form action="<?= base_url('Movimiento/materia/input/update') ?>" method="POST" >
            <input type="hidden" name="concepto" value="<?= $datosd->cm_id;?>">
            <div class="form-group col-md-3">
              <label>Panaderia</label>
              <select class="form-control" name="panaderia" readonly="true">
                <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
              </select>
            </div>
            <? $panaderia = $datosd->bod_id; ?>

            <div class="form-group col-md-3">
              <label>Proveedor</label>
              <select class="form-control " name="proveedor">
                <option value="<?= $datosd->prov_id;?>"><?= $datosd->prov_nombre;?></option>
                <?php foreach($proveedor as $proved) { ?>
                  <option value="<?= $proved->prov_id;?>"><?= $proved->prov_nombre;?></option>
                <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-3">
              <label for="exampleInputEmail1">Fecha</label>
              <input type="text" name="fecha"  id="datepicker" value="<?= $datosd->cm_fecha;?>" class="form-control">
            </div>

            <div class="form-group col-md-6">
              <div class="form-group ">
                <label for="exampleInputEmail1">Observaciones</label>
                <input type="text" name="descripcion"  id="datepicker" v class="form-control" value="<?= $datosd->cm_obs;?>">
              </div>
            </div>

            <div class="form-group col-md-2">
              <div class="form-group ">
                <label for="exampleInputEmail1">.</label>
                <button type="submit" class="btn btn-block btn-default"><i class="fa fa-check-circle"></i> Actualizar</button>
              </div>
            </div>
            </form>
          </div>

        </div>

        <?php  }  ?>

        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detalles del Pedido</h3>
          </div>
          <div class="box-body">

            <?php if ($this->session->flashdata('mensaje_item'))  { ?>
              <div class="alert alert-success alert-dismissible" id="hidemiss">
                <p><?= $this->session->flashdata('mensaje_item'); ?></p>
              </div>
              <script>$( "#hidemiss" ).fadeOut( 3600, "linear" );</script>
            <?php } ?>

            <div class="modal-body col-md-12">

              <div class="col-md-12">

                <table class="table table-bordered order-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Producto</th>
                      <th>Cant empaques.</th>
                      <th>Costo</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody id="loadcarttable">
                    <?php
                          $i = 1;
                        foreach ($materias as $value) {
                          echo "<tr>";
                          echo "<td> ".$i." </td>";
                          echo "<td> ".$value->mat_nombre." </td>";
                          echo "<td> ".$value->mm_cant." </td>";
                          echo "<td> ".number_format($value->mm_costo)." </td>";
                          echo '<td><button type="button" class="btn btn-warning editar" data-toggle="modal" data-target="#myModal" data-id="'.$value->mm_id.'"  data-mat="'.$value->mat_id.'" data-cant="'.$value->mm_cant.'"  data-precio="'.$value->mm_costo.'" data-name="'.$value->mat_nombre.'"><i class="fa fa-pencil-square-o"></i></button> ';
                          echo ' <button type="button" class="btn btn-danger eliminar" data-toggle="modal-delete" data-target="#myModal" id="'.$value->mm_id.'"  data-mat="'.$value->mat_id.'" data-cant="'.$value->mm_cant.'"  data-precio="'.$value->mm_costo.'" data-name="'.$value->mat_nombre.'"><i class="fa fa-trash-o"></i></button></td>';
                          echo "<tr>";
                          $i++;
                        }
                    ?>
                  </tbody>
                </table>
              </div>

            </div>



          </div>
        </div>

      </div>
    </div>
      <!-- /.box-body -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal_delete" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">ELIMINAR</h3>
      </div>
      <div class="modal-footer">
        <button type="button" id="delete"  class="btn btn-primary">Confirmar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
  $(".editar").click(function() {
    var cant   = $(this).data("cant");
    var precio = $(this).data("precio");
    var id     = $(this).data("id");
    var name     = $(this).data("name");
    var materia     = $(this).data("mat");
    $("#input-id").val(id);
    $("#input-precio").val(precio);
    $("#input-cant").val(cant);
    $("#input-cantv").val(cant);
    $("#input-name").val(name);
    $("#input-materia").val(materia);
  });
  $(".eliminar").click(function() {
    var id = $(this).attr("id");
    url = "<?php echo site_url('Controller_mov_mat/delete_mov_in')?>/"+id;
    $('#modal_delete').modal('show');
  });

  $("#delete").click(function(){
  $.ajax({
      url : url,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        location.reload();
      },
      error: function (jqXHR, textStatus, errorThrown){}
    });
  });
</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar item de entrada</h4>
      </div>
      <form action="<?= base_url('Movimiento/materia/input/product/update')?>" method="POST" />
      <div class="modal-body">
        <div class="form-group">
          <label for="usr">Name:</label>
          <input type="hidden" class="form-control" name="concepto" value="<?= $this->uri->segment(5);?>">
          <input type="hidden" class="form-control" id="input-id" name="id" readonly="true">
          <input type="hidden" class="form-control" id="input-materia" name="materia" readonly="true">
          <input type="hidden" class="form-control" name="panaderia" value="<?php $panaderia; ?>" readonly="true">
          <input type="hidden" class="form-control" name="cantdold" id="input-cantv">
          <input type="text" class="form-control" id="input-name" readonly="true">
        </div>
        <div class="form-group">
          <label for="usr">Cantidad:</label>
          <input type="text" class="form-control" name="cantd" id="input-cant">
        </div>
        <div class="form-group">
          <label for="usr">Precio:</label>
          <input type="text" class="form-control" name="price" id="input-precio">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info"><i class="fa fa-floppy-o"></i> Guardar</button>
      </div>
    </form>
    </div>

  </div>
</div>
