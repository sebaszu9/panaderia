<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detalles Materia
      </h1>
    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box-header">

        <a class=" btn  btn-primary" href="<?php echo base_url('Materiasprimas/index');?>">Volver</a>
      </div>
      <div class="row">
        <div class="col-md-6">
    <?php foreach($datos as $test) { ?>
      <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detalles del la Materia</h3>
          </div>
              <!-- form start -->
                <div class="box-body">
                  <table id="table-alla-order" class="table table-bordered table-striped">
                    <tbody>
                      <tr>
                        <th>ID</th>
                        <td><?php echo $test->mat_codigo;?></td>
                      </tr>
                      <tr>
                        <th>Nombre</th>
                        <td><?php echo $test->mat_nombre;?></td>
                      </tr>
                      <tr>
                        <th>Departamento</th>
                        <td>
                          <?php echo $test->mat_categoria;?>
                        </td>
                      </tr>
                      <tr>
                        <th>Codigo de Barras</th>
                        <td>
                          <?php echo $test->mat_barras;?>
                        </td>
                      </tr>
                      <tr>
                        <th>Unidad de Medida</th>
                        <td>
                          <?php echo $test->unidad_nombre;?>s
                        </td>
                      </tr>
                      <tr>
                        <th>Costo por empaque</th>
                        <td>
                          $<?php echo $test->mat_costo;?>
                        </td>
                      </tr>
                      <tr>
                        <th>Unidad de Empacado</th>
                        <td>
                          <?php echo $test->mat_ue;?> <?php echo $test->unidad_nombre;?>s
                        </td>
                      </tr>
                      <tr>
                        <th>Precio/<?php echo ($test->unidad_nombre);?></th>
                        <td>
                          $<?php echo number_format($test->mat_costo/$test->mat_ue);?>
                        </td>
                      </tr>
                      <tr>
                        <th>Inventario Máx</th>
                        <td>
                          <?php echo $test->mat_inv_max;?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
        </div>
      <?php  }  ?>
        </div>
      <!-- /.box-body -->
      </div>


      <div class="col-md-6">
        <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">No inventario</h3>
        </div>
      </div>
      <!-- /.box -->
      </div>



</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
