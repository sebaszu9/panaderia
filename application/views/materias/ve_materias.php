<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Formulario mp
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Materias</a></li>
        <li class="active">Form</li>
      </ol>
    </section>

    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->

      <div class="row">
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">

            <div class="box-header">
              <a class=" btn  btn-primary" href="<?php echo base_url('Materiasprimas/index');?>"> Volver</a>
            </div>

            <div class="box-header with-border">
              <h3 class="box-title">Edicion de Materias Primas</h3>
              <?php if(isset($mensaje)):?>
                <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
                </div>
              <?php endif; ?>
              <?php echo validation_errors(); ?>
            </div>


    <?php foreach($datos as $test) { ?>

      <form role="form" action="<?php echo base_url('Materiasprimas/update/'); ?><?= $test->mat_id; ?>" method="POST" enctype="multipart/form-data">
      <div class="box-body">

        <div class="form-group">
          <label for="exampleInputEmail1">Código Materia</label>
          <input type="hidden" name="id" value="<?php echo $test->mat_id;?>" class="form-control">
          <input type="text"  name="code"  value="<?php echo $test->mat_codigo;?>" class="form-control" value="<?php echo set_value('code'); ?>" placeholder="Ingrese el codigo con el que va a identificar la Categoria">
        </div>

        <div class="form-group">
          <div class="form-group">
            <label for="exampleInputEmail1">Nombre</label>
            <input type="text" name="nombre"   value="<?php echo $test->mat_nombre;?>" class="form-control"  placeholder="Ingrese el nombre de la Materia Prima">
          </div>
        </div>

          <div class="form-group">
            <label>Categoria</label>
            <select class="form-control" name="categoria">
            <option value="<?php  echo $test->mat_categoria;?>" selected><?php echo $test->cat_mat_nombre;?> (antiguo)</option>
            <?php foreach($categorias as $datos) { ?>
              <option value="<?= $datos->cat_mat_id;?>"><?= $datos->cat_mat_nombre." Codigo ".$datos->cat_mat_id;?></option>
            <?php  }  ?>
            </select>
          </div>

          <div class="form-group">
            <label>Inventariable</label>
            <select class="form-control" name="inventario">
                <option value="<?= $test->mat_inv;?>">
                  <?php if ($test->mat_inv == 1) { echo "Si(Actual)"; } else { echo "No(Actual)"; } ?>
                </option>
                <option value="1">Si</option>
                <option value="0">No</option>
            </select>
          </div>

          <div class="form-group">
            <label>Tipo</label>
            <select class="form-control" name="tipo">
                <option value="<?= $test->mat_tipo;?>">
                  <?php $tipomat = $test->mat_tipo; if ($test->mat_tipo == 1) { echo "Ingrediente compuesto"; } else { echo "Materia prima"; } ?>
                </option>
                <option value="0">Materia prima</option>
                <option value="1">Ingrediente compuesto</option>
            </select>
          </div>

          <div class="form-group">
            <label>Descripcion</label>
            <textarea class="form-control" name="descripcion" rows="3" placeholder="Descipcion breve del producto ..."><?php echo $test->mat_descripcion;?></textarea>
          </div>

          <div class="form-group">
            <label>Precio</label>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
              <input type="number" name="costo" class="form-control" value="<?php echo $test->mat_costo;?>">
            </div>
          </div>

          <div class="form-group">
            <label>Unidad de Medicion (Unidad usada en los Empaques de compra)</label>
            <select class="form-control" name="unidad_medida">
            <option value="<?php echo $test->mat_um;?>" selected><?php echo $test->unidad_nombre;?> (antiguo)</option>
            <?php foreach($unidades as $datos) { ?>
              <option value="<?= $datos->unidad_id;?>"><?= $datos->unidad_nombre?> </option>
            <?php  }  ?>
            </select>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Unidad de Empaque</label>
            <input type="text" name="unidad_empaque"   value="<?php echo $test->mat_ue;?>" class="form-control"  placeholder="Si es un bulto de harina de 50 kilogramos el valor sera de 50.">
          </div>

          <div class="form-group">
            <label>Mostrar empaques en la fórmula</label>
            <select class="form-control" name="un">
              <option value="<?= $test->mat_un;?>">
                <?php if ($test->mat_un == 1) { echo "Si(Actual)"; }else { echo "No(Actual)"; } ?>
              </option>
              <option value="1">Si</option>
              <option value="0">No</option>
            </select>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Inventario deseado</label>
            <input type="number" name="inventario_deseado"   value="<?php echo $test->mat_inv_max;?>" class="form-control"  placeholder="Si es un bulto de harina de 50 kilogramos el valor sera de 50.">
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Codigo de barras</label>
            <input type="text" name="barcode"   value="<?php echo $test->mat_barras;?>" class="form-control"  placeholder="Codigo de barras de la materia">
          </div>





      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
      </div>

      </form>
      <?php  }  ?>

    </div>

  </div>


  <!-- general form elements -->
  <?php if ($tipomat==1): ?>
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3>Ingredientes del compuesto</h3>
        </div>
        <div class="box-body">

          <form id="save-ingd">

            <div class="form-group">
              <input type="hidden" readonly="" name="id" value="<?php echo $this->uri->segment(3); ?>" class="form-control" placeholder="Ingrese el codigo con el que va a identificar la Categoria">
              <input type="hidden" name="safety" value="true">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Ingrediente</label>
              <select class="form-control" name="ingrediente">
                <?php foreach($materias as $mat) { ?>
                  <option value="<?php echo $mat->mat_id; ?>"><?php echo $mat->mat_nombre; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Cantidad (gr.)</label>
              <input type="text"name="cantd" class="form-control">
            </div>

            <div class="form-group" id="resadd">
              <button type="submit" class="btn btn-success">Agregar</button>
            </div>

          </form>

        </div>

        <div class="box-footer with-border">
          <table class="table table-bordered">
            <thead>
              <td>Materia</td>
              <td>Cantidad</td>
            </thead>
            <tbody>
              <?php
              $acumulador = 0;
              foreach ($ingrediente as $value) { ?>
                <tr id="tr<?php echo $value->inc_id; ?>">
                  <td><?php echo $value->mat_nombre; ?></td>
                  <td><?php echo $value->inc_cantidad/$value->unidad_escala; ?> <?php echo $value->unidad_nombre; ?>s</td>
                  <td>
                    <?php
                      $costo =(($value->inc_cantidad/$value->unidad_escala)*($value->mat_costo)/($value->mat_ue));
                      $acumulador = $acumulador + $costo;
                      //$peso = $peso + $value->inc_cantidad/$value->unidad_escala;
                      echo  "$".number_format($costo);
                    ?>
                 </td>
                  <td id="td<?php echo $value->inc_id; ?>"><button class="btn btn-default calldel" value="<?php echo $value->inc_id; ?>">Eliminar</button></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <h1>Precio: <?php echo number_format($acumulador); ?></h1>
        </div>


      </div>

    </div>
  <?php endif; ?>







  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>

$( "#save-ingd" ).on( "submit", function( event ) {
  //alert('llamando')
  event.preventDefault();
  console.log( $( this ).serialize() );
  $.ajax({ data:  $( this ).serialize(), url:' <?= base_url('Controller_materias/sava_ingrediente_mat')?>',type:  'post',
     beforeSend: function () {$("#resadd").html("Enviando, espere por favor...");},
     success:  function (response) {
       var res = JSON.parse(response)
       if (res.success==true) {
         alert(res.message)
         location.reload();
       }
       else{  alert(res.message) }
     }
  }).fail( function(error) { $("#resadd").html(JSON.stringify(error)); })

});

$(document).on('click', '.calldel', function(){
//  alert($(this).val())
  var postn = $(this).val();
  var parametros = { "delete"  : true, "posicion" : postn };
   $.ajax({ data: parametros, url:' <?= base_url('Controller_materias/delete_ingcomp')?>',type:  'post',
     beforeSend: function () {$("#td"+postn).html("<tr><td colspan='5'>Enviando, espere por favor...</td></tr>");},
     success:  function (response) {
       var res = JSON.parse(response)
       if (res.success==true) {
         //alert(res.message)
         //location.reload();

         $("#tr"+postn).hide();
       }
       else{  alert(res.message) }
     }
   }).fail( function(error) {
      alert(JSON.stringify(error))
   })
});

</script>
