<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado Departamentos Materias Primas
    </h1>
    </ol>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Categoriasmat/form')?>" class="btn  btn-primary">Nuevo +</a>

          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Acción</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($info as $datos) { ?>
                <tr>
                    <td><?php echo $datos->cat_mat_code;?> </td>
                    <td><?php echo $datos->cat_mat_nombre;?> </td>
                    <td><?php echo $datos->cat_mat_dsc;?> </td>
                    <td><a href="<?php echo base_url('Categoriasmat/edit')."/".$datos->cat_mat_id; ?>" class="btn  btn-warning" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="<?php echo base_url('Categoriasmat/get')."/".$datos->cat_mat_id; ?>" class="btn  btn-danger"   title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Acción</th>
                </tr>
              </tfoot>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
