<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Eliminar
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Materias</a></li>
      <li><a href="#">Departamentos</a></li>
      <li class="active">Eliminar</li>
    </ol>
    <a class=" btn  btn-primary" href="<?php echo base_url('Categoriasmat/index');?>">Volver</a>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <!-- Main Row -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Eliminar Departamento</h3>
            </div>

    <?php foreach($datos as $test) { ?>
  <!-- /.box-header -->
  <div class="box-body">
    <form role="form" action="<?= base_url()?>Categoriasmat/delete/<?= $test->cat_mat_id; ?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="exampleInputEmail1">Código  del Departamento</label>
        <input type="text" class="form-control" readonly="true" name="titulo" placeholder="No tiene código" value="<?php echo $test->cat_mat_code;?>">
      </div>

      <div class="form-group">
          <label for="exampleInputEmail1">Nombre del Departamento</label>
          <input type="text" class="form-control" readonly="true" name="titulo" placeholder="Escribe el nombre de su producto" value="<?php echo $test->cat_mat_nombre;?>">
      </div>

      <div class="form-group">
          <label for="exampleInputEmail1">Descripción del Departamento</label>
          <input type="text" class="form-control" readonly="true" name="titulo" value="<?php echo $test->cat_mat_dsc;?>">
      </div>

<input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
    </form>
  </div>
  <!-- /.box-body -->
    <?php  }  ?>
</div>
<!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
