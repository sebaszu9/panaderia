<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Crear
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Materias</a></li>
      <li>Departamentos</li>
      <li class="active">Crear</li>
    </ol>
  </section>


  <!-- Main content -->
  <section class="content">
    <div class="box-header">
      <a class=" btn  btn-primary" href="<?php echo base_url('Categoriasmat/index');?>">Volver</a>
    </div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Crear Departamento Materias</h3>
          </div>

    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="<?php echo base_url(); ?>Categoriasmat/create" method="POST">
      <?php if(isset($mensaje)):?>
              <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
              </div>
      <?php endif; ?>
      <?php echo validation_errors(); ?>

      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Código Categoria</label>
          <input type="text" name="id"  class="form-control" value="<?php echo set_value('id'); ?>" placeholder="Ingrese el codigo con el que va a identificar la Categoria">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Nombre de la Categoria</label>
          <input type="text" name="nombre" value="<?php echo set_value('nombre'); ?>" class="form-control"  placeholder="Ingrese el nombre de la Categoria">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Descripcion de la categoria</label>
          <textarea name="descripcion" class="form-control" rows="3" placeholder="Descripcion ..."><?php echo set_value('descripcion'); ?></textarea>
        </div>

        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Guardar</button>
        </div>
        </div>
      </div>
      <!-- /.box-body -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
