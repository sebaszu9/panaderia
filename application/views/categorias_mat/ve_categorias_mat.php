<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Editar
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Materias</a></li>
        <li><a href="#">Departamentos</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box-header">

        <a class=" btn  btn-primary" href="<?php echo base_url('Categoriasmat/index');?>">Volver</a>
      </div>
      <div class="row">
        <div class="col-md-6">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Editar Departamento Materias</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php foreach($datos as $test) { ?>

    <form role="form" action="<?php echo base_url('Categoriasmat/update/'); ?><?= $test->cat_mat_id; ?>" method="POST" enctype="multipart/form-data">
      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Código del Departamento</label>
          <input type="hidden" name="id" class="form-control"  value="<?php echo $test->cat_mat_id; ?>">
          <input type="text" name="code" class="form-control"  value="<?php echo $test->cat_mat_code; ?>">

          <label for="exampleInputEmail1">Nombre del Departamento</label>
          <input type="text" name="nombre"  class="form-control"  value="<?php echo $test->cat_mat_nombre; ?>">

          <label for="exampleInputEmail1">Descripcion del Departamento</label>
          <input type="text" name="descripcion"  class="form-control"  value="<?php echo $test->cat_mat_dsc; ?>">

          <div class="form-group">
            <input type="hidden" name="cat_id"  class="form-control" value="<?php echo $test->cat_mat_id; ?>">
          </div>
        </div>



        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
        </div>
        </div>
        </form>
        <?php  }  ?>

      </div>
      <!-- /.box-body -->



</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
