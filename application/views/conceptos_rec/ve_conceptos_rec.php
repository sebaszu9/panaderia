<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Formulario de edición de Conceptos de recetas
      </h1>
    </section>

    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box-header">

        <a class=" btn  btn-primary" href="<?php echo base_url('Concept_ing/index');?>"></i>Volver</a>
      </div>
      <div class="row">
        <div class="col-md-6">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Editar Concepto</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php foreach($datos as $test) { ?>

    <form role="form" action="<?php echo base_url('Concept_ing/update/'); ?><?= $test->cr_id; ?>" method="POST" enctype="multipart/form-data">
      <div class="box-body">


        <div class="form-group">

          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" name="nombre" required class="form-control"  value="<?php echo $test->cr_nombre; ?>">

          <label for="exampleInputEmail1">Descripcion</label>
          <input type="text" name="porcentaje" required class="form-control"  value="<?php echo $test->cr_descripcion; ?>">

          <div class="form-group">
            <input type="hidden" name="id" required class="form-control" value="<?php echo $test->cr_id; ?>">
          </div>
        </div>



        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
        </div>
        </div>
        </form>
        <?php  }  ?>

      </div>
      <!-- /.box-body -->



</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
