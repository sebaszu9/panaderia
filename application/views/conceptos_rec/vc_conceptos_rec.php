<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formulario de Conceptos Ingredientes
    </h1>
  </section>


  <!-- Main content -->
  <section class="content">
    <div class="box-header">

      <a class=" btn  btn-primary" href="<?php echo base_url('Concept_ing/index');?>">Volver</a>
    </div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Crear nuevo concepto</h3>
          </div>

    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="<?php echo base_url(); ?>Concept_ing/create" method="POST">
      <?php if(isset($mensaje)):?>
              <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
              </div>
      <?php endif; ?>
      <?php echo validation_errors(); ?>

      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" name="nombre"  class="form-control"  placeholder="Ingrese el concepto">
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Descripción</label>
          <input type="text" name="descripcion"  class="form-control"  placeholder="Descripcion del concepto">
        </div>

        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Guardar</button>
        </div>
        </div>
      </div>
      <!-- /.box-body -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
