<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Eliminar concepto
    </h1>
    <a class=" btn  btn-primary" href="<?php echo base_url('Concept_ing/index');?>">Volver</a>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <!-- Main Row -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">

            <div class="box-header with-border">
              <h3 class="box-title">Detalles del impuesto a  a eliminar.</h3>
            </div>

    <?php foreach($datos as $test) { ?>
  <!-- /.box-header -->
  <div class="box-body">
    <form role="form" action="<?= base_url()?>Concept_ing/delete/<?= $test->cr_id; ?>" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="exampleInputEmail1">Nombre</label>
        <input type="text" class="form-control" readonly="true" name="titulo"  value="<?php echo $test->cr_nombre;?>">
      </div>

      <div class="form-group">
          <label for="exampleInputEmail1">Descripcion</label>
          <input type="text" class="form-control" readonly="true" name="titulo"  value="<?php echo $test->cr_descripcion;?>">
      </div>

<input type="submit" class="btn btn-danger btn-sm" value="Confirmar Eliminar">
    </form>
  </div>
  <!-- /.box-body -->
    <?php  }  ?>
</div>
<!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
