<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
         Conceptos de Ingredientes
          </h1>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Concept_ing/form')?>" class="btn  btn-primary">Nuevo +</a>

          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Descripcion</th>
                </tr>
                </thead>
                <tbody>
                </tr>
                  <?php foreach($info as $datos) { ?>
                  <tr>

                      <td><?php echo $datos->cr_nombre;?> </td>
                      <td><?php echo $datos->cr_descripcion;?> </td>
                      <td><a href="<?php echo base_url('Concept_ing/edit')."/".$datos->cr_id; ?>" class="btn  btn-warning"title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                          <a href="<?php echo base_url('Concept_ing/get')."/".$datos->cr_id; ?>" class="btn  btn-danger"title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                  </tr>
                  <?php  }  ?>
                </tbody>

            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
