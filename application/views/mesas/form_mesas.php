<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Formulario de creación de mesas
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Mesas</a></li>
			<li><a href="#">Zonas</a></li>
			<li class="active">Mesas</li>
		</ol>
	</section>


	<!-- Main content -->
	<section class="content">
		<div class="box-header">

			<a class=" btn  btn-primary" href="<?php echo base_url('Mesas/mesas/form');?>">Volver</a>
		</div>
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-6">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Crear nueva Mesa</h3>
					</div>

					<form role="form" action="<?php echo base_url(); ?>Mesas/create_mesas" method="POST">
						<?php if(isset($mensaje)):?>
						<div class="alert <?= $alert; ?> alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<?= $mensaje; ?>
						</div>
						<?php endif; ?>
						<?php echo validation_errors(); ?>

						<div class="box-body">
							<div class="form-group">
								<label for="exampleInputEmail1">Código</label>
								<input type="text" name="codigo" value="<?php echo set_value('codigo'); ?>" class="form-control" placeholder="Ingrese el nombre de la mesa">
							</div>

							<div class="input-group mb-3">
              <div class="input-group-append">
									<label class="input-group-text" for="inputGroupSelect02">Zona</label>
								</div>

								<select required class="form-control" name="zona" >
                 <option value="" selected="selected">Seleccione una Zona</option>

                	<?php foreach ($nombre_zona as $value) { ?>

							   	<option value="<?php echo $value->zona_id; ?>"><?php echo $value->zona_nombre; ?></option>

               		<?php } ?>
                </select>


							</div>

							<div class="form-group">
								<label for="exampleInputEmail1">Descripción</label>
								<input type="text" name="descripcion" value="<?php echo set_value('descripcion'); ?>" class="form-control"
								 placeholder="Ingrese la descripción">
							</div>

							<div class="box-footer">
								<button type="submit" name="enviar" value="Create" class="btn btn-primary">Guardar</button>
							</div>
						</div>
				</div>
				<!-- /.box-body -->


	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
