<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formulario de edición de mesas
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Mesas</a></li>
      <li><a href="#">Mesas</a></li>
      <li class="active">Mesas</li>
    </ol>
  </section>


  <!-- Main content -->
  <section class="content">
    <div class="box-header">

      <a class=" btn  btn-primary" href="<?php echo base_url('Mesas/mesas/form');?>">Volver</a>

    </div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Editar Mesa</h3>
          </div>

    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="<?php echo base_url(); ?>Mesas/update_mesas" method="POST">
      <?php if(isset($mensaje)):?>
              <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
              </div>
      <?php endif; ?>
      <?php echo validation_errors(); ?>



    <?php foreach ($data_mesas as $value) {  ?>
      <div class="box-body">

        <div class="form-group">
          <label for="exampleInputEmail1"></label>
          <input type="hidden" name="id"   value=<?php echo $value->mesas_id; ?> class="form-control" >
        </div>



        <div class="form-group">
          <label for="exampleInputEmail1">Código</label>
          <input type="text" name="codigo"   value=<?php echo $value->mesas_codigo; ?> class="form-control" required>
        </div>


        <div class="form-group">
            <label>Zona</label>

            <?php
            $lista = array();
            foreach ($nombre_zona as $zona) {
            $lista[$zona->zona_id]=$zona->zona_nombre;

            }
            echo form_dropdown("zona",$lista,$value->mesas_zona,"class='form-control'");
            ?>
        </div>


        <div class="form-group">
          <label for="exampleInputEmail1">Descripción</label>
          <input type="text" name="descripcion" value="<?php echo $value->mesas_descripcion; ?>" class="form-control" >
        </div>

        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
        </div>
        </div>
      </div>
      <!-- /.box-body -->
        <?php } ?>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
