<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
         Mesas
          </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Mesas</a></li>
      <li><a href="#">Mesas</a></li>
      <li class="active">Zonas</li>
    </ol>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Mesas/zonas_form')?>" class="btn  btn-primary">Nuevo +</a>

          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Descripción</th>
                  
                </tr>
                </thead>
                <tbody>
                </tr>
                  <?php foreach($zonas as $datos) { ?>
                  <tr>

                      <td><?php echo $datos->zona_nombre;?> </td>
                      <td><?php echo $datos->zona_descripcion;?> </td>
                      <td><a href="<?php echo base_url('Controller_mesas/edit_zonas')."/".$datos->zona_id; ?>" class="btn  btn-warning"title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                          <a href="<?php echo base_url("Controller_mesas/remove_zonas")."/".$datos->zona_id; ?>" class="btn  btn-danger"title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                  </tr>
                  <?php  }  ?>
                </tbody>

            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
