<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formulario de edición de zonas
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Mesas</a></li>
      <li><a href="#">Mesas</a></li>
      <li class="active">Zonas</li>
    </ol>
  </section>


  <!-- Main content -->
  <section class="content">
    <div class="box-header">

      <a class=" btn  btn-primary" href="<?php echo base_url('Mesas/zonas/form');?>">Volver</a>

    </div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Editar Zona</h3>
          </div>

    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="<?php echo base_url(); ?>Mesas/update_zonas" method="POST">
      <?php if(isset($mensaje)):?>
              <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
              </div>
      <?php endif; ?>
      <?php echo validation_errors(); ?>

    <?php foreach ($data_zona as $value) {  ?>
      <div class="box-body">

        <div class="form-group">
          <label for="exampleInputEmail1"></label>
          <input type="hidden" name="id"  value=<?php echo $value->zona_id; ?> class="form-control" >
        </div>



        <div class="form-group">
          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" name="nombre" required  value="<?php echo $value->zona_nombre; ?>" class="form-control" >
        </div>





        <div class="form-group">
          <label for="exampleInputEmail1">Descripción</label>
          <input type="text" name="descripcion" value="<?php echo $value->zona_descripcion; ?>" class="form-control" >
        </div>

        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
        </div>
        </div>
      </div>
      <!-- /.box-body -->
        <?php } ?>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
