<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado de Producción
    </h1>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-3">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detalles del Producción</h3>
          </div>

          <div class="box-body">

            <form action="<?= base_url('Movimiento/materia/output/search') ?>" method="POST" >
            <div class="form-group col-md-12">
              <label>Panadería</label>
              <select class="form-control" name="panaderia">
                <option value="0"></option>
              <?php foreach($panaderias as $datosd) { ?>
                <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label>Panaderos</label>
              <select class="form-control" name="concepto">
                <option value="0"></option>
              <?php foreach($concepto as $datos) { ?>
                <option value="<?= $datos->c_id;?>"><?= $datos->c_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Fecha</label>
              <input type="text" name="fecha1"  id="datepicker" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-12">
              <div class="form-group ">
                <label for="exampleInputEmail1"> </label>
                <button type="submit" class="btn btn-block btn-info" ><i class="fa fa-search"></i> Consultar</button>
              </div>
            </div>
          </form>
          </div>

        </div>
        </div>

      <div class="col-md-9">
        <!-- general form elements -->
        <div class="box box-primary">

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Código</th>
                <th>Categoria</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Costo</th>
                <th>Fecha</th>
                <th>Ubicación</th>
                <th>Panadero</th>
                <th>Detalles</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($productos as $datos) { ?>
                <tr>
                    <td><?php echo $datos->prod_id;?> </td>
                    <td><?php echo $datos->cat_prod_nombre;?> </td>
                    <td><?php echo $datos->prod_nombre;?> </td>
                    <td><?php echo $datos->prod_precio;?> </td>
                    <td><?php echo $datos->mp_cant;?> </td>
                    <td><?php echo $datos->mp_costo;?> </td>
                    <td><?php echo $datos->cm_fecha;?> </td>
                    <td><?php echo $datos->bod_nombre;?> </td>
                    <td><?php echo $datos->usr_nombre;?> </td>
                    <td><a href="<?php echo base_url('Produccion/details_admin')."/".intval($datos->cm_id)."/".intval($datos->cm_id-1); ?>" class="btn btn-success" >Detalles</a></td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Código</th>
                  <th>Categoria</th>
                  <th>Nombre</th>
                  <th>Precio</th>
                  <th>Ubicación</th>
                  <th>Panadero</th>
                  <th>Detalles</th>
                </tr>
              </tfoot>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
