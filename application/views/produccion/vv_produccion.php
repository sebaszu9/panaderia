<?php foreach($ing as $data2) { $y = $data2->mm_id_concepto; } foreach($producto as $data1) {   $x1 = $data1->mp_id_concepto; }?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="row">
          <!-- Main Row -->
        <div class="col-md-12">
        <div class="box-header with-border">
          <h3 class="box-title">Detalles del la Producción</h3>
        </div>
        <div class="box-body">
        <table class="table table-bordered table-striped">
            <tbody>
              <?php foreach($produccion as $prod) {  ?>
              <tr>
                <td>Ubicación</td>
                <td><?php echo $prod->bod_nombre;?></td>
              </tr>
              <tr>
                <td>Fecha</td>
                <td><?php echo $prod->cm_fecha;?></td>
              </tr>
              <tr>
                <td>Panadero</td>
                <td><?php echo $prod->usr_nombre;?></td>
              </tr>
              <tr>
                <td>Observaciones</td>
                <td><?php echo $prod->cm_obs;?></td>
              </tr>

            <?php } ?>
            </tbody>
        </table>
        <div class="box-header with-border">
          <h3 class="box-title">ProductosX de la Producción</h3>
        </div>
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>ID Nombre</th>
              <th>Cant Moje</th>
              <th>Cant Emp</th>
              <th>Costo</th>
              <th>Precio</th>
              <th>Utilidad</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($producto as $datos) {  ?>
              <tr>
                <td><?php echo $datos->prod_id;?> <?php echo $datos->prod_nombre;?></td>
                <td><span id="<?php echo $datos->mp_id;?>" class="pull-right badge bg-yellow movimiento" style="font-size:28;"><?php echo $datos->mp_cant;?></span></a></td>
                <td><span id="<?php echo $datos->emp_id;?>" class="pull-right badge bg-blue empaque" style="font-size:28;"><?php echo $datos->emp_cant;?></span></a></td>
                <td>$<?php echo $datos->mp_costo;?></td>
                <td><?= $datos->prod_precio;?></td>
                <td><?php echo number_format(($datos->prod_precio-$datos->mp_costo)/$datos->prod_precio*100)."%";?> </td>
              </tr>
              <?php } ?>
              <?php foreach($materias as $datos2) {  ?>
                <tr>
                  <td><?php echo $datos2->mat_id;?></td>
                  <td>Materia prima</td>
                  <td><?= number_format($datos2->mm_cant/$datos2->unidad_escala); ?> <?= $datos2->unidad_nombre; ?>s</td>
                  <td><?php echo $datos2->mat_nombre;?></td>
                  <td>$<?php echo $datos2->mm_costo;?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <br>
        <a href="<?php echo base_url('Produccion/index_admin')?>" class="btn btn-success" >Volver</a>
        </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-8">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Materias usadas en la producción</h3>
      </div>
      <div class="box-body">
      <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Costo</th>
          <!--  <th>Costo</th> -->
          </tr>
          </thead>
          <tbody>
          </tr>
          <?php $x=0;
          foreach($ing as $ingredientes) {  ?>
            <tr>
              <td><?php echo $ingredientes->mat_id;?></td>
              <td><?php echo $ingredientes->mat_nombre;?></td>
              <td><?= number_format($ingredientes->mm_cant/$ingredientes->unidad_escala); ?> <?= $ingredientes->unidad_nombre; ?>s</td>
             <td><?php  echo "$".number_format($ingredientes->mm_costo);?></td>
            </tr>
            <?php $x = $ingredientes->mm_costo + $x;} ?>
          </tbody>
      </table>
      </div>
      <div class="box-footer with-border">
        <h3 class="box-title">Costo del Moje:<?php  echo "$".number_format($x);?> </h3>
        <div  class="col-md-12">
          <form role="form" action="<?php echo base_url(); ?>Controller_produccion/delete" method="POST">
            <input type="hidden" name="out" value="<?php echo $y; ?>" class="form-control"  >
            <input type="hidden" name="in" value="<?php echo $x1; ?>" class="form-control"  >
            <input type="hidden" name="x" value="1"class="form-control"  >
            <button class="btn btn-danger btn-lg"> Eliminar Producción</button>
          </form>
        </div>
      </div>
  </div>
</div>
  </div>
</section>
</div>

<!-- Modal PRODUCTO EMPAQUE -->
<div class="modal fade" id="modal_empaque" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Cantidad de producto</h3>
          </div>
          <div class="modal-body form">
           <form action="#" id="form_empaque" class="form-horizontal">
              <div class="form-body">
                <div class="form-group">
                   <label class="control-label col-md-3">Cantidad</label>
                   <div class="col-md-9">
                       <input name="cantidad" class="form-control" type="number">
                       <span class="help-block"></span>
                   </div>
                </div>
            </div>
           </form>
         </div>
          <div class="modal-footer">
              <button type="button" id="guardar-empaque"  class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal PRODUCTO MOJADO -->
<div class="modal fade" id="Modalprod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
          <h2 class="modal-title">Ingrese cantidad de producto terminado</h2>
      </div>
      <div class="modal-body">
        <form action="#" id="form" class="form-horizontal">

          <h4>Cantidad</h4>
          <input name="cantidad" placeholder="cantidad" class="form-control" type="number">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnSave"  class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<script type="text/javascript">
$(document).ready(function()
{
  var id;

///MOSTRAR form producto produccion
$(".movimiento").click(function(){
  id = $(this).attr("id");
  $('#form')[0].reset();
  $('#Modalprod').modal('show');
});

// //AJAX INSERTAR OPCION
 $("#btnSave").click(function(){
   $.ajax({
       url : "<?php echo site_url('Controller_produccion/edit_mov_prod/')?>"+id,
       type: "POST",
       data: $('#form').serialize(),
       dataType: "JSON",
       success: function(data)
       {
         $('#Modalprod').modal('hide');
        location.reload();
       },
       error: function (jqXHR, textStatus, errorThrown)
        {
          alert('error');
       }
   });
 });

 $(".empaque").click(function(){
   $('#modal_empaque').modal('show');
   id = $(this).attr("id");
   $('#form_empaque')[0].reset();
  });

 $("#guardar-empaque").click(function(){
   //Ajax Load data from ajax
   $.ajax({
       url : "<?php echo site_url('Calculos/empaque_update/')?>/" + id,
       type: "POST",
       data: $('#form_empaque').serialize(),
       dataType: "JSON",
       success: function(data)
       {
         $('#modal_empaque').modal('hide');
         location.reload();
       },
       error: function (jqXHR, textStatus, errorThrown){}
  });
  });

});
</script>
