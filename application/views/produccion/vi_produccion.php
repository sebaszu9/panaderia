<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado de Producción
    </h1>
  </section>
  <?php
      $kproductos = 0;
      $kcosto = 0;
      $kventa = 0;
      foreach($productos as $info)
      {
        $kproductos = $kproductos + 1;
        $kcosto = $kcosto + ($info->mp_costo*$info->mp_cant);
        $kventa = $kventa + ($info->prod_precio*$info->mp_cant);
      }
  ?>
  <!-- Main content -->
  <section class="content">
    <div class="row">
       <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
           <span class="info-box-icon bg-aqua"><i class="fa fa-industry"></i></span>

           <div class="info-box-content">
             <span class="info-box-text">Productos Elaborados</span>
             <span class="info-box-number"><?php echo $kproductos;?></span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </div>
       <!-- /.col -->
       <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
           <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

           <div class="info-box-content">
             <span class="info-box-text">Costo</span>
             <span class="info-box-number">$<?php echo number_format($kcosto);?></span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </div>
       <!-- /.col -->
       <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
           <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>

           <div class="info-box-content">
             <span class="info-box-text">Precio Venta</span>
             <span class="info-box-number">$<?php echo number_format($kventa);?></span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </div>
       <!-- /.col -->
       <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
           <span class="info-box-icon bg-red"><i class="fa fa-percent"></i></span>

           <div class="info-box-content">
             <span class="info-box-text">Utilidad</span>
             <span class="info-box-number"><?php if ($kventa != 0) {
              echo number_format(($kventa-$kcosto)/$kventa*100);}?>%</span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-3">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detalles del Producción</h3>
          </div>

          <div class="box-body">

            <form action="<?= base_url('Produccion/Produccion') ?>" method="POST" >
            <div class="form-group col-md-12">
              <label>Panadería</label>
              <select class="form-control" name="panaderia">
                <option value="100">Todas</option>
              <?php foreach($panaderias as $datosd) { ?>
                <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label>Panaderos</label>
              <select class="form-control" name="panadero">
                <option value="100">Todos</option>
              <?php foreach($panaderos as $datos) { ?>
                <option value="<?= $datos->usr_id;?>"><?= $datos->usr_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Fecha</label>
              <input type="text" name="date"  id="datepicker" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Fecha 2</label>
              <input type="text" name="date2"  id="datepicker2" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-12">
              <label>Tipo</label>
              <select class="form-control" name="tipo">
                <option value="1">Tabla</option>
                <option value="4">Grafico</option>
              </select>
            </div>

            <div class="form-group col-md-12">
              <div class="form-group ">
                <label for="exampleInputEmail1"> </label>
                <button type="submit" class="btn btn-block btn-info" ><i class="fa fa-search"></i> Consultar</button>
              </div>
            </div>


          </form>
          </div>

        </div>
        </div>

      <div class="col-md-9">
        <!-- general form elements -->
        <div class="box box-primary">

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Costo</th>
                <th>Utilidad</th>
                <th>Cantidad</th>
                <th>Fecha</th>
                <th>Ubicación</th>
                <th>Panadero</th>
                <th>Detalles</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($productos as $datos) { ?>
                <tr>
                    <td><?php echo $datos->prod_id;?> <?php echo $datos->prod_nombre;?> </td>
                    <td><?php echo $datos->prod_precio;?> </td>
                    <td><?php echo $datos->mp_costo;?> </td>
                    <td><?php echo number_format(($datos->prod_precio-$datos->mp_costo)/$datos->prod_precio*100)."%";?> </td>
                    <td><?php echo $datos->mp_cant;?> </td>
                    <td><?php echo $datos->cm_fecha;?> </td>
                    <td><?php echo $datos->bod_nombre;?> </td>
                    <td><?php echo $datos->usr_nombre;?> </td>
                    <td><a href="<?php echo base_url('Produccion/details')."/".intval($datos->cm_id)."/".intval($datos->cm_id-1); ?>" class="btn btn-success" target="_blank" >Detalles</a></td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Nombre</th>
                  <th>Precio</th>
                  <th>Costo</th>
                  <th>Utilidad</th>
                  <th>Cantidad</th>
                  <th>Fecha</th>
                  <th>Ubicación</th>
                  <th>Panadero</th>
                  <th>Detalles</th>
                </tr>
              </tfoot>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
