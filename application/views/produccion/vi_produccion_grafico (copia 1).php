<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado de Producción
    </h1>
  </section>
  <?php
      $kproductos = 0;
      $kcosto = 0;
      $kventa = 0;
      foreach($productos as $info)
      {
        $kproductos = $kproductos + 1;
        $kcosto = $kcosto + ($info->mp_costo*$info->mp_cant);
        $kventa = $kventa + ($info->prod_precio*$info->mp_cant);
      }
  ?>
  <!-- Main content -->
  <section class="content">
    <div class="row">
       <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
           <span class="info-box-icon bg-aqua"><i class="fa fa-industry"></i></span>

           <div class="info-box-content">
             <span class="info-box-text">Productos Elaborados</span>
             <span class="info-box-number"><?php echo $kproductos;?></span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </div>
       <!-- /.col -->
       <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
           <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>
           <div class="info-box-content">
             <span class="info-box-text">Costo</span>
             <span class="info-box-number">$<?php echo number_format($kcosto);?></span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </div>
       <!-- /.col -->
       <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
           <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>

           <div class="info-box-content">
             <span class="info-box-text">Precio Venta</span>
             <span class="info-box-number">$<?php echo number_format($kventa);?></span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </div>
       <!-- /.col -->
       <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
           <span class="info-box-icon bg-red"><i class="fa fa-percent"></i></span>

           <div class="info-box-content">
             <span class="info-box-text">Utilidad</span>
             <span class="info-box-number"><?php if ($kventa != 0) {
              echo number_format(($kventa-$kcosto)/$kventa*100);}?>%</span>
           </div>
           <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-3">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detalles del Producción</h3>
          </div>

          <div class="box-body">

            <form action="<?= base_url('Produccion/Produccion') ?>" method="POST" >
            <div class="form-group col-md-12">
              <label>Panadería</label>
              <select class="form-control" name="panaderia">
                <option value="100">Todas</option>
              <?php foreach($panaderias as $datosd) { ?>
                <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label>Panaderos</label>
              <select class="form-control" name="panadero">
                <option value="100">Todos</option>
              <?php foreach($panaderos as $datos) { ?>
                <option value="<?= $datos->usr_id;?>"><?= $datos->usr_nombre;?></option>
              <?php  }  ?>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Fecha</label>
              <input type="text" name="date"  id="datepicker" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-12">
              <label for="exampleInputEmail1">Fecha 2</label>
              <input type="text" name="date2"  id="datepicker2" class="form-control"  readonly>
            </div>

            <div class="form-group col-md-12">
              <label>Tipo</label>
              <select class="form-control" name="tipo">
                <option value="1">Tabla</option>
                <option value="2">PDF</option>
                <option value="3">Excel</option>
                <option value="4">Grafico</option>
              </select>
            </div>

            <div class="form-group col-md-12">
              <div class="form-group ">
                <label for="exampleInputEmail1"> </label>
                <button type="submit" class="btn btn-block btn-info" ><i class="fa fa-search"></i> Consultar</button>
              </div>
            </div>


          </form>
          </div>

        </div>
        </div>

      <div class="col-md-9">
        <!-- general form elements -->
        <div class="box box-primary">

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <div class="chart">
              <canvas id="areaChart" style="height:250px"></canvas>
            </div>

            <table class="table table-bordered table-striped ">
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Producion</th>
                  <th>Costo</th>
                  <th>Precio</th>
                </tr>
              </thead>
              <tbody>

              <?php

                $totalproduccion = 0;
                $totalprecio     = 0;
                $totalcosto     = 0;

                foreach($ventas as $key => $datp)
                {
                  if ($datp['costo']>0)
                  {
                    $fecha = date('d', strtotime($datp['fecha'])).' de '.$datp['mes'].' ';
                    echo "<tr><td> $fecha  </td> <td> ".$datp['cant']." </td> <td> ".number_format($datp['costo'])." </td> <td> ".number_format($datp['precio'])." </td> </tr>";
                    $totalproduccion = $totalproduccion +$datp['cant'];
                    $totalprecio = $totalprecio +$datp['precio'];
                    $totalcosto = $totalcosto +$datp['costo'];
                  }
                }

                echo "<tr><td> Total </td> <td> ".$totalproduccion." </td> <td> ".number_format($totalprecio)." </td> <td> ".number_format($totalcosto)." </td> </tr>";

                ?>

                </tbody>
              </table>


          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>/assets/plugins/chartjs/Chart.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>/assets/dist/js/app.min.js"></script>


<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
      labels: [
        <?php
        foreach($ventas as $key => $datos)
         {
           echo '"'.date('d', strtotime($datos['fecha'])).' '.$datos['mes'].' ",';
         }
         ?>],
      datasets: [

        {
          label: "Precio",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,0.1)",
          data: [<?php
           foreach($ventas as $key => $datos1)
           {
             $vawa = $datos1['precio'];
              echo $vawa.',';
           }
          ?>]
        },
        {
          label: "Costo",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,0.1)",
          data: [<?php
           foreach($ventas as $key => $datos1)
           {
              echo $datos1['costo'].',';
           }
          ?>]
        },
      ]
    };

    var areaChartDatax = {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          label: "Electronics",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: "Digital Goods",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [28, 48, 40, 19, 86, 27, 90]
        }
      ]
    };

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
    //var lineChartCanvas = document.getElementById('lineChart').getContext('2d');
    var lineChart = new Chart(lineChartCanvas);
    var lineChartOptions = areaChartOptions;
    lineChartOptions.datasetFill = false;
    lineChart.Line(areaChartData, lineChartOptions);


  });
</script>
