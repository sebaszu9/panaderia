<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado de Proveedores
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Proveedores/form')?>" class="btn  btn-primary">+ Nuevo</a>

          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>

                <th>Razon Social</th>
                <th>NIT/Cedula</th>
                <th>Telefono</th>
                <th>Observaciones</th>
                <th>Acción</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($datos as $test) { ?>
                <tr>

                    <td><?php echo $test->prov_nombre;?></td>
                    <td><?php echo $test->prov_nit;?></td>
                    <td><?php echo $test->prov_tel1;?></td>
                    <td><?php echo $test->prov_observaciones?></td>
                    <td><a href="<?php echo base_url('Proveedores/view')."/".$test->prov_id; ?>" class="btn  btn-success "title="Detalles"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a href="<?php echo base_url('Proveedores/edit')."/".$test->prov_id; ?>" class="btn  btn-warning" title="Editar" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a href="<?php echo base_url('Proveedores/get')."/".$test->prov_id; ?>" class="btn  btn-danger" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>

                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
              <tr>
                <th>Razon Social</th>
                <th>NIT/Cedula</th>
                <th>Telefono</th>
                <th>Observaciones</th>
              </tr>
              </tfoot>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
