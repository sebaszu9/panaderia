<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Formulario
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box-header">
      <a class=" btn  btn-primary" href="<?php echo base_url('Proveedores/index');?>">Volver</a>
    </div>
    <!-- Small boxes (Stat box) -->
  <?php foreach($datos as $test) { ?>
<div class="row">
  <div class="col-md-12">
        <div class="box box-primary">
          <!-- general form elements -->
          <div class="row">
         <div class="col-md-6">
            <form role="form" action="<?= base_url()?>Proveedores/update/<?= $test->prov_id; ?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">Editar datos del Proveedor</h3>
                </div>

                <?php if(isset($mensaje)):?>
                        <div class="alert <?= $alert; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?= $mensaje; ?>
                        </div>
                <?php endif; ?>
                <?php echo validation_errors(); ?>
                <div class="form-group">
                  <input type="hidden" class="form-control" name="id" value="<?php echo $test->prov_id; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nombre del Proveedor</label>
                  <input type="text" class="form-control" name="nombre" placeholder="Escribe el nombre del Proveedor" value="<?php echo $test->prov_nombre; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">NIT/Cedula</label>
                  <input type="text" class="form-control" name="nit" placeholder="Nit o cedula del Proveedor" value="<?php echo $test->prov_nit; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Direccion</label>
                  <input type="text" class="form-control" name="direccion" placeholder="Escribe la direccion del Proveedor" value="<?php echo $test->prov_dir; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Correo Electronico</label>
                  <input type="text" class="form-control" name="email" placeholder="Escribe el correo electronico del Proveedor" value="<?php echo $test->prov_email; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Telefono 1</label>
                  <input type="number" class="form-control" name="telefono1" placeholder="Escribe el telefono del Proveedor" value="<?php echo $test->prov_tel1; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Telefono 2</label>
                  <input type="number" class="form-control" name="telefono2" placeholder="Escribe el un telefono secundario del Proveedor" value="<?php echo $test->prov_tel2; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">FAX</label>
                  <input type="number" class="form-control" name="fax" placeholder="Escribe el FAX del Proveedor" value="<?php echo $test->prov_fax; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Celular</label>
                  <input type="number" class="form-control" name="celular" placeholder="Escribe el nombre de su producto" value="<?php echo $test->prov_cel ?>">
                </div>
                <div class="form-group">
                  <label>Observaciones</label>
                  <textarea class="form-control" name="descripcion" rows="3" placeholder="Descipcion breve del producto ..."><?php echo $test->prov_observaciones ?></textarea>
               </div>
              <div class="box-footer">
                <input type="hidden" value="true" name="create">
                <input type="submit" class="btn btn-primary btn-sm" value="Guardar">
              </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box-body">
               <div class="box-header with-border">
                 <h3 class="box-title">Cuenta Bancaria</h3>
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Nombre del Banco</label>
                 <input type="text" class="form-control" name="banco1" placeholder="Escribe el Banco del Proveedor" value="<?php echo $test->prov_cuenta_banco1; ?>">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Numero de cuenta</label>
                 <input type="number" class="form-control" name="cuenta1" placeholder="Escribe la cuenta bancaria del Proveedor" value="<?php echo $test->prov_cuenta_num1 ?>">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Tipo de cuenta</label>
                 <input type="text" class="form-control" name="tipo1" placeholder="Ahorros/Corriente" value="<?php echo $test->prov_cuenta_tipo1 ?>">
               </div>
               <div class="box-header with-border">
                 <h3 class="box-title">Cuenta Bancaria secundaria</h3>
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Nombre banco</label>
                 <input type="text" class="form-control" name="banco1" placeholder="Escribe el Banco del Proveedor" value="<?php echo $test->prov_cuenta_banco2; ?>">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Numero de cuenta</label>
                 <input type="number" class="form-control" name="cuenta1" placeholder="Escribe la cuenta bancaria del Proveedor" value="<?php echo $test->prov_cuenta_num2; ?>">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Tipo de cuenta</label>
                 <input type="text" class="form-control" name="tipo1" placeholder="Ahorros/Corriente" value="<?php echo $test->prov_cuenta_tipo2 ?>">
               </div>
               <div class="box-header with-border">
                 <h3 class="box-title">Persona de Contacto con el Proveedor</h3>
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Nombre del contacto</label>
                 <input type="text" class="form-control" name="contacto" placeholder="nombre del contacto" value="<?php echo $test->prov_cont_nombre; ?>">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Celular del contacto</label>
                 <input type="number" class="form-control" name="celcontacto" placeholder="numer de celular del contacto" value="<?php echo $test->prov_cont_email; ?>">
               </div>
               <div class="form-group">
                 <label for="exampleInputEmail1">Correo electronico del contacto</label>
                 <input type="text" class="form-control" name="emailcontacto" placeholder="correo electronico del contacto" value="<?php echo $test->prov_cont_celular; ?>">
               </div>
             </div>
            </div>
          <!-- /.box -->
      </div>
          <!-- /.BOX-BODY -->
    <?php } ?>
  </div>
    <!-- /.row -->
  </div>
<!-- /.content-wrapper -->
  </section>
  </div>
