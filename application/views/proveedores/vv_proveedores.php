<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detalles del Proveedor
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box-header">
      <a class=" btn  btn-primary" href="<?php echo base_url('Proveedores/index');?>"> Volver </a>
    </div>
    <!-- Small boxes (Stat box) -->

  <?php foreach($datos as $test) { ?>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="row">
          <!-- general form elements -->
          <div class="box-body">
          <div class="box-body col-md-6">
            <div class="box-header with-border">
              <h3 class="box-title">Detalles del Proveedor</h3>
            </div>
          <table id="table-alla-order" class="table table-bordered table-striped">
            <tbody>
              <tr>
                <th>Nombre del Proveedor</th>
                <td><?php echo $test->prov_nombre;?></td>
              </tr>
              <tr>
                <th>NIT/Cedula</th>
                <td><?php echo $test->prov_nit;?></td>
              </tr>
              <tr>
                <th>Direccion</th>
                <td>
                  <?php echo  $test->prov_dir;?>
                </td>
              </tr>
              <tr>
                <th>Correo Electronico</th>
                <td>
                  <?php echo $test->prov_email;   ?>
                </td>
              </tr>
              <tr>
                <th>Telefono 1</th>
                <td>
                  <?php echo $test->prov_tel1; ?>
                </td>
              </tr>
              <th>Telefono 2</th>
              <td>
                <?php echo $test->prov_tel2; ?>
              </td>
            </tr>
           <tr>
            <th>FAX</th>
            <td>
              <?php echo $test->prov_fax; ?>
            </td>
          </tr>
          <tr>
           <th>Celular</th>
           <td>
             <?php echo $test->prov_cel; ?>
           </td>
         </tr>
         <tr>
          <th>Observaciones</th>
          <td>
            <?php echo $test->prov_observaciones; ?>
          </td>
        </tr>
        </tbody>
      </table>
      </div>
      <div class="box-body col-md-6 ">
        <div class="box-header with-border">
          <h3 class="box-title">Cuenta Bancaria #1</h3>
        </div>
      <table id="table-alla-order" class="table table-bordered table-striped">
        <tbody>
          <tr>
            <th>Banco</th>
            <td><?php echo $test->prov_cuenta_banco1;?></td>
          </tr>
          <tr>
            <th>Numero de cuenta </th>
            <td><?php echo $test->prov_cuenta_tipo1;?></td>
          </tr>
          <tr>
            <th>Tipo de cuenta </th>
            <td>
              <?php echo  $test->prov_cuenta_tipo1;?>
            </td>
          </tr>
    </tbody>
  </table>
  </div>

  <div class="box-body col-md-6">
    <div class="box-header with-border">
      <h3 class="box-title">Cuenta Bancaria #2</h3>
    </div>
  <table id="table-alla-order" class="table table-bordered table-striped">
  <tbody>
      <tr>
        <th>Banco </th>
        <td><?php echo $test->prov_cuenta_banco2;?></td>
      </tr>
      <tr>
        <th>Numero de cuenta</th>
        <td><?php echo $test->prov_cuenta_tipo2;?></td>
      </tr>
      <tr>
        <th>Tipo de cuenta</th>
        <td>
          <?php echo  $test->prov_cuenta_tipo2 ;?>
        </td>
      </tr>
</tbody>
</table>
<br>
</div>
<div class="col-md-6">
  <div class="box-body">
    <div class="box-header with-border">
      <h3 class="box-title">Persona de Contacto con el Proveedor</h3>
    </div>
    <table id="table-alla-order" class="table table-bordered table-striped">
    <tbody>
        <tr>
          <th>Nombre del contacto </th>
          <td><?php echo $test->prov_cont_nombre;?></td>
        </tr>
        <tr>
          <th>Celular del contacto</th>
          <td><?php echo $test->prov_cont_email;?></td>
        </tr>
        <tr>
          <th>Correo electronico del contacto</th>
          <td>
            <?php echo  $test->prov_cont_celular ;?>
          </td>
        </tr>
  </tbody>
  </table>
  </div>
  <!-- /.box -->
      </div>
</div>
</div>
          </div>
          <!-- /.BOX-BODY -->
        </div>
        <!-- /.col-md-8 -->
    </div>
    <?php } ?>
    <!-- /.row -->
<!-- /.content-wrapper -->
  </section>
</div>
