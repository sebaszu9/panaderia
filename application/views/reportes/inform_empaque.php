<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Reporte Empaque y Producción Productos
      <small><a href="<?php echo base_url('/Reportes/materias/form')?>" class="btn btn-default">volver</a></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('/Reportes/index')?>">Reportes</a></li>
      <li><a href="#">Grafico</a></li>
    </ol>
  </section>


  <section class="content">

        <!-- Main row -->
        <div class="row">

        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">

                <div class="box-header with-border">
                <h3 class="box-title">Fecha: <?php echo $fecha1; ?> - <?php echo $fecha2; ?></h3>
                </div>

                <div class="box-body">

                    <table class="table table-bordered table-striped " id="example1">
                        <thead>
                            <tr>
                                <th>Productos</th>
                                <th>Producido</th>
                                <th>Empacado</th>
                                <th>Diferencia</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                                $var=0;
                                foreach($productos as $producto){

                                    $cantidad = 0;
                                    $empaque = 0;
                                    $productname = $producto->prod_nombre;

                                    foreach($info as $movprod){

                                        if($producto->prod_id==$movprod->mp_id_prod)
                                        {
                                            $cantidad = $cantidad + $movprod->mp_cant;
                                              $empaque = $empaque + $movprod->emp_cant;
                                        }

                                    }
                                    $var= $cantidad- $empaque;

                                    echo "<tr>";
                                    echo "<td> $productname </td>";
                                    echo "<td> $cantidad </td>";
                                    echo "<td> $empaque </td>";
                                    echo "<td>$var </td>";
                                    echo "</td>";
                                }
                            ?>

                        </tbody>
                    </table>


                </div>
            </div>
        </div>

    <!-- /.Left col -->
    </div>
<!-- /.row (main row) -->
</section>


</div>

<!-- DONUT CHART -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>/assets/plugins/chartjs/Chart.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>/assets/dist/js/app.min.js"></script>
