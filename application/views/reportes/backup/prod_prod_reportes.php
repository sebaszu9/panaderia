<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Producción del Día
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Materias Primas</a></li>
      <li class="active">Listado de Materias</li>
    </ol>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          
          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>ID Producto</th>
                <th>Producto</th>
                <th>Receta</th>
                <th>Cantidad</th>
                <th>Porcentaje</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($info as $datos) { ?>
                <tr>
                    <td><?php echo $datos->prod_cat;?><?php echo $datos->prod_id;?> </td>
                    <td><?php echo $datos->prod_nombre;?> </td>
                    <td><?php echo $datos->rec_nombre;?> </td>
                    <td><?php echo intval($datos->rec_unidades*$datos->prodt_x);?> </td>
                    <td><?php echo intval(100*$datos->prodt_x);?>%</td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>ID Producto</th>
                  <th>Producto</th>
                  <th>Receta</th>
                  <th>Cantidad</th>
                  <th>Porcentaje</th>
                </tr>
              </tfoot>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
