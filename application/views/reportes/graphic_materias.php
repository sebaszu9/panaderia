<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Reporte Gráfico de consumo materia prima día
      <small><a href="<?php echo base_url('/Reportes/materias/form')?>" class="btn btn-default">volver</a></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('/Reportes/index')?>">Reportes</a></li>
      <li><a href="#">Grafico</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">

      <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Grafica</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Tabla</a></li>

              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">

                <div class="chart">
                  <canvas id="barChart" style="height:250px"></canvas>
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">

                <table class="table table-bordered table-striped " id="example1">
                  <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Cantidad Gramos</th>
                    <th>Empaques</th>
                  </tr>
                  </thead>
                  <tbody>

                  <?php

                    $totalproduccion = 0;
                    $totalbulto = 0;

                    foreach($ventas as $key => $datp)
                    {
                      if ($datp['total']>0)
                      {
                        $fecha = date('d', strtotime($datp['fecha'])).' de '.$datp['mes'].' ';
                        echo "<tr>";
                        echo "<td> $fecha  </td> <td> ".number_format($datp['total'])."</td><td> ".number_format($datp['total']/$unidad ,2)."</td>";
                        $resunid = $datp['total']/$unidad;
                        echo "</tr>";
                        $totalbulto = $totalbulto+$resunid;
                        $totalproduccion = $totalproduccion +$datp['total'];
                      }
                    }

                    echo "<tr><td> Total  </td> <td> ".number_format($totalproduccion)." Gramos </td> <td> ".number_format($totalbulto)." Empaques </td></tr>";

                  ?>

                  </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                Invisible
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>

    </div>
    <!-- /.row -->
  </section>

</div>

<!-- DONUT CHART -->

<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>/assets/plugins/chartjs/Chart.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>/assets/dist/js/app.min.js"></script>


<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------


    var areaChartData = {
      labels: [
        <?php
        foreach($ventas as $key => $datos)
         {
           echo '"'.date('d', strtotime($datos['fecha'])).' '.$datos['mes'].' ",';
         }
         ?>],
      datasets: [
        {
          label: "Electronics",
          fillColor: "#00a5e6", //color de area char
          strokeColor: "#00a5e6",// color de linea
          pointColor: "#ff0033", // color de punto
          pointStrokeColor: "#ff00d0",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(255,0,0,1)",
          data: [<?php
           foreach($ventas as $key => $datos1)
           {
              echo $datos1['total'].',';
           }
          ?>]
        }
      ]
    };


    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[0].fillColor = "#00a65a";
    barChartData.datasets[0].strokeColor = "#00a65a";
    barChartData.datasets[0].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);



  });
</script>
