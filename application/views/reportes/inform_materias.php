<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Reporte movimientos Materia prima por concepto de <?php echo $concepto ?>
      <small><a href="<?php echo base_url('/Reportes/materias/form')?>" class="btn btn-default">volver</a></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo base_url('/Reportes/index')?>">Reportes</a></li>
      <li><a href="#">Grafico</a></li>
    </ol>
  </section>


  <section class="content">

        <!-- Main row -->
        <div class="row">

        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">

                <div class="box-header with-border">
                <h3 class="box-title">Fecha: <?php echo $fecha1; ?> - <?php echo $fecha2; ?></h3>
                </div>

                <div class="box-body">

                    <table class="table table-bordered table-striped " id="example1">
                        <thead>
                            <tr>
                                <th>Materias</th>
                                <th>Cantidad</th>
                                <th>U</th>
                                <th>Empaques</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                foreach($materias as $materia){

                                    $cantidad = 0;
                                    $materianame = $materia->mat_nombre;

                                    foreach($info as $movmat){

                                        if($materia->mat_id==$movmat->mm_id_mat)
                                        {
                                            $cantidad = $cantidad + $movmat->mm_cant/$materia->unidad_escala;
                                        }
                                        //echo " mat = ".$movmat->mm_id_mat." - gasto= ".$movmat->mm_cant." gm <br>";
                                    }

                                    $unidad = number_format($cantidad / $materia->mat_ue,1);

                                    $cantidad = number_format($cantidad);

                                    echo "<tr>";
                                    echo "<td> $materianame </td>";
                                    echo "<td> $cantidad </td>";
                                    echo "<td> $materia->unidad_nombre </td>";
                                    echo "<td> $unidad </td>";
                                    echo "</td>";
                                }
                            ?>

                        </tbody>
                    </table>


                </div>
            </div>
        </div>

    <!-- /.Left col -->
    </div>
<!-- /.row (main row) -->
</section>


</div>

<!-- DONUT CHART -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url(); ?>/assets/plugins/chartjs/Chart.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>/assets/dist/js/app.min.js"></script>
