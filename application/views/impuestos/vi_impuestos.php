<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
         Impuestos
          </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Productos</a></li>
      <li><a href="#">Impuestos</a></li>
      <li class="active">Listado Impuestos</li>
    </ol>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Impuestos/form')?>" class="btn  btn-primary">Nuevo +</a>

          </div>

          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Porcentaje</th>
                  <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                </tr>
                  <?php foreach($info as $datos) { ?>
                  <tr>

                      <td><?php echo $datos->imp_nombre;?> </td>
                      <td><?php echo $datos->imp_porcentaje;?> </td>
                      <td><a href="<?php echo base_url('Impuestos/edit')."/".$datos->imp_id; ?>" class="btn  btn-warning"title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                          <a href="<?php echo base_url('Impuestos/get')."/".$datos->imp_id; ?>" class="btn  btn-danger"title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                      </td>
                  </tr>
                  <?php  }  ?>
                </tbody>

            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
