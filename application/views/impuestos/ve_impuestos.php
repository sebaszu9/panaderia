<div class="content-wrapper">
  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Formulario de edición de Impuestos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Productos</a></li>
        <li><a href="#">Impuestos</a></li>
        <li class="active">Editar Impuesto</li>
      </ol>
    </section>

    <!-- general form elements -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="box-header">

        <a class=" btn  btn-primary" href="<?php echo base_url('Impuestos/index');?>"></i>Volver</a>
      </div>
      <div class="row">
        <div class="col-md-6">
  <!-- general form elements -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Editar Impuesto</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php foreach($datos as $test) { ?>

    <form role="form" action="<?php echo base_url('Impuestos/update/'); ?><?= $test->imp_id; ?>" method="POST" enctype="multipart/form-data">
      <div class="box-body">


        <div class="form-group">

          <label for="exampleInputEmail1">Nombre del Impuesto</label>
          <input type="text" name="nombre" required class="form-control"  value="<?php echo $test->imp_nombre; ?>">

          <label for="exampleInputEmail1">Porcentaje del impuesto</label>
          <input type="text" name="porcentaje" required class="form-control"  value="<?php echo $test->imp_porcentaje; ?>">

          <div class="form-group">
            <input type="hidden" name="id" required class="form-control" value="<?php echo $test->imp_id; ?>">
          </div>
        </div>



        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Actualizar</button>
        </div>
        </div>
        </form>
        <?php  }  ?>

      </div>
      <!-- /.box-body -->



</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
