
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Punto de Venta
        <small> POS </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">POS</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">


        <!-- ============= CARRITO =============================== -->

        <div class="col-md-6">

          <div class="box box-solid">

            <div class="box-header with-border">

              <div class="col-md-12">
                <div class="form-group">
                  <!-- Button trigger modal -->
                  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
                    Crear cliente
                  </button>

                  <button type="button" class="btn btn-info pull-right clean-pos" > Nuevo </button>

                  <hr>
                  <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" name="cliente" id="selectcliente">
                    <option selected="selected" value="0">Cliente</option>
                    <?php foreach($cliente as $datos) { ?>
                      <option value="<?= $datos->clt_id; ?>"><?php echo $datos->clt_nombre;?> (<?php echo $datos->clt_cedula;?>) </option>
                    <?php } ?>
                  </select>
                </div>
              </div>


            </div>

            <div class="box-body">

              <h3 >Listado de Productos</h3>


              <p id="storageres">

              </p>

              <div class="table-responsive-sm">

                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Producto</th>
                      <th>Precio</th>
                      <th>Cantidad</th>
                      <th>Descuento</th>
                      <th>Subtotal</th>
                    </tr>
                  </thead>
                  <tbody id="itemcart">
                  </tbody>
                </table>

                <table  class="table table-bordered">
                  <tr>
                    <td id="rowsubtotal">Subtotal:0</td>
                    <td id="rowdescuent">Descuento:0</td>
                  </tr>
                  <tr>
                    <td id="rowimpuesto">Impuestos:0</td>
                    <td id="rowdetotals">Total:0</td>
                  </tr>
                </table>
              </div>


            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success btn-block send-pos" style="font-size:25px;font-weight:bold;font-style:italic;">Pagar</button>
            </div>
            <div id="resbuy">

            </div>

            <br>
          </div>
          <!-- /.box -->
        </div>



        <!-- CIERRA DE CARRITOOOO -->

        <div class="col-md-6">

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Buscador productos</h3>
            </div>
            <div class="box-header with-border">
              <div class="input-group">
                <input type="text" class="form-control" id="datasearch" placeholder="Digite el producto a buscar...">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button">Buscar</button>
                </span>
              </div><!-- /input-group -->
            </div>

            <div class="box-body with-border">
              <div id="box-reset">
              </div>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">Formulario de Cliente</h2>

              </div>
              <div class="modal-body">
                <form id="register-customer">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Cedula</label>
                    <input type="text" name="cedula" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" name="nombre" class="form-control">
                    <input type="hidden" name="register" value="true">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Direccion</label>
                    <input type="text" name="direccion" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Telefono</label>
                    <input type="text" name="telefono" class="form-control">
                  </div>
                  <button type="submit" class="btn btn-success btn-block">Guardar</button>
                </form>
                <div id="messagereg"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->




  <script>
  function loadCartTable () {
    $(document).ready(function(){

      let itemsArray = localStorage.getItem('carrito') ? JSON.parse(localStorage.getItem('carrito')) : [];
      $("#itemcart").html("");

      var total = 0;
      var impuestos = 0 ;
      var descuento = 0;
      itemsArray.forEach(function(element,key){

        console.log(element.name);
        var subtotalx = (element.cant*element.price)-element.desc;
        var div = "<tr id='row"+key+"'>";
        div = div + '<td><button class="btn btn-danger delete-item"  type="button" data-item="'+key+'"><i class="fa fa-trash"></i></button></td>';
        div = div + "<td>"+element.name+"</td>";
        //div = div + '<td><input type="hidden" value="'+element.price+'" class="form-control" readonly></td>';
        //div = div + '<td id="inputa'+key+'" style="display:none;"><div class="input-group"><span class="input-group-addon"><i class="fa fa-dollar"></i></span><input type="text" value="'+element.price+'" data-imp="'+element.impt+'" data-desc="'+element.desc+'" data-item="'+key+'" data-prod="'+element.prod+'" data-cant="'+element.cant+'" data-name="'+element.name+'"  class="form-control input'+key+' press-price" ></div></td>';
        div = div + '<td id="labela'+key+'" ><span class="label label-default show-input-pricex" style="font-size:15px;font-weight:bold;" id="input-price-'+key+'" data-item="'+key+'" > $ '+element.price+'</span></td>';
        div = div + "<td><div class='input-group'>";
        //div = div + '<span class="input-group-btn"><button class="btn btn-default"  type="button"><i class="fa fa-minus"></i></button> </span>';
        div = div + '<input type="text" value="'+element.cant+'" class="form-control toggle-bloq2 presscant" id="inputcant"'+key+'" data-item="'+key+'" data-prod="'+element.prod+'" data-imp="'+element.impt+'" data-cant="'+element.cant+'" data-desc="'+element.desc+'" data-price="'+element.price+'" data-name="'+element.name+'"  readonly>';
        //div = div + '<span class="input-group-btn"><button class="btn btn-default sumar" type="button" data-item="'+key+'"><i class="fa fa-plus"></i></button> </span>';
        div = div + "</div></td>";
        //div = div + "<td>"+(element.impt/100)+"</td>";

        div = div + '<td id="inputprom'+key+'" style="display:none;"><div class="input-group"><span class="input-group-addon"><i class="fa fa-dollar"></i></span><input type="text" value="'+element.desc+'" data-imp="'+element.impt+'" data-item="'+key+'" data-prod="'+element.prod+'" data-cant="'+element.cant+'" data-name="'+element.name+'" data-price="'+element.price+'"  class="form-control input'+key+' press-prom" ></div></td>';
        div = div + '<td id="labelprom'+key+'" ><span class="label label-default show-input-prom" id="input-price-'+key+'" data-item="'+key+'" > $ '+element.desc+'</span></td>';

        div = div + '<td id="labelsubtotal'+key+'"><span class="label label-default"  style="font-size:15px;font-weight:bold;">$ '+subtotalx+'</span></td>';
        div = div +'</tr>';
        total = total+subtotalx;
        descuento = descuento+parseInt(element.desc);
        impuestos = impuestos + (subtotalx*(element.impt/100))
        $("#itemcart").append(div);
      });

      $("#rowsubtotal").html("Subtotal : $"+(total-impuestos));
      $("#rowimpuesto").html("Impuestos: $"+impuestos);
      $("#rowdescuent").html("Descuento: $"+descuento);
      $("#rowdetotals").html("TOTAL : $"+total);

    })
  }
  $(document).ready(function(){

      $("#datasearch").keydown(function(){
          $("#datasearch").css("background-color", "yellow");
      });
      $("#datasearch").keyup(function(){
          $("#datasearch").css("background-color", "white");
          var data = $("#datasearch").val();

          var parametros = { "safety" : true,   "data" : data };
          $.ajax({ data:  parametros, url:' <?= base_url('Pos/buscar/productos')?>',type:  'post',
             beforeSend: function () {$("#box-reset").html("Enviando, espere por favor...");},
             success:  function (response) {$("#box-reset").html(response); }
          }).fail( function(error) { alert(JSON.stringify(error)) })
      });

      $(document).on('click', '.toggle-bloq', function(){
        $(this).attr('readonly', false);
      });

      $(document).on('click', '.toggle-bloq2', function(){
        $(this).attr('readonly', false);
      });


      $(document).on('click', '.show-input-price', function(){
        var prod   = $(this).data("item");
        $("#inputa"+prod).show();
        $("#labela"+prod).hide();
      })

      $(document).on('click', '.show-input-prom', function(){
        var prod   = $(this).data("item");
        $("#inputprom"+prod).show();
        $("#labelprom"+prod).hide();
      })

      //eliminar fila de carrito
      $(document).on('click', '.delete-item', function(){

        var key   = $(this).data("item");
        const data = JSON.parse(localStorage.getItem('carrito'));
        data.splice(key,1);
        localStorage.setItem('carrito', JSON.stringify(data));
        //$("#row"+key).hide(500);
        loadCartTable();
      });

      //actualizar precio de carrito
      $(document).on('keypress', '.press-price', function(e){

            if(e.which == 13) {

              var key  = $(this).data("item");
              var price = $(this).val();
              var name =  $(this).data("name");
              var prod =  $(this).data("prod");
              var cant =  $(this).data("cant");
              var impt =  $(this).data("imp");
              var desc =  $(this).data("desc");

              const item = {
                prod : prod,
                price: price,
                cant : cant,
                name : name,
                impt : impt,
                desc : desc
              }

              //alert(prod)
              const data = JSON.parse(localStorage.getItem('carrito'));
              // elimina y reemplaza al mismo puesto
              data.splice(key,1,item);
              localStorage.setItem('carrito', JSON.stringify(data));
              loadCartTable();
            }
      })

      //actualizar promocion de carrito
      $(document).on('keypress', '.press-prom', function(e){

            if(e.which == 13) {

              var key  = $(this).data("item");
              var price = $(this).data("price");
              var name =  $(this).data("name");
              var prod =  $(this).data("prod");
              var cant =  $(this).data("cant");
              var impt =  $(this).data("imp");
              var desc =  $(this).val();

              const item = {
                prod : prod,
                price: price,
                cant : cant,
                name : name,
                impt : impt,
                desc : desc
              }

              //alert(prod)
              const data = JSON.parse(localStorage.getItem('carrito'));
              // elimina y reemplaza al mismo puesto
              data.splice(key,1,item);
              localStorage.setItem('carrito', JSON.stringify(data));
              loadCartTable();
            }
      })


      // cambiar cantidad
      $(document).on('keypress', '.presscant', function(e){

            if(e.which == 13) {

              var cant = $(this).val();
              var key  = $(this).data("item");
              var name =  $(this).data("name");
              var prod =  $(this).data("prod");
              var price =  $(this).data("price");
              var impt =  $(this).data("imp");
              var desc =  $(this).data("desc");


              const item = {
                prod : prod,
                price: price,
                cant : cant,
                name : name,
                impt : impt,
                desc : desc
              }

              //alert(prod)
              const data = JSON.parse(localStorage.getItem('carrito'));
              // elimina y reemplaza al mismo puesto
              data.splice(key,1,item);
              localStorage.setItem('carrito', JSON.stringify(data));
              //$(this).attr('readonly', true);
              loadCartTable();
            }
      })

      //agregar producto en el item desde entres press
      $(document).on('keypress', '.pressadd', function(e){

            if(e.which == 13) {

              let itemsArray = localStorage.getItem('carrito') ? JSON.parse(localStorage.getItem('carrito')) : [];
              localStorage.setItem('items', JSON.stringify(itemsArray));
              const data = JSON.parse(localStorage.getItem('carrito'));

              var prod   = $(this).data("prod");
              var price = $(this).val();
              var name = $(this).data("name");
              var cant = 1;
              var impt =  $(this).data("imp");

              const item = {
                prod : prod,
                price: price,
                cant : cant,
                name : name,
                impt : impt,
                desc : 0
              }

              data.push(item);
              localStorage.setItem('carrito', JSON.stringify(data));
              loadCartTable();
            }
      })

      // agregar producto en el item desde boton add
      $(document).on('click', '.sendcar', function(){

        let itemsArray = localStorage.getItem('carrito') ? JSON.parse(localStorage.getItem('carrito')) : [];
        localStorage.setItem('carrito', JSON.stringify(itemsArray));
        const data = JSON.parse(localStorage.getItem('carrito'));
        //alert(localStorage.getItem('carrito'))

        var prod   = $(this).data("prod");
        var price = $(this).data("precio");
        var name = $(this).data("name");
        var impt =  $(this).data("imp");
        var cant = 1;

        const item = {
          prod : prod,
          price: price,
          cant : 1,
          name : name,
          impt : impt,
          desc : 0
        }


        data.push(item);
        localStorage.setItem('carrito', JSON.stringify(data));
        loadCartTable();
      });


      $(".vaciarbox").click(function() {

      });

      $( "#register-customer" ).on( "submit", function( event ) {
        event.preventDefault();
        $.ajax({ data:  $( this ).serialize(), url:' <?= base_url('Pos/register/customer')?>',type:  'post',
           beforeSend: function () {$("#messagereg").html("Enviando, espere por favor...");},
           success:  function (response) {
             var response = JSON.parse(response);
             if (response.success==true)
             {
               $("#messagereg").html(response.message);
                location.reload();
             }
             else{
               $("#messagereg").html(response.message);
             }

           }
        }).fail( function(error) {
          alert(JSON.stringify(error))
       })
      });

      $(document).on('click', '.send-pos', function(){

        const carrito = JSON.parse(localStorage.getItem('carrito'));

        if (carrito.length>=1)
        {
          const datapos = {
            pos : true,
            carrito:JSON.stringify(carrito),
            cliente: $("#selectcliente").val()
          }
          $.ajax({ data: datapos, url:' <?= base_url('Controller_pos/create')?>',type:  'post',
             beforeSend: function () {$("#resbuy").html("Enviando, espere por favor...");},
             success:  function (response) {
                const res = JSON.parse(response);
                if(res.success==true)
                {
                  //alert(res.message)
                  $("#resbuy").html(res.message); 
                }
                else{
                  $("#resbuy").html("Error... contacte al administrador."); 
                }
              }
          }).fail( function(error) {
            $("#resbuy").html(JSON.stringify(error));
            //alert(JSON.stringify(error))
          })

        }
        else{
          $("#resbuy").html("<center><b style='font-size:15px;'>No hay producto</b></center>");
        }
      });
      
      $(document).on('click', '.clean-pos', function(){
        const data = [];
        localStorage.setItem('carrito', JSON.stringify(data));
        loadCartTable();
        $("#resbuy").html("");
      });



  });

  loadCartTable();
</script>
