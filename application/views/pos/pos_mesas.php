
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Punto de Venta Mesas
        <small> Mesas </small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">POS mesas</li>
      </ol>
    </section>

    <div class="row">
      <section class="content">

    <!-- ZONAS -->
    <div class="col-md-3">
      <!-- Small boxes (Stat box) -->
          <div class="box box-widget widget-user-2">
            <a type="button" class="btn btn-box-tool" style="width:100%" data-widget="collapse">
            <div class="box-header with-border widget-user-header bg-green">
              <h1 >Zonas</h1>
            </div>
            </a>
            <div class="box-body">
              <?php foreach($zonas as $datos) { ?>
              <div class="box-footer">
                <button id="<?php echo $datos->zona_id;?>" type="submit" class="btn btn-success btn-block zona" style="font-size:35px;font-weight:bold;font-style:italic;"><?php echo $datos->zona_nombre;?></button>
              </div>
              <?php  }  ?>
            </div>
          </div>
            <!-- /.box-body -->
      </div>
          <!-- /.box -->

        <!-- /.box -->

        <div class="col-md-3">
          <div class="box box-widget widget-user-2">
            <a type="button" class="btn btn-box-tool" style="width:100%" data-widget="collapse">
            <div class="box-header with-border widget-user-header bg-red">
              <h1 >Mesas</h1>
            </div>
            </a>
              <div class="box-footer" id="div-mesas">
                <button type="submit" class="btn btn-danger btn-block send-pos" style="font-size:35px;font-weight:bold;font-style:italic;">Selecciona Zona</button>
              </div>
            </div>
            <!-- /.box-body -->
          </div>

        <!-- PRODUCT LIST -->
        <div class="col-md-6" id="div-cuenta">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2" style="margin-bottom:0px;" id="crear_cuenta">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Mesa </h3>
              <h5 class="widget-user-desc">Zona</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Cliente <span class="pull-right badge bg-blue">No hay</span></a></li>
              </ul>
            </div>
          </div>
          <div class="box box-widget widget-user-2" style="margin-bottom:0px;">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Mesa #5</h3>
              <h5 class="widget-user-desc">Zona terraza</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Cliente <span class="pull-right badge bg-blue">No hay</span></a></li>
                <li><a href="#">Mesero <span class="pull-right badge bg-aqua">Luis Garcia</span></a></li>
                <li><a href="#">Personas <span class="pull-right badge bg-green">12</span></a></li>
                <li><a href="#">Descripcion: Los clientes llegaron con reserva</a></li>
                <li><a href="#">Total <span class="pull-right badge bg-red">12.000</span></a></li>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Productos</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul class="products-list product-list-in-box">
              <li class="item">
                <div class="product-img">
                  <img src="http://localhost/alimenteq/pan/image/bread.png" alt="Product Image">
                </div>
                <div class="product-info">
                  <a href="javascript:void(0)" class="product-title">Samsung TV
                    <span class="label label-warning pull-right">$1800</span></a>
                  <span class="product-description">
                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                      </span>
                </div>
              </li>
              <!-- /.item -->
              <li class="item">
                <div class="product-img">
                  <img src="http://localhost/alimenteq/pan/image/bread.png" alt="Product Image">
                </div>
                <div class="product-info">
                  <a href="javascript:void(0)" class="product-title">Bicycle
                    <span class="label label-info pull-right">$700</span></a>
                  <span class="product-description">
                        26" Mongoose Dolomite Men's 7-speed, Navy Blue.
                      </span>
                </div>
              </li>
              <!-- /.item -->
              <li class="item">
                <div class="product-img">
                  <img src="http://localhost/alimenteq/pan/image/bread.png" alt="Product Image">
                </div>
                <div class="product-info">
                  <a href="javascript:void(0)" class="product-title">Xbox One <span
                      class="label label-danger pull-right">$350</span></a>
                  <span class="product-description">
                        Xbox One Console Bundle with Halo Master Chief Collection.
                      </span>
                </div>
              </li>
              <!-- /.item -->
              <li class="item">
                <div class="product-img">
                  <img src="http://localhost/alimenteq/pan/image/bread.png" alt="Product Image">
                </div>
                <div class="product-info">
                  <a href="javascript:void(0)" class="product-title">PlayStation 4
                    <span class="label label-success pull-right">$399</span></a>
                  <span class="product-description">
                        PlayStation 4 500GB Console (PS4)
                      </span>
                </div>
              </li>
              <!-- /.item -->
            </ul>
          </div>
          <div class="box-footer">
                <button type="submit" class="btn btn-primary">Agregar producto</button>
          </div>
          <!-- /.box-body -->
          <div class="box-header with-border">
            <div class="input-group">
              <input type="text" class="form-control" id="datasearch" placeholder="Digite el producto a buscar...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">Buscar</button>
              </span>
            </div><!-- /input-group -->
          </div>
          <div class="box-body with-border">
            <div id="box-reset">
            </div>
          </div>
          <!-- /.box-footer -->
        </div>


        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Productos</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul class="products-list product-list-in-box">
              <li class="item">
                <div class="product-img">
                  <img src="http://localhost/alimenteq/pan/image/bread.png" alt="Product Image">
                </div>
                <div class="product-info">
                  <a href="javascript:void(0)" class="product-title">Samsung TV
                    <span class="label label-warning pull-right">$1800</span></a>
                  <span class="product-description">
                        Samsung 32" 1080p 60Hz LED Smart HDTV.
                      </span>
                </div>
              </li>
              <!-- /.item -->
            </ul>
          </div>
          <div class="box-footer">
                <button type="submit" class="btn btn-primary">Agregar producto</button>
          </div>
          <!-- /.box-body -->
          <!-- /.box-footer -->
        </div>
        </div>
        </div>

        <!-- Modal CREAR cuenta -->
        <div class="modal fade" id="modal_cuenta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-header">
                    <h3 class="modal-title">Crear nueva cuenta</h3>
                </div>
              </div>
              <div class="modal-body">
                <form id="form-cuenta">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Personas</label>
                    <input type="number" name="personas" class="form-control required">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Mesero</label>
                    <input type="text" name="mesero" class="form-control">
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success btn-block" id="btn-cuenta">Guardar</button>
                <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
  // Ajax post
  $(document).ready(function()
  {
  var id;
  ///MOSTRAR
  $(".zona").click(function(){
    id = $(this).attr("id");
    $.ajax({
        url : "<?php echo site_url('Controller_pos/show_mesas/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          $( "#div-mesas" ).empty();
          data.forEach(function(element,key){
                    $("#div-mesas").append(
                      '<button type="submit" class="btn btn-danger btn-block mesa" style="font-size:35px;font-weight:bold;font-style:italic;" id="'+element.mesas_id+'">'+element.mesas_codigo+'</button>');
                })
        },
        error: function (jqXHR, textStatus, errorThrown){}
   });
   });

    $(document).on('click', '.mesa', function(){
      id = $(this).attr("id");
      $.ajax({
          url : "<?php echo site_url('Controller_pos/show_cuenta_mesa/')?>" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
            $( "#div-cuenta" ).empty();
            data.forEach(function(element,key){
                  if (element.cuenta_activo == 0) {
                    var div = '<div class="box box-widget widget-user-2 cuenta" style="margin-bottom:0px;" id="'+element.cuenta_id+'">';
                    div = div +  '<div class="widget-user-header bg-yellow">';
                    div = div +  '<h3 class="widget-user-username">MESA VACÍA </h3>';
                    div = div +  '<h5 class="widget-user-desc">Toca para crear cuenta</h5>';
                    div = div +  '</div>';
                    div = div +  '</div>';
                    $("#div-cuenta").append(div);
                  }
                  else if (element.cuenta_activo == 1) {
                    var div = '<div class="box box-widget widget-user-2" style="margin-bottom:0px;" id="'+element.cuenta_mesa+'">';
                    div = div +  '<div class="widget-user-header bg-yellow">';
                    div = div +  '<h3 class="widget-user-username">MESA '+element.cuenta_mesa+' LLENA </h3>';
                    div = div +  '<h5 class="widget-user-desc">Zona</h5>';
                    div = div +  '</div>';
                    div = div +  '<div class="box-footer no-padding">';
                    div = div +  '<ul class="nav nav-stacked">';
                    div = div +  '<li><a href="#">Cliente <span class="pull-right badge bg-blue">No hay</span></a></li>';
                    div = div +  '</ul>';
                    div = div +  '</div>';
                    div = div +  '</div>';
                    div = div +  '<div class="box box-warning">';
                    div = div +  '<div class="box-header with-border">';
                    div = div +  '<h3 class="box-title">Productos</h3>';
                    div = div +  '<div class="box-tools pull-right">';
                    div = div +  '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>';
                    div = div +  '</button>';
                    div = div +  '</div>';
                    div = div +  '</div>';
                    div = div +  '<div class="box-body">';
                    div = div +  '<ul class="products-list product-list-in-box">';
                    div = div +  '<li class="item">';
                    div = div +  '<div class="product-img">';
                    div = div +  '<img src="http://localhost/alimenteq/pan/image/bread.png" alt="Product Image">';
                    div = div +  '</div>';
                    div = div +  '<div class="product-info">';
                    div = div +  '<a href="javascript:void(0)" class="product-title">Samsung TV<span class="label label-warning pull-right">$1800</span></a><span class="product-description">Samsung 32" 1080p 60Hz LED Smart HDTV.</span></div></li><!-- /.item --></ul>';
                    div = div +  '</div>';
                    div = div +  '<div class="box-footer">';
                    div = div +  '<button type="submit" class="btn btn-primary">Agregar producto</button>';
                    div = div +  '</div>';
                    div = div +  '</div>';

                    $("#div-cuenta").append(div);
                }
            })
          },
          error: function (jqXHR, textStatus, errorThrown){}
     });
   });///CIERRE DIBUJO DE CUENTAS

   ///CREACIÓN DE CUENTAS
$(document).on('click', '.cuenta', function(){
id = $(this).attr("id");
$('#modal_cuenta').modal('show');
});
////////////////////////////
////GUARDAR MODAL CREAR CUENTA
$("#btn-cuenta").click(function(){
  $.ajax({
       url : "<?php echo site_url('Controller_pos/crear_cuenta/')?>" + id,
       type: "POST",
       data: $('#form-cuenta').serialize(),
       dataType: "JSON",
       success: function(data)
       {
         //alert(id);
        $('#modal_cuenta').modal('hide');
        //location.reload();
       },
       error: function (jqXHR, exception) {},
  });

  $.ajax({
      url : "<?php echo site_url('Controller_pos/show_cuenta/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $( "#div-cuenta" ).empty();
        data.forEach(function(element,key){
                if (element.cuenta_activo == 1) {
                var div = '<div class="box box-widget widget-user-2" style="margin-bottom:0px;" id="'+element.cuenta_mesa+'">';
                div = div +  '<div class="widget-user-header bg-yellow">';
                div = div +  '<h3 class="widget-user-username">MESA '+element.cuenta_mesa+' OCUPADA </h3>';
                div = div +  '<h5 class="widget-user-desc">Zona</h5>';
                div = div +  '</div>';
                div = div +  '<div class="box-footer no-padding">';
                div = div +  '<ul class="nav nav-stacked">';
                div = div +  '<li><a href="#">Cliente <span class="pull-right badge bg-blue">No hay</span></a></li>';
                div = div +  '</ul>';
                div = div +  '</div>';
                div = div +  '</div>';
                div = div +  '<div class="box box-warning">';
                div = div +  '<div class="box-header with-border">';
                div = div +  '<h3 class="box-title">Productos</h3>';
                div = div +  '<div class="box-tools pull-right">';
                div = div +  '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>';
                div = div +  '</button>';
                div = div +  '</div>';
                div = div +  '</div>';
                div = div +  '<div class="box-body">';
                div = div +  '<ul class="products-list product-list-in-box">';
                div = div +  '<li class="item">';
                div = div +  '<div class="product-img">';
                div = div +  '<img src="http://localhost/alimenteq/pan/image/bread.png" alt="Product Image">';
                div = div +  '</div>';
                div = div +  '<div class="product-info">';
                div = div +  '<a href="javascript:void(0)" class="product-title">Samsung TV<span class="label label-warning pull-right">$1800</span></a><span class="product-description">Samsung 32" 1080p 60Hz LED Smart HDTV.</span></div></li><!-- /.item --></ul>';
                div = div +  '</div>';
                div = div +  '<div class="box-footer">';
                div = div +  '<button type="submit" class="btn btn-primary">Agregar producto</button>';
                div = div +  '</div>';
                div = div +  '</div>';
                $("#div-cuenta").append(div);
            }
        })
      },
      error: function (jqXHR, textStatus, errorThrown){}
 });

});

 });//cierre document ready
  </script>
