


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>35</h3>

              <p>Precio Produción</p>
            </div>
            <div class="icon">
              <i class="ion ion-social-usd"></i>
            </div>
            <a href="<?= base_url('/Pedidos/pending')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <?php
              /*
              $total = 0;

              foreach ($venta->result() as $value) {
              $total = $total + $value->ped_total_final;
            }
            */
            ?>
            <h3><sup style="font-size: 20px">$</sup>750000</h3>

            <p>Costo</p>
          </div>
          <div class="icon">
            <i class="ion ion-social-usd"></i>
          </div>
          <a class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>1650000</h3>

              <p>Costo</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-basket"></i>
            </div>
            <a  class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>36</h3>

              <p> Capacidad</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-flame"></i>
            </div>
            <a  class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">

            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID Producto</th>
                  <th>Producto</th>
                  <th>Receta</th>
                  <th>Cantidad</th>
                  <th>Porcentaje</th>
                </tr>
                </thead>
                <tbody>
                </tr>
                  <?php foreach($info as $datos) { ?>
                  <tr>
                      <td><?php echo $datos->prod_cat;?><?php echo $datos->prod_id;?> </td>
                      <td><?php echo $datos->prod_nombre;?> </td>
                      <td><?php echo $datos->rec_nombre;?> </td>
                      <td><?php echo intval($datos->rec_unidades*$datos->prodt_x);?> </td>
                      <td><?php echo intval(100*$datos->prodt_x);?>%</td>
                  </tr>
                  <?php  }  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID Producto</th>
                    <th>Producto</th>
                    <th>Receta</th>
                    <th>Cantidad</th>
                    <th>Porcentaje</th>
                  </tr>
                </tfoot>
              </table>

            </div>
            <!-- /.box -->

          </div>
        </div>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
