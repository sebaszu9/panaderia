<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Detalles del Movimiento Producto </h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <?php foreach($entrada as $datosd) { ?>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Información</h3>
          </div>

          <div class="box-body">

            <div class="form-group col-md-3">
              <label>Concepto</label>
              <select class="form-control" name="panaderia" readonly>
                <option value="<?= $datosd->cm_concepto;?>"><?= $datosd->c_nombre;?></option>
              </select>
            </div>

            <div class="form-group col-md-3">
              <label>Panaderia</label>
              <select class="form-control" name="panaderia" readonly>
                <option value="none"><?= $datosd->bod_nombre;?></option>
              </select>
            </div>

            <div class="form-group col-md-3">
              <label for="exampleInputEmail1">Documento</label>
              <input type="text" name="fecha" value="" class="form-control"  readonly>
            </div>

            <?php foreach ($proveedor as $prov) { ?>
            <div class="form-group col-md-3">
              <label>Proveedor</label>
              <select class="form-control " name="proveedor" readonly>
                <option value="none"><?= $prov->prov_nombre;?></option>
              </select>
            </div>
            <?php } ?>

            <div class="form-group col-md-3">
              <label for="exampleInputEmail1">Fecha</label>
              <input type="text" name="fecha"  id="datepicker" value="<?= $datosd->cm_fecha;?>" class="form-control"  readonly>
            </div>



            <div class="form-group col-md-6">
              <div class="form-group ">
                <label for="exampleInputEmail1">Observaciones</label>
                <input type="text" name="descripcion"  id="datepicker" class="form-control" value="<?= $datosd->cm_obs;?>" readonly="true">
              </div>
            </div>


            <div class="form-group col-md-2">
              <div class="form-group ">
                <label for="exampleInputEmail1" style="color:white">Pillao</label>
                <button type="submit" class="btn btn-block btn-danger deletas" value="<?= $datosd->cm_id;?>"> Eliminar entrada <i class="fa fa-trash"></i></button>
              </div>
            </div>

          </div>

          <div class="form-group" id="pololo">

          </div>

          <div class="box-body">

            <div class="col-md-12">

              <table class="table table-bordered order-table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Producto</th>
                    <th>Cant empaques.</th>
                    <th>Costo</th>
                    <th>Eliminar</th>
                  </tr>
                </thead>
                <tbody id="loadcarttable">
                  <?php
                        $i = 1;
                      foreach ($productos as $value) {
                        echo "<tr>";
                        echo "<td> ".$i." </td>";
                        echo "<td> ".$value->prod_nombre." </td>";
                        echo "<td> ".$value->mp_cant." </td>";
                        echo "<td> ".number_format($value->mp_costo)." </td>";
                        echo "<td> <button class='btn btn-default delete-item-mov' value='".$value->mp_id."'><i class='fa fa-trash'></i></button> </td>";
                        echo "<tr>";
                        $i++;
                      }
                  ?>
                </tbody>
              </table>
            </div>
          </div>

          <script>
            $(".deletas").click(function() {

              swal({
                title: 'Eliminar',
                text: " ¿Está  seguro que quiere eliminar este registro permanentemente de la base de datos?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar.'
              }).then((result) => {
                if (result.value) {
                  var code  = $(this).val();
                  var carrito = { "safety"   : true, "concepto" : code };
                  $.ajax({ data:  carrito, url:' <?= base_url('Movimiento/producto/delete/register')?>',type:  'post',
                     beforeSend: function () {$("#pololo").html("Cargando, espere por favor...");},
                     success:  function (response) {
                       //alert(response)
                       var response = JSON.parse(response);
                       if (response.status==true) {
                         alert('El registro ha sido eliminado exitosamente!')
                         window.location.replace("<?= base_url('Inventario/movimiento/index') ?>");
                       }
                      }
                   }).fail( function(error) {
                      alert(JSON.stringify(error))
                   })


                }
              })

            });

            $(".delete-item-mov").click(function() {

              swal({
                title: 'Eliminar',
                text: " ¿Desea eliminar un item de movimiento?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar.'
              }).then((result) => {
                if (result.value) {
                  var id = $(this).val()
                  var carrito = { "safety"   : true, "item" : id };
                  $.ajax({ data:  carrito, url:' <?= base_url('Movimiento/producto/delete/item')?>',type:  'post',
                     beforeSend: function () {$("#pololo").html("Cargando, espere por favor...");},
                     success:  function (response) {
                      // alert(response)
                       var response = JSON.parse(response);
                       if (response.status==true) {
                         swal("El item ha sido eliminado exitosamente!");
                         //alert('El item ha sido eliminado exitosamente!')
                         //window.location.replace("<?= base_url('Inventario/movimiento/index') ?>");
                       }
                      }
                   }).fail( function(error) {
                      alert(JSON.stringify(error))
                   })
                }
              })

            });
          </script>
        </div>
        <?php  }  ?>

      </div>
    </div>
      <!-- /.box-body -->
  </section>
  <!-- /.content -->



</div>
<!-- /.content-wrapper -->
