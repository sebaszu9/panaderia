<div class="content-wrapper">
  <!-- Main content -->
  <section class="content-header">
    <h1>
      Inventario
    </h1>
  </section>
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">

            <form method="post" action="<?= base_url('Inventario/stock/materia')?>" >

              <div class="form-group col-md-3">
                <label>Sede de Panaderia</label>
                <select class="form-control" name="sede" readonly="">
                  <option value="0"></option>
                  <?php foreach($sede as $datos) { ?>
                    <option value="<?php echo  $datos->bod_id;?>"><?php echo  $datos->bod_nombre;?></option>
                  <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label style="color:white;">.</label><br/>
                <input type="submit" class="btn btn-primary" value="Cargar">
              </div>

              <input type="hidden" name="filtrar" value="true">

            </form>

            <form class="pull-right" method="post" action="<?= base_url('/Inventario/excel/materia')?>">
              <input type="hidden" name="excel" value="true">
              <input type="hidden" name="data" value="<?php echo $panaderia; ?>">
              <button class="btn btn-success">Exportar en Excel <i class="fa fa-file-excel-o"></i></button>
            </form>

            <form class="pull-right" method="post" action="<?= base_url('/Inventario/pdf/materia')?>">
              <input type="hidden" name="excel" value="true">
              <input type="hidden" name="data" value="<?php echo $panaderia; ?>">
              <button class="btn btn-danger">Exportar en PDF <i class="fa fa-file-excel-o"></i></button>
            </form>


          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Costo</th>
                <th>Empaque</th>
                <th>Cantidad</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($inventario as $datos) { ?>
                <tr>
                    <td><?php echo $datos->mat_id;?> </td>
                    <td><?php echo $datos->mat_nombre;?> </td>
                    <td><?php echo $datos->mat_costo;?> </td>
                    <td><?php echo number_format(($datos->sm_cant/$datos->unidad_escala)/$datos->mat_ue);?> </td>
                    <td><?php echo number_format($datos->sm_cant/$datos->unidad_escala).' '.$datos->unidad_nombre;?> </td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  <th>Categoria</th>
                  <th>Descripcion</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
