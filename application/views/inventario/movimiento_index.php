<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado de Movimientos de Inventario
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Inventario</a></li>
      <li class="active">Movimiento</li>
    </ol>
  </section>


  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <?php if ($this->session->flashdata('mensaje_item'))  { ?>
          <div class="alert alert-success alert-dismissible" id="hidemiss">
            <p><?= $this->session->flashdata('mensaje_item'); ?></p>
          </div>
          <script>$( "#hidemiss" ).fadeOut( 3600, "linear" );</script>
        <?php } ?>
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
              <a href="<?php echo base_url('Inventario/movimiento/productos/form') ?>" class="btn btn-success" ><i class="fa fa-search"></i> Nuevo movimiento de Productos</a>
              <a href="<?php echo base_url('Inventario/movimiento/materias/form') ?>" class="btn btn-success" ><i class="fa fa-search"></i> Nuevo movimiento de Materias</a>
          </div>

          <div class="box-body">
            <form action="<?= base_url('Inventario/movimiento/search') ?>" method="POST" >

              <div class="form-group col-md-3">
                <label>De</label>
                <select class="form-control" name="parte">
                  <option value="1">Producto</option>
                  <option value="2">Materia Prima</option>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label>Sede de Panaderia</label>
                <select class="form-control" name="panaderia">
                  <option value="0"></option>
                <?php foreach($panaderias as $datosd) { ?>
                  <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label>Concepto</label>
                <select class="form-control" name="concepto">
                  <option value="">Todos</option>
                <?php foreach($concepto as $datos) { ?>
                  <?php  if ($datos->c_tipo==1) { $tipo = "Entrada"; }else { $tipo = "Salida";   }?>
                  <option value="<?= $datos->c_id;?>"><?= $datos->c_nombre;?> (<?php echo $tipo; ?>)</option>
                <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label for="exampleInputEmail1">Fecha inicial</label>
                <input type="text" name="fecha1"  id="datepicker" class="form-control"  readonly>
              </div>

              <div class="form-group col-md-3">
                <label for="exampleInputEmail1"> Fecha final</label>
                <input type="text" name="fecha2"  id="datepicker2" class="form-control"  readonly>
              </div>

              <div class="form-group col-md-3">
                <div class="form-group ">
                  <label for="exampleInputEmail1"> </label>
                  <button type="submit" class="btn btn-block btn-primary" ><i class="fa fa-search"></i> Consultar</button>
                </div>
              </div>

            </form>
          </div>

        </div>
      </div>

        <div class="col-md-12">
        <?php if ($this->session->flashdata('search'))  { ?>

          <?php $info = $this->session->flashdata('search') ?>

          <div class="box box-primary">

            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Sede</th>
                    <th>Concepto</th>
                    <th>Tipo</th>
                    <th>Proveedor</th>
                    <th>Fecha</th>
                    <th>Usuario</th>
                    <th>Detalles</th>
                  </tr>
                </thead>
                <tbody>
                </tr>
                  <?php foreach($info as $datos) { ?>
                  <tr>
                      <td><?php echo $datos->cm_id;?> </td>
                      <td><?php echo $datos->bod_nombre;?> </td>
                      <td><?php echo $datos->c_nombre;?> </td>
                      <td><?php if ($datos->cm_tipo==1) { echo "<b style='color:green'>Entrada</b>"; } else{ echo "<b style='color:red'>Salida</b>"; }?> </td>
                      <td><?php echo $datos->prov_nombre;?> </td>
                      <td><?php echo $datos->cm_fecha;?> </td>
                      <td><?php echo $datos->usr_nombre;?> </td>
                      <?php if ($datos->cm_parte==2) { ?>
                        <td><a href="<?php echo base_url('Movimiento/materia/detalles/').$datos->cm_id; ?>" class="btn  btn-success" target="_blank">Detalles</a></td>
                      <?php  } else { ?>
                        <td><a href="<?php echo base_url('Movimiento/producto/detalles/').$datos->cm_id; ?>" class="btn  btn-success" target="_blank">Detalles</a></td>
                      <?php  }?>

                  </tr>
                  <?php  }  ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Proveedor</th>
                    <th>Fecha</th>
                    <th>Editar</th>
                    <th>Detalles</th>
                  </tr>
                </tfoot>
              </table>

            </div>

          </div>
        <?php } ?>



      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->
