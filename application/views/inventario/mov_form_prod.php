<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> Movimiento del Producto </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Inventario</a></li>
      <li class="active">Movimiento</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">

      <form action="<?= base_url('Inventario/movimiento/productos/create') ?>" method="POST" >
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Nuevo de Movimiento de Inventario</h3>
              <?php echo validation_errors(); ?>

              <?php if ($this->session->flashdata('mensaje'))  { ?>
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p><i class="icon fa fa-exclamation-circle"></i> <?= $this->session->flashdata('mensaje') ?></p>
                </div>
              <?php } ?>

            </div>

            <div class="box-body">

              <div class="form-group col-md-3">
                <label for="exampleInputEmail1">Fecha</label>
                <input type="text" name="fecha"  id="datepicker" value="<?php echo date('Y/m/d'); ?>" class="form-control">
              </div>

              <div class="form-group col-md-3">
                <label for="exampleInputEmail1">Documento</label>
                <input type="text" name="documento" value="<?php echo set_value('documento'); ?>" class="form-control">
              </div>

              <div class="form-group col-md-3">
                <label>Panaderia</label>
                <select class="form-control" name="panaderia">
                  <option value=""></option>
                <?php foreach($panaderias as $datosd) { ?>
                  <option value="<?= $datosd->bod_id;?>"><?= $datosd->bod_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label>Concepto</label>
                <select class="form-control" name="concepto" required="true" id="selecttipo">
                  <option value=""></option>
                <?php foreach($concepto as $datos) { $tipo = ""; ?>
                  <?php  if ($datos->c_tipo==1) { $tipo = "Entrada"; }else { $tipo = "Salida";   }?>
                  <option value="<?= $datos->c_id;?>" data-tipo="<?= $datos->c_tipo;?>"><?= $datos->c_nombre;?> (<?php echo $tipo; ?>)</option>
                <?php  }  ?>
                </select>
                <input type="hidden" name="tipo" id="tipomov" class="form-control" value>
              </div>

              <div class="form-group col-md-3">
                <label>Proveedor</label>
                <select class="form-control select2" name="proveedor">
                <?php foreach($proveedor as $datos) { ?>
                  <option value="<?= $datos->prov_id;?>"><?= $datos->prov_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-6">
                <div class="form-group ">
                  <label for="exampleInputEmail1">Observaciones</label>
                  <input type="text" name="descripcion"  id="datepicker" v class="form-control" placeholder="Descripcion ...">
                </div>
              </div>

            </div>

            <div class="box-header with-border">
              <h3 class="box-title">Productos</h3>
            </div>

            <div class="box-footer with-border">

              <div class="form-group col-md-3">
                <label>Producto</label>
                <select class="form-control select2" name="producto" id="selecprod">
                <?php foreach($productos as $datosq) { ?>
                  <option value="<?= $datosq->prod_id;?>" data-cat="<?= $datosq->prod_nombre;?>"><?= $datosq->prod_nombre;?></option>
                <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label for="exampleInputEmail1">Cantidad</label>
                <input type="number" name="cantidad" id="cantidad" class="form-control" value="1">
              </div>

              <div class="form-group col-md-3">
                <label for="exampleInputEmail1">Precio</label>
                <input type="number" name="precio" id="precio" class="form-control">
              </div>

              <div class="form-group col-md-3">
                <label style="color:white">.</label>
                <button type="button" class="btn btn-block btn-success addcart"><i class="fa fa-cart-plus"></i></button>
              </div>

              <div class="col-md-12"><hr></div>

              <div class="col-md-12">

                <table class="table table-bordered order-table">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Producto</th>
                      <th>Cant.</th>
                      <th>Costo</th>
                      <th>Eliminar</th>
                    </tr>
                  </thead>
                  <tbody id="loadcarttable">
                    <?php
                        if ($this->session->userdata('pedido_producto')) {

                          $i = 1;
                          $carritop = $this->session->userdata('pedido_producto');

                          foreach ($carritop as $value) {
                            $posicion = $i-1;
                            echo "<tr>";
                            echo "<td> ".$i." </td>";
                            echo "<td> ".$value['nombre']." </td>";
                            echo "<td> ".$value['cantidad']." </td>";
                            echo "<td> ".number_format($value['costo'])." </td>";
                            echo '<td> <button type="button" class="btn btn-info deletet" value="'.$posicion.'"><i class="fa fa-trash"></i></button></td>';
                            echo "<tr>";
                            $i++;
                          }
                        }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="box-footer with-border">

              <div class="form-group col-md-2">
                <div class="form-group ">
                  <input type="hidden" value="1" name="parte">
                  <button type="submit" class="btn btn-block btn-info" ><i class="fa fa-check-circle"></i> Guardar</button>
                  <br>
                  <a href="<?php echo base_url('Inventario/movimiento/index') ?>" class="btn btn-default" >Atrás</a>
                </div>
              </div>

            </div>


          </div>
        </div>
      </form>

    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->


<script>

$(document).on('click', '.addcart', function(){

  var code  = $("#selecprod").val();
  var cantd = $("#cantidad").val();
  var costo = $("#precio").val();
  var name  = $("#selecprod").find(':selected').data("cat");

  if (costo==='') {   costo = 0;  }

    var carrito = {
      "safety"   : true,
      "nombre"   : name,
      "producto" : code,
      "cantidad" : cantd,
      "costo"    : costo
    };

    $.ajax({ data:  carrito, url:' <?= base_url('Controller_productos/addcart_prod')?>',type:  'post',
       beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
       success:  function (response) {$("#loadcarttable").html(response); }
     })

});

$( "#selecttipo" ).change(function() {
  var select = $(this).find(':selected').data("tipo");
  $("#tipomov").val(select);
});

$(document).on('click', '.deletet', function(){
  var code  = $(this).val();
  var carrito = {
    "safety"   : true,
    "delete"   : 54,
    "posicion"   : code,
  };

  $.ajax({ data:  carrito, url:' <?= base_url('Controller_productos/delete_array')?>',type:  'post',
     beforeSend: function () {$("#loadcarttable").html("Cargando, espere por favor...");},
     success:  function (response) {$("#loadcarttable").html(response); }
   })

});

</script>
