<div class="content-wrapper">
  <!-- Main content -->
  <section class="content-header">
    <h1> Inventario </h1>
  </section>
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">

            <form method="post" action="<?= base_url('Inventario/stock/productos')?>" >

              <div class="form-group col-md-3">
                <label>Sede de Panaderia</label>
                <select class="form-control" name="sede" readonly="">
                  <option value="0"></option>
                  <?php foreach($sede as $datos) { ?>
                    <option value="<?php echo  $datos->bod_id;?>"><?php echo  $datos->bod_nombre;?></option>
                  <?php  }  ?>
                </select>
              </div>

              <div class="form-group col-md-3">
                <label style="color:white;">.</label><br/>
                <input type="submit" class="btn btn-primary" value="Cargar">
              </div>

              <input type="hidden" name="filtrar" value="true">

            </form>

            <form class="pull-right" method="post" action="<?= base_url('/Inventario/excel/productos')?>">
              <input type="hidden" name="excel" value="true">
              <input type="hidden" name="data" value="<?php echo $panaderia; ?>">
              <button class="btn btn-success">Exportar en Excel <i class="fa fa-file-excel-o"></i></button>
            </form>

            <form class="pull-right" method="post" action="<?= base_url('/Inventario/pdf/productos')?>">
              <input type="hidden" name="excel" value="true">
              <input type="hidden" name="data" value="<?php echo $panaderia; ?>">
              <button class="btn btn-danger">Exportar en PDF <i class="fa fa-file-excel-o"></i></button>
            </form>


          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Costo</th>
                <th>Cantidad</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($inventario as $datos) { ?>
                <tr>
                    <td><?php echo $datos->prod_id;?>     </td>
                    <td><?php echo $datos->prod_nombre;?> </td>
                    <td><?php echo $datos->prod_costo;?>  </td>
                    <td><?php echo $datos->sp_cant;?>  </td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  <th>Categoria</th>
                  <th>Descripcion</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
