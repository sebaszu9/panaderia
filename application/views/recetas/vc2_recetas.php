<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php foreach($receta as $datos) { ?>

      <!-- Main content -->
  <section class="content">
        <!-- Small boxes (Stat box) -->
    <div class="row">
     <div class="col-md-6">
      <div class="box box-primary">
        <div class="row">
          <!-- Main Row -->
          <div class="col-md-12">
            <!-- general form elements -->

                <div class="box-header with-border">
                  <h3 class="box-title">Detalles del la Receta</h3>
                </div>
                    <!-- form start -->
                      <div class="box-body">
                        <table id="table-alla-order" class="table table-bordered table-striped">
                          <tbody>
                            <tr>
                              <td>ID<?php echo $datos->rec_id;?></td>
                              <td>
                                Nombre:
                                <?php echo $datos->rec_nombre;?>
                              </td>
                              <td>Observaciones:
                                <?php echo $datos->rec_obs; ?>
                              </td>
                              <td>
                                <button  id="<?php echo $datos->rec_id;?>" class="btn btn-warning btn-sm edit" >Editar</button>
                                <button  id="<?php echo $datos->rec_id;?>" class="btn btn-danger btn-sm delete_rec" >Eliminar</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
              <!-- /.box -->
          </div>
          <!-- /.col-md-->
          <?php  }  ?>


      <div class="col-md-12" >
        <!-- general form elements -->
        <div class="box-header with-border">
          <h3 class="box-title">Ingredientes Base de la Receta  </h3>
        </div>
          <div class="box-body">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Concepto</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Costo</th>
                  <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                </tr>
                <?php

                $acumulador = 0;
                $peso = 0;

                foreach($ingredientes as $datos2) {

                $ingrediente = $this->model_materias->load_ing_comp_per_mat($datos2->mat_id);


                ?>
                  <tr>
                      <td><?= $datos2->cr_nombre; ?></td>
                      <td><?= $datos2->mat_codigo; ?> <?= $datos2->mat_nombre; ?></td>
                      <td> <?=  number_format($datos2->ing_cant/$datos2->unidad_escala); ?> <?= $datos2->unidad_nombre; ?>s</td>
                      <td>
                        <?php

                          if ($datos2->mat_tipo==1) {
                            echo '<button class="btn btn-success showtr" value="'.$datos2->mat_id.'">+</button>';
                          }
                          else {
                            $costo =(($datos2->ing_cant/$datos2->unidad_escala)*($datos2->mat_costo)/($datos2->mat_ue));
                            $acumulador = $acumulador + $costo;
                            $peso = $peso + $datos2->ing_cant/$datos2->unidad_escala;
                            echo  "$".number_format($costo);
                          }

                        ?>
                     </td>
                      <td>
                        <a href="<?php echo base_url('Materiasprimas/details')."/".$datos2->mat_id; ?>" class="btn btn-success"title="Detalles" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <button  id="<?=$datos2->ing_id; ?>" class="btn btn-warning btn-sm cantidad_base"  title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        <button  id="<?=$datos2->ing_id; ?>" class="btn btn-danger btn-sm delete_mov" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                      </td>
                  </tr>
                  </tbody>
                  <tbody id="tr<?php echo $datos2->mat_id; ?>" style="display:none;">
                  <?php

                  $acumuladorx = 0; $gramo = 0; $costox = 0;

                  foreach ($ingrediente as $ing) {
                    $datos2->ing_cant/$datos2->unidad_escala;//cantidad de ingrediente compuesto

                    $gramo = $ing->inc_cantidad/$ing->unidad_escala;
                    $gramisimo = (($datos2->ing_cant/$datos2->unidad_escala)*$gramo)/$datos2->mat_ue;

                    $costox =($gramisimo*($ing->mat_costo)/($ing->mat_ue));
                    $acumuladorx = $acumuladorx + $costox;

                    echo "<tr >" ;
                    echo "<td></td>";
                    echo "<td> $ing->mat_nombre </td>";
                    echo "<td> $gramisimo gramos </td>";
                    echo "<td> ".number_format($costox)." </td>";
                    echo "</tr>";



                  }

                  $acumulador = $acumulador + $acumuladorx;

                  $peso = $peso +$gramo;

                  ?>
                  </tbody>
                  <?php  }  ?>

            </table>
            <table class="table table-bordered table-striped">
              <tbody>
                <tr>
                  <th><h3>Costo Total Base </h3> <h3>$<?= number_format($acumulador); ?></h3></th>
                  <th><h3>Peso del moje </h3> <h3><?= number_format($peso); ?> grs</h3></th>
                </tr>
                <tr>
                  <th><a id="add" class="btn btn-primary btn-md" ><i class="fa fa-plus" aria-hidden="true"></i> Ingrediente</a>
                  <th><a href="<?php echo base_url(); ?>Recetas/index" class="btn btn-success btn-md" >Guardar</a>
                </tr>
              </tbody>

            </table>
          </div><!-- /.box -->
    </div>
      <!-- /.col-md-->
      </div>
    </div>
  </div>


  <div class="col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Productos de la Receta</h3>
        <a id="addProduct" class="btn btn-primary btn-md" ><i class="fa fa-plus" aria-hidden="true"></i> Producto</a>
        <a id="addMateria" class="btn btn-primary btn-md" ><i class="fa fa-plus" aria-hidden="true"></i> Materia</a>
      </div>
      <?php foreach($productos_rec as $datos5) {
        $acumulador1= $acumulador;
        $peso1= $peso;?>
        <div class="box box-primary">
          <div class="box-header with-border">
            <table class="table ">
              <thead>
                <tr>
                  <th><h3 class="box-title"><?php echo $datos5->prod_id; ?> <?php echo $datos5->prod_nombre; ?></h3></th>
                  <th><h3 class="box-title">Cantidad:<?php echo $datos5->pr_cant; ?></h3></th>
                  <th><h3 class="box-title">Peso unidad: <?php echo number_format($peso/$datos5->pr_cant); ?>grs</h3></th>
                  <th><button  id="<?=$datos5->pr_id; ?>" class="btn btn-warning btn-sm cantidad_prod"  title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></th>
                  <th><button  id="<?=$datos5->pr_id; ?>" class="btn btn-danger btn-sm delete_prod" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button></th>
                  <th><a id="<?php echo $datos5->pr_id; ?>" class="btn btn-primary btn-md addIng" ><i class="fa fa-plus" aria-hidden="true"></i></a></th>
                </tr>
              </thead>
            </table>
          <table class="table ">
            <tr>
              <th><h3 class="box-title">Descripción</h3>: <h3 class="box-title"><?php echo $datos5->pr_detalles; ?></h3></th>
              <th><button  id="<?=$datos5->pr_id; ?>" class="btn btn-warning btn-sm pique"  title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></th>
            </tr>
          </table>
            <?php $k = 0;
            foreach($ingredientes_prod as $contador) {
                  if ($contador->ing_pr_prod==$datos5->pr_id){
                  $k = $k + 1;
                  }}
                  if ($k > 0 ) {
            ?>
            <table  class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Concepto</th>
                  <th>Sub Ing</th>
                  <th>Cantidad</th>
                  <th>Costo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
              </tr>
              <?php foreach($ingredientes_prod as $datos7) {
                    if ($datos7->ing_pr_prod==$datos5->pr_id){
              ?>
                <tr>
                    <td><?php echo $datos7->mat_id;?> </td>
                    <td><?php echo $datos7->cr_nombre;?> </td>
                    <td><?php echo $datos7->mat_nombre;?> </td>
                    <td><?=  number_format($datos7->ing_pr_cant/$datos7->unidad_escala); ?> <?= $datos7->unidad_nombre; ?>s</td>
                    <td><?php
                              $costo =(($datos7->ing_pr_cant/$datos7->unidad_escala)*($datos7->mat_costo)/($datos7->mat_ue));
                              $acumulador1 = $acumulador1 + $costo;
                              $peso1 = $peso1 + $datos7->ing_pr_cant/$datos7->unidad_escala;
                              echo  "$".number_format($costo);
                    ?></td>
                    <td><a href="<?php echo base_url('Materiasprimas/details')."/".$datos7->mat_id; ?>" class="btn btn-success"title="Detalles" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a id="<?php echo $datos7->ing_pr_id;?>" class="cantidad_pr btn btn-warning" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a id="<?php echo $datos7->ing_pr_id;?>" class="delete_pr btn btn-danger"title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
            <?php } } ?>
              </tbody>
            </table>
            <?php } ?>
            <hr>
            <table  class="table table-bordered table-striped">
            <tbody>
              <tr>
                <td><b>Costo Total</b>: $<?= number_format($acumulador1); ?></td>
                <td><b>Costo/U:</b> $<?= number_format($acumulador1/$datos5->pr_cant); ?></td>
                <td><b>Precio</b> $<?php echo $datos5->prod_precio; ?></td>
                <td><b>Peso con relleno:</b><?php echo number_format($peso1/$datos5->pr_cant); ?>grs</td>
                <td><b>Utilidad</b>%<?php echo number_format(($datos5->prod_precio-($acumulador1/$datos5->pr_cant))/$datos5->prod_precio*100); ?></td>
              </tr>
            </tbody>
            </table>
          </div>
        </div>
        <?php } ?>


        <?php foreach($materias_rec as $datos8) { ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <table class="table ">
                <thead>
                  <tr>
                    <th><h3 class="box-title">ID <?php echo $datos8->mat_id; ?></h3></th>
                    <th><h3 class="box-title">Tipo: Materia reproceso</h3></th>
                    <th><h3 class="box-title">Nombre:<?php echo $datos8->mat_nombre; ?></h3></th>
                    <th><h3 class="box-title"><?=  number_format($datos8->mr_cant/$datos8->unidad_escala); ?> <?= $datos8->unidad_nombre; ?>s</h3></th>
                    <th><button  id="<?=$datos8->mr_id; ?>" class="btn btn-warning btn-sm cantidad_mat" title="Editar cantidad"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></th>
                    <th><button  id="<?=$datos8->mr_id; ?>" class="btn btn-danger btn-sm delete_mat" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <?php } ?>

    </div>
  </div>
</div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>


<!-- Bootstrap modal ADICIONAR MATERIA -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Adicionar Ingrediente Base</h3>
          </div>
          <div class="modal-body form">
               <form action="#" id="form" class="form-horizontal">
                   <div class="form-body">
                     <div class="form-group">
                         <label class="control-label col-md-3">Materia Prima</label>
                         <div class="col-md-9">
                           <select name="materia" class="form-control" style="width: 300px;">
                             <?php foreach($materias as $datos3) { ?>
                               <option value="<?=$datos3->mat_id; ?>"><?=$datos3->mat_nombre; ?> Unidad:<?=$datos3->unidad_nombre; ?></option>
                                <?php } ?>
                           </select>
                         </div>
                     </div>
                     <input name="receta" value="<?=$datos->rec_id; ?>" class="form-control" type="hidden">
                     <div class="form-group">
                         <label class="control-label col-md-3">Concepto</label>
                         <div class="col-md-9">
                           <select name="concepto" class="form-control" style="width: 300px;">
                             <?php foreach($conceptos as $concepto) { ?>
                               <option value="<?=$concepto->cr_id; ?>"><?=$concepto->cr_nombre; ?></option>
                                <?php } ?>
                           </select>
                         </div>
                     </div>
                     <div class="form-group">
                       <label class="control-label col-md-3">Cantidad</label>
                       <div class="col-md-9">
                           <input name="cantidad" placeholder="Cantidad" class="form-control" type="number">
                           <span class="help-block"></span>
                       </div>
                    </div>
                </div>
               </form>
            </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              <button type="button" id="btnSave"  class="btn btn-primary">Guardar</button>
          </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<!-- /.content-wrapper -->

<!-- Bootstrap modal ADICIONAR MATERIA COMO RESULTADO-->
<div class="modal fade" id="modal_mat" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Adicionar Materias Primas resultado de la receta</h3>
          </div>
          <div class="modal-body form">
               <form action="#" id="formMatRec" class="form-horizontal">
                   <div class="form-body">
                     <div class="form-group">
                         <label class="control-label col-md-3">Materia Prima</label>
                         <div class="col-md-9">
                           <select name="materia" class="form-control select2" style="width: 300px;">
                             <?php foreach($materias as $datos6) { ?>
                               <option value="<?=$datos6->mat_id; ?>"><?=$datos6->mat_nombre; ?> Unidad:<?=$datos6->unidad_nombre; ?></option>
                                <?php } ?>
                           </select>
                         </div>
                     </div>
                     <input name="receta" value="<?=$datos->rec_id; ?>" class="form-control" type="hidden">
                     <div class="form-group">
                       <label class="control-label col-md-3">Cantidad</label>
                       <div class="col-md-9">
                           <input name="cantidad" placeholder="Cantidad" class="form-control" type="number">
                           <span class="help-block"></span>
                       </div>
                    </div>
                </div>
               </form>
            </div>
          <div class="modal-footer">
              <button type="button" id="btnSaveMat"  class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
      </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<!-- /.content-wrapper -->

<!-- Bootstrap modal ADICIONAR PRODUCTO -->
<div class="modal fade" id="modal_Product" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Adicionar Producto a la receta</h3>
          </div>
          <div class="modal-body form">
                         <form action="#" id="form_prod" class="form-horizontal">
                             <div class="form-body">
                               <div class="form-group">
                                   <label class="control-label col-md-3">Productos</label>
                                   <div class="col-md-9">
                                     <select name="producto" class="form-control select2" style="width: 300px;">
                                       <?php foreach($productos as $datos4) { ?>
                                         <option value="<?=$datos4->prod_id; ?>"><?=$datos4->prod_nombre; ?></option>
                                          <?php } ?>
                                     </select>
                                   </div>
                               </div>
                               <input name="receta" value="<?=$datos->rec_id; ?>" class="form-control" type="hidden">
                               <div class="form-group">
                                 <label class="control-label col-md-3">Cantidad</label>
                                 <div class="col-md-9">
                                     <input name="cantidad" placeholder="Unidades" class="form-control" type="number">
                                     <span class="help-block"></span>
                                 </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label col-md-3">Descripcion</label>
                                <div class="col-md-9">
                                    <input name="descripcion" placeholder="Tamaño del pique por ejemplo o gramaje del pan." class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                             </div>
                          </div>
                         </form>
                       </div>
                    <div class="modal-footer">
              <button type="button" id="btnSaveProd"  class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<!-- /.content-wrapper -->

<div class="modal fade" id="modal_cantidad" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Ingresa la nueva Cantidad</h3>
          </div>
          <div class="modal-body form">
                         <form action="#" id="formcantidad" class="form-horizontal">
                             <div class="form-body">
                               <input name="receta" value="<?=$datos->rec_id; ?>" class="form-control" type="hidden">
                               <div class="form-group">
                                 <label class="control-label col-md-3">Cantidad</label>
                                 <div class="col-md-9">
                                     <input name="cantidad" placeholder="Cantidad" class="form-control" type="number">
                                     <span class="help-block"></span>
                                 </div>
                              </div>
                          </div>
                         </form>
                       </div>
                    <div class="modal-footer">
              <button type="button" id="savecantidad"  class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_receta" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Receta Datos</h3>
          </div>
          <div class="modal-body form">
                         <form action="#" id="form_receta" class="form-horizontal">
                             <div class="form-body">
                               <input name="receta" value="<?=$datos->rec_id; ?>" class="form-control" type="hidden">
                               <div class="form-group">
                                 <label class="control-label col-md-3">Nombre</label>
                                 <div class="col-md-9">
                                     <input name="nombre" class="form-control" type="text">
                                     <span class="help-block"></span>
                                 </div>
                                 <label class="control-label col-md-3">Descripcion</label>
                                 <div class="col-md-9">
                                     <input name="descripcion" class="form-control" type="text">
                                     <span class="help-block"></span>
                                 </div>
                              </div>
                          </div>
                         </form>
                       </div>
                    <div class="modal-footer">
              <button type="button" id="guardar"  class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_descripcion" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">Descripcion del Producto</h3>
          </div>
          <div class="modal-body form">
                         <form action="#" id="form_descripcion" class="form-horizontal">
                            <div class="form-body">
                               <input name="receta" value="<?=$datos->rec_id; ?>" class="form-control" type="hidden">
                               <div class="form-group">
                                 <label class="control-label col-md-3">Descripcion</label>
                                 <div class="col-md-9">
                                     <input name="descripcion" class="form-control" type="text">
                                     <span class="help-block"></span>
                                 </div>
                              </div>
                          </div>
                         </form>
                       </div>
                    <div class="modal-footer">
              <button type="button" id="guardar-detalles"  class="btn btn-primary">Guardar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal_delete" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title">ELIMINAR</h3>
          </div>
                    <div class="modal-footer">
              <button type="button" id="delete"  class="btn btn-primary">Confirmar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </div>
        </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
// Ajax post
$(document).ready(function()
{
var save_ing;
var save_method; //for save method string
var id;
///MOSTRAR VARIACION
$("#add").click(function(){ /// ADICIONAR INGREDIENTE
  $('#form')[0].reset();
  $('#modal_form').modal('show');
  save_ing = "<?php echo site_url('Recetas/add_ing')?>";
 });
 $("#addProduct").click(function(){
   $('#modal_Product').modal('show');
  });
$("#addMateria").click(function(){
  $('#modal_mat').modal('show');
 });
 $(".pique").click(function(){
   id = $(this).attr("id");
   $('#form_descripcion')[0].reset();
   //Ajax Load data from ajax
   $.ajax({
       url : "<?php echo site_url('Controller_recetas/edit_descripcion/')?>/" + id,
       type: "GET",
       dataType: "JSON",
       success: function(data)
       {
           $('[name="descripcion"]').val(data[0].pr_detalles);
           $('#modal_descripcion').modal('show');
       },
       error: function (jqXHR, textStatus, errorThrown){}
  });
  });
$(".addIng").click(function(){//ADICIONAR SUN INGREDIENTE
  id = $(this).attr("id");
  $('#form')[0].reset();
  $('#modal_form').modal('show');
  save_ing = "<?php echo site_url('Recetas/add_ing_prod/')?>"+id;
 });
 ///editar ingrediente base
 $(".cantidad_base").click(function(){
   id = $(this).attr("id");
   save_ing = "<?php echo site_url('Controller_recetas/update_ing/')?>"+id;
   $('#form')[0].reset();
   //Ajax Load data from ajax
   $.ajax({
       url : "<?php echo site_url('Controller_recetas/edit_ing_base/')?>/" + id,
       type: "GET",
       dataType: "JSON",
       success: function(data)
       {
           $('[name="materia"]').val(data[0].ing_mat);
           $('[name="cantidad"]').val(data[0].ing_cant/data[0].unidad_escala);
           $('[name="concepto"]').val(data[0].ing_concepto);
           $('#modal_form').modal('show');
           $('.modal-title').text('Editar ingrediente base'); // Set title to Bootstrap modal title
       },
       error: function (jqXHR, textStatus, errorThrown){}
  });
  });
  $(".cantidad_prod").click(function(){
    id = $(this).attr("id");
    url = "<?php echo site_url('Controller_recetas/update_prod')?>/"+id;
    $('#modal_cantidad').modal('show');
   });
 $(".cantidad_pr").click(function(){
   id = $(this).attr("id");
   save_ing = "<?php echo site_url('Controller_recetas/update_subing/')?>"+id;
   $('#form')[0].reset();
   //Ajax Load data from ajax
   $.ajax({
       url : "<?php echo site_url('Controller_recetas/edit_sub_ing/')?>/" + id,
       type: "GET",
       dataType: "JSON",
       success: function(data)
       {
           $('[name="materia"]').val(data[0].ing_pr_mat);
           $('[name="cantidad"]').val(data[0].ing_pr_cant/data[0].unidad_escala);
           $('[name="concepto"]').val(data[0].ing_pr_concepto);
           $('#modal_form').modal('show');
           $('.modal-title').text('Editar ingrediente base'); // Set title to Bootstrap modal title
       },
       error: function (jqXHR, textStatus, errorThrown){}
  });
  });
  $(".cantidad_mat").click(function(){
    id = $(this).attr("id");
    url = "<?php echo site_url('Controller_recetas/update_mat')?>/"+id;
    $('#modal_cantidad').modal('show');
   });

// //AJAX INSERTAR VARIACION
$("#btnSave").click(function(){
 $.ajax({
      url : save_ing,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data)
      {
       $('#modal_form').modal('hide');
       location.reload();
      },
      error: function (jqXHR, exception) {},
   });
 });

 ////AJAX EDITING DATPS DE LA RECETA
  $(".edit").click(function(){
    id = $(this).attr("id");
    $('#form_receta')[0].reset();
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('Controller_recetas/edit_receta/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nombre"]').val(data[0].rec_nombre);
            $('[name="descripcion"]').val(data[0].rec_obs);
            $('#modal_receta').modal('show');
            $('.modal-title').text('Editar receta'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
  });

  $("#guardar").click(function(){
    $.ajax({
         url : "<?php echo site_url('Controller_recetas/update_receta/')?>"+ id,
         type: "POST",
         data: $('#form_receta').serialize(),
         dataType: "JSON",
         success: function(data)
         {
          $('#modal_receta').modal('hide');
          location.reload();
         },
         error: function (jqXHR, exception) {},
    });
  });

  $("#guardar-detalles").click(function(){
    $.ajax({
         url : "<?php echo site_url('Controller_recetas/update_detalles/')?>"+ id,
         type: "POST",
         data: $('#form_descripcion').serialize(),
         dataType: "JSON",
         success: function(data)
         {
          $('#modal_descripcion').modal('hide');
          location.reload();
         },
         error: function (jqXHR, exception) {},
    });
  });

 $("#btnSaveProd").click(function(){
   $.ajax({
        url : "<?php echo site_url('Recetas/add_prod')?>",
        type: "POST",
        data: $('#form_prod').serialize(),
        dataType: "JSON",
        success: function(data)
        {
         $('#modal_Product').modal('hide');
         location.reload();
        },
        error: function (jqXHR, exception) {},
   });
 });
 $("#btnSaveMat").click(function(){
   $.ajax({
        url : "<?php echo site_url('Recetas/add_mat/')?>",
        type: "POST",
        data: $('#formMatRec').serialize(),
        dataType: "JSON",
        success: function(data)
        {
         $('#formMatRec').modal('hide');
         location.reload();
        },
        error: function (jqXHR, exception) {
       },
   });
 });

// //AJAX ELIMINAR MOV
$(".delete_mov").click(function(){
  var id = $(this).attr("id");
  url = "<?php echo site_url('Recetas/delete_ing')?>/"+id;
  $('#modal_delete').modal('show');
});
// //AJAX ELIMINAR PRODUCTO
 $(".delete_prod").click(function(){
   var id = $(this).attr("id");
   url = "<?php echo site_url('Recetas/delete_prod')?>/"+id;
   $('#modal_delete').modal('show');
});
// //AJAX ELIMINAR SUBINGREDIENTES
  $(".delete_pr").click(function(){
    var id = $(this).attr("id");
    url = "<?php echo site_url('Recetas/delete_pr')?>/"+id;
    $('#modal_delete').modal('show');
 });
 // //AJAX ELIMINAR MATERIAS REPROCESOS
   $(".delete_mat").click(function(){
     var id = $(this).attr("id");
     url = "<?php echo site_url('Recetas/delete_mat')?>/"+id;
     $('#modal_delete').modal('show');
  });

  $("#delete").click(function(){
  $.ajax({
      url : url,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        location.reload();
      },
      error: function (jqXHR, textStatus, errorThrown){}
    });
  });

  $(".delete_rec").click(function(){
    var id = $(this).attr("id");
    if(confirm('Está completamente seguro de eliminar toda la receta?'))
   {
     window.location.href = '<?php echo base_url(); ?>Recetas/delete/'+id;
   }
 });
 ////ACTUALIZADOR DE Cantidad
 $("#savecantidad").click(function(){
   $.ajax({
        url : url,
        type: "POST",
        data: $('#formcantidad').serialize(),
        dataType: "JSON",
        success: function(data)
        {
         $('#modal_cantidad ').modal('hide');
         location.reload();
        },
        error: function (jqXHR, exception) {},
   });
 });

 ////ACTUALIZADOR DE Cantidad
 $(".showtr").click(function(){
   //alert('llamando '+$(this).val())
   $('#tr'+$(this).val()).toggle();
 });
});
</script>
