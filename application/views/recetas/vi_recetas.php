<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Listado de Recetas de Productos
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Recetas</a></li>
      <li class="active">Listado de Recetas</li>
    </ol>
  </section>



  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-8">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <a href="<?= base_url('Recetas/form')?>" class="btn  btn-primary">Nueva +</a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <div id="result" class="box-body">

            <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>

                <th>ID</th>
                <th>Nombre</th>
                <th>Observaciones</th>
                <th>Activo</th>
                <th>Detalles</th>
              </tr>
              </thead>
              <tbody>
              </tr>
                <?php foreach($info as $datos) { ?>
                <tr>
                    <td><?php echo $datos->rec_id;?> </td>
                    <td><?php echo $datos->rec_nombre;?> </td>
                    <td><?php echo $datos->rec_obs;?> </td>
                    <?php if ($datos->rec_activo==1) { ?>
                      <td>
                        <button class="btn btn-block btn-success active-ban" value="<?= $datos->rec_id; ?>">Activado</button>
                      </td>
                    <?php }elseif ($datos->rec_activo==0) { ?>
                      <td>
                        <button class="btn btn-block btn-danger desactive-ban" value="<?= $datos->rec_id; ?>">Desactivado</button>
                      </td>
                    <?php }else{ ?>
                      <td>Error <?php echo $datos->rec_activo ?></td>
                    <?php } ?>

                    <td><a href="<?php echo base_url('Recetas/create2')."/".$datos->rec_id; ?>" class="btn  btn-success">Detalles</a></td>
                </tr>
                <?php  }  ?>
              </tbody>
              <tfoot>
                <tr>
                  <th>Nombre</th>
                  <th>Producto</th>
                  <th>Descripción</th>
                  <th>Activo</th>
                  <th>Detalles</th>
                </tr>
              </tfoot>
            </table>

          </div>
          <!-- /.box -->

        </div>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->

  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

  <script>

  $( ".active-ban" ).click(function() {
    var url = '<?php echo base_url('Recetas/active') ?>/'+$(this).val()+'/state/0';
    $("#result").load(url);
  });
  $( ".desactive-ban" ).click(function() {
    var url = '<?php echo base_url('Recetas/active') ?>/'+$(this).val()+'/state/1';
    $("#result").load(url);
  });
  </script>
</div>
<!-- /.content-wrapper -->
