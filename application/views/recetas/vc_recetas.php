<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Crear Nueva Receta
    </h1>
  </section>


  <!-- Main content -->
  <section class="content">

    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Crear nueva Receta</h3>
          </div>

    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="<?php echo base_url(); ?>Recetas/create" method="POST">
      <?php if(isset($mensaje)):?>
              <div class="alert <?= $alert; ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?= $mensaje; ?>
              </div>
      <?php endif; ?>
      <?php echo validation_errors(); ?>

      <div class="box-body">


        <div class="form-group">
          <label for="exampleInputEmail1">Nombre</label>
          <input type="text" name="nombre"  value="<?php echo set_value('cant'); ?>"class="form-control">
        </div>

        <div class="form-group">
          <label>Observaciones</label>
          <textarea class="form-control" name="descripcion" rows="3" placeholder="Descipcion breve del producto ..."><?php echo set_value('desscripcion'); ?></textarea>
        </div>

        <input type="hidden" name="tipo" value="out"class="form-control"  >


        <div class="box-footer">
          <button type="submit" name="enviar" value="Create" class="btn btn-primary">Guardar</button>
        </div>
        </div>
      </div>
      <!-- /.box-body -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
