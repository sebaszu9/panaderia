<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-12 col-xs-12" style="margin:20px">
      <div class="card text-center">
        <div class="card-body">
          <img class="card-img-top"  src="<?php echo base_url(); ?>image/sb.png" style="width:500px">
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>Productos</h3>

            <p>Listado</p>
          </div>
          <div class="icon">
            <i class="fa fa-shopping-cart"></i>
          </div>
          <a href="<?php echo base_url(); ?>Productos/index"class="small-box-footer">
              Ver más <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-orange">
          <div class="inner">
            <h3>Materias Primas</h3>

            <p>Listado</p>
          </div>
          <div class="icon">
            <i class="fa fa-archive"></i>
          </div>
          <a href="<?php echo base_url(); ?>Materiasprimas/index" class="small-box-footer">
              Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-teal">
          <div class="inner">
            <h3>Recetas</h3>

            <p>Listado</p>
          </div>
          <div class="icon">
            <i class="fa fa-birthday-cake"></i>
          </div>
          <a href="<?php echo base_url(); ?>Recetas/index" class="small-box-footer">
              Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </a>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>Movimientos</h3>
            <p>Listado</p>
          </div>
          <div class="icon">
            <i class="fa fa-cubes"></i>
          </div>
          <a href="<?php echo base_url('Movimiento/materia/input/index'); ?>" class="small-box-footer">
              Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>

        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-maroon">
          <div class="inner">
            <h3>Proveedores</h3>

            <p>Listado</p>
          </div>
          <div class="icon">
            <i class="fa fa-truck"></i>
          </div>
          <a href="<?php echo base_url(); ?>Proveedores/index" class="small-box-footer">
              Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>Producción</h3>

            <p>Listado</p>
          </div>
          <div class="icon">
            <i class="fa fa-industry"></i>
          </div>
          <a href="<?php echo base_url(); ?>Produccion/index_admin" class="small-box-footer">
              Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-purple">
          <div class="inner">
            <h3>Usuarios</h3>

            <p>Listado</p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <a  href="<?php echo base_url(); ?>Usuario/index" class="small-box-footer">
              Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>Reportes</h3>

            <p>Listado</p>
          </div>
          <div class="icon">
            <i class="fa fa-area-chart"></i>
          </div>
          <a href="#" class="small-box-footer">
              Ver más <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->

  </section>
</div>
