
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['main'] = 'Welcome';
$route['forbidden']=  'Welcome/prohibido';
$route['default_controller'] = 'Controller_login/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['autenticar'] = 'Controller_login/autenticar';
$route['login'] = 'Controller_login/login';
$route['logout'] = 'Controller_login/logout';
//2$route['autenticar'] = 'Controller_usuario/autenticar';

// PRODUCTOS
$route['Productos/'] = 'controller_productos/index';
$route['Productos/index'] = 'controller_productos/index';
$route['Productos/form'] = 'controller_productos/form';
$route['Productos/create'] = 'controller_productos/create';
$route['Productos/edit/:any'] = 'controller_productos/edit';
$route['Productos/update/:any'] = 'controller_productos/update';
$route['Productos/get/:any'] = 'controller_productos/get';
$route['Productos/delete/:any'] = 'controller_productos/delete';
$route['Productos/details/:any'] = 'controller_productos/details';
$route['Productos/inventario'] = 'controller_productos/inventario';
$route['Productos/excel'] = 'controller_productos/excel';
$route['Productos/pdf'] = 'controller_productos/pdf';

// CATEGORIAS DE PRODUCTOS
$route['Categoriasprod/'] = 'Controller_cat_prod/index';
$route['Categoriasprod/index'] = 'Controller_cat_prod/index';
$route['Categoriasprod/form'] = 'Controller_cat_prod/form';
$route['Categoriasprod/create'] = 'Controller_cat_prod/create';
$route['Categoriasprod/edit/:any'] = 'Controller_cat_prod/edit';
$route['Categoriasprod/update/:any'] = 'Controller_cat_prod/update';
$route['Categoriasprod/get/:any'] = 'Controller_cat_prod/get';
$route['Categoriasprod/delete/:any'] = 'Controller_cat_prod/delete';

// IMPUESTOS DE PRODUCTOS
$route['Impuestos/'] = 'controller_impuestos/index';
$route['Impuestos/index'] = 'controller_impuestos/index';
$route['Impuestos/form'] = 'controller_impuestos/form';
$route['Impuestos/create'] = 'controller_impuestos/create';
$route['Impuestos/edit/:num'] = 'controller_impuestos/edit';
$route['Impuestos/update/:num'] = 'controller_impuestos/update';
$route['Impuestos/get/:num'] = 'controller_impuestos/get';
$route['Impuestos/delete/:num'] = 'controller_impuestos/delete';


// MESAS

$route['Mesas/mesas/form'] = 'Controller_mesas/mesas';
$route['Mesas/zonas/form'] = 'Controller_mesas/zonas';
$route['Mesas/zonas_form'] = 'Controller_mesas/form_zonas';
$route['Mesas/mesas_form'] = 'Controller_mesas/form_mesas';
$route['Mesas/create_zonas'] = 'Controller_mesas/create_zonas';
$route['Mesas/create_mesas'] = 'Controller_mesas/create_mesas';
$route['Mesas/update_zonas'] = 'Controller_mesas/update_zonas';
$route['Mesas/update_mesas'] = 'Controller_mesas/update_mesas';

$route['Mesas/delete_zonas/:num'] = 'Controller_mesas/remove_zonas';
$route['Mesas/zonas_edit'] = 'Controller_mesas/edit_zonas';


// MATERIAS PRIMAS
$route['Materiasprimas/'] = 'controller_materias/index';
$route['Materiasprimas/index'] = 'controller_materias/index';
$route['Materiasprimas/form'] = 'controller_materias/form';
$route['Materiasprimas/create'] = 'controller_materias/create';
$route['Materiasprimas/edit/:any'] = 'controller_materias/edit';
$route['Materiasprimas/update/:any'] = 'controller_materias/update';
$route['Materiasprimas/get/:any'] = 'controller_materias/get';
$route['Materiasprimas/delete/:any'] = 'controller_materias/delete';
$route['Materiasprimas/details/:any'] = 'controller_materias/details';
$route['Materiasprimas/inv'] = 'controller_materias/inventario';

// CATEGORIAS DE MATERIAS PRIMAS
$route['Categoriasmat/'] = 'Controller_cat_mat/index';
$route['Categoriasmat/index'] = 'Controller_cat_mat/index';
$route['Categoriasmat/form'] = 'Controller_cat_mat/form';
$route['Categoriasmat/create'] = 'Controller_cat_mat/create';
$route['Categoriasmat/edit/:any'] = 'Controller_cat_mat/edit';
$route['Categoriasmat/update/:any'] = 'Controller_cat_mat/update';
$route['Categoriasmat/get/:any'] = 'Controller_cat_mat/get';
$route['Categoriasmat/delete/:any'] = 'Controller_cat_mat/delete';

// PROVEEDORES
$route['Proveedores/'] = 'Controller_proveedores/index';
$route['Proveedores/index'] = 'Controller_proveedores/index';
$route['Proveedores/form'] = 'Controller_proveedores/form';
$route['Proveedores/create'] = 'Controller_proveedores/create';
$route['Proveedores/edit/:num'] = 'Controller_proveedores/edit';
$route['Proveedores/update/:num'] = 'Controller_proveedores/update';
$route['Proveedores/get/:num'] = 'Controller_proveedores/get';
$route['Proveedores/delete/:num'] = 'Controller_proveedores/delete';
$route['Proveedores/view/:num'] = 'Controller_proveedores/view';

///PEDIDOS MATERIAS
$route['Pedidos/index'] = 'Controller_mov_mat/index';
$route['Pedidos/details/:num'] = 'Controller_mov_mat/details_pedido';
$route['Pedidos/edit/:num'] = 'Controller_mov_mat/edit_pedido';
$route['Pedidos/form'] = 'Controller_mov_mat/form_pedidos';
$route['Pedidos/create'] = 'Controller_mov_mat/create_pedido';
$route['Pedidos/create2/:num'] = 'Controller_mov_mat/create_pedido2';
$route['Pedidos/update'] = 'Controller_mov_mat/update_pedido';
$route['add_mov'] = 'Controller_mov_mat/add_mov';
$route['Pedidos/delete/:num'] = 'Controller_mov_mat/eliminar_pedido';

///PEDIDOS PRODUCTOS
$route['Pedidosprod/index'] = 'Controller_mov_prod/index';
$route['Pedidosprod/details/:num'] = 'Controller_mov_prod/details_pedido';
$route['Pedidosprod/edit/:num'] = 'Controller_mov_prod/edit_pedido';
$route['Pedidosprod/form'] = 'Controller_mov_prod/form_pedidos';
$route['Pedidosprod/create'] = 'Controller_mov_prod/create_pedido';
$route['Pedidosprod/create2/:num'] = 'Controller_mov_prod/create_pedido2';
$route['Pedidosprod/update'] = 'Controller_mov_prod/update_pedido';
$route['add_mov2'] = 'Controller_mov_prod/add_mov';
$route['delete_mov2/:num'] = 'Controller_mov_prod/delete_mov';

///SALIDAS MATERIAS
$route['Salidasmat/index'] = 'Controller_mov_mat/index_s';
$route['Salidasmat/details/:num'] = 'Controller_mov_mat/details_salida';
$route['Salidasmat/edit/:num'] = 'Controller_mov_mat/edit_salida';
$route['Salidasmat/form'] = 'Controller_mov_mat/form_salidas';
$route['Salidasmat/create'] = 'Controller_mov_mat/create_salida';
$route['Salidasmat/create2/:num'] = 'Controller_mov_mat/create_salida2';
$route['Salidasmat/update'] = 'Controller_mov_mat/update_salida';
$route['Salidasmat/add_mov'] = 'Controller_mov_mat/add_mov_s';
$route['Salidasmat/delete_mov/:num'] = 'Controller_mov_mat/delete_mov_s';

///SALIDAS PRODUCTOS
$route['Salidasinv/index'] = 'Controller_mov_prod/index_s';
$route['Salidasinv/details/:num'] = 'Controller_mov_prod/details_salida';
$route['Salidasinv/edit/:num'] = 'Controller_mov_prod/edit_salida';
$route['Salidasinv/form'] = 'Controller_mov_prod/form_salidas';
$route['Salidasinv/create'] = 'Controller_mov_prod/create_salida';
$route['Salidasinv/create2/:num'] = 'Controller_mov_prod/create_salida2';
$route['Salidasinv/update'] = 'Controller_mov_prod/update_salida';
$route['Salidasinv/add_mov'] = 'Controller_mov_prod/add_mov_s';
$route['Salidasinv/delete_mov/:num'] = 'Controller_mov_prod/delete_mov_s';

///MODULO DE RECETAS PRODUCTOS
$route['Recetas/index'] = 'Controller_recetas/index';
$route['Recetas/details/:num'] = 'Controller_recetas/details_receta';
$route['Recetas/edit/:num'] = 'Controller_recetas/edit';
$route['Recetas/form'] = 'Controller_recetas/form';
$route['Recetas/create'] = 'Controller_recetas/create';
$route['Recetas/create2/:num'] = 'Controller_recetas/create_2';
$route['Recetas/update'] = 'Controller_recetas/update';
$route['Recetas/add_ing'] = 'Controller_recetas/add_ing';
$route['Recetas/add_ing_prod/:num'] = 'Controller_recetas/add_ing_prod';
$route['Recetas/add_prod'] = 'Controller_recetas/add_prod';
$route['Recetas/add_mat'] = 'Controller_recetas/add_mat';
$route['Recetas/delete_ing/:num'] = 'Controller_recetas/delete_ing';
$route['Recetas/delete_prod/:num'] = 'Controller_recetas/delete_prod';
$route['Recetas/delete_mat/:num'] = 'Controller_recetas/delete_mat';
$route['Recetas/delete_pr/:num'] = 'Controller_recetas/delete_pr';
$route['Recetas/delete/:num'] = 'Controller_recetas/delete_rec';
$route['Recetas/active/:num/state/:num'] = 'Controller_recetas/active';
$route['Recetas/seleccionar_receta'] = 'Controller_recetas/seleccionar_receta';
$route['Recetas/seleccionar_ing/:num'] = 'Controller_recetas/seleccionar_ingrediente';
//$route['Recetas/calculo/:num/:num'] = 'Controller_recetas/calculo';
$route['Recetas/calculo/:num/:num'] = 'Calculos/recetas';
$route['Recetas/prod_term/:any'] = 'Controller_recetas/prod_calc';
$route['Recetas/mat_term/:any'] = 'Controller_recetas/mat_calc';

///MODULO DE CONCEPTOS DE INGREDIENTES RECETAS
$route['Concept_ing/'] = 'Controller_conceptos_rec/index';
$route['Concept_ing/index'] = 'Controller_conceptos_rec/index';
$route['Concept_ing/form'] = 'Controller_conceptos_rec/form';
$route['Concept_ing/create'] = 'Controller_conceptos_rec/create';
$route['Concept_ing/edit/:num'] = 'Controller_conceptos_rec/edit';
$route['Concept_ing/update/:num'] = 'Controller_conceptos_rec/update';
$route['Concept_ing/get/:num'] = 'Controller_conceptos_rec/get';
$route['Concept_ing/delete/:num'] = 'Controller_conceptos_rec/delete';

///MODULO PRODUCCION
$route['Produccion/index_admin'] = 'Controller_produccion/index_admin';
$route['Produccion/Produccion'] = 'Controller_produccion/produccion';
$route['Produccion/sede/form'] = 'Controller_produccion/produccion_sedes_form';
$route['Produccion/sede/search'] = 'Controller_produccion/produccion_sedes_search';
$route['Produccion/index'] = 'Controller_produccion/index';
$route['Produccion/create'] = 'Controller_produccion/create';
//$route['Produccion/details/:num/:num'] = 'Controller_produccion/details';
$route['Produccion/details_admin/:num/:num'] = 'Controller_produccion/details_admin';
$route['Produccion/edit/:num'] = 'Controller_produccion/edit';
$route['Produccion/form'] = 'Controller_produccion/form';
$route['Produccion/update'] = 'Controller_produccion/update';
$route['Produccion/edit_ing/:num'] = 'Controller_produccion/edit_ing';
$route['Produccion/edit_prod/:num'] = 'Controller_produccion/edit_prod';

// produccion para panadero
//$route['Calculos/produccion/index'] = 'Calculos/produccion';
$route['Produccion/details/:num/:num'] = 'Calculos/produccion_details';
////////LOGIN usuario
$route['Usuario/login/admin'] = 'Controller_usuario/login_admin';

///////
$route['Usuario/index'] = 'Controller_usuario/index';
$route['Usuario/edit/:num'] = 'Controller_usuario/edit';
$route['Usuario/form'] = 'Controller_usuario/form';
$route['Usuario/create'] = 'Controller_usuario/create';

////////Reportes
$route['Reportes/prod_dia'] = 'Controller_reportes/index';
$route['Grafico/materia/:num'] = 'Controller_reportes/tabla_materia';
$route['Grafico/producto/:num'] = 'Controller_reportes/tabla_producto';
//$route['Grafico/empaque/:num'] = 'Controller_reportes/tabla_producto';

////// Pedidos productos
$route['Movimiento/producto/input/form']     = 'Controller_mov_prod/form_input';
$route['Movimiento/producto/input/create']   = 'Controller_mov_prod/create_input';
$route['Movimiento/producto/input/index']    = 'Controller_mov_prod/index_input';
$route['Movimiento/producto/input/search'] = 'Controller_mov_prod/search_input';
$route['Movimiento/producto/input/details/:num']  = 'Controller_mov_prod/detail_input';
$route['Movimiento/producto/input/edit/:num']  = 'Controller_mov_prod/edit_input';
$route['Movimiento/producto/input/update']  = 'Controller_mov_prod/update_input';
$route['Movimiento/producto/input/delete/:num']  = 'Controller_mov_prod/delete_input';
$route['Movimiento/producto/input/product/update']  = 'Controller_mov_prod/update_product_mov';

// salidas_bod
$route['Movimiento/producto/output/form']     = 'Controller_mov_prod/form_output';
$route['Movimiento/producto/output/create']   = 'Controller_mov_prod/create_output';
$route['Movimiento/producto/output/index']    = 'Controller_mov_prod/index_output';
$route['Movimiento/producto/output/search'] = 'Controller_mov_prod/search_output';
$route['Movimiento/producto/output/details/:num']  = 'Controller_mov_prod/detail_output';
$route['Movimiento/producto/output/edit/:num']  = 'Controller_mov_prod/edit_output';
$route['Movimiento/producto/output/update']  = 'Controller_mov_prod/update_output';
$route['Movimiento/producto/output/delete/:num']  = 'Controller_mov_prod/delete_output';
$route['Movimiento/producto/output/product/update']  = 'Controller_mov_prod/update_product_mov';

////// Pedidos Materias
$route['Movimiento/materia/input/form']     = 'Controller_mov_mat/form_input';
$route['Movimiento/materia/input/create']   = 'Controller_mov_mat/create_input';
$route['Movimiento/materia/input/index']    = 'Controller_mov_mat/index_input';
$route['Movimiento/materia/input/search'] = 'Controller_mov_mat/search_input';
$route['Movimiento/materia/input/details/:num']  = 'Controller_mov_mat/detail_input';
$route['Movimiento/materia/input/edit/:num']  = 'Controller_mov_mat/edit_input';
$route['Movimiento/materia/input/update']  = 'Controller_mov_mat/update_input';
$route['Movimiento/materia/input/delete/:num']  = 'Controller_mov_mat/delete_input';
$route['Movimiento/materia/input/product/update']  = 'Controller_mov_mat/update_product_mov';

// salidas_bod
$route['Movimiento/materia/output/form']     = 'Controller_mov_mat/form_output';
$route['Movimiento/materia/output/create']   = 'Controller_mov_mat/create_output';
$route['Movimiento/materia/output/index']    = 'Controller_mov_mat/index_output';
$route['Movimiento/materia/output/search'] = 'Controller_mov_mat/search_output';
$route['Movimiento/materia/output/details/:num']  = 'Controller_mov_mat/detail_output';
$route['Movimiento/materia/output/edit/:num']  = 'Controller_mov_mat/edit_output';
$route['Movimiento/materia/output/update']  = 'Controller_mov_mat/update_output';
$route['Movimiento/materia/output/delete/:num']  = 'Controller_mov_mat/delete_output';
$route['Movimiento/materia/output/product/update']  = 'Controller_mov_mat/update_product_mov';

////IMPORTAR INFORMACION!!!!
$route['Import/index']  = 'Controller_import/index';
$route['Import/cat_mat']  = 'Controller_import/cat_mat';
$route['Import/cat_prod']  = 'Controller_import/cat_prod';
$route['Import/prod']  = 'Controller_import/prod';
$route['Import/mat']  = 'Controller_import/mat';
$route['Import/inv']  = 'Controller_import/inv';
$route['Import/inv']  = 'Controller_import/inv';
/////INTERFACE mobile
$route['welcome/mobile']  = 'Welcome/mobile';

//inventario
$route['Inventario/index']  = 'Controller_inventario/index';
$route['Inventario/search'] = 'Controller_inventario/search';
$route['Inventario/excel'] = 'Controller_inventario/excel';
$route['Inventario/pdf'] = 'Controller_inventario/pdf';


// MOVIMIENTO DE  INVENTARIO
$route['Inventario/movimiento/index'] = 'Controller_inventario/movimiento_index';
$route['Inventario/movimiento/search'] = 'Controller_inventario/movimiento_consult';
$route['Inventario/movimiento/productos/form'] = 'Controller_inventario/mov_prod_form';
$route['Inventario/movimiento/materias/form'] = 'Controller_inventario/mov_mat_form';
$route['Inventario/movimiento/productos/create'] = 'Controller_inventario/mov_prod_create';
$route['Inventario/movimiento/materias/create'] = 'Controller_inventario/mov_mat_create';
$route['Inventario/exce/materia'] = 'Controller_inventario/excel_mat';
$route['Inventario/pdf/materia'] = 'Controller_inventario/pdf_mat';
$route['Inventario/excel/productos'] = 'Controller_inventario/excel_prod';
$route['Inventario/pdf/productos'] = 'Controller_inventario/pdf_prod';
// stock de Inventario
$route['Inventario/stock/materia'] = 'Controller_inventario/inv_stock_mat';
$route['Inventario/stock/productos'] = 'Controller_inventario/inv_stock_prod';
//$route['Inventario/stock/materia'] = 'Controller_inventario/inv_stock_mat';
$route['Movimiento/materia/detalles/:num'] = 'Controller_inventario/mov_mat_details';
$route['Movimiento/materia/delete/register'] = 'Controller_inventario/delete_mov_mat';
$route['Movimiento/materia/delete/item'] = 'Controller_inventario/delete_item_mov_mat';
// materias
$route['Movimiento/producto/detalles/:num'] = 'Controller_inventario/mov_prod_details';
$route['Movimiento/producto/delete/register'] = 'Controller_inventario/delete_mov_prod';
$route['Movimiento/producto/delete/item'] = 'Controller_inventario/delete_item_mov_prod';

// REPORTEEES
$route['Reportes/materias/form']  = 'Controller_reportes/form_materias';
$route['Reportes/materias/search']  = 'Controller_reportes/search_materias';
$route['Reportes/materias/search/informe']  = 'Controller_reportes/search_materias_informe';

$route['Reportes/produccion/form']  = 'Controller_reportes/form_produccion';
$route['Reportes/produccion/search']  = 'Controller_reportes/search_produccion';
$route['Reportes/produccion/search/informe']  = 'Controller_reportes/search_produccion_informe';

$route['Reportes/empaque']  = 'Controller_reportes/empaque';

// pos
$route['Pos/index']  = 'Controller_pos/index';
$route['Pos/beta']  = 'Controller_pos/beta';
$route['Pos/buscar/productos']  = 'Controller_pos/search_product';
$route['Pos/factura/:num']  = 'Controller_pos/factura';
//$route['Pos/add/cart']  = 'Controller_pos/add_cart';
//$route['Pos/load/cart']  = 'Controller_pos/load_cart';
//$route['Pos/change_quality']  = 'Controller_pos/change_quality';
//$route['Pos/delete/cart']  = 'Controller_pos/delete_item';
$route['Pos/register/customer']  = 'Controller_pos/register_customer';

//
$route['Ventas/index']  = 'Controller_ventas/index';
$route['Ventas/search']  = 'Controller_ventas/details';
$route['Ventas/delete/ventas']  = 'Controller_ventas/delete_ventas';
$route['Ventas/delete/item']  = 'Controller_ventas/delete_item';

$route['Orden/produccion/index']  = 'Controller_order_prod/index';
$route['Orden/produccion/buscar']  = 'Controller_order_prod/search';
$route['Orden/produccion/eliminar']  = 'Controller_order_prod/delete';
$route['Orden/produccion/nuevo']  = 'Controller_order_prod/form';
$route['Orden/produccion/crear']  = 'Controller_order_prod/create';
$route['Orden/produccion/detalles/:num']  = 'Controller_order_prod/details';
$route['Orden/produccion/imprimir/:num']  = 'Controller_order_prod/pdf';
$route['Orden/produccion/update/status']  = 'Controller_order_prod/update_status';


$route['Orden/produccion/producto/add']  = 'Controller_order_prod/add_prod_producto';
$route['Orden/produccion/producto/del']  = 'Controller_order_prod/del_prod_producto';
$route['Orden/produccion/producto/update-state']  = 'Controller_order_prod/upd_prod_producto';

//=============================================================
$route['Orden/producto/index'] = 'Controller_order_pedido/index';
$route['Orden/producto/form']  = 'Controller_order_pedido/form';
$route['Orden/producto/buscar']  = 'Controller_order_pedido/search';
$route['Orden/producto/create']  = 'Controller_order_pedido/create';
$route['Orden/producto/update']  = 'Controller_order_pedido/update';
$route['Orden/producto/delete']  = 'Controller_order_pedido/delete';
$route['Orden/producto/detalles/:num']  = 'Controller_order_pedido/details';
$route['Produccion/orden/detalles/:num/:num'] = 'Calculos/produccion_details_order';

$route['Orden/producto/item/add']  = 'Controller_order_pedido/add_item';
$route['Orden/producto/item/del']  = 'Controller_order_pedido/delete_item';



// CLIENTES
$route['Clientes/'] = 'Controller_clientes/index';
$route['Clientes/index'] = 'Controller_clientes/index';
$route['Clientes/form'] = 'Controller_clientes/form';
$route['Clientes/create'] = 'Controller_clientes/create';
$route['Clientes/edit/:any'] = 'Controller_clientes/edit';
$route['Clientes/update/:any'] = 'Controller_clientes/update';
$route['Clientes/get/:any'] = 'Controller_clientes/get';
$route['Clientes/delete/:any'] = 'Controller_clientes/delete';

// PEDIDOS ADMIN
$route['Pedidos/hoy'] = 'Controller_pedidos/today';
$route['Pedidos/formulario'] = 'Controller_pedidos/form_history';
$route['Pedidos/historico'] = 'Controller_pedidos/history';
$route['Pedidos/edit/:num'] = 'Controller_pedidos/edit_pedido';
$route['Pedidos/form_item'] = 'Controller_pedidos/form_reporte';
$route['Pedidos/reporte'] = 'Controller_pedidos/pedido_reporte';
//VENTAS ADMIN
$route['Ventas/hoy'] = 'Controller_ventas/today';
///REPARTIDORES vi_rutas
$route['Rutas/index'] = 'Controller_repartidores/index';
$route['Rutas/edit/:num'] = 'Controller_repartidores/edit';
$route['Rutas/update/:num'] = 'Controller_repartidores/update';
