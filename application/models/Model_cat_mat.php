<?php

/**
 *
 */
class Model_cat_mat extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('categorias_mat', $param);
  }

  function display()
  {
      $this->db->select('*');
      $this->db->from('categorias_mat');
      $this->db->where('cat_mat_del', 0);
      return $this->db->get()->result();
  }


  function read($id)
  {
    $this->db->select('*');
    $this->db->where('cat_mat_id', $id);
    $this->db->where('cat_mat_del', 0);
    $this->db->from('categorias_mat');
    return $this->db->get()->result();
  }

  function delete($id)
  {
    $this->db->where('cat_mat_id',$id);
    $param['cat_mat_del'] = 1;
    $exito = $this->db->update('categorias_mat', $param);
    //$this->db->where('cat_mat_id', $id);
    //$this->db->delete('categorias_mat');
    //echo "<script type=text/javascript>alert('Se ha  eliminado la  categoria');</script>";

    }

  function update($param)
  { $id = $param ['cat_mat_id'];
    $this->db->where('cat_mat_id', $id);
    $this->db->update('categorias_mat', $param);
    echo "<script type=text/javascript>alert('Se ha actualizado exitosamente la nueva categoria');</script>";
  }

  function check_id ($id)
  {
    $this->db->select('*');
    $this->db->where('cat_mat_id', $id);
    $this->db->where('cat_mat_del', 0);
    $this->db->from('categorias_mat');

    $query = $this->db->get()->result();

    if( count($query) > 0)
    {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}



?>
