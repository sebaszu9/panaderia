<?php

/**
 *
 */
class Model_stock_prod extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function display()
  {
    $this->db->select('*');
    $this->db->from('stock_prod');
    $this->db->join('productos', 'productos.prod_id = stock_prod.sp_prod');

    return $this->db->get()->result();
  }

  function insert_stock($param)
  {
    $this->db->insert('stock_prod', $param);
  }

  function val_stock_exist($prod)
  {
    $this->db->select('*');
    $this->db->where('sp_prod',$prod);
    $query = $this->db->get('stock_prod');

    if ($query->num_rows() > 0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }

  }
  function display_stock($param)
  {
    $this->db->select('*');
    $this->db->where('sp_prod',$param);
    $this->db->join('panaderias', 'panaderias.bod_id = stock_prod.sp_location');
    $query = $this->db->get('stock_prod');

    if ($query->num_rows() > 0 )
    {
        return $query;
    }
    else
    {
        return FALSE;
    }
  }

  function get_stock($param)
  {
    $this->db->select('*');
    $this->db->where('sp_prod',$param ['id']);
    $this->db->where('sp_location',$param ['sede']);
    $this->db->join('panaderias', 'panaderias.bod_id = stock_prod.sp_location');
    $query = $this->db->get('stock_prod');

    if ($query->num_rows() > 0 )
    {
        return $query;
    }
    else
    {
        return FALSE;
    }
  }

  function update_stock($param)
  {
    $this->db->where('sp_prod',$param['sp_prod']);
    $this->db->update('stock_prod', $param);
  }

  function load_per_bakery($sede)
  {
    $this->db->select('*');
    $this->db->from('stock_prod');
    $this->db->where('sp_location',$sede);
    $this->db->join('productos', 'productos.prod_id = stock_prod.sp_prod');
    return $this->db->get()->result();
  }

}
?>
