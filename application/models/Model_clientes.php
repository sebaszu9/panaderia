<?php

/**
 *
 */
class Model_clientes extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function create($param)
  {
    $this->db->insert('cliente', $param);
  }

  function display() {
    $this->db->select('*');
    $this->db->from('cliente');
    $this->db->join('users', 'users.id = cliente.clt_ruta');
    $this->db->where('clt_del', '0');
    return $this->db->get()->result();
  }


function read($id)
{
  $this->db->select('*');
  $this->db->where('clt_id', $id);
  $this->db->from('cliente');

  return $this->db->get()->result();
}

  function delete($id)
  {
    $this->db->where('clt_id', $id);
    $this->db->from('cliente');
    $this->db->update('clt_del',1);
    echo "<script type=text/javascript>alert('Se ha  eliminado el cliente');</script>";
  }

  function update($param)
  { $id = $param ['clt_id'];
    $this->db->where('clt_id', $id);
    $this->db->update('cliente', $param);
    echo "<script type=text/javascript>alert('Se ha actualizado exitosamente el cliente');</script>";
  }

}



?>
