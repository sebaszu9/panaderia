<?php


class Model_order_pedido extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('orden_producto', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  public function delete($id)
  {
    $this->db->where('op_id',$id);
    $this->db->delete('orden_producto');
  }

  public function delete_item_order($id)
  {
    $this->db->where('ipo_orden',$id);
    $this->db->delete('item_producto_orden');
  }

  public function search()
  {
    $this->db->from('orden_producto');
    $this->db->join('panaderias', 'panaderias.bod_id = orden_producto.op_sede');
    $this->db->join('proveedores', 'proveedores.prov_id = orden_producto.op_proveedor');
    if ($this->input->post('panaderia')>=1) {
       $this->db->where('op_sede', $this->input->post('panaderia'));
    }
    if ($this->input->post('proveedor')) {
       $this->db->where('op_proveedor', $this->input->post('proveedor'));
    }

    if ($this->input->post('fecha1')&&$this->input->post('fecha2'))
    {
      $fecha1 = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $fecha2 = date('Y-m-d', strtotime($this->input->post('fecha2')));
      $this->db->where('op_fecha >=', $fecha1);
      $this->db->where('op_fecha <=', $fecha2);
    }
    elseif ($this->input->post('fecha1'))
    {
      $fecha = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $this->db->where('op_fecha =',$fecha);
    }

    return $this->db->get()->result();
  }

  public function insert_item($param)
  {
    $this->db->insert('item_producto_orden', $param);
  }

  public function load_ultim_10()
  {
    //$this->db->where('orp_id', $id);
    $this->db->from('orden_producto');
    $this->db->join('panaderias', 'panaderias.bod_id = orden_producto.op_sede');
    $this->db->join('proveedores', 'proveedores.prov_id = orden_producto.op_proveedor');
    $this->db->limit('10');
    return $this->db->get()->result();
  }

  public function read($id)
  {
    $this->db->where('op_id', $id);
    $this->db->from('orden_producto');
    $this->db->join('panaderias', 'panaderias.bod_id = orden_producto.op_sede');
    $this->db->join('proveedores', 'proveedores.prov_id = orden_producto.op_proveedor');
    return $this->db->get()->result();
  }

  public function delete_item($id)
  {
    $this->db->where('ipo_id',$id);
    $this->db->delete('item_producto_orden');
  }

  public function update($param)
  {
    $this->db->where('op_id',$param['op_id']);
    $this->db->update('orden_producto',$param);
  }

  public function read_item_producto($id)
  {
    $this->db->where('ipo_orden', $id);
    $this->db->from('item_producto_orden');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = item_producto_orden.ipo_producto');
    return $this->db->get()->result();
  }

}

?>
