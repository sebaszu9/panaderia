<?php

class model_parametrizacion extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function read()
  {
    $this->db->select('*');
    // $this->db->where("para_int",'0');
    $this->db->from('parametrizacion');
    return $this->db->get()->result();
  }
  function update($data)
  {
    $this->db->where("para_int",$data["para_int"]);
    $this->db->update("parametrizacion",$data);
  }

}

?>
