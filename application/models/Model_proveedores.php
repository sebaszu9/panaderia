<?php

/**
 *
 */
class Model_proveedores extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('proveedores', $param);
  }

  function display() {
    $this->db->select('*');
    $this->db->from('proveedores');
    $this->db->where('prov_del', 0);
    return $this->db->get()->result();
  }

  function read($id)
  {
    $this->db->select('*');
    $this->db->where('prov_id', $id);
    $this->db->where('prov_del', 0);
    $this->db->from('proveedores');

    return $this->db->get()->result();
  }

  function delete($id)
  {
    $this->db->where('prov_id',$id);
    $param['prov_del'] = 1;
    $exito = $this->db->update('proveedores', $param);
    /*
    $this->db->where('prov_id', $id);
    $this->db->delete('proveedores');
    echo "<script type=text/javascript>alert('Se ha  eliminado el Proovedor');</script>";
    */
    }

  function update($param)
  {
    $id = $param ['prov_id'];
    echo $id;
    $this->db->where('prov_id', $id);
    $this->db->update('proveedores', $param);
    echo "<script type=text/javascript>alert('Se ha actualizado exitosamente el Proveedor');</script>";
  }

}



?>
