<?php

/**
 *
 */
class model_recetas extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('recetas_prod', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  function update($param)
  {
    $this->db->where('rec_id', $param['rec_id']);
    $this->db->update('recetas_prod', $param);
  }

  function display()
  {
    $this->db->select('*');
    $this->db->from('recetas_prod');
    //$this->db->join('productos', 'productos.prod_id = recetas_prod.rec_prod');

    return $this->db->get()->result();
  }

  function search($data)
  {
    $this->db->select('*');
    $this->db->where('rec_activo', "1");
    $this->db->from('recetas_prod');
    $this->db->like('rec_nombre',$data);
    //$this->db->join('productos', 'productos.prod_id = recetas_prod.rec_prod');
    $query =  $this->db->get();

    if ($query->num_rows() > 0 )
    {
        return $query;
    }
    else
    {
        return FALSE;
    }

  }

  function display_active()
  {
    $this->db->select('*');
    $this->db->where('rec_activo', "1");
    $this->db->from('recetas_prod');
    //$this->db->join('productos', 'productos.prod_id = recetas_prod.rec_prod');
    return $this->db->get()->result();
  }

  function display_ing($id)
  {
    $this->db->select('*');
    $this->db->where('ing_rec', $id);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = ingredientes_prod.ing_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('conceptos_rec', 'conceptos_rec.cr_id = ingredientes_prod.ing_concepto');
    $this->db->from('ingredientes_prod');

    return $this->db->get()->result();
  }

  function read($id)
  {
    $this->db->select('*');
    $this->db->where('rec_id', $id);
    $this->db->from('recetas_prod');

    return $this->db->get()->result();
  }

  function read_ing($id)
  {
    $this->db->select('*');
    $this->db->where('ing_cant >=',0);
    $this->db->where('ing_rec',$id);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = ingredientes_prod.ing_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('conceptos_rec', 'conceptos_rec.cr_id = ingredientes_prod.ing_concepto');
    $this->db->from('ingredientes_prod');

    return $this->db->get()->result();
  }

  function insert_ing($param)
  {
    $this->db->insert('ingredientes_prod', $param);
  }

  function delete_ing($id)
  {
    $this->db->where('ing_id',$id);
    $this->db->delete('ingredientes_prod');
  }

  public function active($param){
    $id = $param ['rec_id'];
    $data ['rec_activo'] = $param ['state'];
    $this->db->where('rec_id', $id);
    $this->db->update('recetas_prod', $data);
  }

  function insert_calc($param)
  {
    $this->db->insert('calculos_rec', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  function read_calc($id)
  {
    $this->db->select('*');
    $this->db->where('calc_id', $id);
    $this->db->join('recetas_prod', 'recetas_prod.rec_id = calculos_rec.calc_rec');
    //$this->db->join('productos', 'productos.prod_id = recetas_prod.rec_prod');
    $this->db->from('calculos_rec');
    return $this->db->get()->result();
  }

  function delete_rec($id)
  {
    $this->db->where('rec_id',$id);
    $this->db->delete('recetas_prod');
  }

  function delete_rec_ing($id)
  {
    $this->db->where('ing_rec',$id);
    $this->db->delete('ingredientes_prod');
  }
  function update_ing ($param)
  {
    $this->db->where('ing_id',$param['ing_id']);
    $this->db->update('ingredientes_prod',$param);
  }

  function display_prod($id)
  {
    $this->db->select('*');
    $this->db->where('pr_rec',$id);
    $this->db->from('prod_recetas');
    $this->db->join('productos', 'productos.prod_id = prod_recetas.pr_prod');
    return $this->db->get()->result();
  }

  function load_prod_per_prod($id)
  {
    $this->db->select('*');
    $this->db->where('pr_prod',$id);
    $this->db->from('prod_recetas');
    $this->db->join('productos', 'productos.prod_id = prod_recetas.pr_prod');
    return $this->db->get()->result();
  }

  function load_prod_per_prod2($paramrec)
  {
    $this->db->select('*');
    $this->db->where('pr_rec',$paramrec['receta']);
    $this->db->where('pr_prod',$paramrec['producto']);
    $this->db->from('prod_recetas');
    $this->db->join('productos', 'productos.prod_id = prod_recetas.pr_prod');
    return $this->db->get()->result();
  }


  function display_ing_prod($id)
  {
    $this->db->select('*');
    $this->db->where('pr_rec',$id);
    $this->db->from('ingredientes_pr');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = ingredientes_pr.ing_pr_mat');
    $this->db->join('prod_recetas', 'prod_recetas.pr_id = ingredientes_pr.ing_pr_prod');
    $this->db->join('productos', 'productos.prod_id = prod_recetas.pr_prod');
    $this->db->join('conceptos_rec', 'conceptos_rec.cr_id = ingredientes_pr.ing_pr_concepto');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');

    return $this->db->get()->result();
  }

  function insert_prod($param)
  {
    $this->db->insert('prod_recetas', $param);
  }

  function insert_ing_prod($param)
  {
    $this->db->insert('ingredientes_pr', $param);
  }

  function insert_prod_calc($param)
  {
    $this->db->insert('prod_calculos', $param);
  }

  function display_prod_calc($id)
  {
    $this->db->select('*');
    $this->db->where('pc_calculo',$id );
    $this->db->join('productos', 'productos.prod_id = prod_calculos.pc_prod');
    $this->db->from('prod_calculos');
    return $this->db->get()->result();
  }

  function read_prod_calc($id)
  {
    $this->db->select('*');
    $this->db->where('pc_id',$id);
    $this->db->from('prod_calculos');
    $this->db->join('productos', 'productos.prod_id = prod_calculos.pc_prod');

    return $this->db->get()->result();
  }

  function update_prod_calc($param)
  {
    $this->db->where('pc_id',$param['pc_id']);
    $this->db->update('prod_calculos',$param);
  }

  function insert_mat($param)
  {
    $this->db->insert('mat_recetas', $param);
  }

  function display_mat($id)
  {
    $this->db->select('*');
    $this->db->where('mr_rec',$id);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mat_recetas.mr_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->from('mat_recetas');

    return $this->db->get()->result();
  }

  function display_mat_calc($id)
  {
    $this->db->select('*');
    $this->db->where('mc_calculo',$id);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mat_calculo.mc_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->from('mat_calculo');

    return $this->db->get()->result();
  }

  function insert_mat_calc($param)
  {
    $this->db->insert('mat_calculo', $param);
  }

  function read_mat_calc($id)
  {
    $this->db->select('*');
    $this->db->where('mc_id',$id);
    $this->db->from('mat_calculo');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mat_calculo.mc_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');

    return $this->db->get()->result();
  }

  function update_mat_calc($param)
  {
    $this->db->where('mc_id',$param['mc_id']);
    $this->db->update('mat_calculo',$param);
  }

  function delete_prod($id) ///ELIMINA PRODUCTOS DE LA RECETA
  {
    $this->db->where('pr_id',$id);
    $this->db->delete('prod_recetas');
  }

  function delete_pr_id($id) ////ELIMINA SUBINGREDIENTES POR ID
  {
    $this->db->where('ing_pr_id',$id);
    $this->db->delete('ingredientes_pr');
  }

  function delete_pr_prod($id) ////ELIMINA SUBINGREDIENTES POR ID
  {
    $this->db->where('ing_pr_prod',$id);
    $this->db->delete('ingredientes_pr');
  }

  function delete_mat($mat)////ELIMINA MATERIAS POR PRODUCTO
  {
    $this->db->where('mr_id',$mat);
    $this->db->delete('mat_recetas');
  }

  function read_ing_base($id) /// le ingrediente base de la receta por individual
  {
    $this->db->select('*');
    $this->db->where('ing_id',$id );
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = ingredientes_prod.ing_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('conceptos_rec', 'conceptos_rec.cr_id = ingredientes_prod.ing_concepto');
    $this->db->from('ingredientes_prod');
    return $this->db->get()->result();
  }

  function update_prod($param)////ACTUALIZA LA CANTIDA DEL Producto DE LA RECETA
  {
    $this->db->where('pr_id',$param['pr_id']);
    $this->db->update('prod_recetas',$param);
  }

  function read_mat($id)
  {
    $this->db->select('*');
    $this->db->where('mr_id',$id);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mat_recetas.mr_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->from('mat_recetas');

    return $this->db->get()->result();
  }

  function update_mat($param)////ELIMINA MATERIAS POR PRODUCTO
  {
    $this->db->where('mr_id',$param['mr_id']);
    $this->db->update('mat_recetas',$param);
  }

  function read_subing($id)
  {
    $this->db->select('*');
    $this->db->where('ing_pr_id',$id);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = ingredientes_pr.ing_pr_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('conceptos_rec', 'conceptos_rec.cr_id = ingredientes_pr.ing_pr_concepto');
    $this->db->from('ingredientes_pr');

    return $this->db->get()->result();
  }

  function update_subing($param)////actualiza subingrediente cantidad
  {
    $this->db->where('ing_pr_id',$param['ing_pr_id']);
    $this->db->update('ingredientes_pr',$param);
  }
  function update_receta($param)////actualiza datos receta
  {
    $this->db->where('rec_id',$param['rec_id']);
    $this->db->update('recetas_prod',$param);
  }

  function display_concepto()
  {
    $this->db->select('*');
    $this->db->from('conceptos_rec');
    return $this->db->get()->result();
  }

  function read_pr($id)
  {
    $this->db->select('*');
    $this->db->where('pr_id',$id);
    $this->db->from('prod_recetas');
    $this->db->join('productos', 'productos.prod_id = prod_recetas.pr_prod');

    return $this->db->get()->result();
  }
}
?>
