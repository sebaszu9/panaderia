<?php

/**
 *
 */
class Model_materias extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('materiasprimas', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  function display()
  {
    $this->db->select('*');
    $this->db->from('materiasprimas');
    $this->db->where('mat_del', 0);
    $this->db->join('categorias_mat', 'categorias_mat.cat_mat_id = materiasprimas.mat_categoria');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->order_by('mat_nombre ASC');
    return $this->db->get()->result();
  }

  public function load_per_catg($id)
  {
    $this->db->select('*');
    $this->db->from('materiasprimas');
    $this->db->where('mat_del', 0);
    $this->db->where('mat_categoria', $id);
    return $this->db->get()->result();
  }


  function read($id)
  {
    $this->db->select('*');
    $this->db->where('mat_id', $id);
    $this->db->where('mat_del', 0);
    $this->db->from('materiasprimas');
    $this->db->join('categorias_mat', 'categorias_mat.cat_mat_id = materiasprimas.mat_categoria');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    return $this->db->get()->result();
  }

  function delete($id)
  {

    $this->db->where('mat_id',$id);
    $param['mat_del'] = 1;
    $exito = $this->db->update('materiasprimas', $param);

    /*
    $this->db->where('mat_id', $id);
    $this->db->delete('materiasprimas');
    */
    //echo "<script type=text/javascript>alert('Se ha  eliminado la materia prima');</script>";

    }

  function update($param)
  {
    $id = $param ['mat_id'];
    $this->db->where('mat_id', $id);
    $this->db->update('materiasprimas', $param);
    echo "<script type=text/javascript>alert('Se ha actualizado exitosamente la matria prima');</script>";
  }

  public function load_ing_comp_per_mat($mat)
  {
    $this->db->select('*');
    $this->db->from('ingrediente_compuesto');
    $this->db->where('inc_padre', $mat);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = ingrediente_compuesto.inc_ingrediente');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->order_by('mat_nombre','ASC');
    return $this->db->get()->result();
  }

  public function insert_mat_ingrediente($mat)
  {
    $this->db->insert('ingrediente_compuesto', $mat);
    //$id = $this->db->insert_id();
    //return $id;
  }

  public function delete_ingcomp($id)
  {
    $this->db->where('inc_id', $id);
    $this->db->delete('ingrediente_compuesto');
  }

}



?>
