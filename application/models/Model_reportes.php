<?php

/**
 *
 */
class Model_reportes extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function prod_day($day)
  {
    $this->db->select('*');
    $this->db->where('prodt_date', $day);
    $this->db->from('produccion');
    $this->db->join('recetas', 'recetas.rec_id = produccion.prodt_rec');
    $this->db->join('productos', 'productos.prod_id = recetas.rec_prod');

    return $this->db->get()->result();
  }

  function dislpay_mm($date1,$date2,$id)
  {
    $this->db->select('*');
    $this->db->where('mm_id_mat', $id);
    $this->db->where('cmm_tipo', 'out');
    $this->db->where('cmm_fecha >=', $date1);
    $this->db->where('cmm_fecha <=', $date2);
    $this->db->join('conceptos_mm', 'conceptos_mm.cmm_id = mov_mat.mm_id_concepto');

    $this->db->from('mov_mat');
    return $this->db->get()->result();
  }

  function dislpay_mp($date1,$date2,$id)
  {
    $this->db->select('*');
    $this->db->where('mp_id_prod', $id);
    $this->db->where('cmp_tipo', 'in');
    $this->db->where('cmp_fecha >=', $date1);
    $this->db->where('cmp_fecha <=', $date2);
    $this->db->from('mov_prod');
    $this->db->join('conceptos_mp', 'conceptos_mp.cmp_id = mov_prod.mp_id_concepto');
    return $this->db->get()->result();
  }

}


?>
