<?php

/**
 *
 */
class Model_impuestos extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('impuestos', $param);
  }

  function display()
  {
    $this->db->select('*');
    $this->db->from('impuestos');
    $this->db->where('imp_del', 0);
    return $this->db->get()->result();
  }


function read($id)
{
  $this->db->select('*');
  $this->db->where('imp_id', $id);
  $this->db->where('imp_del', 0);
  $this->db->from('impuestos');

  return $this->db->get()->result();

}

  function delete($id)
  {
    $this->db->where('imp_id', $id);
    $this->db->delete('impuestos');
    echo "<script type=text/javascript>alert('Se ha  eliminado la  impuesto');</script>";
  }

  function delete_pipe($id)
  {

    $this->db->where('imp_id',$id);
    $param['imp_del'] = 1;
    $exito = $this->db->update('impuestos', $param);

    if ($exito==true)
    {
      return true;
    }
    else
    {
      $error2  = $this->db->error();
      $error = json_encode($error2);
      echo " 235345 Error...  ".$error;
    }

/*
    $fields = array( 'imp_delete' => array('type' => 'int', 'value' => 0,'null' => FALSE) );
    $good = $this->dbforge->add_column('impuestos', $fields);

    if ($good)
    {
      echo "fue un exito....";
    }
    else{
      echo "xx error xxx ";
    }
*/
    //echo "Error: " .   $error = $this->db->error();
    /*

    /*
    $this->db->where('imp_id',$id);
    $param['imp_delete'] = 1;
    $this->db->update('impuestos', $param);
    */
  }

  function update($param)
  {
    $this->db->where('imp_id', $param ['imp_id']);
    $this->db->update('impuestos', $param);
    echo "<script type=text/javascript>alert('Se ha actualizado exitosamente el impuesto');</script>";
  }
  function val_exist($id)
  {
    $this->db->select('*');
    $this->db->where('prod_imp', $id);
    $query = $this->db->get('productos');

    if ($query->num_rows() > 0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
  }

}



?>
