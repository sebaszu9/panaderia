<?php

/**
 *
 */
class Model_pedidos extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function load_today()
  {

    $hoy = date("Y-m-d");
    // echo $fecha->format('U = Y-m-d H:i:s') . "\n";
    // echo $fecha->format('Y-m-d H:i:s') . "\n";
    // $sede = $this->session->userdata('sede');
    $this->db->select('*');
    $this->db->from('pos_pedidos');
    $this->db->join('users', 'users.id = pos_pedidos.pp_usuario','left');
    $this->db->join('cliente', 'cliente.clt_id = pos_pedidos.pp_cliente','left');
    // $this->db->where('pp_delete', '1');
    // $this->db->where('pp_sede', $sede);
    $mañana = date('Y-m-d',strtotime($hoy. ' + 1 days'));
    $this->db->where('pp_fecha >=', $hoy);
    $this->db->where('pp_fecha <=', $mañana);
    return $this->db->get()->result();
  }

  public function load_date($data)
  {
    // $sede = $this->session->userdata('sede');
    // echo 'VENDEDOR modelo:'.$data['vendedor'];
    $this->db->select('*');
    if ($data['cliente']!=0) {
      $this->db->where('pp_cliente <=', $data['cliente']);
    }
    if ($data['vendedor']!=0) {
      // echo "entro";
      $this->db->where('pp_usuario', $data['vendedor']);
    }
    if ($data['fecha2']==0) {
      // code...
      $data['fecha2'] = date('Y-m-d',strtotime($data['fecha1']. ' + 1 days'));
      // echo "fecha tratada".$data['fecha2'];
      $this->db->where('pp_fecha >=', $data['fecha1']);
      $this->db->where('pp_fecha <=', $data['fecha2']);

    }
    if ($data['fecha2']!=0) {
      // code...
      $this->db->where('pp_fecha >=', $data['fecha1']);
      $this->db->where('pp_fecha <=', $data['fecha2']);
    }
    $this->db->join('users', 'users.id = pos_pedidos.pp_usuario','left');
    $this->db->join('cliente', 'cliente.clt_id = pos_pedidos.pp_cliente','left');
    $this->db->from('pos_pedidos');
    return $this->db->get()->result();
  }

  public function read($id)
  {
    $this->db->select('*');
    $this->db->where('pp_id',$id);
    $this->db->join('users', 'users.id = pos_pedidos.pp_usuario','left');
    $this->db->join('cliente', 'cliente.clt_id = pos_pedidos.pp_cliente','left');
    $this->db->from('pos_pedidos');
    return $this->db->get()->result();
  }

  public function read_details($id)
  {
    $this->db->select('*');
    $this->db->where('ppi_pedido',$id);
    $this->db->join('productos', 'productos.prod_id = pos_pedido_item.ppi_producto','left');
    $this->db->join('descuento_concepto', 'descuento_concepto.dc_id = pos_pedido_item.ppi_concepto');
    $this->db->from('pos_pedido_item');
    return $this->db->get()->result();
  }

  public function read_item($id)
  {
    $this->db->select('*');
    $this->db->where('ppi_id',$id);
    $this->db->join('productos', 'productos.prod_id = pos_pedido_item.ppi_producto','left');
    $this->db->join('descuento_concepto', 'descuento_concepto.dc_id = pos_pedido_item.ppi_concepto');
    $this->db->from('pos_pedido_item');
    return $this->db->get()->result();
  }

  public function delete_item($id)
  {
    $this->db->where('ppi_id', $id);
    $this->db->delete('pos_pedido_item');
  }

  public function insert_item($data)
  {
    $this->db->insert('pos_pedido_item',$data);
  }


  public function update($datos)
  {
    $this->db->select('*');
    $this->db->where('pp_id',$datos['pp_id']);
    $this->db->update('pos_pedidos',$datos);
    $this->db->from('pos_pedidos');
    return $this->db->get()->result();
  }

  public function load_mov_concept_pedido($datos)
  {
    $this->db->select('*');
    $this->db->join('pos_pedidos', 'pos_pedidos.pp_id = pos_pedido_item.ppi_pedido','left');
    // $this->db->join('productos', 'productos.prod_id = pos_pedido_item.ppi_producto','left');
    // $this->db->join('descuento_concepto', 'descuento_concepto.dc_id = pos_pedido_item.ppi_concepto');
    $hoy = $datos['fecha'];
    $manana = date('Y-m-d',strtotime($hoy. ' + 1 days'));
    // echo "//////////////////////////////////Hoy".$hoy."-mañana".$manana;
    $this->db->where('pp_fecha >=', $hoy);
    $this->db->where('pp_fecha <=', $manana);
    // $this->db->where('pp_fecha',$datos['fecha']);
    $this->db->where('ppi_producto',$datos['producto']);
    $this->db->where('ppi_concepto',$datos['concepto']);
    $this->db->from('pos_pedido_item');
    return $this->db->get()->result();
  }

  // public function load_per_day()
  // {
  //   //echo " ahqiwjiqwqw";
  //   $hoy = date("Y-m-d");
  //   $sede = $this->session->userdata('sede');
  //   $this->db->select('*');
  //   $this->db->where('pvi_fecha', $hoy);
  //   $this->db->where('pvi_sede', $sede);
  //   $this->db->join('usuario', 'usuario.usr_id = pos_venta_info.pvi_usuario','left');
  //   $this->db->join('cliente', 'cliente.clt_id = pos_venta_info.pvi_cliente','left');
  //   $this->db->from('pos_venta_info');
  //   return $this->db->get()->result();
  // }
  //
  // public function delete($id)
  // {
  //   $this->db->where('id_venta', $id);
  //   $this->db->delete('ventas');
  // }
  //
  // public function read($id)
  // {
  //   $this->db->select('*');
  //   $this->db->where('pvi_id',$id);
  //   $this->db->join('usuario', 'usuario.usr_id = pos_venta_info.pvi_usuario','left');
  //   $this->db->join('cliente', 'cliente.clt_id = pos_venta_info.pvi_cliente','left');
  //   $this->db->from('pos_venta_info');
  //   return $this->db->get()->result();
  // }
  //
  // public function read_details($id)
  // {
  //   $this->db->select('*');
  //   $this->db->where('pvd_venta',$id);
  //   $this->db->join('productos', 'productos.prod_id = pos_venta_item.pvd_prod','left');
  //   $this->db->from('pos_venta_item');
  //   return $this->db->get()->result();
  // }
  //
  // public function delete_details($id)
  // {
  //   $this->db->where('dtv_id', $id);
  //   $this->db->delete('detalles_venta');
  // }


}



?>
