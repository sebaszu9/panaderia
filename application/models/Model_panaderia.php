<?php

/**
 *
 */
class Model_panaderia extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('categorias_mat', $param);
  }

  public function load()
  {
    $this->db->select('*');
    $this->db->from('panaderias');
    return $this->db->get()->result();
  }

  public function convert_text_date($date)
  {
    //$data = "2018-05-02";
    $mes = date('m', strtotime($date));
    $mesres = "";
    //echo  $mes;
    if ($mes==1) {
      return $mesres = "Enero";
    }
    elseif ($mes==2) {
      return $mesres =  "Febrero";
    }
    elseif ($mes==3) {
      return $mesres =  "Marzo";
    }
    elseif ($mes==4) {
      return $mesres =  "Abril";
    }
    elseif ($mes==5) {
      return $mesres =  "Mayo";
    }
    elseif ($mes==6) {
      return $mesres =  "Junio";
    }
    elseif ($mes==7) {
      return $mesres =  "Julio";
    }
    elseif ($mes==8) {
      return $mesres =  "Agosto";
    }
    elseif ($mes==9) {
      return $mesres =  "Septiembre";
    }
    elseif ($mes==10) {
      return $mesres =  "Octubre";
    }
    elseif ($mes==11) {
      return $mesres =  "Noviembre";
    }
    elseif ($mes==12) {
      return $mesres =  "Diciembre";
    }
    else {
      return $mesres =  "Error";
    }
  }


}



?>
