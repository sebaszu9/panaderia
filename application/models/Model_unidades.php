<?php

/**
 *
 */
class Model_unidades extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('unidades', $param);
  }

  function display() {
    $this->db->select('*');
    $this->db->from('unidades');
    //$this->db->join('impuestos', 'impuestos.imp_id = productos.prod_imp');

    return $this->db->get()->result();
  }

  function unidades_tipo($tipo) {
    $this->db->select('*');
    $this->db->where('unidad_tipo',$tipo);
    $this->db->from('unidades');
    //$this->db->join('impuestos', 'impuestos.imp_id = productos.prod_imp');

    return $this->db->get()->result();
  }
}



?>
