<?php

/**
 *
 */
class model_ventas extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('pos_venta_info',$param);
    $id = $this->db->insert_id();
    return $id;
  }

  function insert_details($paramd)
  {
    $this->db->insert('pos_venta_item',$paramd);
  }

  public function load_per_day()
  {
    //echo " ahqiwjiqwqw";
    $hoy = date("Y-m-d");
    $sede = $this->session->userdata('sede');
    $this->db->select('*');
    $this->db->where('pvi_fecha', $hoy);
    $this->db->where('pvi_sede', $sede);
    $this->db->join('usuario', 'usuario.usr_id = pos_venta_info.pvi_usuario','left');
    $this->db->join('cliente', 'cliente.clt_id = pos_venta_info.pvi_cliente','left');
    $this->db->from('pos_venta_info');
    return $this->db->get()->result();
  }

  public function delete($id)
  {
    $this->db->where('id_venta', $id);
    $this->db->delete('ventas');
  }

  public function read($id)
  {
    $this->db->select('*');
    $this->db->where('pvi_id',$id);
    $this->db->join('usuario', 'usuario.usr_id = pos_venta_info.pvi_usuario','left');
    $this->db->join('cliente', 'cliente.clt_id = pos_venta_info.pvi_cliente','left');
    $this->db->from('pos_venta_info');
    return $this->db->get()->result();
  }

  public function read_details($id)
  {
    $this->db->select('*');
    $this->db->where('pvd_venta',$id);
    $this->db->join('productos', 'productos.prod_id = pos_venta_item.pvd_prod','left');
    $this->db->from('pos_venta_item');
    return $this->db->get()->result();
  }

  public function delete_details($id)
  {
    $this->db->where('dtv_id', $id);
    $this->db->delete('detalles_venta');
  }


}



?>
