<?php

/**
 *
 */
class Model_orden_prod extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    ini_set('date.timezone','America/Bogota');
  }

  function insert($param)
  {
    $this->db->insert('orden_produccion', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  public function load_ultim_10()
  {
    //$this->db->where('orp_id', $id);
    $this->db->from('orden_produccion');
    $this->db->join('panaderias', 'panaderias.bod_id = orden_produccion.orp_sede');
    //$this->db->join('recetas_prod', 'recetas_prod.rec_id = orden_produccion.orp_receta');
    $this->db->order_by('orp_id','desc');
    //$this->db->limit('10');
    return $this->db->get()->result();
  }

  public function load_per_sede($sede)
  {

    $this->db->from('orden_produccion');
    $this->db->join('panaderias', 'panaderias.bod_id = orden_produccion.orp_sede');
    //$this->db->join('recetas_prod', 'recetas_prod.rec_id = orden_produccion.orp_receta');
    $this->db->where('orp_sede', $sede);
    return $this->db->get()->result();
  }

  public function load_per_sede_day($sede)
  {
    $hoy = date("Y/m/d");
    $this->db->from('orden_produccion');
    $this->db->join('panaderias', 'panaderias.bod_id = orden_produccion.orp_sede');
    //$this->db->join('recetas_prod', 'recetas_prod.rec_id = orden_produccion.orp_receta');
    $this->db->where('orp_sede', $sede);
    $this->db->where('orp_fecha', $hoy);
    return $this->db->get()->result();
  }

  public function search()
  {
    $this->db->from('orden_produccion');
    $this->db->join('panaderias', 'panaderias.bod_id = orden_produccion.orp_sede');
    $this->db->join('recetas_prod', 'recetas_prod.rec_id = orden_produccion.orp_receta');
    if ($this->input->post('fecha')) {
      $this->db->where('orp_fecha', $this->input->post('fecha'));
    }
    if ($this->input->post('sede')>=1) {
      $this->db->where('orp_sede', $this->input->post('sede'));
    }
    if ($this->input->post('receta')) {
      $this->db->where('orp_receta', $this->input->post('receta'));
    }
    //$this->db->limit('10');
    return $this->db->get()->result();
  }

  public function read($id)
  {
    $this->db->where('orp_id', $id);
    $this->db->from('orden_produccion');
    $this->db->join('panaderias', 'panaderias.bod_id = orden_produccion.orp_sede');
    //$this->db->join('recetas_prod', 'recetas_prod.rec_id = orden_produccion.orp_receta');
    return $this->db->get()->result();
  }

  public function insert_produccion_product($param)
  {
    $this->db->insert('produccion_producto', $param);
  }

  public function read_prod_producto($id)
  {
    $this->db->where('pp_orden', $id);
    $this->db->from('produccion_producto');
    $this->db->join('productos', 'productos.prod_id = produccion_producto.pp_producto');
    $this->db->join('recetas_prod', 'recetas_prod.rec_id = produccion_producto.pp_receta');
    return $this->db->get()->result();
  }

  public function read_prod_producto_id($id)
  {
    $this->db->where('pp_id', $id);
    $this->db->from('produccion_producto');
    $this->db->join('productos', 'productos.prod_id = produccion_producto.pp_producto');
    $this->db->join('recetas_prod', 'recetas_prod.rec_id = produccion_producto.pp_receta');
    return $this->db->get()->result();
  }

  public function read_prod_id($id)
  {
    $this->db->where('pp_id', $id);
    $this->db->from('produccion_producto');
    $this->db->join('productos', 'productos.prod_id = produccion_producto.pp_producto');
    return $this->db->get()->result();
  }

  public function del_prod_producto($id)
  {
    $this->db->where('pp_id',$id);
    $this->db->delete('produccion_producto');
  }

  public function del_prod_producto_ord($id)
  {
    $this->db->where('pp_orden',$id);
    $this->db->delete('produccion_producto');
  }

  public function delete($id)
  {
    $this->db->where('orp_id',$id);
    $this->db->delete('orden_produccion');
  }

  public function update_prod_producto($param)
  {
    $this->db->where('pp_id',$param['pp_id']);
    $this->db->update('produccion_producto',$param);
  }



}

?>
