<?php

/**
 *
 */
class Model_conceptos_rec extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('conceptos_rec', $param);
  }



  function display() {
    $this->db->select('*');
    $this->db->from('conceptos_rec');

    return $this->db->get()->result();
}


function read($id)
{
  $this->db->select('*');
  $this->db->where('cr_id', $id);
  $this->db->from('conceptos_rec');

  return $this->db->get()->result();

}

function read_concept($id)
{
  $this->db->select('*');
  $this->db->where('c_id', $id);
  $this->db->from('conceptos');
  return $this->db->get()->result();
}

  function delete($id)
  {
    $this->db->where('cr_id', $id);
    $this->db->delete('conceptos_rec');
    echo "<script type=text/javascript>alert('Se ha  eliminado el concepto');</script>";
    }

  function update($param)
  {
    $this->db->where('cr_id', $param ['cr_id']);
    $this->db->update('conceptos_rec', $param);
    echo "<script type=text/javascript>alert('Se ha actualizado exitosamente el concepto');</script>";
  }

  function val_exist($id)
  {
    $this->db->select('*');
    $this->db->where('ing_concepto', $id);
    $query = $this->db->get('recetas_prod');

    if ($query->num_rows() > 0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
  }

}



?>
