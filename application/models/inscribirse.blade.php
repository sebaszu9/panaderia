<!DOCTYPE html>

<?
session_start();
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class=" bg-light" style="border: 1px solid #ced4da; width:100%; height:70px; margin: 0px; padding:10px; position:solid; text-align:right;" >
      <a class="btn btn-info" target="_blank" href="{{ action('ControllerInicio@form_registro_publico') }}">
        REGISTRARSE
      </a>
    </div>

    <div class="row bg-white" style=" width: 60%; margin:50px 20%; border: 1px solid #ced4da; border-radius: 6px; padding:10px;">
      <div class="col-md-12" style="text-align:center; margin:10px 0px;">
          <h2>Inscribirse a un evento</h2>
      </div>
      <?php
      date_default_timezone_set("America/Bogota");
      $a=date("Y");
      $m=date("m");
      $d=date("d");
      $data= $a.'-'.$m.'-'.$d;
      $disabled="";
      $disa="";
      ?>
      @foreach ($evento as $value)
        <div class="row" style="width:90%;">
          <?php if($data<=$value->fecha){$disabled=""; $disa="";}else{$disabled="disabled"; $disa="pointer-events: none;";}?>
          <div class="col-md-11">
            <div class="row">
              <div class="col-md-6">
                <label class="form-control"{{$disabled}}>{{$value->nombre}}</label>
              </div>
              <div class="col-md-3">
                <label class="form-control"{{$disabled}}>{{$value->fecha}}</label>
              </div>
              <div class="col-md-3">
                <label class="form-control"{{$disabled}}>{{$value->hora}}</label>
              </div>
            </div>
          </div>
          <div class="col-md-1">
            @if($data<=$value->fecha)
              <a href='{{action('ControllerInicio@consult_cc',$value->id)}}' target='_blank' class='btn btn-info'>Inscribirse</a>
            @endif

          </div>
        </div>
      @endforeach
    </div>
  </body>
</html>
<style media="screen">
body{
  padding: 0px;
  background-color: #eef;
}
div{
  padding: 1px 0px;
}
label{
  width:100%;
 display:inline-block;
}
</style>
