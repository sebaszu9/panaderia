<?php

class Model_mesas extends CI_Model {
    function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  // ZONAS

  function display_zonas (){

    $query=$this->db->query("SELECT *
                      FROM mesas_zona
                      WHERE mesas_zona.zona_delete=0");

    return $query->result();

  }

  function insert_zonas($datos)
  {
    $this->db->insert("mesas_zona",$datos);
  }

  function delete_zonas($data)
  {
    $this->db->where("zona_id",$data["zona_id"]);
    $this->db->update("mesas_zona",$data);

  }

  function edit_zonas($id)
  {
    $query=$this->db->query("SELECT *
                      FROM mesas_zona
                      WHERE mesas_zona.zona_id=$id AND mesas_zona.zona_delete=0");

    return $query->result();
  }

  function update_zonas($data)
  {
    $this->db->where("zona_id",$data["zona_id"]);
    $this->db->update("mesas_zona",$data);
  }

  function display_mesas(){
    $query=$this->db->query("SELECT *
                             FROM mesas_mesa
                             INNER JOIN mesas_zona
                             ON mesas_zona.zona_id=mesas_mesa.mesas_zona
                             WHERE mesas_mesa.mesas_delete=0");
    return $query->result();

  }

  function display_mesas_by_zona($id){
      $this->db->select('*');
      $this->db->from('mesas_mesa');
      $this->db->where('mesas_zona', $id);
      $this->db->where('mesas_delete', 0);
      return $this->db->get()->result();
  }
  function display_cuenta_mesa($id){
      $this->db->select('*');
      $this->db->from('cuentas');
      $this->db->join('mesas_mesa', 'mesas_mesa.mesas_id = cuentas.cuenta_mesa');
      $this->db->where('cuenta_mesa', $id);
      return $this->db->get()->result();
  }
  function display_cuenta($id){
      $this->db->select('*');
      $this->db->from('cuentas');
      $this->db->join('mesas_mesa', 'mesas_mesa.mesas_id = cuentas.cuenta_mesa');
      $this->db->where('cuenta_id', $id);
      return $this->db->get()->result();
  }

  function update_cuenta($data){
      $this->db->from('cuentas');
      $this->db->where('cuenta_id', $data['cuenta_id']);
      $this->db->update("cuentas",$data);
  }

  function insert_mesas($datos)
  {
    $this->db->insert("mesas_mesa",$datos);
    $insert_id = $this->db->insert_id();

    return  $insert_id;
  }
  function insert_info_mesas($datos)
  {
    $this->db->insert("cuentas",$datos);
   //recuperar el id de la mesa para usarlo en la tabla cuenta
  }

  function edit_mesas($id)
  {
    $query=$this->db->query("SELECT *
                      FROM mesas_mesa
                      WHERE mesas_mesa.mesas_id=$id AND mesas_mesa.mesas_delete=0");

    return $query->result();
  }


  function delete_mesas($data)
  {

    $this->db->where("mesas_id",$data["mesas_id"]);
    $this->db->update("mesas_mesa",$data);

  }

  function update_mesas($data){

    $this->db->where("mesas_id",$data["mesas_id"]);
    $this->db->update("mesas_mesa",$data);
  }



}


?>
