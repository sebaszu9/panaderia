<?php

/**
 *
 */
class model_movimiento extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function load_concept()
  {
    //basura
  }

  public function search()
  {
    $this->db->select('*');
    $this->db->from('movimiento');
    $this->db->join('relacion_pedidos', 'relacion_pedidos.rp_cm = movimiento.cm_id','left');
    $this->db->join('proveedores', 'proveedores.prov_id = relacion_pedidos.rp_prov','left');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    $this->db->join('usuario',  'usuario.usr_id = movimiento.cm_usuario');
    $this->db->join('conceptos',  'conceptos.c_id = movimiento.cm_concepto');
    // consulta de proveedor
    //if ($this->input->post('proveedor'))
    //{ $this->db->where('rp_prov',$this->input->post('proveedor')); }
    //consulta de sede panaderia
    if ($this->input->post('concepto'))
    {   $this->db->where('cm_concepto',$this->input->post('concepto')); }
    if ($this->input->post('panaderia'))
    {   $this->db->where('cm_ubicacion',$this->input->post('panaderia')); }
    //consulta de fecha
    if ($this->input->post('fecha1')&&$this->input->post('fecha2'))
    {
      $fecha1 = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $fecha2 = date('Y-m-d', strtotime($this->input->post('fecha2')));
      $this->db->where('cm_fecha >=', $fecha1);
      $this->db->where('cm_fecha <=', $fecha2);
    }
    elseif ($this->input->post('fecha1'))
    {
      $fecha = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $this->db->where('cm_fecha >=',$fecha);
    }
    $this->db->where('cm_parte',$this->input->post('parte'));

    return $this->db->get()->result();
  }

  function read_mov_id($id)
  {
    $this->db->select('*');
    $this->db->where('cm_id', $id);
    $this->db->from('movimiento');
    $this->db->join('conceptos', 'conceptos.c_id = movimiento.cm_concepto');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    return $this->db->get()->result();
  }

  function read_order_proovedor($id)
  {
    $this->db->select('*');
    $this->db->where('rp_cm',$id);
    $this->db->from('relacion_pedidos');
    $this->db->join('proveedores', 'proveedores.prov_id = relacion_pedidos.rp_prov');
    return $this->db->get()->result();
  }

  function delete_mov($id)
  {
    $this->db->where('cm_id',$id);
    $this->db->delete('movimiento');
  }

  function delete_mov_proveedor($id)
  {
    $this->db->where('rp_cm',$id);
    $this->db->delete('relacion_pedidos');
  }

  function delete_mov_mat_per_mov($id)
  {
    $this->db->where('mm_id_concepto',$id);
    $this->db->delete('mov_mat');
  }

  function delete_mov_prod_per_prod($id)
  {
    $this->db->where('mp_id_concepto',$id);
    $this->db->delete('mov_prod');
  }

  function reporte($search)
  {
    $this->db->select('*');
    $this->db->from('movimiento');
    $this->db->where('cm_concepto', $search['concepto']);        
    $this->db->where('cm_fecha >=', $search['fecha_1']);
    $this->db->where('cm_fecha <=', $search['fecha_2']);
    //$this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    if ($search['sede']>=1)
    {   
      $this->db->where('cm_ubicacion',$search['sede']); 
    }
    
    return $this->db->get()->result();
  }


}

?>
