<?php

/**
 *
 */
class model_mov_mat extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function load_concept(){
    $this->db->from('conceptos');
    return $this->db->get()->result();
  }

  function insert_cmm($param)
  {
    $this->db->insert('movimiento', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  function update_cmm($param)
  {
    $this->db->where('cm_id', $param['cm_id']);
    $this->db->update('movimiento', $param);
  }

  function insert_rp($param)
  {
    $this->db->insert('relacion_pedidos', $param);
  }

  function update_rp($param)
  {
    $this->db->where('rp_cm', $param['rp_cm']);
    $this->db->update('relacion_pedidos', $param);
  }

  public function search($tipo)
  {
    $this->db->select('*');
    $this->db->where('cm_tipo', $tipo);
    $this->db->from('movimiento');
    $this->db->join('relacion_pedidos', 'relacion_pedidos.rp_cm = movimiento.cm_id');
    $this->db->join('proveedores', 'proveedores.prov_id = relacion_pedidos.rp_prov');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    $this->db->join('usuario',  'usuario.usr_id = movimiento.cm_usuario');
    // consulta de proveedor
    if ($this->input->post('proveedor'))
    {   $this->db->where('rp_prov',$this->input->post('proveedor')); }
    //consulta de sede panaderia
    if ($this->input->post('panaderia'))
    {   $this->db->where('cm_ubicacion',$this->input->post('panaderia')); }
    //consulta de fecha
    if ($this->input->post('fecha1')&&$this->input->post('fecha2'))
    {
      $fecha1 = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $fecha2 = date('Y-m-d', strtotime($this->input->post('fecha2')));
      $this->db->where('cm_fecha >=', $fecha1);
      $this->db->where('cm_fecha <=', $fecha2);
    }
    elseif ($this->input->post('fecha1'))
    {
      $fecha = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $this->db->where('cm_fecha >=',$fecha);
    }

    return $this->db->get()->result();
  }

  function reporte()
  {
    $this->db->select('*');
    $this->db->where('cm_tipo', 0);
    $this->db->where('cm_concepto', 2);
    $this->db->from('movimiento');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    if ($this->input->post('panaderia')>=1)
    {   $this->db->where('cm_ubicacion',$this->input->post('panaderia')); }
    //consulta de fecha
    if ($this->input->post('fecha1')&&$this->input->post('fecha2'))
    {
      $fecha1 = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $fecha2 = date('Y-m-d', strtotime($this->input->post('fecha2')));
      $this->db->where('cm_fecha >=', $fecha1);
      $this->db->where('cm_fecha <=', $fecha2);
    }
    elseif ($this->input->post('fecha1'))
    {
      $fecha = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $this->db->where('cm_fecha >=',$fecha);
    }
    return $this->db->get()->result();
  }

  function load_movimiento($tipo)
  {
    $this->db->select('*');
    $this->db->where('cm_tipo', $tipo);
    $this->db->from('movimiento');
    $this->db->join('relacion_pedidos', 'relacion_pedidos.rp_cm = movimiento.cm_id');
    $this->db->join('proveedores', 'proveedores.prov_id = relacion_pedidos.rp_prov');

    return $this->db->get()->result();
  }

  function display_cmm($tipo)
  {
    $this->db->select('*');
    $this->db->where('cm_tipo', $tipo);
    $this->db->from('movimiento');
    return $this->db->get()->result();
  }

  function read_cmm($id)
  {
    $this->db->select('*');
    $this->db->where('cm_id', $id);
    $this->db->from('movimiento');
    $this->db->join('relacion_pedidos', 'relacion_pedidos.rp_cm = movimiento.cm_id');
    $this->db->join('proveedores', 'proveedores.prov_id = relacion_pedidos.rp_prov');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    return $this->db->get()->result();
  }

  public function search_output()
  {
    $this->db->select('*');
    $this->db->where('cm_tipo',0);
    $this->db->from('movimiento');
    $this->db->join('conceptos', 'conceptos.c_id = movimiento.cm_concepto');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    $this->db->join('usuario',  'usuario.usr_id = movimiento.cm_usuario');
    //consulta de sede panaderia
    if ($this->input->post('concepto'))
    {   $this->db->where('c_id',$this->input->post('concepto')); }
    //consulta de sede panaderia
    if ($this->input->post('panaderia'))
    {   $this->db->where('cm_ubicacion',$this->input->post('panaderia')); }
    //consulta de fecha
    if ($this->input->post('fecha1')&&$this->input->post('fecha2'))
    {
      $fecha1 = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $fecha2 = date('Y-m-d', strtotime($this->input->post('fecha2')));
      $this->db->where('cm_fecha >=', $fecha1);
      $this->db->where('cm_fecha <=', $fecha2);
    }
    elseif ($this->input->post('fecha1'))
    {
      $fecha = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $this->db->where('cm_fecha >=',$fecha);
    }

    return $this->db->get()->result();
  }

  function read_cmm_output($id)
  {
    $this->db->select('*');
    $this->db->where('cm_id', $id);
    $this->db->from('movimiento');
    $this->db->join('conceptos', 'conceptos.c_id = movimiento.cm_concepto');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    return $this->db->get()->result();
  }

  function read_mov_mat($id)
  {
    $this->db->select('*');
    $this->db->where('mm_id_concepto', $id);
    $this->db->where('mm_cant >=',0);
    $this->db->from('mov_mat');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mov_mat.mm_id_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    return $this->db->get()->result();
  }

  function read_mov_mat_by_mov_advanced($search)
  {
    $this->db->select('*');
    //$this->db->where('mm_id_concepto', $id);
    $this->db->where('mm_cant >=',0);
    $this->db->from('mov_mat');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mov_mat.mm_id_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    $this->db->where('cm_concepto', $search['concepto']);
    $this->db->where('cm_fecha >=', $search['fecha_1']);
    $this->db->where('cm_fecha <=', $search['fecha_2']);
    if ($search['sede']>=1)
    {
      $this->db->where('cm_ubicacion',$search['sede']);
    }
    return $this->db->get()->result();
  }

  function read_mov_mat_materia($consult)
  {
    $this->db->select('*');
    $this->db->where('mm_id_concepto', $consult['movimiento']);
    $this->db->where('mm_id_mat', $consult['materias']);
    $this->db->where('mm_cant >=',0);
    $this->db->from('mov_mat');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mov_mat.mm_id_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    return $this->db->get()->result();
  }

  function read_mov($id)
  {
    $this->db->select('*');
    $this->db->where('mm_id', $id);
    $this->db->where('mm_cant >=',0);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mov_mat.mm_id_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    $this->db->from('mov_mat');

    return $this->db->get()->result();
  }

  function read_relacion($id)
  {
    $this->db->select('*');
    $this->db->where('rp_cm',$id);
    $this->db->from('relacion_pedidos');
    $this->db->join('proveedores', 'proveedores.prov_id = relacion_pedidos.rp_prov');
    return $this->db->get()->result();
  }

  function insert_mov($param)
  {
    $this->db->insert('mov_mat', $param);
  }

  function val_mov_exist($param)
  {
    $this->db->select('*');
    $this->db->where('mm_id_concepto', $param['mm_id_concepto']);
    $this->db->where('mm_id_mat',$param['mm_id_mat']);
    $query = $this->db->get('mov_mat');

    if ($query->num_rows() > 0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }

  }

  function update_mov($param)
  {
    $this->db->where('mm_id_concepto', $param['mm_id_concepto']);
    $this->db->where('mm_id_mat',$param['mm_id_mat']);
    $this->db->update('mov_mat', $param);
  }

  function delete_mov($id)
  {
    $this->db->where('mm_id',$id);
    $this->db->delete('mov_mat');
  }

  public function delete_relation_prov_per_cmm($id)
  {
    $this->db->where('rp_cm',$id);
    $this->db->delete('relacion_pedidos');
  }

  function delete_mov_per_cmm($id)
  {
    $this->db->where('mm_id_concepto',$id);
    $this->db->delete('mov_mat');
  }

  function delete_cmm($id)
  {
    $this->db->where('cm_id',$id);
    $this->db->delete('movimiento');
  }

  // ================ pipe =================
  public function update_mov_mat($param)
  {
    $data ['mm_cant']  = $param['cantd'];
    $data ['mm_costo'] = $param['price'];
    $this->db->where('mm_id',$param['id']);
    $this->db->update('mov_mat',$data);
  }

  public function update_mov_mat_prod($param)
  {
    $this->db->where('mm_id',$param['mm_id']);
    $this->db->update('mov_mat',$param);
  }

  function read_produccion($id)
  {
    $this->db->select('*');
    $this->db->where('cm_id', $id);
    $this->db->from('movimiento');
    $this->db->join('usuario', 'usuario.usr_id = movimiento.cm_usuario');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    return $this->db->get()->result();
  }

  ///-----funcion de mov mat para sacar promedios y reportes
  function prom_mov($param)
  {
    $this->db->select('*');
    $this->db->where('cm_fecha<=', $param['date2']);
    $this->db->where('cm_fecha>=', $param['date1']);
    $this->db->where('cm_concepto', $param['concepto']);
    $this->db->where('mm_id_mat', $param['mat']);
    $this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    $this->db->join('usuario', 'usuario.usr_id = movimiento.cm_usuario');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    $this->db->from('mov_mat');
    return $this->db->get()->result();
  }
  function produccion($data)
  {
    $this->db->select('*');
    $this->db->where('cm_fecha',$data['date']);
    $this->db->where('cm_ubicacion',$data['ubicacion']);
    $this->db->where('cm_tipo','1');
    $this->db->from('mov_mat');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = mov_mat.mm_id_mat');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    $this->db->join('panaderias', 'movimiento.cm_ubicacion = panaderias.bod_id');
    $this->db->join('categorias_mat', 'categorias_mat.cat_mat_id = materiasprimas.mat_categoria');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->join('usuario', 'movimiento.cm_usuario = usuario.usr_id');
    return $this->db->get()->result();
  }
}
?>
