<?php

/**
 *
 */
class Model_stock_mat extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function load_per_bakery($sede)
  {
    $this->db->select('*');
    $this->db->from('stock_mat');
    $this->db->where('sm_location',$sede);
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = stock_mat.sm_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    return $this->db->get()->result();
  }

  function display()
  {
    $this->db->select('*');
    $this->db->from('stock_mat');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = stock_mat.sm_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    return $this->db->get()->result();
  }

  function insert_stock($param)
  {
    $this->db->insert('stock_mat', $param);
  }

  function val_stock_exist($mat)
  {
    $this->db->select('*');
    $this->db->where('sm_mat',$mat);
    $query = $this->db->get('stock_mat');

    if ($query->num_rows() > 0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }

  }
  function get($mat)
  {
    $this->db->select('*');
    $this->db->where('sm_mat',$mat);
    $this->db->join('panaderias', 'panaderias.bod_id = stock_mat.sm_location');
    $query = $this->db->get('stock_mat');


    if ($query->num_rows() > 0 )
    {
        return $query;
    }
    else
    {
        return FALSE;
    }
  }
  function get_stock($mat)
  {
    $this->db->select('*');
    $this->db->where('sm_mat',$mat['id']);
    $this->db->where('sm_location',$mat['sede']);
    $query = $this->db->get('stock_mat');

    if ($query->num_rows() > 0 )
    {
        return $query;
    }
    else
    {
        return FALSE;
    }
  }

  function update_stock($param)
  {
    $this->db->where('sm_mat',$param['sm_mat']);
    $this->db->where('sm_location',$param['sm_location']);
    $this->db->update('stock_mat', $param);
  }

}
?>
