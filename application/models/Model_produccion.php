<?php

/**
 *
 */
class Model_produccion extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('produccion', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  function insert_ing($param)
  {
    $this->db->insert('ing_prod', $param);
  }

  function display_prod()
  {
    $this->db->select('*');
    $this->db->from('produccion');
    $this->db->join('productos', 'productos.prod_id = recetas.rec_prod');

    return $this->db->get()->result();
  }

  function display_ing($id)
  {
    $this->db->select('*');
    $this->db->from('recetas');
    $this->db->join('materiasprimas', 'materiasprimas.mat_id = ingredientes.ing_mat');
    $this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    $this->db->from('ing_prod');

    return $this->db->get()->result();
  }

}
?>
