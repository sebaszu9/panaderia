<?php

/**
 *
 */
class Model_productos extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('productos', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  public function search($data)
  {
    $this->db->select('*');
    $this->db->join('categorias_prod', 'categorias_prod.cat_prod_id = productos.prod_cat');
    $this->db->join('impuestos', 'impuestos.imp_id = productos.prod_imp');
    $this->db->from('productos');
    $this->db->order_by('prod_nombre ASC');
    $this->db->like('prod_nombre',$data);
    $this->db->where('prod_del', 0);
    $query =  $this->db->get();

    if ($query->num_rows() > 0 )
    {
        return $query;
    }
    else
    {
        return FALSE;
    }
  }

  function load()
  {
    $this->db->select('*');
    $this->db->from('productos');
    $this->db->where('prod_del', 0);
    $this->db->join('categorias_prod', 'categorias_prod.cat_prod_id = productos.prod_cat');
    $this->db->order_by('prod_nombre ASC');

    return $this->db->get()->result();
  }

  function display() {
    $this->db->select('*');
    $this->db->from('productos');
    $this->db->where('prod_del', 0);
    $this->db->join('categorias_prod', 'categorias_prod.cat_prod_id = productos.prod_cat');
    $this->db->join('impuestos', 'impuestos.imp_id = productos.prod_imp');
    $this->db->order_by('prod_nombre ASC');

    return $this->db->get()->result();
  }

  public function load_per_catg($id)
  {
    $this->db->select('*');
    $this->db->from('productos');
    $this->db->where('prod_del', 0);
    $this->db->where('prod_cat', $id);
    return $this->db->get()->result();
  }

  function read($id)
  {
    $this->db->select('*');
    $this->db->where('prod_id', $id);
    $this->db->from('productos');
    $this->db->where('prod_del', 0);
    $this->db->join('categorias_prod', 'categorias_prod.cat_prod_id = productos.prod_cat');
    $this->db->join('impuestos', 'impuestos.imp_id = productos.prod_imp');

    return $this->db->get()->result();
  }

  function delete($id)
  {
    $this->db->where('prod_id',$id);
    $param['prod_del'] = 1;
    $exito = $this->db->update('productos', $param);
    //$this->db->where('prod_id', $id);
    //$this->db->delete('productos');
  }

  function update($param)
  {
    $this->db->where('prod_id', $param ['prod_id']);
    $this->db->update('productos', $param);
    //echo "<script type=text/javascript>alert('Se ha actualizado exitosamente el producto');</script>";
  }

  function val_exist($id)
  {
    $this->db->select('*');
    $this->db->where('prod_id', $id);
    $query = $this->db->get('productos');

    if ($query->num_rows() > 0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
  }

  function read_combo($id)
  {
    $this->db->select('*');
    $this->db->where('comb_padre', $id);
    $this->db->from('combos');
    $this->db->join('productos', 'productos.prod_id = combos.comb_padre');
    return $this->db->get()->result();
  }

  function insert_combo($param)
  {
    $this->db->insert('combos', $param);
  }

  function delete_combo($id)
  {
    $this->db->where('comb_id', $id);
    $this->db->delete('combos');
  }

  function insert_recet($param)
  {
    $this->db->insert('receta_producto', $param);
  }

  function read_recet_prod($id)
  {
    $this->db->select('*');
    $this->db->where('rp_producto', $id);
    $this->db->from('receta_producto');
    //$this->db->join('productos', 'productos.prod_id = combos.comb_padre');
    return $this->db->get()->result();
  }

  function read_ingred_rec()
  {
    $this->db->select('*');
    $this->db->where('rp_producto', $id);
    $this->db->from('ingredientes_prod');
    //$this->db->join('productos', 'productos.prod_id = combos.comb_padre');
    return $this->db->get()->result();
  }

}



?>
