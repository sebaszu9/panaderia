<?php

/**
 *
 */
class Model_cliente extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $this->db->insert('cliente', $param);
  }

  function load() {
    $this->db->select('*');
    $this->db->from('cliente');
    return $this->db->get()->result();
  }

}

?>
