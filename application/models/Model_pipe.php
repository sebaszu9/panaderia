<?php

/**
 *
 */
class Model_pipe extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function add_column_impt()
  {
    $this->load->dbforge();
    $fields = array( 'imp_del' => array('type' => 'int', 'value' => 0,'null' => FALSE) );
    $good = $this->dbforge->add_column('impuestos', $fields);

    if ($good==true) {
      echo "adicionado la columna de Impuesto <br>";
    }
    else{
      echo "error de impuesto <br>";
    }
  }

  public function add_column_dep_prod()
  {
    $this->load->dbforge();
    $fields = array( 'cat_prod_del' => array('type' => 'int', 'value' => 0,'null' => FALSE) );
    $good = $this->dbforge->add_column('categorias_prod', $fields);

    if ($good==true) {
      echo "adicionado la columna de Categoria de producto <br>";
    }
    else{
      echo "error de producto -";
    }
  }

  public function add_column_prod()
  {
    $this->load->dbforge();
    $fields = array( 'prod_del' => array('type' => 'int', 'value' => 0,'null' => FALSE) );
    $good = $this->dbforge->add_column('productos', $fields);

    if ($good==true) {
      echo "adicionado la columna de producto <br>";
    }
    else{
      echo "error de impuesto <br>";
    }
  }

  public function add_column_dep_mat()
  {
    $this->load->dbforge();
    $fields = array( 'cat_mat_del' => array('type' => 'int', 'value' => 0,'null' => FALSE) );
    $good = $this->dbforge->add_column('categorias_mat', $fields);

    if ($good==true)
    {
      echo "adicionado la columna de Categoria de materia <br>";
    }
    else{
      echo "error de impuesto <br>";
    }
  }

  public function add_column_mat()
  {
    $this->load->dbforge();
    $fields = array( 'mat_del' => array('type' => 'int', 'value' => 0,'null' => FALSE) );
    $good = $this->dbforge->add_column('materiasprimas', $fields);

    if ($good==true) {
      echo "adicionado la columna de materia <br>";
    }
    else{
      echo "error de impuesto <br>";
    }
  }

  public function add_column_prov()
  {
    $this->load->dbforge();
    $fields = array( 'prov_del' => array('type' => 'int', 'value' => 0,'null' => FALSE) );
    $good = $this->dbforge->add_column('proveedores', $fields);

    if ($good==true) {
      echo "adicionado la columna de Proveedor <br>";
    }
    else{
      echo "error de proveedor <br>";
    }
  }

}

?>
