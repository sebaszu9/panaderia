<?php

class model_usuario extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insert($param)
  {
    $hora  =  date("g:i A");
    $fecha =  date("Y-m-d");
    $hoy = "$fecha $hora";
    $data ['usr_nombre'] = $param ['nombre'];
    $data ['usr_correo'] = $param ['correo'];
    $data ['usr_sede'] = $param ['sede'];
    $data ['usr_password'] = $param ['password'];
    $data ['usr_fecha_registrado'] = $hoy;
    $data ['usr_estado'] = 1;
    $data ['usr_tipo_usuario'] = $param ['tipo'];

    $this->db->insert('usuario', $data);
  }

  public function autenticar_login($param)
  {
    $this->db->select('*');
    $this->db->where('usr_correo',$param ['correo']);
    $this->db->where('usr_password',sha1($param ['password']));

    $consulta = $this->db->get('usuario');

    if($consulta->num_rows() == 1)
    { return $consulta; }
    else
    { return false; }
  }

  public function valid_email($data)
  {
    $consulta = $this->db->get_where('usuario', array('usr_correo'=>$data['correo']));
    if($consulta->num_rows() >= 1)
    { return true; }
    else
    { return false;  }
  }

  public function valid_emails($data)
  {
    $consulta = $this->db->get_where('usuario', array('usr_correo'=>$data['correo']));
    if($consulta->num_rows() >= 1)
    { return true; }
    else
    { return false;  }
  }

  public function load()
  {
    $this->db->select('*');
    $this->db->join('tipo_usuario', 'tipo_usuario.tipus_id = usuario.usr_tipo_usuario','left');
    $this->db->join('panaderias', 'panaderias.bod_id = usuario.usr_sede');
    $consulta = $this->db->get('usuario')->result();
    return $consulta;
  }

  public function load_seller()
  {
    $this->db->select('*');
    // $this->db->join('tipo_usuario', 'tipo_usuario.tipus_id = usuario.usr_tipo_usuario','left');
    // $this->db->join('panaderias', 'panaderias.bod_id = usuario.usr_sede');
    // $this->db->where('usr_tipo_usuario', 4);
    $consulta = $this->db->get('users')->result();
    return $consulta;
  }

  public function load_seller_id($id)
  {
    $this->db->select('*');
    // $this->db->join('tipo_usuario', 'tipo_usuario.tipus_id = usuario.usr_tipo_usuario','left');
    // $this->db->join('panaderias', 'panaderias.bod_id = usuario.usr_sede');
    $this->db->where('id', $id);
    $consulta = $this->db->get('users')->result();
    return $consulta;
  }

  public function update_repartidor($param)
  {
        $this->db->where('id', $param['id']);
        $this->db->update('users', $param);
  }

  public function delete($code)
  {
    $this->db->where('usr_id', $code);
    $this->db->delete('usuario');
  }

  public function change_state($param)
  {
    $data ['usr_estado'] = $param ['state'];
    $this->db->where('usr_id', $param ['user']);
    $this->db->update('usuario', $data);
  }

  public function read($data)
  {
    $this->db->select('*');
    $this->db->where('usr_id', $data);
    $this->db->join('tipo_usuario', 'tipo_usuario.tipus_id = usuario.usr_tipo_usuario','left');
    $this->db->from('usuario');
    return $this->db->get()->result();
  }

  public function update($param)
  {
    $id = $param ['code'];
    $data ['usr_nombre'] = $param ['nombre'];
    $data ['usr_correo'] = $param ['correo'];
    $data ['usr_tipo_usuario'] = $param ['tipo'];
    $this->db->where('usr_id', $id);
    $this->db->update('usuario', $data);
  }

  public function update_password($param)
  {
    $id = $param ['code'];
    $data ['usr_password'] = $param ['key'];
    $this->db->where('usr_id', $id);
    $this->db->update('usuario', $data);
  }

  public function read_panaderos()
  {
    $this->db->select('*');
    $this->db->where('usr_tipo_usuario', '2');
    $this->db->where('usr_estado','1');
    $this->db->join('tipo_usuario', 'tipo_usuario.tipus_id = usuario.usr_tipo_usuario','left');
    $this->db->from('usuario');
    return $this->db->get()->result();
  }

  public function read_per_type($id)
  {
    $this->db->select('*');
    $this->db->where('usr_tipo_usuario',$id);
    $this->db->where('usr_estado','1');
    $this->db->join('tipo_usuario', 'tipo_usuario.tipus_id = usuario.usr_tipo_usuario','left');
    $this->db->from('usuario');
    return $this->db->get()->result();
  }


}

?>
