<?php

/**
 *
 */
class model_mov_prod extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function load_concept()
  {
    $this->db->from('conceptos');
    $this->db->order_by('c_nombre','ASC');
    return $this->db->get()->result();
  }

  function insert_cmp($param)
  {
    $this->db->insert('movimiento', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  function update_cmp($param)
  {
    $this->db->where('cmp_id', $param['cmp_id']);
    $this->db->update('movimiento', $param);
  }

  function insert_rp($param)
  {
    $this->db->insert('relacion_pedidos', $param);
  }
  function update_rp($param)
  {
    $this->db->where('rp_cmp', $param['rp_cmp']);
    $this->db->update('relacion_pedidos', $param);
  }

  function display_cmp($tipo)
 {
    $this->db->where('cmp_tipo', $tipo);
    $this->db->select('*');
    $this->db->from('movimiento');

    return $this->db->get()->result();
  }

  function read_cmp($id)
  {
    $this->db->select('*');
    $this->db->where('cm_id', $id);
    $this->db->from('movimiento');
    $this->db->join('relacion_pedidos', 'relacion_pedidos.rp_cm = movimiento.cm_id');
    $this->db->join('proveedores', 'proveedores.prov_id = relacion_pedidos.rp_prov');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');

    return $this->db->get()->result();
  }

  function read_mov_prod($id)
  {
    $this->db->select('*');
    $this->db->where('mp_id_concepto', $id);
    $this->db->where('mp_cant >=',0);
    $this->db->from('mov_prod');
    $this->db->join('empaque', 'empaque.emp_mov_prod = mov_prod.mp_id');
    $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_prod.mp_id_concepto');

    return $this->db->get()->result();
  }


  function read_mov($id)
  {
      $this->db->select('*');
      $this->db->where('mp_id', $id);
      $this->db->where('mp_cant >=',0);
      $this->db->from('mov_prod');
      $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
      //$this->db->join('movimiento', 'movimiento.cmp_id = mov_prod.mp_id_concepto');

      return $this->db->get()->result();
    }

  function read_relacion($id)
  {
    $this->db->select('*');
    $this->db->where('rp_cmp',$id);
    $this->db->from('relacion_pp');
    $this->db->join('proveedores', 'proveedores.prov_id = relacion_pp.rp_prov');

    return $this->db->get()->result();
  }

  function insert_mov($param)
  {
    $this->db->insert('mov_prod', $param);
    $id = $this->db->insert_id();
    return $id;
  }

  function create_empaque($param)
  {
    $this->db->insert('empaque', $param);
  }

  function update_empaque($param)
  {
    $this->db->where('emp_id',$param['emp_id']);
    $this->db->update('empaque', $param);
  }

  function display_empaque ($param)
  {
    $this->db->select('*');
    $this->db->where('cm_fecha',$param['date']);
    $this->db->from('empaque');
    $this->db->join('mov_prod', 'mov_prod.mp_id = empaque.emp_mov_prod');
    $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_prod.mp_id_concepto');
    return $this->db->get()->result();
  }
  function val_mov_exist($param)
  {
    $this->db->select('*');
    $this->db->where('mp_id_concepto', $param['mp_id_concepto']);
    $this->db->where('mp_id_prod',$param['mp_id_prod']);
    $query = $this->db->get('mov_prod');

    if ($query->num_rows() > 0 )
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }

  }

  function update_mov($param)
  {
    $this->db->where('mp_id', $param['mp_id']);
    $this->db->update('mov_prod', $param);
  }

  function update_mov_prod($param)
  {
    $this->db->where('mp_id',$param['mp_id']);
    $this->db->update('mov_prod', $param);
  }

  function delete_mov($id)
  {
    $this->db->where('mp_id',$id);
    $this->db->delete('mov_prod');
  }

  function delete_cmp($id)
  {
    $this->db->where('cm_id',$id);
    $this->db->delete('movimiento');
  }

  /*function produccion_dia($data)
  {
    $this->db->select('*');
    $this->db->where('cm_fecha',$data['date']);
    $this->db->from('mov_prod');
    $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_prod.mp_id_concepto');
    $this->db->join('panaderias', 'movimiento.cm_ubicacion = panaderias.bod_id');
    $this->db->join('categorias_prod', 'categorias_prod.cat_prod_id = productos.prod_cat');
    $this->db->join('usuario', 'movimiento.cm_usuario = usuario.usr_id');
    return $this->db->get()->result();
  }*/

  function prom_mov($param)
  {
    $this->db->select('*');
    $this->db->where('cm_fecha<=', $param['date2']);
    $this->db->where('cm_fecha>=', $param['date1']);
    $this->db->where('cm_concepto', $param['concepto']);
    $this->db->where('mp_id_prod', $param['prod']);
    $this->db->from('mov_prod');
    $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_prod.mp_id_concepto');
    $this->db->join('panaderias', 'movimiento.cm_ubicacion = panaderias.bod_id');
    $this->db->join('categorias_prod', 'categorias_prod.cat_prod_id = productos.prod_cat');
    $this->db->join('usuario', 'movimiento.cm_usuario = usuario.usr_id');
    return $this->db->get()->result();
  }

  function produccion($data)
  {
    $this->db->select('*');

    if ($this->input->post('date')&&$this->input->post('date2'))
    {
      $fecha1 = date('Y-m-d', strtotime($this->input->post('date')));
      $fecha2 = date('Y-m-d', strtotime($this->input->post('date2')));
      $this->db->where('cm_fecha >=', $fecha1);
      $this->db->where('cm_fecha <=', $fecha2);
    }
    elseif ($this->input->post('date'))
    {
      $this->db->where('cm_fecha >=',$data['date']);
    }
    else{
      $this->db->where('cm_fecha',$data['date']);
    }



    if ($data['ubicacion'] != '100') {
      $this->db->where('cm_ubicacion',$data['ubicacion']);
    }
    if ($data['panadero'] != '100') {
      $this->db->where('cm_usuario',$data['panadero']);
    }
    $this->db->from('mov_prod');
    $this->db->join('empaque', 'empaque.emp_mov_prod = mov_prod.mp_id');
    $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_prod.mp_id_concepto');
    $this->db->join('panaderias', 'movimiento.cm_ubicacion = panaderias.bod_id');
    $this->db->join('categorias_prod', 'categorias_prod.cat_prod_id = productos.prod_cat');
    $this->db->join('usuario', 'movimiento.cm_usuario = usuario.usr_id');
    return $this->db->get()->result();
  }

  function produccion_by_two_sed($data)
  {
    //echo "Entro aca";
    $this->db->select('*');

    if ($this->input->post('date')&&$this->input->post('date2'))
    {
      $fecha1 = date('Y-m-d', strtotime($this->input->post('date')));
      $fecha2 = date('Y-m-d', strtotime($this->input->post('date2')));
      $this->db->where('cm_fecha >=', $fecha1);
      $this->db->where('cm_fecha <=', $fecha2);
    }
    elseif ($this->input->post('date'))
    {
      $this->db->where('cm_fecha',$this->input->post('date'));
    }

    $this->db->where('cm_ubicacion',$data['sede']);
    $this->db->from('mov_prod');
    $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
    $this->db->join('movimiento','movimiento.cm_id = mov_prod.mp_id_concepto');
    $this->db->join('panaderias','movimiento.cm_ubicacion = panaderias.bod_id');
    //$this->db->join('categorias_prod', 'categorias_prod.cat_prod_id = productos.prod_cat');
    $this->db->join('usuario', 'movimiento.cm_usuario = usuario.usr_id');
    $this->db->order_by("cm_fecha", "asc");

    $query =  $this->db->get();
    if ($query->num_rows() > 0 )
    {
      //echo " Si hay ".$data['sede'];
      return $query->result();
    }
    else
    {
      //echo " No hay ".$data['sede'];
      return $query->result();
    }
    //return $this->db->get()->result();
  }



  function reporte($tipo)
  {
    $this->db->select('*');
    $this->db->where('cm_tipo', 1);
    //$this->db->where('cm_concepto', 2);
    $this->db->from('movimiento');
    $this->db->join('panaderias', 'panaderias.bod_id = movimiento.cm_ubicacion');
    if ($this->input->post('panaderia')>=1)
    {   $this->db->where('cm_ubicacion',$this->input->post('panaderia')); }
    //consulta de fecha
    if ($this->input->post('fecha1')&&$this->input->post('fecha2'))
    {
      $fecha1 = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $fecha2 = date('Y-m-d', strtotime($this->input->post('fecha2')));
      $this->db->where('cm_fecha >=', $fecha1);
      $this->db->where('cm_fecha <=', $fecha2);
    }
    elseif ($this->input->post('fecha1'))
    {
      $fecha = date('Y-m-d', strtotime($this->input->post('fecha1')));
      $this->db->where('cm_fecha >=',$fecha);
    }
    return $this->db->get()->result();
  }

  function read_mov_prod_produccion($consult)
  {
    $this->db->select('*');
    $this->db->where('mp_id_concepto', $consult['movimiento']);
    $this->db->where('mp_id_prod', $consult['producto']);
    $this->db->where('mp_cant >=',0);
    $this->db->from('mov_prod');
    $this->db->join('empaque', 'empaque.emp_mov_prod = mov_prod.mp_id');
    //$this->db->join('materiasprimas', 'materiasprimas.mat_id = mov_mat.mm_id_mat');
    //$this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    //$this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    return $this->db->get()->result();
  }

  function read_mov_prod_by_mov_advanced($search)
  {
    $this->db->select('*');
    //$this->db->where('mp_id_concepto', $consult['movimiento']);
    //$this->db->where('mp_id_prod', $consult['producto']);
    $this->db->where('mp_cant >=',0);
    $this->db->from('mov_prod');
    $this->db->join('empaque', 'empaque.emp_mov_prod = mov_prod.mp_id');
    $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_prod.mp_id_concepto');
    $this->db->where('cm_concepto', $search['concepto']);
    $this->db->where('cm_fecha >=', $search['fecha_1']);
    $this->db->where('cm_fecha <=', $search['fecha_2']);
    if ($search['sede']>=1)
    {
      $this->db->where('cm_ubicacion',$search['sede']);
    }
    //$this->db->join('materiasprimas', 'materiasprimas.mat_id = mov_mat.mm_id_mat');
    //$this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    //$this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    return $this->db->get()->result();
  }


  function read_empaque_by_date($search)
  {
    $this->db->select('*');
    $this->db->from('empaque');
    $this->db->join('mov_prod', 'mov_prod.mp_id = empaque.emp_mov_prod');
    $this->db->join('productos', 'productos.prod_id = mov_prod.mp_id_prod');
    $this->db->join('movimiento', 'movimiento.cm_id = mov_prod.mp_id_concepto');
    $this->db->where('cm_concepto', '2');
    $this->db->where('cm_fecha >=', $search['fecha_1']);
    $this->db->where('cm_fecha <=', $search['fecha_2']);
    if ($search['sede']>=1)
    {
      $this->db->where('cm_ubicacion',$search['sede']);
    }
    //$this->db->join('materiasprimas', 'materiasprimas.mat_id = mov_mat.mm_id_mat');
    //$this->db->join('unidades', 'unidades.unidad_id = materiasprimas.mat_um');
    //$this->db->join('movimiento', 'movimiento.cm_id = mov_mat.mm_id_concepto');
    return $this->db->get()->result();
  }




}
?>
