<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // Incluimos el archivo fpdf
    require_once APPPATH."/third_party/fpdf/fpdf.php";

    //Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
    class Pdf extends FPDF {
        public function __construct() {
            parent::__construct('P','mm',array(55,1500));

        }

       // El pie del pdf
      //  public function Footer(){
      //      $this->SetY(-15);
      //      $this->SetFont('Arial','I',6);
      //      //$this->Cell(0,5,'Page '.$this->PageNo(),0,1,'C');
      //      // $this->Cell(0,5,'Software Alimenteq ',0,0,'C');
      // }
    }
?>
