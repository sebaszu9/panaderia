<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct()
	 {
		 parent::__construct ();
		 $this->load->helper('url');
		 $this->load->library('session');
		 $this->load->model('Model_produccion');
		 $this->load->model('Model_reportes');
	 }

	public function home()
	{
		$this->very_session();
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('home');
		$this->load->view('layout/footer');
	}

	public function mobile()
	{
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('mobile_home');
		$this->load->view('layout/footer');
	}

	public function prohibido()
	{
		echo "Forbidden";
	}

	function very_session()
	{

		$this->load->library('validacion_panaderia');
		$result = $this->validacion_panaderia->check_panaderia();

		//echo $this->session->userdata('usuario');
		if (!$this->session->userdata('usuario')==1) {
			redirect(base_url());
		}elseif ($result==true) {
			echo "Prohibido";
		}
	}

}
