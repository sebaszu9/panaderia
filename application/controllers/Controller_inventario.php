<?php

  /**
   *
   */
  class Controller_inventario extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_cat_mat');
      $this->load->model('model_stock_mat');
      $this->load->model('model_panaderia');
      $this->load->model('model_mov_prod');
      $this->load->model('model_mov_mat');
      $this->load->model('model_movimiento');
      $this->load->model('model_productos');
      $this->load->model('model_materias');
      $this->load->model('model_proveedores');
      $this->load->model('model_stock_prod');
      $this->load->library('form_validation');
      $this->load->library('session');
      $this->load->library('pdf');
    }

    public function index()
    {
      $this->very_session();
      $data['inventario'] = $this->model_stock_mat->load_per_bakery(0);
      $data['sede'] = $this->model_panaderia->load();
      $data['panaderia'] = 0;

      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('inventario/datatable.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function inv_stock_matx()
    {
      $this->very_session();
      $data['inventario'] = $this->model_stock_mat->load_per_bakery(0);
      $data['sede'] = $this->model_panaderia->load();
      $data['panaderia'] = 0;

      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('inventario/inv_stock_mat.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function inv_stock_mat()
    {
      $this->very_session();
      if ($this->input->post('filtrar'))
      {
        $sede = $this->input->post('sede');
        $data['inventario'] = $this->model_stock_mat->load_per_bakery($sede);
        $data['sede'] = $this->model_panaderia->load();
        $data['panaderia'] = $sede;

        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('inventario/inv_stock_mat.php',$data);
        $this->load->view('layout/footer.php');
      }
      else
      {
        $this->inv_stock_matx();
      }
    }

    public function inv_stock_prodx()
    {
      $this->very_session();
      $data['inventario'] = $this->model_stock_prod->load_per_bakery(0);
      $data['sede'] = $this->model_panaderia->load();
      $data['panaderia'] = 0;

      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('inventario/inv_stock_prod.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function inv_stock_prod()
    {
      $this->very_session();
      if ($this->input->post('filtrar'))
      {
        $sede = $this->input->post('sede');
        $data['inventario'] = $this->model_stock_prod->load_per_bakery($sede);
        $data['sede'] = $this->model_panaderia->load();
        $data['panaderia'] = $sede;

        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('inventario/inv_stock_prod.php',$data);
        $this->load->view('layout/footer.php');
      }
      else
      {
        $this->inv_stock_prodx();
      }
    }

    public function movimiento_index()
    {
      $this->very_session();
      $data['concepto'] = $this->model_mov_prod->load_concept();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('inventario/movimiento_index.php',$data);
      $this->load->view('layout/footer.php');
    }

    // ***********************************************
    // ============= MOVIMIENTO DE PRODUCTO ==========
    //************************************************
    public function mov_prod_form()
    {
      $this->very_session();
      $data['concepto'] = $this->model_mov_prod->load_concept();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['productos'] = $this->model_productos->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('inventario/mov_form_prod.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function mov_prod_details()
    {
      $this->very_session();
      if ($this->uri->segment(4))
      {
        $id = $this->uri->segment(4);
        $data['productos'] = $this->model_mov_prod->read_mov_prod($id);
        $data['entrada'] = $this->model_movimiento->read_mov_id($id);
        $data['proveedor'] = $this->model_movimiento->read_order_proovedor($id);
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('inventario/mov_det_prod.php',$data);
        $this->load->view('layout/footer.php');
      }
    }

    // crear movimiento de producto
    public function mov_prod_create()
    {
      $this->form_validation->set_rules('panaderia', 'Panaderia', 'required');
      $this->form_validation->set_rules('proveedor', 'Proveedor', 'required');
      $this->form_validation->set_rules('concepto', 'Concepto', 'required');
      $this->form_validation->set_rules('fecha', 'Fecha', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->mov_prod_form();
      }
      elseif ($this->session->userdata('pedido_producto')==null||count($this->session->userdata('pedido_producto'))==0)
      {
        $this->session->set_flashdata('mensaje','Error, No hay producto agregado.');
        redirect('Inventario/movimiento/productos/form');
      }
      else
      {
        $param ['cm_tipo'] =  $this->input->post('tipo');
        //$param ['cm_concepto'] = 1;
        $param ['cm_concepto'] = $this->input->post('concepto');
        $param ['cm_obs'] = $this->input->post('descripcion');
        $param ['cm_ubicacion'] = $this->input->post('panaderia');
        $param ['cm_parte'] = $this->input->post('parte');
        $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
        $param ['cm_usuario'] = $this->session->userdata('iduser');
        //$param ['cm_usuario'] = 2;
        // inserta movimiento
        $concepto =  $this->model_mov_prod->insert_cmp($param);

        // inserta movimiento si es proovedor
        $relacion ['rp_prov'] = $this->input->post('proveedor');
        $relacion ['rp_cm']  = $concepto;
        $this->model_mov_prod->insert_rp($relacion);

        $carritop = $this->session->userdata('pedido_producto');

        foreach ($carritop as $value) {

          $data = array(
                 'mp_cant'        => $value['cantidad'],
                 'mp_id_concepto' => $concepto,
                 'mp_id_prod'     => $value['producto'],
                 'mp_costo'       => $value['costo'],
             );

          $this->model_mov_prod->insert_mov($data);

          $mat['id']   = $value['producto'];
          $mat['tipo'] =  $this->input->post('tipo');
          $mat['cant'] = $value['cantidad'];
          $mat['sede'] = $this->input->post('panaderia');

          $this->update_inventory_prod($mat);
          $costo['prod_id'] = $value['producto'];
          $costo['prod_costo'] = $value['costo'];
          $this->model_productos->update($costo);
        }
        //session_destroy();
        unset($_SESSION["pedido_producto"]);
        $this->session->set_flashdata('mensaje','Guardado exitosamente.');

        echo "Guardo exitosamente";
        redirect('Inventario/movimiento/index');
      }

    }

    public function delete_mov_prod()
    {
      if ($this->input->post('concepto'))
      {
        $id = $this->input->post('concepto');

        /*$mov = $this->model_mov_prod->read_mov_prod($id);

        foreach ($mov as $key)
        {
          $prod['id'] = $key->mp_id_mat;
          $prod['tipo'] = 0;
          $prod['cant'] = $key->mp_cant*$key->mat_inv;
          $prod['sede'] = $key->cm_ubicacion;
          $this->update_inventory_mat($prod);
        }
        */

        $this->model_movimiento->delete_mov_prod_per_prod($id);
        $this->model_movimiento->delete_mov_proveedor($id);
        $this->model_movimiento->delete_mov($id);
        echo json_encode(array("status" => TRUE));
      }
      else{
        echo json_encode(array("status" => FALSE));
      }
     }

     public function delete_item_mov_prod()
     {
       if ($this->input->post('item'))
       {
         $id = $this->input->post('item');
         $mov = $this->model_mov_prod->read_mov($id);

         foreach ($mov as $key)
         {
           $prod['id']   = $key->mp_id_mat;
           $prod['tipo'] = 0;
           $prod['cant'] = $key->mp_cant*$key->mat_inv;
           $prod['sede'] = $key->cm_ubicacion;
           $this->update_inventory_mat($prod);
         }

         //$this->model_movimiento->delete_mov_mat_per_mov($id);
         //$this->model_movimiento->delete_mov_proveedor($id);
         $this->model_mov_mat->delete_mov($id);
         echo json_encode(array("status" => TRUE));
       }
       else{
         echo json_encode(array("status" => FALSE));
       }
     }

    public function update_inventory_prod($prod)
    {
      $this->very_session();

      $param = $this->model_stock_prod->get_stock($prod);

      if ($param==FALSE)
      {
        $number2 = $prod['cant'];
        $param ['sp_cant'] = $number2;
        $param ['sp_prod'] = $prod['id'];
        $param ['sp_location'] = $prod['sede'];
        // inserta movimiento concepto
        $concepto =  $this->model_stock_prod->insert_stock($param);
      }
      else {
        # code...
        foreach ($param->result() as $value) {
          ///Obtiene el valor actual, con un ciclo
          $number1 = $value->sp_cant;
        }
        //La cantidad que se va a sumar o a restar vienenen la funcion
        $number2 = $prod['cant'];

        if ($prod['tipo'] == 1) {
          $stock = $number1 + $number2;
        }
        elseif ($prod['tipo'] == 0) {
          $stock = $number1 - $number2;
        }
        $data2 = array(
               'sp_cant' => $stock,
               'sp_prod' => $prod['id'],
               'sp_location' => $prod['sede'],
           );
        $this->model_stock_prod->update_stock($data2);
      }
    }

    // ***********************************************
    // ============= MOVIMIENTO DE MATERIAS ==========
    //************************************************

    public function mov_mat_form()
    {
      $this->very_session();
      $data['concepto'] = $this->model_mov_prod->load_concept();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['materias'] = $this->model_materias->display();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('inventario/mov_form_mat.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function mov_mat_create()
    {

      $this->form_validation->set_rules('panaderia', 'Panaderia', 'required');
      //$this->form_validation->set_rules('proveedor', 'Proveedor', 'required');
      $this->form_validation->set_rules('fecha', 'Fecha', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->mov_mat_form();
      }
      elseif ($this->session->userdata('pedido_materia')==null||count($this->session->userdata('pedido_materia'))==0) {
        $this->session->set_flashdata('mensaje','Error, No hay producto agregado.');
        redirect('Inventario/movimiento/materias/form');
      }
      else
      {
        $param ['cm_tipo'] = $this->input->post('tipo');
        $param ['cm_concepto'] =$this->input->post('conceptos');
        $param ['cm_obs'] = $this->input->post('descripcion');
        $param ['cm_ubicacion'] = $this->input->post('panaderia');
        $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
        $param ['cm_usuario'] = $this->session->userdata('iduser');
        $param ['cm_parte'] = $this->input->post('parte');
        //$param ['cm_usuario'] = 2;
        // inserta movimiento concepto
        $concepto =  $this->model_mov_mat->insert_cmm($param);

        if ($this->input->post('proveedor')) {
          $relacion ['rp_prov'] = $this->input->post('proveedor');
          $relacion ['rp_cm']  = $concepto;
          $this->model_mov_mat->insert_rp($relacion);
        }

        $carritop = $this->session->userdata('pedido_materia');

        foreach ($carritop as $value) {

          $data = array(
                 'mm_cant'        => $value['cantidad'],
                 'mm_id_concepto' => $concepto,
                 'mm_id_mat'      => $value['producto'],
                 'mm_costo'       => $value['costo'],
             );

          $this->model_mov_mat->insert_mov($data);

          $mat['id'] = $value['producto'];
          $mat['tipo'] = $this->input->post('tipo');
          //$mat['cant'] = $value['cantidad']*$value->mat_inv;
          $mat['cant'] = $value['cantidad'];
          $mat['sede'] = $this->input->post('panaderia');

          $this->update_inventory_mat($mat);
          $costo['mat_id'] = $value['producto'];
          $costo['mat_costo'] = $value['costo'];
          $this->model_materias->update($costo);
        }

        //session_destroy();
        unset($_SESSION["pedido_materia"]);
        $this->session->set_flashdata('mensaje','Guardado exitosamente.');
        echo "Guardo exitosamente";
        redirect('Inventario/movimiento/index');
      }
    }



   public function mov_mat_details()
   {
     $this->very_session();
     if ($this->uri->segment(4)) {

       $id = $this->uri->segment(4);
       $data['materias'] = $this->model_mov_mat->read_mov_mat($id);
       $data['entrada'] = $this->model_movimiento->read_mov_id($id);
       $data['proveedor'] = $this->model_movimiento->read_order_proovedor($id);
       $this->load->view('layout/header.php');
       $this->load->view('layout/sidebar.php');
       $this->load->view('inventario/mov_det_mat.php',$data);
       $this->load->view('layout/footer.php');
     }
   }

   public function delete_mov_mat()
   {
     if ($this->input->post('concepto'))
     {
       $id = $this->input->post('concepto');
       $mov = $this->model_mov_mat->read_mov_mat($id);

       foreach ($mov as $key)
       {
         $mat['id'] = $key->mm_id_mat;
         $mat['tipo'] = 0;
         $mat['cant'] = $key->mm_cant*$key->mat_inv;
         $mat['sede'] = $key->cm_ubicacion;
         $this->update_inventory_mat($mat);
       }

       $this->model_movimiento->delete_mov_mat_per_mov($id);
       $this->model_movimiento->delete_mov_proveedor($id);
       $this->model_movimiento->delete_mov($id);
       echo json_encode(array("status" => TRUE));
     }
     else{
       echo json_encode(array("status" => FALSE));
     }
    }

    public function delete_item_mov_mat()
    {
      if ($this->input->post('item'))
      {
        $id = $this->input->post('item');
        $mov = $this->model_mov_mat->read_mov($id);

        foreach ($mov as $key)
        {
          $mat['id']   = $key->mm_id_mat;
          $mat['tipo'] = 0;
          $mat['cant'] = $key->mm_cant*$key->mat_inv;
          $mat['sede'] = $key->cm_ubicacion;
          $this->update_inventory_mat($mat);
        }

        //$this->model_movimiento->delete_mov_mat_per_mov($id);
        //$this->model_movimiento->delete_mov_proveedor($id);
        $this->model_mov_mat->delete_mov($id);
        echo json_encode(array("status" => TRUE));
      }
      else{
        echo json_encode(array("status" => FALSE));
      }
    }

    public function update_inventory_mat($mat)
    {

      $param = $this->model_stock_mat->get_stock($mat);
      $materias = $this->model_materias->read($mat['id']);

      foreach ($materias as $value2) {
        ///Obtiene el valor actual, con un ciclo
        $ue = $value2->mat_ue;
        $um = $value2->unidad_escala;
      }

      if ($param==FALSE)
      {
        //echo "No existe stock y se crea uno.<br>";
        $number2 = $mat['cant']*$ue*$um;
        $param ['sm_cant'] = $number2;
        $param ['sm_mat'] = $mat['id'];
        $param ['sm_location'] = $mat['sede'];
        // inserta movimiento concepto
        $concepto =  $this->model_stock_mat->insert_stock($param);
      }
      else {
        # code...
        foreach ($param->result() as $value) {
          ///Obtiene el valor actual, con un ciclo
          $number1 = $value->sm_cant;
        }
        //La cantidad que se va a sumar o a restar vienenen la funcion
        $number2 = $mat['cant']*$ue*$um;

        if ($mat['tipo'] == 1) {
          $stock = $number1 + $number2;
        }
        elseif ($mat['tipo'] == 0) {
          $stock = $number1 - $number2;
        }
        $data2 = array(
               'sm_cant' => $stock,
               'sm_mat' => $mat['id'],
               'sm_location' => $mat['sede'],
               //'sm_costo' => $mat['costo'],
           );
        $this->model_stock_mat->update_stock($data2);
        //echo "actualizo exitosamente <br>";
      }
    }


    public function movimiento_consult()
    {
      $search = $this->model_movimiento->search();
      $this->session->set_flashdata('search',$search);
      $this->movimiento_index();
      //redirect(base_url('/Inventario/movimiento/index'));
    }
/*
    public function search()
    {
      $this->very_session();
      if ($this->input->post('filtrar'))
      {
        $sede = $this->input->post('sede');
        $data['inventario'] = $this->model_stock_mat->load_per_bakery($sede);
        $data['sede'] = $this->model_panaderia->load();
        $data['panaderia'] = $sede;

        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('inventario/datatable.php',$data);
        $this->load->view('layout/footer.php');
      }
    }
*/



    public function pdf_mat()
    {
      $this->very_session();
      if ($this->input->post('excel'))
      {
        $sede = $this->input->post('data');
        $inventario = $this->model_stock_mat->load_per_bakery($sede);

        $pdf = new Pdf();
        $this->pdf->AddPage('L','Letter');

        $this->pdf->SetTitle("REPORTE");
        $this->pdf->SetLeftMargin(4);
        $this->pdf->SetRightMargin(4);
        $this->pdf->SetFont('Arial', '', 14);

        $this->pdf->Cell(25,06,'',0,0);
        $this->pdf->Cell(25,06,'Reporte de Inventario de Materia Primas',0,1);
        $this->pdf->Cell(25,06,'',0,1);

        $this->pdf->Cell(10,06,'',0,0);
        $this->pdf->Cell(25,06,'ID',1,0);
        $this->pdf->Cell(120,06,'Nombre',1,0);
        $this->pdf->Cell(25,06,'Costo',1,0);
        $this->pdf->Cell(15,06,'Emp.',1,0);
        $this->pdf->Cell(40,06,'Cantidad ',1,1);

        foreach ($inventario as $value) {

          $empaque  = number_format(($value->sm_cant/$value->unidad_escala)/$value->mat_ue);
          $cantidad = number_format($value->sm_cant/$value->unidad_escala).' '.$value->unidad_nombre;

          $this->pdf->Cell(10,06,'',0,0);
          $this->pdf->Cell(25,06,$value->mat_id,1,0);
          $this->pdf->Cell(120,06,$value->mat_nombre,1,0);
          $this->pdf->Cell(25,06,$value->mat_costo,1,0);
          //$this->pdf->Cell(35,05,$value->med_temperatura,1,0);
          $this->pdf->Cell(15,06,$empaque,1,0);
          $this->pdf->Cell(40,06,$cantidad,1,1);
          //$this->pdf->Cell(25,05,$value->med_filtro,1,1);

        }
        $this->pdf->output('Inventario.pdf', 'I');
      }

    }

    public function excel_mat()
    {
      $this->very_session();
      if ($this->input->post('excel'))
      {
        $sede = $this->input->post('data');
        $inventario = $this->model_stock_mat->load_per_bakery($sede);

        $waka['fields'][0] = array('name' =>'ID' );
        $waka['fields'][1] = array('name' =>'Nombre' );
        $waka['fields'][2] = array('name' =>'Costo' );
        $waka['fields'][4] = array('name' =>'Empaque' );
        $waka['fields'][4] = array('name' =>'Cantidad' );

        $headers = '';
        $filarow = '';

        foreach ($waka['fields'] as $field) {
           $headers .= $field['name'] ."\t";
        }

        foreach ($inventario as $value) {
           $row = '';
           $row .= $value->mat_id  ."\t";
           $row .= $value->mat_nombre  ."\t";
           $row .= $value->mat_costo  ."\t";
           $row .= ($value->sm_cant/$value->unidad_escala)/$value->mat_ue."\t";
           $row .= $value->sm_cant/$value->unidad_escala.' '.$value->unidad_nombre."\t";
           $filarow .= trim($row)."\n";
        }

        $filarow = str_replace("\r","",$filarow);

        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=Reporte de Inventario- Materias Prima".$sede.".xls");
        echo mb_convert_encoding("$headers\n$filarow",'utf-16','utf-8');

      }
    }

    public function excel_prod()
    {
      $this->very_session();
      if ($this->input->post('excel'))
      {
        $sede = $this->input->post('data');
        $inventario = $this->model_stock_prod->load_per_bakery($sede);

        $waka['fields'][0] = array('name' =>'ID' );
        $waka['fields'][1] = array('name' =>'Nombre' );
        $waka['fields'][2] = array('name' =>'Costo' );
        $waka['fields'][4] = array('name' =>'Cantidad' );

        $headers = '';
        $filarow = '';

        foreach ($waka['fields'] as $field) {
           $headers .= $field['name'] ."\t";
        }

        foreach ($inventario as $value) {
           $row = '';
           $row .= $value->prod_id  ."\t";
           $row .= $value->prod_nombre  ."\t";
           $row .= $value->prod_costo  ."\t";
           $row .= $value->sp_cant  ."\t";
           $filarow .= trim($row)."\n";
        }

        $filarow = str_replace("\r","",$filarow);

        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=Reporte de Inventario-Productos".$sede.".xls");
        echo mb_convert_encoding("$headers\n$filarow",'utf-16','utf-8');

      }
    }

    public function pdf_prod()
    {
      $this->very_session();
      if ($this->input->post('excel'))
      {
        $sede = $this->input->post('data');
        $inventario = $this->model_stock_prod->load_per_bakery($sede);

        $pdf = new Pdf();
        $this->pdf->AddPage('L','Letter');

        $this->pdf->SetTitle("REPORTE");
        $this->pdf->SetLeftMargin(4);
        $this->pdf->SetRightMargin(4);
        $this->pdf->SetFont('Arial', '', 14);

        $this->pdf->Cell(25,06,'',0,0);
        $this->pdf->Cell(25,06,'Reporte de Inventario de Productos',0,1);
        $this->pdf->Cell(25,06,'',0,1);

        $this->pdf->Cell(10,06,'',0,0);
        $this->pdf->Cell(25,06,'ID',1,0);
        $this->pdf->Cell(120,06,'Nombre',1,0);
        $this->pdf->Cell(25,06,'Costo',1,0);
        $this->pdf->Cell(40,06,'Cantidad ',1,1);

        foreach ($inventario as $value) {

          $this->pdf->Cell(10,06,'',0,0);
          $this->pdf->Cell(25,06,$value->prod_id,1,0);
          $this->pdf->Cell(120,06,$value->prod_nombre,1,0);
          $this->pdf->Cell(25,06,$value->prod_costo,1,0);
          $this->pdf->Cell(40,06,$value->sp_cant,1,1);

        }
        $this->pdf->output('Inventario.pdf', 'I');
      }

    }

    function very_session()
  	{
  		//echo $this->session->userdata('usuario');
  		if (!$this->session->userdata('usuario')==1) {
  			//echo "Fordibben";
        redirect(base_url());
  		}
  	}

  }


?>
