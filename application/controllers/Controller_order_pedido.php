<?php

  /**
   *
   */
  class Controller_order_pedido extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_panaderia');
      $this->load->model('model_recetas');
      $this->load->model('Model_orden_prod');
      $this->load->model('model_panaderia');
      $this->load->model('model_materias');
      $this->load->model('model_proveedores');
      $this->load->model('model_order_pedido');
      $this->load->library('pdf');
      $this->load->library('session');
      $this->load->library('form_validation');
    }

    public function index()
    {
      $this->very_session();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['materias'] = $this->model_materias->display();
      $data['orden'] = $this->model_order_pedido->load_ultim_10();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('orden/index_pedido',$data);
      $this->load->view('layout/footer.php');
    }

    public function search()
    {
      if ($this->input->post('search')==true)
      {
        $this->very_session();
        $data['panaderias'] = $this->model_panaderia->load();
        $data['proveedor'] = $this->model_proveedores->display();
        $data['materias'] = $this->model_materias->display();
        $data['orden'] = $this->model_order_pedido->search();
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('orden/index_pedido',$data);
        $this->load->view('layout/footer.php');
      }
      else{
        echo "Not found 404";
      }
    }

    public function form()
    {
      $this->very_session();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['proveedor'] = $this->model_proveedores->display();
      //$data['productos'] = $this->model_productos->load();
      $data['materias'] = $this->model_materias->display();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('orden/form_pedido',$data);
      $this->load->view('layout/footer.php');
    }

    public function create()
    {
      $this->form_validation->set_rules('panaderia', 'Panaderia', 'required');
      $this->form_validation->set_rules('proveedor', 'Proveedor', 'required');
      $this->form_validation->set_rules('fecha', 'Fecha', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      elseif ($this->session->userdata('pedido_producto')==null||count($this->session->userdata('pedido_producto'))==0)
      {
        $this->session->set_flashdata('mensaje','Error, No hay producto agregado.');
        redirect('Orden/producto/form');
      }
      else
      {

        $param['op_fecha']     = $this->input->post('fecha');
        $param['op_usuario']   = $this->session->userdata('iduser');
        $param['op_sede']      = $this->input->post('panaderia');
        $param['op_proveedor'] = $this->input->post('proveedor');

        $idpedido = $this->model_order_pedido->insert($param);

        $carritop = $this->session->userdata('pedido_producto');

        foreach ($carritop as $value) {

          $data = array(
                 'ipo_cantidad'  => $value['cantidad'],
                 'ipo_orden'  => $idpedido,
                 'ipo_producto'  => $value['producto'],
                 'ipo_precio'    => $value['costo'],
             );

             $this->model_order_pedido->insert_item($data);

        }

        redirect(base_url('Orden/producto/detalles/'.$idpedido));
      }
    }

    public function update()
    {
        if ($this->input->post('update')==true)
        {
          $param['op_fecha']     = $this->input->post('fecha');
          $param['op_id']        = $this->input->post('pedido');
          $param['op_sede']      = $this->input->post('panaderia');
          $param['op_proveedor'] = $this->input->post('proveedor');
          $this->model_order_pedido->update($param);
          $this->session->set_flashdata('mensaje','Los datos estan actualziados exitosamente.');
          redirect(base_url('Orden/producto/detalles/'.$param['op_id']));
        }
        else
        {
          echo "Error";
        }
    }

    public function details()
    {
      $this->very_session();
      if ($this->uri->segment(4))
      {
        $id = $this->uri->segment(4);
        $data['orden']    = $this->model_order_pedido->read($id);
        $data['materia'] = $this->model_order_pedido->read_item_producto($id);
        $data['materias'] = $this->model_materias->display();
        $data['panaderias'] = $this->model_panaderia->load();
        $data['proveedor'] = $this->model_proveedores->display();
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('orden/details_pedido',$data);
        $this->load->view('layout/footer.php');

      }
    }

    public function add_item()
    {
      if ($this->input->post('insert')==true) {

        $data = array(
           'ipo_cantidad'  => $this->input->post('cantidad'),
           'ipo_orden'     => $this->input->post('pedido'),
           'ipo_producto'  => $this->input->post('producto'),
           'ipo_precio'    => $this->input->post('costo')
       );

        $this->model_order_pedido->insert_item($data);
        $response['success'] = true;
        $response['message'] = "Insertado exitosamene";
        echo json_encode($response);
      }
      else
      {
        $response['success'] = false;
        $response['message'] = "error";
        echo json_encode($response);
      }
    }

    public function delete_item()
    {
      if ($this->input->post('delete')==true)
      {

        $id = $this->input->post('posicion');
        $this->model_order_pedido->delete_item($id);
        $response['success'] = true;
        $response['message'] = "eliminado exitosamene";
        echo json_encode($response);
      }
      else
      {
        $response['success'] = false;
        $response['message'] = "error";
        echo json_encode($response);
      }
    }

    public function delete()
    {
      if ($this->input->post('delete')==TRUE) {
        $id = $this->input->post('id');
        $this->model_order_pedido->delete($id);
        $this->model_order_pedido->delete_item_order($id);
        $response['success'] = true;
        $response['message'] = "eliminado exitosamene";
        echo json_encode($response);
      }
      else
      {
        $response['success'] = false;
        $response['message'] = "error";
        echo json_encode($response);
      }
    }


    function very_session()
  	{
  		//echo $this->session->userdata('usuario');
  		if (!$this->session->userdata('usuario')==1) {
  			//echo "Fordibben";
        redirect(base_url());
  		}
  	}

  }

?>
