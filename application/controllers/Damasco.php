<?php

  /**
   *
   */
  class Damasco extends CI_Controller
  {
    function __construct()
    {
      parent::__construct ();
      $this->load->model('model_pipe');
    }

    public function ejecutar()
    {
      $this->model_pipe->add_column_impt();
      $this->model_pipe->add_column_dep_prod();
      $this->model_pipe->add_column_prod();
      $this->model_pipe->add_column_dep_mat();
      $this->model_pipe->add_column_mat();
      $this->model_pipe->add_column_prov();
    }
  }


?>
