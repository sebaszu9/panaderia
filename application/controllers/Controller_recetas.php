<?php

  /**
   *
   */
  class Controller_recetas extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_recetas');
      $this->load->model('model_materias');
      $this->load->model('model_unidades');
      $this->load->model('model_productos');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    public function index()
    {
      $this->very_session();
      $data['info'] = $this->model_recetas->display();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('recetas/vi_recetas.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function form()
    {
      $this->very_session();
      $data['productos'] = $this->model_productos->display();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('recetas/vc_recetas.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function create ()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');
      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      else
      {
        $param ['rec_nombre'] = $this->input->post('nombre');
        $param ['rec_obs'] = $this->input->post('descripcion');
        $res =  $this->model_recetas->insert($param);
        redirect('Recetas/create2/'.$res);
      }
    }

    public function update ()
    {
      $param ['rec_id'] = $this->input->post('id');
      $param ['rec_obs'] = $this->input->post('descripcion');
      $param ['rec_unidades'] = $this->input->post('cant');

        $this->model_recetas->update($param);
        redirect('Recetas/edit/'.$this->input->post('id'));
    }

    public function create_2()
    {
      $this->very_session();
      $id = $this->uri->segment(3);

      $data['receta'] = $this->model_recetas->read($id);
      $data['ingredientes'] = $this->model_recetas->display_ing($id);
      $data['ingredientes_prod'] = $this->model_recetas->display_ing_prod($id);
      $data['productos'] = $this->model_productos->display();
      $data['productos_rec'] = $this->model_recetas->display_prod($id);
      $data['materias'] = $this->model_materias->display();
      $data['materias_rec'] = $this->model_recetas->display_mat($id);
      $data['conceptos'] = $this->model_recetas->display_concepto();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('recetas/vc2_recetas.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function add_ing()
    {
      //VALIDAR SI EXISTE ESTE PRODUCTO ACTUALMENTE YA ANEXADO AL PEDIDO
      $mat = $this->input->post('materia');
      $a = $this->input->post('cantidad');
      $mat = $this->model_materias->read($mat);
      foreach ($mat as $m) {
        $b = $m->unidad_escala;
      }
      $cantidad = $a*$b;
      $data = array(
             'ing_mat' => $this->input->post('materia'),
             'ing_cant' => $cantidad,
             'ing_rec' => $this->input->post('receta'),
             'ing_concepto' => $this->input->post('concepto'),
         );
      $this->model_recetas->insert_ing($data);
      echo json_encode(array("status" => TRUE));
    }

    public function add_ing_prod()
    {
      $id = $this->uri->segment(3);
      $mat = $this->input->post('materia');
      $a = $this->input->post('cantidad');
      $mat = $this->model_materias->read($mat);
      foreach ($mat as $m) {
        $b2 = $m->unidad_escala;
      }
      $cantidad = $a*$b2;
      $data = array(
             'ing_pr_mat' => $this->input->post('materia'),
             'ing_pr_cant' => $cantidad,
             'ing_pr_prod' => $id,
             'ing_pr_concepto' => $this->input->post('concepto'),
         );

        $this->model_recetas->insert_ing_prod($data);
        echo json_encode(array("status" => TRUE));
    }

    public function add_prod()
    {
      $data = array(
             'pr_prod' => $this->input->post('producto'),
             'pr_cant' => $this->input->post('cantidad'),
             'pr_rec' => $this->input->post('receta'),
             'pr_detalles' => $this->input->post('descripcion'),
         );

      $this->model_recetas->insert_prod($data);
      echo json_encode(array("status" => TRUE));
    }

    public function add_mat()
    {
      //VALIDAR SI EXISTE ESTE PRODUCTO ACTUALMENTE YA ANEXADO AL PEDIDO
      $mat = $this->input->post('materia');
      $a = $this->input->post('cantidad');
      $mat = $this->model_materias->read($mat);
      foreach ($mat as $m) {
        $b = $m->unidad_escala;
      }
      $cantidad = $a*$b;
      $data = array(
             'mr_mat' => $this->input->post('materia'),
             'mr_cant' => $cantidad,
             'mr_rec' => $this->input->post('receta'),
         );
     $this->model_recetas->insert_mat($data);
     echo json_encode(array("status" => TRUE));
    }

    public function delete_ing()
    {
      $id = $this->uri->segment(3);
      $this->model_recetas->delete_ing($id);
      echo json_encode(array("status" => TRUE));
   }

   public function active(){
     header('Access-Control-Allow-Origin: *');
     if ($this->uri->segment(5) === FALSE)
     {
       echo "Safety BlackTurpial";
     }
     elseif ($this->uri->segment(5)==1) {
       $param ['rec_id'] = $this->uri->segment(3);
       $param ['state'] = 1;
       $this->model_recetas->active($param);
       echo '<div class="alert alert-success alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 Activo producto exitosamente!
               </div>';
       echo "<script>setTimeout('document.location.reload()',10); </script>";
     }
     elseif ($this->uri->segment(5)==0) {
       $param ['rec_id'] = $this->uri->segment(3);
       $param ['state'] = 0;
       $this->model_recetas->active($param);
       echo '<div class="alert alert-success alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 desactivo producto exitosamente!
               </div>';
       echo "<script>setTimeout('document.location.reload()',10); </script>";

     }
   }

   public function  seleccionar_receta()
   {
     $this->very_session();
     $data['info'] = $this->model_recetas->display_active();
     $this->load->view('layout/header2.php');
     $this->load->view('layout/sidebar2.php');
     $this->load->view('recetas/vsr_recetas.php',$data);
     $this->load->view('layout/footer2.php');
   }

   public function  seleccionar_ingrediente()
   {
     $id = $this->uri->segment(3);
     $param ['calc_rec'] = $id;
     $calc = $this->model_recetas->insert_calc($param);
     $products = $this->model_recetas->display_prod($id);
     $materias = $this->model_recetas->display_mat($id);
     foreach ($products as $key)
     {
       $var['pc_prod'] = $key->pr_prod;
       $var['pc_calculo'] = $calc;
       $var['pc_cant'] = '0';
       $var['pc_cant_rec'] = $key->pr_cant;
       $this->model_recetas->insert_prod_calc($var);
     }
     foreach ($materias as $key2)
     {
       $var1['mc_mat'] = $key2->mr_mat;
       $var1['mc_calculo'] = $calc;
       $var1['mc_cant'] = '0';
       $var1['mc_cant_rec'] = $key2->mr_cant;
       $this->model_recetas->insert_mat_calc($var1);
     }
     redirect('Recetas/calculo/'.$id.'/'.$calc);
    }

    public function prod_calc()
    {
      $a= 0;
      $b= 0;
      $id = $this->uri->segment(3); // id del producto
      $rec = $this->input->post('receta'); // id del producto
      //echo "Receta =".$rec;
      $tipo = $this->input->post('tipo');
      $cant = $this->input->post('cantidad');
      if ($tipo == 2)
      {
        //cargo la lista de ingredientes
        $var = $this->model_recetas->display_ing($rec);
        $x = 1;
        //creo un ciclo para reccorrer los ingredientes
        //El contador x me sirve para que solo itere UNA vez
        foreach ($var as $m) {
          if ($x == 1) {
            $id_mat = $m->mat_id;
            //almaceno el valor de la escala del ingredienre en b
            $b = $m->unidad_escala;
            //almaceno la cantidad del ingrediente en a
            $a = $m->ing_cant;
          }
          ///agrego +1 para que el contador no vuelva a permitir ejecutar el ciclo
          $x = $x+1;
        }
        ///$b*$cant convierte el valor ingresado en valores universales
        ///  luego divido para hallar un factor de proporcion
        /// que sera igual a $c
        $c = ($b*$cant)/$a;
        //
        $var2 = $this->model_recetas->read_prod_calc($id);
        $x = 1;
        //ejecuto otro ciclo para obtener la cantidad de producto en la receta
        foreach ($var2 as $m2) {
          if ($x == 1) {
            $cant = $m2->pc_cant_rec;
          }
          $x = $x+1;
        }
        //multiplico la cantidad de la receta por el factor de proporcion
        $cant = $cant*$c;
        $data = array(
          'pc_id' => $id,
          'pc_cant' => $cant,
        );
        $this->model_recetas->update_prod_calc($data);
        echo json_encode(array("status" => TRUE));
      }
      elseif ($tipo == 1) {
        $data = array(
          'pc_id' => $id,
          'pc_cant' => $this->input->post('cantidad'),
        );
        $this->model_recetas->update_prod_calc($data);
        echo json_encode(array("status" => TRUE));
      }
    }

    public function mat_calc()
    {
        $a= 0;
        $b= 0;
        $id = $this->uri->segment(3); // id del materia de clculo
        $rec = $this->input->post('receta'); // id del producto;
        $tipo = $this->input->post('tipo');
        $cant = $this->input->post('cantidad');
        if ($tipo == 2) {
          $var = $this->model_recetas->display_ing($rec);
          $x = 1;
          foreach ($var as $m) {
            if ($x == 1) {
              $id_mat = $m->mat_id;
              $b = $m->unidad_escala;
              $a = $m->ing_cant;
            }
            $x = $x+1;
          }
          $c = ($b*$cant)/$a;
          //echo $c;
          $var2 = $this->model_recetas->read_mat_calc($id);
          $x = 1;
          foreach ($var2 as $m2) {
            if ($x == 1) {
              $cant = $m2->mc_cant_rec;
            }
            $x = $x+1;
          }
          $cant = $cant*$c;
          $data = array(
            'mc_id' => $id,
            'mc_cant' => $cant,
          );
          $this->model_recetas->update_mat_calc($data);
          echo json_encode(array("status" => TRUE));
        }
        elseif ($tipo == 1) {
          $var1 = $this->model_recetas->read_mat_calc($id);
          foreach ($var1 as $m2) {
            $m = $m2->mat_id;
          }
          //echo $m;
          $mat = $this->model_materias->read($m);
          foreach ($mat as $m1) {
            $b = $m1->unidad_escala;
          }
          $c = ($b*$cant);
          $data = array(
            'mc_id' => $id,
            'mc_cant' => $c,
          );
          $this->model_recetas->update_mat_calc($data);
          echo json_encode(array("status" => TRUE));
        }

    }
    public function delete_ajax()
    {
        $id = $this->uri->segment(3);
        $this->model_recetas->delete_rec($id);
        $this->model_recetas->delete_rec_ing($id);
        redirect('Recetas/index');
    }

    public function delete_prod()
    {
      $id = $this->uri->segment(3);
      $this->model_recetas->delete_prod($id);
      $this->model_recetas->delete_pr_prod($id);
      echo json_encode(array("status" => TRUE));
    }

    public function delete_pr()
    {
      $id = $this->uri->segment(3);
      $this->model_recetas->delete_pr_id($id);
      echo json_encode(array("status" => TRUE));
    }

    public function delete_mat()
    {
      $id = $this->uri->segment(3);
      $this->model_recetas->delete_mat($id);
      echo json_encode(array("status" => TRUE));
    }

    public function delete_rec()
    {
      $id = $this->uri->segment(3);
      $productos = $this->model_recetas->display_prod($id);

      foreach ($productos as $key) {
      $this->model_recetas->delete_prod($key->pr_id);
      $this->model_recetas->delete_pr_prod($key->pr_id);
      }
      $this->model_recetas->delete_rec_ing($id);
      $this->model_recetas->delete_rec($id);
      redirect('Recetas/index/');
    }

    public function update_ing()
    {
      $id = $this->uri->segment(3);
      $a = $this->input->post('cantidad');
      $mat = $this->model_recetas->read_ing_base($id);
      foreach ($mat as $m) {
        $b2 = $m->unidad_escala;
      }
      $cantidad = $a*$b2;
      $data = array(
            'ing_mat' => $this->input->post('materia'),
            'ing_cant' => $cantidad,
            'ing_rec' => $this->input->post('receta'),
            'ing_concepto' => $this->input->post('concepto'),
            'ing_id' => $id,
         );
      $this->model_recetas->update_ing($data);
      echo json_encode(array("status" => TRUE));
    }

    public function update_prod()
    {
      $id = $this->uri->segment(3);
      $a = $this->input->post('cantidad');
      $data = array(
             'pr_id' => $id,
             'pr_cant' => $this->input->post('cantidad'),
         );

      $this->model_recetas->update_prod($data);
      echo json_encode(array("status" => TRUE));
    }

    public function update_mat()
    {
      $b = 0;
      $id = $this->uri->segment(3);
      $a = $this->input->post('cantidad');
      $mat = $this->model_recetas->read_mat($id);
      foreach ($mat as $m) {
        $b = $m->unidad_escala;
      }
      $cantidad = $a*$b;
      $data = array(
             'mr_id' => $id,
             'mr_cant' => $cantidad,
         );
     $this->model_recetas->update_mat($data);
     echo json_encode(array("status" => TRUE));
    }

    public function update_subing()
    {
      $b = 0;
      $id = $this->uri->segment(3);
      $a = $this->input->post('cantidad');
      $mat = $this->model_recetas->read_subing($id);
      foreach ($mat as $m) {
        $b = $m->unidad_escala;
      }
      $cantidad = $a*$b;
      $data = array(
             'ing_pr_id' => $id,
             'ing_pr_mat' => $this->input->post('materia'),
             'ing_pr_cant' => $cantidad,
             'ing_pr_concepto' => $this->input->post('concepto'),
         );
     $this->model_recetas->update_subing($data);
     echo json_encode(array("status" => TRUE));
    }

    public function edit_receta()
    {
      $id = $this->uri->segment(3);
      $data = $this->model_recetas->read($id);
      echo json_encode($data);
    }

    public function edit_ing_base()
    {
      $id = $this->uri->segment(3);
      $data = $this->model_recetas->read_ing_base($id);
      echo json_encode($data);
    }

    public function edit_sub_ing()
    {
      $id = $this->uri->segment(3);
      $data = $this->model_recetas->read_subing($id);
      echo json_encode($data);
    }

    public function update_receta()
    {
      $id = $this->uri->segment(3);
      $data = array(
             'rec_id' => $this->uri->segment(3),
             'rec_nombre' => $this->input->post('nombre'),
             'rec_obs' => $this->input->post('descripcion'),
         );
      $this->model_recetas->update_receta($data);
      echo json_encode(array("status" => TRUE));
    }

    public function edit_descripcion()
    {
      $id = $this->uri->segment(3);
      $data = $this->model_recetas->read_pr($id);
      echo json_encode($data);
    }

    public function update_detalles()
    {
      $id = $this->uri->segment(3);
      $data = array(
             'pr_id' => $this->uri->segment(3),
             'pr_detalles' => $this->input->post('descripcion'),
         );
      $this->model_recetas->update_prod($data);
      echo json_encode(array("status" => TRUE));
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1||!$this->session->userdata('usuario')==2) {
        //echo "Fordibben";
        redirect(base_url());
      }
    }


}
 ?>
