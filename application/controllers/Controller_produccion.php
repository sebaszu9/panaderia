<?php
  /**
   *
   */
  class Controller_produccion extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('Model_produccion');
      $this->load->model('model_materias');
      $this->load->model('model_recetas');
      $this->load->model('model_productos');
      $this->load->model('model_mov_mat');
      $this->load->model('model_mov_prod');
      $this->load->model('Model_stock_mat');
      $this->load->model('model_usuario');
      $this->load->model('model_panaderia');
      $this->load->model('Model_stock_prod');
      $this->load->library('pdf');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    public function llamarlo(){
      require('Controller_mov_mat.php');
      $test = new Controller_mov_mat();
      $test->index_input();
    }
/*
    {
      $this->very_session();
      $datos['date'] =date('Y-m-d');
      $datos['ubicacion'] = $this->session->userdata('sede');
      $datos['panadero'] = $this->session->userdata('iduser');
      $data['productos'] = $this->model_mov_prod->produccion($datos);
      $data['materias'] = $this->model_mov_mat->produccion($datos);
      $this->load->view('layout/header2.php');
      $this->load->view('layout/sidebar2.php');
      $this->load->view('produccion/vim_produccion.php',$data);
      $this->load->view('layout/footer2.php');
    }
*/
    public function index_admin()
    {
      $this->very_session();
      $datos['date'] = date('Y-m-d');
      $datos['ubicacion'] = '100';
      $datos['panadero'] = '100';
      $data['panaderos'] = $this->model_usuario->read_panaderos();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['productos'] = $this->model_mov_prod->produccion($datos);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('produccion/vi_produccion_e.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function produccion_sedes_form()
    {
      $this->very_session();
      $datos['date'] = date('Y-m-d');
      $datos['ubicacion'] = '100';

      $data['panaderias'] = $this->model_panaderia->load();
      //$data['productos'] = $this->model_mov_prod->produccion_by_two_sed($datos);
      $data['productos'] = [];
      $data['registro2'] = array();

      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('produccion/vi_produccion_sedes.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function produccion_sedes_search()
    {
      $this->very_session();
      $data['panaderias'] = $this->model_panaderia->load();
      $cantidadsede = $this->input->post('cant-sede');

      // consulta
      $data['registro2'] = array();

      if ($cantidadsede>=3) {
        echo "<script>alert('Maximo 2 sedes'); history.back(0)</script>";
      }
      elseif ($cantidadsede==0) {
        echo "<script>alert('Error, no ha seleccionado sede.'); history.back(0)</script>";
      }
      elseif ($this->input->post('date')==null) {
        echo "<script>alert('Por favor, Seleccione la fecha 1'); history.back(0)</script>";
      }
      elseif ($this->input->post('date2')==null) {
        echo "<script>alert('Por favor, Seleccione la fecha 2'); history.back(0)</script>";
      }
      else
      {


        $date1 =  date('Y-m-d', strtotime($this->input->post('date')."+ ".$this->input->post('date2')));

        for ($i = 1; $i <= $cantidadsede; $i++)
        {
          //echo "string";
            $sede = "selectsede".$i;
            $datos['sede'] = $this->input->post($sede);
            //$data[$i-1]['registro']  =  $this->model_mov_prod->produccion_by_two_sed($datos);
            $data['registro2'][$i-1]  =  $this->model_mov_prod->produccion_by_two_sed($datos);
            //$data['sede'][$i-1] = $datos['sede'];
            //echo "Consulto ".$datos['sede'];

            //subdetalles
            //$datosede['registro'][$i-1] =  $this->model_mov_prod->produccion_by_two_sed($datos);
            //$datosede['sede'][$i-1] = $datos['sede'];
        }

        //$datosregix = $data['registro'];

        //echo json_encode($data['registro2']);

        $date1 = date('Y-m-d', strtotime($this->input->post('date')));
        $date2 = date('Y-m-d', strtotime($this->input->post('date2')));

        //echo "string";

        //      echo json_encode($data['registro2'][1]);
        $pila = array();

        for ($ix = 1; $ix <= $cantidadsede; $ix++)
        {

          $datasede = array();

  //        echo "<br>==================== $ix =======================";

          //echo json_encode($data['registro2'][$ix-1]);
          //echo json_encode($datosregi);
          $rawdata = array();
          $i = 0;

          for ($idat = $date1; $idat <= $date2; $idat =  date('Y-m-d', strtotime($idat."+ 1 days")))
          {
            //echo json_encode($datosregi);
            $a = array($i => $idat);
            $b = array($i => 0);
            $textdate = $this->convert_text_date($idat);
            $c = array($i => $textdate);
            $d = array($i => 0);
            $e = array($i => 0);
            $f = array($i => '');
            $totalcosto = 0;
            $totalprecio = 0;


            //echo "<br>--------------------- $idat -------------------------";
            foreach ($data['registro2'][$ix-1] as  $datos)
            {


              $fecha = $datos->cm_fecha;

                if ($fecha == $idat)
                {
                  //echo "<br>- $datos->cm_id ".$datos->bod_nombre."-".$datos->cm_fecha." = ".$idat;
                  //echo "$fecha == $datos->mp_costo <br>";
                  $totalcosto  = $totalcosto +($datos->mp_costo*$datos->mp_cant);
                  $totalprecio = $totalprecio+($datos->prod_precio*$datos->mp_cant);
                  $b[$i] = $b[$i]+$totalcosto;
                  $e[$i] = $e[$i]+$totalprecio;
                  $d[$i] = $d[$i]+1;
                  $f[$i] = $datos->bod_nombre;
                }
                else
                {
                  //echo "<br>* $datos->cm_id ".$datos->bod_nombre."-".$datos->cm_fecha." = ".$idat;
                }


            }
              //echo "<br> ----------------------------------------------------------->";

              $rawdata[$i] =  array(
                  "fecha"  =>$a[$i],
                  "costo"  =>$b[$i],
                  "precio" =>$e[$i],
                  "mes"    =>$c[$i],
                  "cant"   =>$d[$i],
                  "sede"   =>$f[$i],
                );

              $i++;

              //echo json_encode($rawdata);
            } //while ($date1 <= $date2);

            //echo json_encode($rawdata);

            //$pila = array("naranja", "plátano");
            array_push($pila, $rawdata);
          //$datasede[$ix]['reporte'] = $rawdata;

        }// cierre de varios sede
        //echo json_encode($pila);

        $data['grafica'] = $pila;

        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('produccion/vi_produccion_sedes.php',$data);
        $this->load->view('layout/footer.php');


      }



    }

    public function produccion()
    {
      $this->very_session();
      $datos['date'] = $this->input->post('date');
      $datos['ubicacion'] = $this->input->post('panaderia');
      $datos['panadero'] = $this->input->post('panadero');
      $data['panaderos'] = $this->model_usuario->read_panaderos();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['productos'] = $this->model_mov_prod->produccion($datos);


      if ($this->input->post('tipo')==1)
    {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('produccion/vi_produccion_e.php',$data);
        $this->load->view('layout/footer.php');
     }
     /*
      elseif ($this->input->post('tipo')==2)
      {

          $pdf = new Pdf();
          $this->pdf->AddPage('L','Letter');

          $this->pdf->SetTitle("REPORTE DE PRODUCCION");
          $this->pdf->SetLeftMargin(4);
          $this->pdf->SetRightMargin(4);
          $this->pdf->SetFont('Arial', '', 14);

          $fecha = $datos['date'];
          $this->pdf->Cell(25,06,'',0,0);
          $this->pdf->Cell(25,06,utf8_decode('Reporte de producción Fecha '.$fecha),0,1);
          $this->pdf->Cell(25,06,'',0,1);

          $this->pdf->Cell(5,06,'',0,0);
          $this->pdf->Cell(110,06,'Nombre',1,0);
          $this->pdf->Cell(15,06,'Precio',1,0);
          $this->pdf->Cell(15,06,'Costo',1,0);
          $this->pdf->Cell(15,06,'Utild.',1,0);
          $this->pdf->Cell(15,06,'Cant.',1,0);
          //$this->pdf->Cell(30,06,'Fecha',1,0);
          $this->pdf->Cell(25,06,'Ubicacion',1,0);
          $this->pdf->Cell(40,06,'Panadero.',1,1);

          foreach ($data['productos'] as $value) {

            $this->pdf->Cell(5,06,'',0,0);
            $this->pdf->Cell(110,06,$value->prod_id.' '.utf8_decode($value->prod_nombre),1,0);
            $this->pdf->Cell(15,06,$value->prod_precio,1,0);
            $this->pdf->Cell(15,06,$value->mp_costo,1,0);
            $this->pdf->Cell(15,06,number_format(($value->prod_precio-$value->mp_costo)/$value->prod_precio*100)."%",1,0);
            $this->pdf->Cell(15,06,$value->mp_cant,1,0);
            //$this->pdf->Cell(30,06,$value->cm_fecha,1,0);
            $this->pdf->Cell(25,06,$value->bod_nombre,1,0);
            $this->pdf->Cell(40,06,$value->usr_nombre,1,1);

          }
          $this->pdf->output('Producion.pdf', 'I');
      }*/
      /*
      elseif ($this->input->post('tipo')==3)
      {

          $sede = $this->input->post('data');
          $productos = $this->model_productos->display();

          $waka['fields'][0] = array('name' =>'Nombre' );
          $waka['fields'][1] = array('name' =>'Precio' );
          $waka['fields'][2] = array('name' =>'Costo' );
          $waka['fields'][3] = array('name' =>'Utilidad' );
          $waka['fields'][4] = array('name' =>'Cantidad' );
          $waka['fields'][5] = array('name' =>'Fecha' );
          $waka['fields'][6] = array('name' =>'Ubicación' );
          $waka['fields'][7] = array('name' =>'Panadero' );

          $headers = '';
          $filarow = '';

          foreach ($waka['fields'] as $field) {
             $headers .= $field['name'] ."\t";
          }

          foreach ($data['productos'] as $value) {
             $row = '';
             $row .= $value->prod_id.' '.utf8_decode($value->prod_nombre)."\t";
             $row .= $value->prod_precio  ."\t";
             $row .= $value->mp_costo  ."\t";
             $row .= number_format(($value->prod_precio-$value->mp_costo)/$value->prod_precio*100)."%"  ."\t";
             $row .= $value->mp_cant  ."\t";
             $row .= $value->cm_fecha  ."\t";
             $row .= $value->bod_nombre  ."\t";
             $row .= $value->usr_nombre  ."\t";
             $filarow .= trim($row)."\n";
          }

          $filarow = str_replace("\r","",$filarow);

          header("Content-type: application/x-msdownload");
          header("Content-Disposition: attachment; filename=Producion del dia.xls");
          echo mb_convert_encoding("$headers\n$filarow",'utf-16','utf-8');
      }
      */
      elseif ($this->input->post('tipo')==4)
      {
        if ($this->input->post('date2')==null)
        {
          echo "<script>alert('Por favor, Seleccione la Fecha Final.');history.back();</script>";
        }
        else
        {
          $date1 = date('Y-m-d', strtotime($this->input->post('date')));
          $date2 = date('Y-m-d', strtotime($this->input->post('date2')));
          $info = $data['productos'];

          $rawdata = array();
          $i = 0;
          while ($date1 <= $date2):

            $a = array($i => $date1);
            $b = array($i => 0);
            $textdate = $this->convert_text_date($date1);
            $c = array($i => $textdate);
            $d = array($i => 0);
            $e = array($i => 0);

            foreach($data['productos'] as $datos)
            {

              $totalcosto = 0;
              $totalprecio = 0;
              $fecha = $datos->cm_fecha;

              if ($fecha == $date1)
              {
                //echo "$fecha == $datos->mp_costo <br>";
                $totalcosto  = $totalcosto +($datos->mp_costo*$datos->mp_cant);
                $totalprecio = $totalprecio+($datos->prod_precio*$datos->mp_cant);
                $b[$i] = $b[$i]+$totalcosto;
                $e[$i] = $e[$i]+$totalprecio;
                $d[$i] = $d[$i]+1;
              }

              $rawdata[$i] =  array(
                  "fecha"  =>$a[$i],
                  "costo"  =>$b[$i],
                  "precio" =>$e[$i],
                  "mes"    =>$c[$i],
                  "cant"   =>$d[$i],
                );

            }
            $i++;
            $date1 =  date('Y-m-d', strtotime($date1."+ 1 days"));
          endwhile;
          //echo json_encode($rawdata);
          $data['ventas'] = $rawdata;
          $this->load->view('layout/header.php');
          $this->load->view('layout/sidebar.php');
          $this->load->view('produccion/vi_produccion_grafico.php',$data);
          $this->load->view('layout/footer.php');
        }

      }
      else
      {
        echo "No Disponible";
      }
    }

    function prueba()
    {
      for ($i = 1; $i <= 10; $i++) {
          //echo "<br> $i";

          for ($ix = 1; $ix <= 10; $ix++) {
            $res = $i*$ix;
              echo "<br> $i x $ix =  $res";
          }
      }
    }

/*
    public function produccion_backup()
    {
      $this->very_session();
      $datos['date'] = $this->input->post('date');
      $datos['ubicacion'] = $this->input->post('panaderia');
      $datos['panadero'] = $this->input->post('panadero');
      $data['panaderos'] = $this->model_usuario->read_panaderos();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['productos'] = $this->model_mov_prod->produccion($datos);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('produccion/vi_produccion.php',$data);
      $this->load->view('layout/footer.php');
    }
*/
    public function create()
    {
      $this->very_session();
      $id = $this->input->post('receta');
      $calc = $this->input->post('calculo');
      $observaciones = $this->input->post('observaciones');
      $location = $this->session->userdata('sede');
      $user = $this->input->post('panadero');
      $receta = $this->model_recetas->read($id);
      $ing = $this->model_recetas->display_ing($id);
      $ingredientes_prod = $this->model_recetas->display_ing_prod($id);
      $productos = $this->model_recetas->display_prod_calc($calc);
      $materias = $this->model_recetas->display_mat_calc($calc);
      $costo = 0;

      ////SI ES ADMIN PIDE FECHA SI ES PANADERO NO LO HACE//
      if ($this->session->userdata('role')==1) {
        $date = $this->input->post('fecha');
      }
      elseif ($this->session->userdata('role')==2) {
        $date = date('Y-m-d');
      }

      //conceptos para los movimientos de inventario de productos y materias
      //concepto movimiento materia
      $cmm = array(
             'cm_tipo' => '0',
             'cm_fecha' => $date,
             'cm_concepto' => 2,
             'cm_obs' => $observaciones,
             'cm_ubicacion' => $location,
             'cm_usuario' => $user
         );
      //concepto movimiento producto
      $cmp = array(
              'cm_tipo' => '1',
              'cm_fecha' => $date,
              'cm_concepto' => 2,
              'cm_obs' => $observaciones,
              'cm_ubicacion' => $location,
              'cm_usuario' => $user,
          );
      $id_cmm = $this->model_mov_mat->insert_cmm($cmm);
      $id_cmp = $this->model_mov_prod->insert_cmp($cmp);

      //CALCULAMOS EL FACTOR X DE LA RECETA
      $x = 0;
      foreach($productos as $datos) {
      $x = $x + ($datos->pc_cant/$datos->pc_cant_rec);
      }
      foreach($materias as $mate) {
      $x = $x + ($mate->mc_cant/$mate->mc_cant_rec);
      }

      ///CREA LOS MOVIMIENTOS DE LAS MATERIAS BASE Y ACTUALIZA SU inventario
      foreach ($ing as $value) {
        if ($value->mat_tipo==0)
        {
          $mov_mat = array(
            'mm_cant' => $value->ing_cant*$x,
            'mm_id_concepto' => $id_cmm,
            'mm_id_mat' => $value->ing_mat,
            'mm_costo' => ($value->mat_costo/$value->mat_ue)*$value->ing_cant*$x/$value->unidad_escala,
          );
          $costo = $costo + ($value->mat_costo/$value->mat_ue)*$value->ing_cant/$value->unidad_escala;
          $this->model_mov_mat->insert_mov($mov_mat);
          $mat['id'] = $value->ing_mat;
          $mat['tipo'] = 0;
          $mat['cant'] = $value->ing_cant*$x*$value->mat_inv;
          $mat['sede'] = $location;
          $this->update_inventory_mat($mat);
        }
        else
        {
          $ingcomp= $this->model_materias->load_ing_comp_per_mat($value->mat_id);
          foreach ($ingcomp as $ingc) {

            $gramisimo = (($x*$value->ing_cant/$value->unidad_escala)*$ingc->inc_cantidad)/$value->mat_ue;
            $mov_mat = array(
              'mm_cant' => $gramisimo,
              'mm_id_concepto' => $id_cmm,
              'mm_id_mat' => $ingc->mat_id,
              'mm_costo' => ($ingc->mat_costo/$ingc->mat_ue)*$ingc->inc_cantidad*$x/$ingc->unidad_escala,
            );
            $costo = $costo + ($ingc->mat_costo/$ingc->mat_ue)*$ingc->inc_cantidad/$ingc->unidad_escala;
            $this->model_mov_mat->insert_mov($mov_mat);
            $mat['id'] = $ingc->mat_id;
            $mat['tipo'] = 0;
            $mat['cant'] = $gramisimo*$ingc->mat_inv;
            $mat['sede'] = $location;
            $this->update_inventory_mat($mat);

          }
        }
     }
     ///RECORRE EL ARRAY PRODUCTOS Y SUS SUBINGREDIENTES
     //Posteriormente crea los movimientos de cada SUBINGREDIENTES y
     ///Actualiza su inventario
        foreach($productos as $data) {
          if ($data->pc_cant > 0) {

            $x =($data->pc_cant/$data->pc_cant_rec);
            foreach($ingredientes_prod as $datos7) {
              if ($datos7->prod_id==$data->prod_id){
                $mov_mat = array(
                  'mm_cant' => $datos7->ing_pr_cant*$x,
                  'mm_id_concepto' => $id_cmm,
                  'mm_id_mat' => $datos7->ing_pr_mat,
                  'mm_costo' => ($datos7->mat_costo/$datos7->mat_ue)*$datos7->ing_pr_cant*$x/$datos7->unidad_escala,
                );
                $this->model_mov_mat->insert_mov($mov_mat);
                $mat['id'] = $datos7->ing_pr_mat;
                $mat['tipo'] = 0;
                $mat['cant'] = $datos7->ing_pr_cant*$x*$datos7->mat_inv;
                $mat['sede'] = $location;
                $this->update_inventory_mat($mat);
              }
            }
          }
         }
     ///CREA LOS MOVIMIENTOS DE LOS PRODUCTOS PRODUCIDOS Y
     //ACTUALIZARÁ SU inventario
     foreach ($productos as $value1) {
       //solo movimientos que sean mayores que Controller_produccion
       // para no introducir productos de la receta que no han sido incluidos en esta produccion
       if ($value1->pc_cant > 0) {

         $x =($value1->pc_cant/$value1->pc_cant_rec);
         $subcosto = 0;
         foreach($ingredientes_prod as $datos17) {
           if ($datos17->prod_id==$value1->prod_id){
             $subcosto = $subcosto +($datos17->mat_costo/$datos17->mat_ue)*$datos17->ing_pr_cant/$datos7->unidad_escala;
           }
         }
         $total = $subcosto + $costo;
          $mov_prod = array(
            'mp_id_concepto' => $id_cmp,
            'mp_id_prod' => $value1->prod_id,
            'mp_cant' => $value1->pc_cant,
            'mp_costo' => $total/$value1->pc_cant_rec,
          );
          $mp_id = $this->model_mov_prod->insert_mov($mov_prod);

          ///////////////////////////////////////
          ////////CREAMOS EL MOVIMIENTO DE EMPAQUE
          ///////////////////////////////////////
          $empaque = array(
            'emp_mov_prod' => $mp_id,
            'emp_cant' => 0,
          );
          $this->model_mov_prod->create_empaque($empaque);

          //creamos el vector que alimentara la function que actualiza el invenatario
          $mat['id'] = $value1->prod_id;
          $mat['tipo'] = 1;
          $mat['cant'] = $value1->pc_cant*$value1->prod_inv;
          $mat['sede'] = $location;
          $this->update_inventory_prod($mat);
          $producto = array(
            'prod_id' => $value1->prod_id,
            'prod_costo' => $total/$value1->pc_cant_rec,
          );
          $this->model_productos->update($producto);
       }
     }
     /////CREA MOVIMIENTOS MATERIAS EN PRODUCCION Y ACTUALIZA SU INVENTARIO
     foreach ($materias as $value2) {
       $mov_prod = array(
         'mm_id_concepto' => $id_cmp,
         'mm_id_mat' => $value2->mat_id,
         'mm_cant' => $value2->mc_cant,
         'mm_costo' => $total*($value2->mc_cant/$value2->mc_cant_rec),
       );
       if ($value2->mc_cant > 0) {
         $this->model_mov_mat->insert_mov($mov_prod);
         //creamos el vector que alimentara la function que actualiza el invenatario
         $mat['id'] = $value2->mat_id;
         $mat['tipo'] = 1;
         $mat['cant'] = $value2->mc_cant*$value2->mat_inv;
         $mat['sede'] = $location;
         $this->update_inventory_mat($mat);
       }
     }
     //redirect('Produccion/details/'.$id_cmp.'/'.$id_cmm);
     echo json_encode(array("status" => TRUE, "idmp"=>$id_cmp, "idcm"=>$id_cmm));
    }
    public function delete()
    {
      $this->very_session();
      $in = $this->input->post('in');
      $out = $this->input->post('out');
      $x = $this->input->post('x');
      $ing = $this->model_mov_mat->read_mov_mat($out);
      $producto = $this->model_mov_prod->read_mov_prod($in);
      $materias = $this->model_mov_mat->read_mov_mat($in);

      ///busca LOS MOVIMIENTOS Productos Y ACTUALIZA SU inventario
      foreach($producto as $datos) {
          $mat['id'] = $datos->mp_id_prod;
          $mat['tipo'] = 0;
          $mat['cant'] = $datos->mp_cant*$datos->prod_inv;
          $mat['sede'] = $datos->cm_ubicacion;
          $this->update_inventory_prod($mat);
          $this->model_mov_prod->delete_mov($datos->mp_id);
          }
      ////ELIMINA MOVIMIENTOS MATERIAS PRODUCIDAS
      ///Y ACTUALIZA EL INVENTARIO
      foreach($materias as $datos2) {
        $mat['id'] = $datos2->mat_id;
        $mat['tipo'] = 0;
        $mat['cant'] = $datos2->mm_cant*$datos2->mat_inv;
        $mat['sede'] = $datos2->cm_ubicacion;
        $this->update_inventory_mat($mat);
        $this->model_mov_mat->delete_mov($datos2->mm_id);
        }
        /// ELIMINA CADA MOVIMIENTO USADO COMO ingredientes
        /// Y ACTUALIZA EL NUMERO DE inventario
     foreach($ing as $ingredientes) {
          $mat['id'] = $ingredientes->mat_id;
          $mat['tipo'] = 1;
          $mat['cant'] = $ingredientes->mm_cant*$ingredientes->mat_inv;
          $mat['sede'] = $ingredientes->cm_ubicacion;
          $this->update_inventory_mat($mat);
          $this->model_mov_mat->delete_mov($ingredientes->mm_id);
          }

     $this->model_mov_mat->delete_cmm($out);
     $this->model_mov_prod->delete_cmp($in);

     if ($x == 1) {
       redirect('Produccion/index_admin/');
     }
     elseif ($x == 2) {
       redirect('Calculos/produccion/');
     }
    }

    public function edit_mov_prod()
    {
      $id = $this->uri->segment(3);
      $mov = $this->model_mov_prod->read_mov($id);
      foreach($mov as $nm) {
           $nuevo ['mp_id'] = $nm->mp_id;
           $nuevo ['mp_cant'] = $this->input->post('cantidad');
           $nuevo ['mp_id_concepto'] = $nm->mp_id_concepto;
           $nuevo ['mp_id_prod'] = $nm->mp_id_prod;
           $nuevo ['mp_costo'] = $nm->mp_costo;
           }
      $this->model_mov_prod->update_mov($nuevo);
      echo json_encode(array("status" => TRUE));
    }
    public function update_inventory_mat($mat)
    {
      $this->very_session();
      $param = $this->Model_stock_mat->get_stock($mat);
      $materias = $this->model_materias->read($mat['id']);

      if ($param==FALSE)
      {
        $number2 = $mat['cant'];
        $param ['sm_cant'] = $number2;
        $param ['sm_mat'] = $mat['id'];
        //$param ['sm_costo'] = $mat['costo'];
        $param ['sm_location'] = $mat['sede'];
        // inserta movimiento concepto
        $concepto =  $this->Model_stock_mat->insert_stock($param);
      }
      else {
        # code...
        foreach ($param->result() as $value) {
          ///Obtiene el valor actual, con un ciclo
          $number1 = $value->sm_cant;
        }
        //La cantidad que se va a sumar o a restar vienenen la funcion
        $number2 = $mat['cant'];

        if ($mat['tipo'] == 1) {
          $stock = $number1 + $number2;
        }
        elseif ($mat['tipo'] == 0) {
          $stock = $number1 - $number2;
        }
        $data2 = array(
               'sm_cant' => $stock,
               'sm_mat' => $mat['id'],
               'sm_location' => $mat['sede'],
           );
        $this->Model_stock_mat->update_stock($data2);
      }
    }

    public function update_inventory_prod($prod)
    {
      $this->very_session();
      $param = $this->Model_stock_prod->get_stock($prod);

      if ($param==FALSE)
      {
        $number2 = $prod['cant'];
        $param ['sp_cant'] = $number2;
        $param ['sp_prod'] = $prod['id'];
        $param ['sp_location'] = $prod['sede'];
        // inserta movimiento concepto
        $concepto =  $this->Model_stock_prod->insert_stock($param);
      }
      else {
        # code...
        foreach ($param->result() as $value) {
          ///Obtiene el valor actual, con un ciclo
          $number1 = $value->sp_cant;
        }
        //La cantidad que se va a sumar o a restar vienenen la funcion
        $number2 = $prod['cant'];

        if ($prod['tipo'] == 1) {
          $stock = $number1 + $number2;
        }
        elseif ($prod['tipo'] == 0) {
          $stock = $number1 - $number2;
        }
        $data2 = array(
               'sp_cant' => $stock,
               'sp_prod' => $prod['id'],
               'sp_location' => $prod['sede'],
             );
        $this->Model_stock_prod->update_stock($data2);
      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==2||!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url());
      }
    }

    public function convert_text_date($date)
    {
      //$data = "2018-05-02";
      $mes = date('m', strtotime($date));
      //echo  $mes;
      if ($mes==1) {
        return  "Enero";
      }
      elseif ($mes==2) {
        return  "Febrero";
      }
      elseif ($mes==3) {
        return  "Marzo";
      }
      elseif ($mes==4) {
        return  "Abril";
      }
      elseif ($mes==5) {
        return  "Mayo";
      }
      elseif ($mes==6) {
        return  "Junio";
      }
      elseif ($mes==7) {
        return  "Julio";
      }
      elseif ($mes==8) {
        return  "Agosto";
      }
      elseif ($mes==9) {
        return  "Septiembre";
      }
      elseif ($mes==10) {
        return  "Octubre";
      }
      elseif ($mes==11) {
        return  "Noviembre";
      }
      elseif ($mes==12) {
        return  "Diciembre";
      }
      else {
        return  "Error";
      }
    }

  }
 ?>
