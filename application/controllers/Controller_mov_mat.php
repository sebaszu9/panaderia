<?php

  class Controller_mov_mat extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_materias');
      $this->load->model('model_panaderia');
      $this->load->model('model_proveedores');
      $this->load->model('model_cat_mat');
      $this->load->model('model_mov_mat');
      $this->load->library('session');
      $this->load->library('pdf');
      $this->load->model('Model_stock_mat');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    public function index_input()
    {
      $this->very_session();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/entrada/vi_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function search_input()
    {
      $this->very_session();
      $search = $this->model_mov_mat->search();

      if ($this->input->post('tipo')==1)
      {
        $this->session->set_flashdata('search',$search);
        redirect(base_url('Movimiento/materia/input/index'), 'refresh');
      }
      elseif ($this->input->post('tipo')==2) {

        $pdf = new Pdf();
        $this->pdf->AddPage('L','Letter');

        $this->pdf->SetTitle("REPORTE DE POVEEDOR");
        $this->pdf->SetLeftMargin(4);
        $this->pdf->SetRightMargin(4);
        $this->pdf->SetFont('Arial', '', 14);

        $fecha = $this->input->post('fecha1');
        $fecha2 = $this->input->post('fecha2');

        $this->pdf->Cell(25,06,'',0,0);
        $this->pdf->Cell(25,06,utf8_decode('Reporte de Compra Proovedor '.$fecha.' - '.$fecha2),0,1);
        $this->pdf->Cell(25,06,'',0,1);

        $this->pdf->Cell(5,06,'',0,0);
        $this->pdf->Cell(15,06,'ID',1,0);
        $this->pdf->Cell(90,06,'Nombre',1,0);
        $this->pdf->Cell(35,06,'Fecha',1,0);
        $this->pdf->Cell(50,06,'Ubicacion',1,0);
        $this->pdf->Cell(50,06,'Costo',1,1);


        foreach ($search as $value)
        {
          $this->pdf->Cell(5,06,'',0,0);
          $this->pdf->Cell(15,06,$value->cm_id,1,0);
          $this->pdf->Cell(90,06,utf8_decode($value->prov_nombre),1,0);
          $this->pdf->Cell(35,06,$value->cm_fecha,1,0);
          $this->pdf->Cell(50,06,$value->bod_nombre,1,0);

          $materias = $this->model_mov_mat->read_mov_mat($value->cm_id);

          $totalmaterias = 0;

          foreach ($materias as $mat) {
            $totalmaterias = $mat->mm_costo + $totalmaterias;
          }

          $this->pdf->Cell(50,06,number_format($totalmaterias),1,1);

        }

        $this->pdf->output('Reporte del proveedor.pdf', 'I');
      }
      elseif ($this->input->post('tipo')==3) {

            $waka['fields'][0] = array('name' =>'ID' );
            $waka['fields'][1] = array('name' =>'Nombre' );
            $waka['fields'][2] = array('name' =>'Fecha' );
            $waka['fields'][3] = array('name' =>'Ubicacion' );
            $waka['fields'][4] = array('name' =>'Costo' );

            $headers = '';
            $filarow = '';

            foreach ($waka['fields'] as $field) {
               $headers .= $field['name'] ."\t";
            }

            foreach ($search as $value) {

              $totalmaterias = 0;
              $materias = $this->model_mov_mat->read_mov_mat($value->cm_id);
              foreach ($materias as $mat) {
                $totalmaterias = $mat->mm_costo + $totalmaterias;
              }

               $row = '';
               $row .= $value->cm_id  ."\t";
               $row .= utf8_decode($value->prov_nombre)."\t";
               $row .= $value->cm_fecha  ."\t";
               $row .= $value->bod_nombre  ."\t";
               $row .= $totalmaterias."\t";
               $filarow .= trim($row)."\n";
            }

            $filarow = str_replace("\r","",$filarow);

            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=Reporte del proveedor.xls");
            echo mb_convert_encoding("$headers\n$filarow",'utf-16','utf-8');
      }
      else {
        echo "Error not found type";
      }

    }

    public function detail_input()
    {
      $this->very_session();
      $id = $this->uri->segment(5);

      $data['entrada'] = $this->model_mov_mat->read_cmm($id);
      $data['materias'] = $this->model_mov_mat->read_mov_mat($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/entrada/vd_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function edit_input()
    {
      $this->very_session();
      $id = $this->uri->segment(5);
      $data['proveedor'] = $this->model_proveedores->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['entrada'] = $this->model_mov_mat->read_cmm($id);
      $data['materias'] = $this->model_mov_mat->read_mov_mat($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/entrada/ve_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function update_input()
    {
      $this->very_session();
      $param ['cm_id']   = $this->input->post('concepto');
      $param ['cm_obs']   =  $this->input->post('descripcion');
      $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
      // inserta movimiento concepto
      $this->model_mov_mat->update_cmm($param);

      $relacion ['rp_prov'] = $this->input->post('proveedor');
      $relacion ['rp_cm'] = $this->input->post('concepto');
      $this->model_mov_mat->update_rp($relacion);

      redirect('Movimiento/materia/input/edit/'.$param['cm_id']);
    }

     public function form_input()
    {
      $this->very_session();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['categoria'] = $this->model_cat_mat->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/entrada/vc_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

    // actualiza el item de entrada
    public function update_product_mov()
    {
      //echo "actualizo exitosamente";
      $concepto = $this->input->post('concepto');
      $cantidad_ant = $this->input->post('cantdold');
      $param ['id'] = $this->input->post('id');
      $param ['cantd'] = $this->input->post('cantd');
      $param ['price'] = $this->input->post('price');
      $this->model_mov_mat->update_mov_mat($param);

      $cantidad = $param ['cantd'] - $cantidad_ant;
      // modificar inventario
      $mat['id'] = $this->input->post('materia');
      $mat['tipo'] = 1;
      $mat['costo'] = $this->input->post('price');
      $mat['cant'] = $cantidad*$key->mat_inv;
      $mat['sede'] = $this->input->post('panaderia');

      $this->update_inventory($mat);

      $this->session->set_flashdata('mensaje_item','Actualizado.');
      redirect('Movimiento/materia/input/edit/'.$concepto);
    }

    //********************************************************
    //***************** MOVIMIENTO DE SALIDA *****************
    //********************************************************

    public function form_output(){
      $this->very_session();
      $data['concepto'] = $this->model_mov_mat->load_concept();
      $data['categoria'] = $this->model_cat_mat->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/salida/vc_salida.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function index_output()
    {
      $this->very_session();
      $data['concepto'] = $this->model_mov_mat->load_concept();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/salida/vi_salida.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function search_output()
    {
      $this->very_session();
      echo "Consultando...";
      $search = $this->model_mov_mat->search_output();
      $this->session->set_flashdata('search',$search);
      redirect(base_url('Movimiento/materia/output/index'), 'refresh');
    }

    public function detail_output()
    {
      $this->very_session();
      $id = $this->uri->segment(5);

      $data['entrada'] = $this->model_mov_mat->read_cmm_output($id);
      $data['materias'] = $this->model_mov_mat->read_mov_mat($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/salida/vd_salida.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function edit_output()
    {
      $this->very_session();
      $id = $this->uri->segment(5);
      $data['concepto'] = $this->model_mov_mat->load_concept();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['entrada'] = $this->model_mov_mat->read_cmm_output($id);
      $data['materias'] = $this->model_mov_mat->read_mov_mat($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/salida/ve_salida.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function update_output()
    {
      $this->very_session();
      $param ['cm_id']   = $this->input->post('id');
      $param ['cm_concepto']   = $this->input->post('concepto');
      $param ['cm_obs']   =  $this->input->post('descripcion');
      $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
      // inserta movimiento concepto
      $this->model_mov_mat->update_cmm($param);

      $relacion ['rp_prov'] = $this->input->post('proveedor');
      $relacion ['rp_cm'] = $this->input->post('concepto');
      $this->model_mov_mat->update_rp($relacion);

      redirect('Movimiento/materia/output/edit/'.$param['cm_id']);
    }

    public function create_output()
    {
      $this->form_validation->set_rules('panaderia', 'Panaderia', 'required');
      $this->form_validation->set_rules('concepto', 'Concepto', 'required');
      $this->form_validation->set_rules('fecha', 'Fecha', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form_output();
      }
      elseif ($this->session->userdata('pedido_materia')==null||count($this->session->userdata('pedido_materia'))==0) {
        $this->session->set_flashdata('mensaje','Error, No hay materia agregado.');
        redirect('Movimiento/materia/output/form');
      }
      else
      {
        $param ['cm_tipo'] = 0;
        $param ['cm_concepto'] = $this->input->post('concepto_cod');
        //$param ['cm_concepto'] = 1;
        $param ['cm_obs'] = $this->input->post('descripcion');
        $param ['cm_ubicacion'] = $this->input->post('panaderia');
        $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
        //$param ['cm_usuario'] = $this->session->userdata('usr_id');
        $param ['cm_usuario'] = 2;
        // inserta movimiento concepto
        $concepto =  $this->model_mov_mat->insert_cmm($param);

        $carritop = $this->session->userdata('pedido_materia');

        foreach ($carritop as $value) {

          $data = array(
                 'mm_cant'        => $value['cantidad'],
                 'mm_id_concepto' => $concepto,
                 'mm_id_mat'      => $value['producto'],
                 'mm_costo'       => $value['costo'],
             );

          $this->model_mov_mat->insert_mov($data);

          $mat['id'] = $value['producto'];
          $mat['tipo'] = 0;
          $mat['costo'] = $value['costo'];
          $mat['cant'] = $value['cantidad']*$value->mat_inv;
          $mat['sede'] = $this->input->post('panaderia');

          $this->update_inventory($mat);
        }
        session_destroy();
        $this->session->set_flashdata('mensaje','Guardado exitosamente.');
        redirect('Movimiento/materia/output/details/'.$concepto);
      }
    }

    public function create_input()
    {

      $this->form_validation->set_rules('panaderia', 'Panaderia', 'required');
      $this->form_validation->set_rules('proveedor', 'Proveedor', 'required');
      $this->form_validation->set_rules('fecha', 'Fecha', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form_pedidos();
      }
      elseif ($this->session->userdata('pedido_materia')==null||count($this->session->userdata('pedido_materia'))==0) {
        $this->session->set_flashdata('mensaje','Error, No hay producto agregado.');
        redirect('Movimiento/materia/input/form');
      }
      else
      {
        $param ['cm_tipo'] = 1;
        $param ['cm_concepto'] = 1;
        $param ['cm_obs'] = $this->input->post('descripcion');
        $param ['cm_ubicacion'] = $this->input->post('panaderia');
        $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
        //$param ['cm_usuario'] = $this->session->userdata('usr_id');
        $param ['cm_usuario'] = 2;
        // inserta movimiento concepto
        $concepto =  $this->model_mov_mat->insert_cmm($param);

        $relacion ['rp_prov'] = $this->input->post('proveedor');
        $relacion ['rp_cm']  = $concepto;
        $this->model_mov_mat->insert_rp($relacion);


        $carritop = $this->session->userdata('pedido_materia');

        foreach ($carritop as $value) {

          $data = array(
                 'mm_cant'        => $value['cantidad'],
                 'mm_id_concepto' => $concepto,
                 'mm_id_mat'      => $value['producto'],
                 'mm_costo'       => $value['costo'],
             );

          $this->model_mov_mat->insert_mov($data);

          $mat['id'] = $value['producto'];
          $mat['tipo'] = 1;
          $mat['cant'] = $value['cantidad']*$value->mat_inv;
          $mat['sede'] = $this->input->post('panaderia');

          $this->update_inventory($mat);
          $costo['mat_id'] = $value['producto'];
          $costo['mat_costo'] = $value['costo'];
          $this->model_materias->update($costo);
        }
        session_destroy();
        $this->session->set_flashdata('mensaje','Guardado exitosamente.');
        redirect('Movimiento/materia/input/details/'.$concepto);
      }
    }

    public function form_pedidos()
    {
      $this->very_session();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['categoria'] = $this->model_cat_mat->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/entrada/vc_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }
    public function delete_mov_in()
    {
      $id = $this->uri->segment(3);
      $mov = $this->model_mov_mat->read_mov($id);

      foreach ($mov as $key)
      {
        $mat['id'] = $key->mm_id_mat;
        $mat['tipo'] = 0;
        $mat['cant'] = $key->mm_cant*$key->mat_inv;
        $mat['sede'] = $key->cm_ubicacion;
      }
      $this->update_inventory($mat);
      $this->model_mov_mat->delete_mov($id);
      echo json_encode(array("status" => TRUE));
   }

   public function delete_mov_out()
   {
     $id = $this->uri->segment(3);
     $mov = $this->model_mov_mat->read_mov($id);

     foreach ($mov as $key)
     {
       $mat['id'] = $key->mm_id_mat;
       $mat['tipo'] = 1;
       $mat['cant'] = $key->mm_cant*$key->mat_inv;
       $mat['sede'] = $key->cm_ubicacion;
     }
     $this->update_inventory($mat);
     $this->model_mov_mat->delete_mov($id);
     echo json_encode(array("status" => TRUE));
  }

    public function update_inventory($mat){

      $param = $this->Model_stock_mat->get_stock($mat);
      $materias = $this->model_materias->read($mat['id']);

      foreach ($materias as $value2) {
        ///Obtiene el valor actual, con un ciclo
        $ue = $value2->mat_ue;
        $um = $value2->unidad_escala;
      }

      if ($param==FALSE)
      {
        echo "No existe stock y se crea uno.<br>";
        $number2 = $mat['cant']*$ue*$um;
        $param ['sm_cant'] = $number2;
        $param ['sm_mat'] = $mat['id'];
        $param ['sm_location'] = $mat['sede'];
        // inserta movimiento concepto
        $concepto =  $this->Model_stock_mat->insert_stock($param);
      }
      else {
        # code...
        foreach ($param->result() as $value) {
          ///Obtiene el valor actual, con un ciclo
          $number1 = $value->sm_cant;
        }
        //La cantidad que se va a sumar o a restar vienenen la funcion
        $number2 = $mat['cant']*$ue*$um;

        if ($mat['tipo'] == 1) {
          $stock = $number1 + $number2;
        }
        elseif ($mat['tipo'] == 0) {
          $stock = $number1 - $number2;
        }
        $data2 = array(
               'sm_cant' => $stock,
               'sm_mat' => $mat['id'],
               'sm_location' => $mat['sede'],
               //'sm_costo' => $mat['costo'],
           );
        $this->Model_stock_mat->update_stock($data2);
        echo "actualizo exitosamente <br>";
      }
    }

    public function delete_input()
    {
      $id = $this->uri->segment(5);
      $mov = $this->model_mov_mat->read_mov_mat($id);
      foreach ($mov as $key)
      {
        $mat['id'] = $key->mm_id_mat;
        $mat['tipo'] = 0;
        $mat['cant'] = $key->mm_cant*$key->mat_inv;
        $mat['sede'] = $key->cm_ubicacion;
        $id_mov = $key->mm_id;
        echo 'entro';
        $this->update_inventory($mat);
      }
      $this->model_mov_mat->delete_mov_per_cmm($id);
      $this->model_mov_mat->delete_relation_prov_per_cmm($id);
      $this->model_mov_mat->delete_cmm($id);
      echo "eliminado exitosamente";

      redirect('Movimiento/materia/input/index');

    }

    public function delete_output()
    {
      $id = $this->uri->segment(5);
      $mov = $this->model_mov_mat->read_mov_mat($id);
      foreach ($mov as $key)
      {
        $mat['id'] = $key->mm_id_mat;
        $mat['tipo'] = 1;
        $mat['cant'] = $key->mm_cant*$key->mat_inv;
        $mat['sede'] = $key->cm_ubicacion;
        $id_mov = $key->mm_id;
        echo 'entro';
        $this->update_inventory($mat);
      }
      $this->model_mov_mat->delete_mov_per_cmm($id);
      $this->model_mov_mat->delete_cmm($id);
      echo "eliminado exitosamente";

      redirect('Movimiento/materia/output/index');
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('id')==1) {
        //echo "Fordibben";
        redirect(base_url());
      }
    }
  }
 ?>
