<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Controller_import extends CI_Controller {

  function __construct()
  {
    parent::__construct ();
    $this->load->library('session');
    $this->load->model('model_cat_mat');
  }

  public function index()
  {
    //$this->very_session();
    $this->load->view('layout/header.php');
    $this->load->view('layout/sidebar.php');
    $this->load->view('import/index');
    $this->load->view('layout/footer.php');
  }

  function very_session()
  {
    if (!$this->session->userdata('logged')==TRUE) {
      header("Location:".base_url('Welcome/login'));
    }
  }

  public function excel()
	{
		$data =  $this->model_medidas->get();
    echo json_encode($data['fields']);
		//to_excel($data, "archivoexcel");
	}

   public function cat_mat()
   {
     if (!empty($_FILES['file']['name']))
     {
      $pathinfo = pathinfo($_FILES["file"]["name"]);
       if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls'|| $pathinfo['extension'] == 'ods')
           && $_FILES['file']['size'] > 0 ) {
        // Nombre Temporal del Archivo
        $inputFileName = $_FILES['file']['tmp_name'];
        //Lee el Archivo usando ReaderFactory
        $reader = ReaderFactory::create(Type::XLSX);
        //Esta linea mantiene el formato de nuestras horas y fechas
        //Sin esta linea Spout convierte la hora y fecha a su propio formato
        //predefinido como DataTime
        $reader->setShouldFormatDates(true);
        // Abrimos el archivo
        $reader->open($inputFileName);
        $count = 1;
        //Numero de Hojas en el Archivo
        foreach ($reader->getSheetIterator() as $sheet) {
            // Numero de filas en el documento EXCEL
            foreach ($sheet->getRowIterator() as $row) {
            // Lee los Datos despues del encabezado
            // El encabezado se encuentra en la primera fila
             if($count > 1) {
                $data = array(
                  'cat_mat_id' => $row[0],
                  'cat_mat_nombre' => $row[1],
                  'cat_mat_dsc' => $row[2]
               );
               //json_encode($data);
               $this->model_cat_mat->insert($data);
              }
                $count++;
             }
         }
         // cerramos el archivo EXCEL
          $reader->close();
          echo "<script>alert('Importo exitosamente'); history.back();</script>";
       } else {
        echo "Seleccione un tipo de Archivo Valido";
       }
     } else {
     echo "Seleccione un Archivo EXCEL";
      }
     }

     public function mat()
     {
       if (!empty($_FILES['file']['name']))
       {
        $pathinfo = pathinfo($_FILES["file"]["name"]);
         if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls'|| $pathinfo['extension'] == 'ods')
             && $_FILES['file']['size'] > 0 ) {
          // Nombre Temporal del Archivo
          $inputFileName = $_FILES['file']['tmp_name'];
          //Lee el Archivo usando ReaderFactory
          $reader = ReaderFactory::create(Type::XLSX);
          //Esta linea mantiene el formato de nuestras horas y fechas
          //Sin esta linea Spout convierte la hora y fecha a su propio formato
          //predefinido como DataTime
          $reader->setShouldFormatDates(true);
          // Abrimos el archivo
          $reader->open($inputFileName);
          $count = 1;
          //Numero de Hojas en el Archivo
          foreach ($reader->getSheetIterator() as $sheet) {
              // Numero de filas en el documento EXCEL
              foreach ($sheet->getRowIterator() as $row) {
              // Lee los Datos despues del encabezado
              // El encabezado se encuentra en la primera fila
               if($count > 1) {
                  $data = array(
                    'mat_id' => $row[0],
                    'mat_nombre' => $row[1],
                    'mat_cat' => $row[2],
                    'mat_barras' => $row[3],
                    'mat_descripcion' => $row[4],
                    'mat_um' => $row[5],
                    'mat_ue' => $row[6],
                    'mat_inv_max' => $row[7],
                    'mat_costo' => $row[8],
                    'mat_inv' => $row[9]
                 );
                 //json_encode($data);
                 $this->model_cat_mat->insert($data);
                }
                  $count++;
               }
           }
           // cerramos el archivo EXCEL
            $reader->close();
            echo "<script>alert('Importo exitosamente'); history.back();</script>";
         } else {
          echo "Seleccione un tipo de Archivo Valido";
         }
       } else {
       echo "Seleccione un Archivo EXCEL";
        }
       }

       public function prod()
       {
         if (!empty($_FILES['file']['name']))
         {
          $pathinfo = pathinfo($_FILES["file"]["name"]);
           if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls'|| $pathinfo['extension'] == 'ods')
               && $_FILES['file']['size'] > 0 ) {
            // Nombre Temporal del Archivo
            $inputFileName = $_FILES['file']['tmp_name'];
            //Lee el Archivo usando ReaderFactory
            $reader = ReaderFactory::create(Type::XLSX);
            //Esta linea mantiene el formato de nuestras horas y fechas
            //Sin esta linea Spout convierte la hora y fecha a su propio formato
            //predefinido como DataTime
            $reader->setShouldFormatDates(true);
            // Abrimos el archivo
            $reader->open($inputFileName);
            $count = 1;
            //Numero de Hojas en el Archivo
            foreach ($reader->getSheetIterator() as $sheet) {
                // Numero de filas en el documento EXCEL
                foreach ($sheet->getRowIterator() as $row) {
                // Lee los Datos despues del encabezado
                // El encabezado se encuentra en la primera fila
                 if($count > 1) {
                    $data = array(
                      'prod_id' => $row[0],
                      'prod_nombre' => $row[1],
                      'prod_precio' => $row[2],
                      'prod_costo' => $row[3],
                      'prod_cat' => $row[4],
                      'prod_barcode' => $row[5],
                      'prod_imp' => $row[6],
                      'prod_descripcion' => $row[7],
                      'prod_imagen' => $row[8],
                      'mat_inv' => $row[9]
                   );
                   //json_encode($data);
                   $this->model_cat_mat->insert($data);
                  }
                    $count++;
                 }
             }
             // cerramos el archivo EXCEL
              $reader->close();
              echo "<script>alert('Importo exitosamente'); history.back();</script>";
           } else {
            echo "Seleccione un tipo de Archivo Valido";
           }
         } else {
         echo "Seleccione un Archivo EXCEL";
          }
         }
}

?>
