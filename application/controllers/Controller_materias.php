<?php

  /**
   *
   */
  class Controller_materias extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_materias');
      $this->load->model('model_mov_mat');
      $this->load->model('model_cat_mat');
      $this->load->model('model_unidades');
      $this->load->model('Model_stock_mat');
      $this->load->library ('form_validation');
      $this->load->library('session');

    }

    // carga la fila de tabla para agregar las materias
    public function load_mat_table()
    {
      if ($this->input->post('safety')==true)
      {
        $catg = $this->input->post('categoria');
        $data = $this->model_materias->load_per_catg($catg);

        foreach($data as $datos) {

          echo "<tr>";
          echo "<td> $datos->mat_nombre <input type='hidden' id='nombre".$datos->mat_id."' value='".$datos->mat_nombre."' ></td>";
          echo '<td> <input type="number" id="cant'.$datos->mat_id.'" value="1" class="form-control"> </td>';
          echo '<td><div class="input-group"><span class="input-group-addon"><i class="fa fa-dollar"></i></span><input type="number" id="costo'.$datos->mat_id.'" class="form-control"> </div></td>';
          echo '<td> <button value="'.$datos->mat_id.'" type="button" class="btn btn-success addcar"><i class="fa fa-cart-plus"></i></button></td>';
          echo "</tr>";
        }
      }
      else {
        echo "data not found";
      }
    }

    // agrega carrito de pedido proveedor
    public function addcart_mat()
    {
      if ($this->input->post('safety')==true)
      {
        if (!$this->session->userdata('pedido_materia')==TRUE) {

          $carritop = array();

          $item =  array(
                      "producto" =>$this->input->post('producto'),
                      "nombre" =>$this->input->post('nombre'),
                      "cantidad" =>$this->input->post('cantidad'),
                      "costo"    =>$this->input->post('costo')
                    );

          array_push($carritop,$item);

          $dataseciom['pedido_materia'] = $carritop;
          $this->session->set_userdata($dataseciom);

          $this->load_cart_table();
        }
        else
        {
          $carritop = $this->session->userdata('pedido_materia');

          $item =  array(
                      "producto" =>$this->input->post('producto'),
                      "nombre" =>$this->input->post('nombre'),
                      "cantidad" =>$this->input->post('cantidad'),
                      "costo"    =>$this->input->post('costo')
                    );

          array_push($carritop,$item);
          $dataseciom['pedido_materia'] = $carritop;
          $this->session->set_userdata($dataseciom);

          $this->load_cart_table();
        }
      }
      else
      {
        echo "data not found";
      }
    }

    // carga el carrito de la tabla producto
    private function load_cart_table(){

      $i = 1;
      $carritop = $this->session->userdata('pedido_materia');

      foreach ($carritop as $value) {
        $posicion = $i-1;
        echo "<tr>";
        echo "<td> ".$i." </td>";
        echo "<td> ".$value['nombre']." </td>";
        echo "<td> ".$value['cantidad']." </td>";
        echo "<td> ".number_format($value['costo'])." </td>";
        echo '<td> <button type="button" class="btn btn-info deletet" value="'.$posicion.'"><i class="fa fa-trash"></i></button></td>';
        echo "<tr>";
        $i++;
      }
    }

    // eliminar posicion de carrito
    public function delete_array(){

      if ($this->input->post('safety')==true)
      {
        // obtiene parametro de posicion
        $item = $this->input->post('posicion');

        // obtiene carrito en el Session
        $carritop = $this->session->userdata('pedido_materia');
        // elimina una posicion de array en el carrito
        unset($carritop[$item]);
        $array = array_values($carritop);
        // declara variable
        $dataseciom['pedido_materia'] = $array;
        // y enviar session
        $this->session->set_userdata($dataseciom);
        // cargar carrito de table
        $this->load_cart_table();

      }
      else
      {
        echo "No lo hay.";
      }
    }

    function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->model_materias->display();
      $this->load->view('materias/vi_materias',$data);
      $this->load->view('layout/footer.php');
    }

    function form ()
    {
      $this->very_session();
      $data['categorias'] = $this->model_cat_mat->display();
      $data['unidades'] = $this->model_unidades->display();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/vc_materias',$data);
      $this->load->view('layout/footer.php');
    }

    function create ()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');
      $this->form_validation->set_rules('costo', 'Precio', 'required');
      $this->form_validation->set_rules('unidad_empaque', 'Unidad de empacado', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      else
      {
        $param ['mat_codigo'] = $this->input->post('id');
        $param ['mat_nombre'] = $this->input->post('nombre');
        $param ['mat_categoria'] = $this->input->post('categoria');
        $param ['mat_barras'] = $this->input->post('barcode');
        $param ['mat_descripcion'] = $this->input->post('descripcion');
        $param ['mat_um'] = $this->input->post('unidad_medida');
        $param ['mat_ue'] = $this->input->post('unidad_empaque');
        $param ['mat_inv_max'] = $this->input->post('inventario_deseado');
        $param ['mat_inv'] = $this->input->post('inventario');
        $param ['mat_tipo'] = $this->input->post('tipo');
        $param ['mat_un'] = $this->input->post('un');
        $param ['mat_costo'] = $this->input->post('costo');

        $id = $this->model_materias->insert($param);
        if ($param ['mat_tipo'] == 1) {
          redirect('/Materiasprimas/edit/'.$id );
        }
        else {
          redirect('/Materiasprimas/index/');
        }
      }
    }

      public function details ()
      {
        $this->very_session();
        $id = $this->uri->segment(3);
        $var = $this->Model_stock_mat->get($id);
        $data['datos'] = $this->model_materias->read($id);
        $data['stock'] = $this->Model_stock_mat->get($id);
        /// parametros movimientos materias
        $param['date2'] = date('Y-m-d');
        $date = date('Y-m-d');
        $param['date1'] =  date('Y-m-d',(strtotime ( '-7 day' , strtotime ( $date) ) ));
        $param['mat']=$id;
        $param['concepto']='2';
        $data['movimientos']=$this->model_mov_mat->prom_mov($param);
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');

        if ($var == FALSE) {
          $this->load->view('materias/vv2_materias',$data);
        }
        else {
          $this->load->view('materias/vv_materias',$data);
        }
        //$this->load->view('materias/vv_materias',$data);
        $this->load->view('layout/footer.php');
      }

    function get ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_materias->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/vd_materias',$data);
      $this->load->view('layout/footer.php');
    }

    function delete ()
    {
      $id = $this->uri->segment(3);
      $this->model_materias->delete($id);
      echo "<script type=text/javascript>alert('Se ha eliminado exitosamente la  materia');</script>";
      redirect('/Materiasprimas/index/');
    }

    function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_materias->read($id);
      $data['categorias'] = $this->model_cat_mat->display();
      $data['unidades'] = $this->model_unidades->display();
      $data['materias'] = $this->model_materias->display();
      $data['ingrediente'] = $this->model_materias->load_ing_comp_per_mat($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('materias/ve_materias',$data);
      $this->load->view('layout/footer.php');
    }

    function update()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->edit();
      }
      else {
        $param ['mat_id'] = $this->input->post('id');
        $param ['mat_codigo'] = $this->input->post('code');
        $param ['mat_nombre'] = $this->input->post('nombre');
        $param ['mat_categoria'] = $this->input->post('categoria');
        $param ['mat_barras'] = $this->input->post('barcode');
        $param ['mat_descripcion'] = $this->input->post('descripcion');
        $param ['mat_um'] = $this->input->post('unidad_medida');
        $param ['mat_ue'] = $this->input->post('unidad_empaque');
        $param ['mat_inv_max'] = $this->input->post('inventario_deseado');
        $param ['mat_inv'] = $this->input->post('inventario');
        $param ['mat_un'] = $this->input->post('un');
        $param ['mat_costo'] = $this->input->post('costo');
        $param ['mat_tipo'] = $this->input->post('tipo');

        $this->model_materias->update($param);

        redirect('/Materiasprimas/index/');
      }
    }

    public function promedio_consumo ()
    {

    }

    public function sava_ingrediente_mat()
    {
      if ($this->input->post('safety'))
      {
        $param['inc_padre'] = $this->input->post('id');
        $param['inc_ingrediente'] = $this->input->post('ingrediente');
        $param['inc_cantidad'] = $this->input->post('cantd')*0.001;
        $this->model_materias->insert_mat_ingrediente($param);

        $message['message'] = "Guardo exitosamente";
        $message['success'] = true;
        echo json_encode($message);
      }
      else
      {
        $message['message'] = "ErrorException";
        $message['success'] = false;
        echo json_encode($message);
      }
    }

    public function delete_ingcomp()
    {
      if ($this->input->post('delete')==true)
      {
        $id = $this->input->post('posicion');
        $this->model_materias->delete_ingcomp($id);
        $message['message'] = "Eliminado exitosamente";
        $message['success'] = true;
        echo json_encode($message);
      }
      else
      {
        $message['message'] = "ErrorException";
        $message['success'] = false;
        echo json_encode($message);
      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url());
      }
    }

  }


 ?>
