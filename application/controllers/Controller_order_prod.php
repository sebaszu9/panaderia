<?php

  /**
   *
   */
  class Controller_order_prod extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_panaderia');
      $this->load->model('model_recetas');
      $this->load->model('model_materias');
      $this->load->model('Model_orden_prod');
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->library('pdf');
    }

    public function index()
    {
      $this->very_session();
      $date = date('Y-m-d');
      $data['sede'] = $this->model_panaderia->load($date);
      $data['recetas'] = $this->model_recetas->display($date);
      $data['orden'] = $this->Model_orden_prod->load_ultim_10();

      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('orden/index_produccion',$data);
      $this->load->view('layout/footer.php');
    }

    public function form()
    {
      $this->very_session();
      $date = date('Y-m-d');
      $data['sede'] = $this->model_panaderia->load($date);
      $data['recetas'] = $this->model_recetas->display($date);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('orden/form_produccion',$data);
      $this->load->view('layout/footer.php');
    }

    public function search()
    {
      $this->very_session();
      $this->form_validation->set_rules('fecha',  'Fecha', 'required');
      if ($this->form_validation->run() == FALSE )
      {
        $this->index();
      }
      else
      {
        $date = date('Y-m-d');
        $data['sede'] = $this->model_panaderia->load($date);
        $data['recetas'] = $this->model_recetas->display($date);
        $data['orden'] = $this->Model_orden_prod->search();

        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('orden/index_produccion',$data);
        $this->load->view('layout/footer.php');

      }
    }

    public function create()
    {
      $this->very_session();
      $this->form_validation->set_rules('sede',   'Sede', 'required');
      $this->form_validation->set_rules('fecha',  'Fecha', 'required');
      //$this->form_validation->set_rules('receta', 'Receta', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      else
      {
        $param['orp_sede']   = $this->input->post('sede');
        $param['orp_fecha']  = $this->input->post('fecha');
        $param['orp_estado'] = 1;
        $param['orp_receta'] = 0;
        $id = $this->Model_orden_prod->insert($param);

        redirect('Orden/produccion/detalles/'.$id);

      }

    }

    public function delete()
    {
      if ($this->input->post('delete')==true)
      {
        $id    = $this->input->post('value');
        $this->Model_orden_prod->delete($id);
        $this->Model_orden_prod->del_prod_producto_ord($id);

        //redirect(base_url('Orden/produccion/index'));

        $response["message"] = "Elimino exitomanete. ";
        $response["success"] = true;
        echo json_encode($response);
      }
    }

    public function details()
    {
      if ($this->uri->segment(4))
      {

        $id = $this->uri->segment(4);
        $data['orden']    = $this->Model_orden_prod->read($id);
        $data['detalles'] = $this->Model_orden_prod->read_prod_producto($id);
        $data['recetas']  = $this->model_recetas->display();
        $data['detalles'] = $this->Model_orden_prod->read_prod_producto($id);

        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('orden/details_produccion',$data);
        $this->load->view('layout/footer.php');

      }
    }

    public function loadrecetaprod()
    {
      $id = $this->uri->segment(3);
      $producto = $this->model_recetas->display_prod($id);

      foreach($producto as $select){
        echo "<option value='".$select->prod_id."'> ".$select->prod_nombre."</option>";
      }
      //echo json_encode($producto);
    }


    // agregar producto de receta en el modulo de orden del produccion
    public function add_prod_producto()
    {
      if ($this->input->post('save')==true)
      {
        $tipo = $this->input->post('tipo');
        if ($tipo == 2)
        {
          $rec  = $this->input->post('receta');
          $cant = $this->input->post('cantidad');
          $prod = $this->input->post('producto');
          //cargo la lista de ingredientes
          $var = $this->model_recetas->display_ing($rec);
          $x = 1;
          //creo un ciclo para reccorrer los ingredientes
          //El contador x me sirve para que solo itere UNA vez
          foreach ($var as $m)
          {
            if ($x == 1)
            {
              $id_mat = $m->mat_id;
              //almaceno el valor de la escala del ingredienre en b
              $b = $m->unidad_escala;
              //almaceno la cantidad del ingrediente en a
              $a = $m->ing_cant;
            }
            ///agrego +1 para que el contador no vuelva a permitir ejecutar el ciclo
            $x = $x+1;
          }
          $c = ($b*$cant)/$a;

          $paramrec['receta']=$rec;
          $paramrec['producto']=$prod;
          $prodrec = $this->model_recetas->load_prod_per_prod2($paramrec);

          foreach ($prodrec as  $prodrecs)
          {
            $cantd_rect = $prodrecs->pr_cant;
          }

          //multiplico la cantidad de la receta por el factor de proporcion
          $cant = $cantd_rect*$c;

          $param['pp_producto'] = $this->input->post('producto');
          $param['pp_cant']     = $cant;
          $param['pp_orden']    = $this->input->post('orden');
          $param['pp_receta']    = $this->input->post('receta');
          $this->Model_orden_prod->insert_produccion_product($param);
        }
        elseif ($tipo == 1)
        {
          // code...
          $param['pp_producto'] = $this->input->post('producto');
          $param['pp_cant']     = $this->input->post('cantidad');
          $param['pp_orden']    = $this->input->post('orden');
          $param['pp_receta']    = $this->input->post('receta');
          $this->Model_orden_prod->insert_produccion_product($param);
        }

        $response["message"] = "Subió exitomanete. ";
        $response["success"] = true;
        echo json_encode($response);


      }
      else
      {
        $response["message"] = "No found post. ";
        $response["success"] = false;
        echo json_encode($response);
      }
    }

    public function update_status()
    {
      if ($this->input->post('update')==true)
      {
        // code...
        $param['pp_id']     = $this->input->post('value');
        $param['pp_estado'] = $this->input->post('status');
        $this->Model_orden_prod->update_prod_producto($param);

        $response["message"] = "Actualizado exitomanete. ";
        $response["success"] = true;
        echo json_encode($response);
      }
      else
      {
        $response["message"] = "No found post. ";
        $response["success"] = false;
        echo json_encode($response);
      }
    }

    public function add_prod_producto_backup()
    {
      if ($this->input->post('save')==true)
      {
        // code...
        $param['pp_producto'] = $this->input->post('producto');
        $param['pp_cant']     = $this->input->post('cantidad');
        $param['pp_orden']    = $this->input->post('orden');
        $this->Model_orden_prod->insert_produccion_product($param);

        $response["message"] = "Subió exitomanete. ";
        $response["success"] = true;
        echo json_encode($response);
      }
      else
      {
        $response["message"] = "No found post. ";
        $response["success"] = false;
        echo json_encode($response);
      }
    }

    public function del_prod_producto()
    {
      if ($this->input->post('delete')==true)
      {
        $id    = $this->input->post('value');
        $this->Model_orden_prod->del_prod_producto($id);

        $response["message"] = "Elimino exitomanete. ";
        $response["success"] = true;
        echo json_encode($response);
      }
    }

    public function pdf()
    {
      $id = $this->uri->segment(4);
      $receta = $this->Model_orden_prod->read_prod_producto_id($id);

      foreach($receta as $orded) {

        $pdf = new Pdf();
        $this->pdf->AddPage('P','Letter');

        $this->pdf->SetTitle("RECETA");
        $this->pdf->SetLeftMargin(4);
        $this->pdf->SetRightMargin(4);
        $this->pdf->SetFont('Arial', '', 18);
        $this->pdf->Cell(25,06,utf8_decode('Detalles de la Producción'),0,1);
        $this->pdf->Cell(25,06,'',0,1);

        $this->pdf->SetFont('Arial', '', 16);
        /*
        $this->pdf->Cell(5,06,'',0,0);
        $this->pdf->Cell(80,10,'Sede',1,0);
        $this->pdf->Cell(80,10,$orded->bod_nombre,1,1);
        $this->pdf->Cell(5,06,'',0,0);
        $this->pdf->Cell(80,10,'Fecha',1,0);
        $this->pdf->Cell(80,10,$orded->orp_fecha,1,1);
        $this->pdf->Cell(5,06,'',0,0);
        */
        $this->pdf->Cell(5,06,'',0,0);
        $this->pdf->Cell(80,10,'Receta',1,0);
        $this->pdf->Cell(80,10,utf8_decode($orded->rec_nombre),1,1);

        $this->pdf->Cell(5,06,'',0,1);
        $this->pdf->Cell(5,06,'',0,0);
        $this->pdf->Cell(25,06,utf8_decode('Productos de la Producción'),0,1);
        $this->pdf->Cell(5,06,'',0,1);

        $x = 0;


        $this->pdf->Cell(5,06,'',0,0);
        $this->pdf->Cell(100,10,utf8_decode($orded->prod_nombre),1,0);
        $this->pdf->Cell(60,10,$orded->pp_cant,1,1);

        $paramrec['receta']=$orded->pp_receta;
        $paramrec['producto']=$orded->prod_id;
        // datos para calcular recetas
        $prodrec = $this->model_recetas->load_prod_per_prod2($paramrec);
        $cantd_rect = 1;
        foreach ($prodrec as  $prodrecs)
        {
          //echo "string";
          $cantd_rect = $prodrecs->pr_cant;
        }
        $x = $x + ($orded->pp_cant/$cantd_rect);


        $ing = $this->model_recetas->display_ing($orded->pp_receta);

        $this->pdf->Cell(25,06,'',0,1);
        $this->pdf->Cell(5,06,'',0,0);
        $this->pdf->Cell(25,06,utf8_decode('Materias en la Producción'),0,1);

        $this->pdf->SetFont('Arial', '', 14);

        $this->pdf->Cell(5,06,'',0,1);
        $this->pdf->Cell(5,06,'',0,0);
        $this->pdf->Cell(120,10,'Nombre',1,0);
        $this->pdf->Cell(60,10,'Cantidad',1,1);

        foreach($ing as $ingredientes)
        {

          $catnida = "";
          if ($ingredientes->mat_un == "1"){ $catnida = $catnida."".number_format($x*$ingredientes->ing_cant/$ingredientes->unidad_escala/$ingredientes->mat_ue)."U -";}
          $catnida =  $catnida." ".number_format($x*$ingredientes->ing_cant/$ingredientes->unidad_escala)." ".$ingredientes->unidad_nombre."s";

          $this->pdf->Cell(5,06,'',0,0);
          $this->pdf->Cell(120,10,utf8_decode($ingredientes->mat_nombre),1,0);
          $this->pdf->Cell(60,10,$catnida,1,1);

        }

        //subingrediente


          $prodrec = $this->model_recetas->load_prod_per_prod($orded->prod_id);

          foreach($prodrec as $details) {

            if ($details->prod_id == $orded->prod_id) {

              $this->pdf->Cell(5,06,'',0,1);
              $this->pdf->Cell(5,06,'',0,0);
              $this->pdf->Cell(140,10,'Subingrediente: '.utf8_decode($orded->prod_nombre),1,1);
              //$this->pdf->Cell(60,10,'prueba',1,1);

              //$details->pr_detalles;

            }
          }

          $cantd_rect2 = 0;
          $prodrec = $this->model_recetas->load_prod_per_prod($orded->prod_id);

          foreach ($prodrec as  $prodrecs)
          {
            $cantd_rect2 = $prodrecs->pr_cant;
          }

          $x = ($orded->pp_cant/$cantd_rect2);

          $ingredientes_prod = $this->model_recetas->display_ing_prod($orded->rec_id);

          foreach($ingredientes_prod as $datos7) {

            if ($datos7->prod_id==$orded->prod_id){

              $chanitd = "";
              if ($datos7->mat_un == "1"){ $chanitd = $chanitd." ".$x*$datos7->ing_pr_cant/$datos7->unidad_escala/$datos7->mat_ue."U -"; }
              $chanitd = $chanitd." ".number_format($datos7->ing_pr_cant*$x/$datos7->unidad_escala)." ".$datos7->unidad_nombre."s";

              $this->pdf->Cell(5,06,'',0,0);
              $this->pdf->Cell(120,10,utf8_decode($datos7->cr_nombre." ".$datos7->mat_nombre),1,0);
              $this->pdf->Cell(60,10,$chanitd,1,1);

            }
          }


        $this->pdf->output('Producion.pdf', 'I');
      }


    }


    function very_session()
  	{
  		//echo $this->session->userdata('usuario');
  		if (!$this->session->userdata('usuario')==1) {
  			//echo "Fordibben";
        redirect(base_url());
  		}
  	}

    public function upd_prod_producto()
    {
      if ($this->input->post('update')==true)
      {
        $param['pp_id']     = $this->input->post('value');
        $param['pp_estado'] = $this->input->post('state');
        $this->Model_orden_prod->update_prod_producto($param);

        $response["message"] = "Actualizado exitomanete. ";
        $response["success"] = true;
        echo json_encode($response);
      }
      else {
        $response["message"] = "No hay datos. ";
        $response["success"] = false;
        echo json_encode($response);
      }
    }

  }

?>
