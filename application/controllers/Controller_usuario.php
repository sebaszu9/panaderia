    <?php

  /**
   *
   */
  class Controller_usuario extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->library('session');
      $this->load->model('model_usuario');
      $this->load->model('model_panaderia');
      $this->load->library('form_validation');
      $this->load->library('encrypt');
      $this->load->library('session');
    }

    public function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->model_usuario->load();
      $this->load->view('usuario/vi_usuario',$data);
      $this->load->view('layout/footer.php');
    }

    public function form()
    {
      $this->very_session();
      $data['info'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('usuario/vc_usuario',$data);
      $this->load->view('layout/footer.php');
    }

    public function create()
    {
      if($this->input->post('create')==true)
      {
        $this->form_validation->set_rules('nombre', 'Nombre del Usuario', 'required');
        $this->form_validation->set_rules('email', 'Correo electronico', 'required|callback_valid_email');
        $this->form_validation->set_rules('key', 'Contraseña', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('key2', 'Confirmacion de Contraseña', 'matches[key]');

        $this->form_validation->set_message('valid_email','<div class="alert alert-danger"> El %s Ya está Registrado !</div>');

        if ($this->form_validation->run() == FALSE)
        {
          $data['info'] = $this->model_panaderia->load();
          $this->load->view('layout/header.php');
          $this->load->view('layout/sidebar.php');
          $this->load->view('usuario/vc_usuario',$data);
          $this->load->view('layout/footer.php');
        }
        else
        {
          $param ['nombre']   = $this->input->post('nombre');
          $param ['correo']   = $this->input->post('email');
          $param ['tipo']     = $this->input->post('tipo');
          $param ['sede']     = $this->input->post('panaderia');
          $param ['password'] = sha1($this->input->post('key'));

          $res =  $this->model_usuario->insert($param);
          $this->session->set_flashdata('mensaje','Guardo exitosamente. ');
          redirect(base_url('Controller_usuario/form'));
        }
      }else{
        echo "Safety BlackTurpial error";
      }
    }

    function valid_email($correo)
  	{
      $param['correo'] = $correo;
  		$resd = $this->model_usuario->valid_email($param);
      if ($resd==false)
      {
        return true;
      }
      else{
        return false;
      }
  	}
    public function delete()
    {
      if($this->input->post('safety')==true)
      {
        $code = $this->input->post('codigo');
        //$code = 32;
        $this->model_usuario->delete($code);
        //echo "Eliminado";
        $response['success'] = true;
        $response['message'] = "Eliminado exitosamente!";
        echo json_encode($response);
      }
      else{
        $response['success'] = false;
        $response['message'] = "Error! 404";
        echo json_encode($response);
      }
    }

    public function change_state()
    {
      if($this->input->post('safety')==true)
      {
        $param ['user']  = $this->input->post('user');;
        $param ['state'] = $this->input->post('status');
        $this->model_usuario->change_state($param);
        $response['success'] = true;
        $response['message'] = "actualizado exitosamente!";
        echo json_encode($response);
      }
      else{
        $response['success'] = false;
        $response['message'] = "Error! 404";
        echo json_encode($response);
      }
    }

    public function edit()
    {

      if ($this->uri->segment(3)==null)
      {
        echo "Safety BlackTurpial";
      }
      else
      {
        $this->very_session();
        $param = $this->uri->segment(3);
        $data['info'] =  $this->model_usuario->read($param);
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('usuario/ve_usuario',$data);
        $this->load->view('layout/footer.php');
      }
    }

    public function update()
    {
      if ($this->input->post('code'))
      {
        $param ['nombre']  = $this->input->post('nombre');
        $param ['correo']  = $this->input->post('email');
        $param ['tipo']    = $this->input->post('tipo');
        $param ['code']    = $this->input->post('code');

        $res =  $this->model_usuario->update($param);
        $this->session->set_flashdata('mensaje','Actualizo exitosamente. ');
        redirect(base_url('Controller_usuario/edit/'.$param ['code']));
      }
      else {
        echo "Safety BlackTurpial";
      }
    }

    public function update_password()
    {
      if ($this->input->post('code')) {
        $param ['key']    = sha1($this->input->post('key'));
        $param ['key2']    = sha1($this->input->post('key2'));
        $param ['code']    = $this->input->post('code');

        if ($param ['key']==null||$param ['key2']==null) {
          $response['success'] = false;
          $response['message'] = "Error! Debe ingresar Contraseña.";
          echo json_encode($response);
        }
        elseif ($param ['key']==$param ['key2'] ) {
          $this->model_usuario->update_password($param);
          $response['success'] = true;
          $response['message'] = "Actualizado exitosamente!";
          echo json_encode($response);
        }
        else {
          $response['success'] = false;
          $response['message'] = "Error! Debe ingresar 2 Contraseñas correctamente.";
          echo json_encode($response);
        }
        /*
        $res =  $this->model_usuario->update($param);
        $this->session->set_flashdata('mensaje','Actualizo exitosamente. ');
        redirect(base_url('Controller_usuario/edit/'.$param ['code']));
        */
      }
      else
      {
        echo "Safety BlackTurpial";
      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url());
      }
    }



}
  ?>
