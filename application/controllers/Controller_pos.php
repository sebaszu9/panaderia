<?php

  /**
   *
   */
  class Controller_pos extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->library('session');
      $this->load->model('model_productos');
      $this->load->model('model_usuario');
      $this->load->model('model_cat_prod');
      $this->load->model('model_cliente');
      $this->load->model('model_ventas');
      $this->load->model('Model_mesas');
      $this->load->library('pdf');
      //$this->load->library('form_validation');
      header('Access-Control-Allow-Origin: *');
    }

    // viejo---------------------------------->
    public function index()
    {
      $this->very_session();
      //$data['cliente'] = $this->model_cliente->load();
      //$data['vendedor'] = $this->model_usuario->read_per_type(4);
      //$data['categoria'] = $this->model_cat_prod->display();
      //$this->load->view('layout/header.php');
      //$this->load->view('layout/sidebar.php',$data);
      $this->load->view('pos/pos-react');
      //$this->load->view('layout/footer.php');
    }


    public function mesas()
    {
      $this->very_session();
      $data['zonas'] = $this->Model_mesas->display_zonas();
      //$data['vendedor'] = $this->model_usuario->read_per_type(4);
      //$data['categoria'] = $this->model_cat_prod->display();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('pos/pos_mesas',$data);
      $this->load->view('layout/footer.php');
    }

    public function show_mesas()
    {
      $id = $this->uri->segment(3);
      $data = $this->Model_mesas->display_mesas_by_zona($id);
      echo json_encode($data);
    }

    public function show_cuenta_mesa()
    {
      $id = $this->uri->segment(3);
      $data = $this->Model_mesas->display_cuenta_mesa($id);
      echo json_encode($data);
    }

    public function show_cuenta()
    {
      $id = $this->uri->segment(3);
      $data = $this->Model_mesas->display_cuenta($id);
      echo json_encode($data);
    }

    public function crear_cuenta()
    {
      $id = $this->uri->segment(3);
      $data = array(
             'cuenta_id' => $id,
             'cuenta_personas' => $this->input->post('personas'),
             'cuenta_activo' => 1,
         );
      $data = $this->Model_mesas->update_cuenta($data);
      echo json_encode(array("status" => TRUE));
    }

    public function index_sus()
    {
      $this->very_session();
      $data['cliente'] = $this->model_cliente->load();
      $data['vendedor'] = $this->model_usuario->read_per_type(4);
      $data['categoria'] = $this->model_cat_prod->display();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php',$data);
      $this->load->view('pos/index',$data);
      $this->load->view('layout/footer.php');
    }

    public function beta()
    {
      $this->very_session();
      $data['cliente'] = $this->model_cliente->load();
      $data['vendedor'] = $this->model_usuario->read_per_type(4);
      $data['categoria'] = $this->model_cat_prod->display();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php',$data);
      $this->load->view('pos/index2',$data);
      $this->load->view('layout/footer.php');
    }


    public function register_customer()
    {
      if ($this->input->post('register'))
      {

        if ($this->input->post('nombre')==null) {
          $response['success']=false;
          $response['message']="Por favor digite el nombre.";
          echo json_encode($response);
        }
        elseif ($this->input->post('cedula')==null) {
          $response['success']=false;
          $response['message']="Por favor digite la cedula.";
          echo json_encode($response);
        }
        else
        {
          //$param['clt_id'] = '1000';
          $param['clt_nombre']    = $this->input->post('nombre');
          $param['clt_cedula']    = $this->input->post('cedula');
          $param['clt_direccion'] = $this->input->post('direccion');
          $param['clt_telefono']  = $this->input->post('telefono');

          $this->model_cliente->insert($param);
          $response['success']=true;
          $response['message']="Registrado exitosamente.";
          echo json_encode($response);
        }

      }
      else{
        $response['success']=false;
        $response['message']="Error... not found data.";
        echo json_encode($response);
      }
    }

    public function search_product()
    {

      if ($this->input->post('safety'))
      {

        $data = $this->input->post('data');
        $query = $this->model_productos->search($data);

        if ($query==FALSE) {
          echo '<h3>No existen productos.</h3>';
        }
        else
        {

          echo '<ul class="products-list product-list-in-box">';

          foreach ($query->result() as $value) {

            $colom = '<li class="item">';
            $colom = $colom.'<div class="product-img">';
            $colom = $colom.'<img src="'.base_url('image/bread.png').'" alt="Product Image">';
            $colom = $colom.'</div>';
            $colom = $colom.'<div class="product-info">';
            $colom = $colom.'<b style="color:orange;">'.$value->cat_prod_nombre.'</b> '.$value->prod_nombre.' ';
            //$colom = $colom.'<div class="pull-right"><span class="label label-warning " style="font-size:15px">$'.$value->prod_precio.'</span> ';
            //$colom = $colom.'<div class="input-group pull-right col-md-4">';
            //$colom = $colom.'<span class="input-group-addon"><i class="fa fa-dollar"></i> '.$value->prod_precio.'</span> ';
            $colom = $colom.'<div class="pull-right"><button class="btn btn-info sendcar" style="font-size:20px;font-weight:bold;" data-prod="'.$value->prod_id.'" data-imp="'.$value->imp_porcentaje.'"  data-cats="'.$value->cat_prod_nombre.'" data-precio="'.$value->prod_precio.'" data-name="'.$value->prod_nombre.'" data-image="'.$value->prod_imagen.'" type="button"><i class="fa fa-dollar"></i> '.$value->prod_precio.' <i class="fa fa-cart-plus"></i> </button></div>';
            //$colom = $colom.'</div>';
            //$colom = $colom.'<button type="button" class="btn btn-success sendcar" data-prod="'.$value->prod_id.'" data-cats="'.$value->cat_prod_nombre.'" data-precio="'.$value->prod_precio.'" data-name="'.$value->prod_nombre.'" data-image="'.$value->prod_imagen.'" > <i class="fa fa-cart-plus"></i> </button></div>';
            $colom = $colom.'<span class="product-description">';
            $colom = $colom.' <b>Descripción:</b> '.$value->prod_descripcion.' ';
            $colom = $colom.'</span>';
            $colom = $colom.'</div>';
            $colom = $colom.'</li>';

            echo $colom;
          }
          echo "</ul>";
        }
      }
      else
      {
        echo "Not found Search";
      }
    }

    public function search_product_basura()
    {

      if ($this->input->post('safety'))
      {

        $data = $this->input->post('data');
        $query = $this->model_productos->search($data);

        if ($query==FALSE) {
          echo '<h3>No existen productos.</h3>';
        }
        else
        {

          echo '<ul class="products-list product-list-in-box">';

          foreach ($query->result() as $value) {

            $colom = '<li class="item">';
            $colom = $colom.'<div class="product-img">';
            $colom = $colom.'<img src="'.base_url('image/bread.png').'" alt="Product Image">';
            $colom = $colom.'</div>';
            $colom = $colom.'<div class="product-info">';
            $colom = $colom.'<b style="color:orange;">'.$value->cat_prod_nombre.'</b> '.$value->prod_nombre.' ';
            //$colom = $colom.'<div class="pull-right"><span class="label label-warning " style="font-size:15px">$'.$value->prod_precio.'</span> ';
            $colom = $colom.'<div class="input-group pull-right col-md-4">';
            $colom = $colom.'<span class="input-group-addon"><i class="fa fa-dollar"></i></span> ';
            $colom = $colom.'<input type="text" value='.$value->prod_precio.' id="price'.$value->prod_id.'"  class="form-control toggle-bloq pressadd" data-prod="'.$value->prod_id.'" data-imp="'.$value->imp_porcentaje.'" data-name="'.$value->prod_nombre.'" readonly> ';
            $colom = $colom.'<span class="input-group-btn"><button class="btn btn-success sendcar" data-prod="'.$value->prod_id.'" data-imp="'.$value->imp_porcentaje.'"  data-cats="'.$value->cat_prod_nombre.'" data-precio="'.$value->prod_precio.'" data-name="'.$value->prod_nombre.'" data-image="'.$value->prod_imagen.'" type="button"><i class="fa fa-cart-plus"></i></button> </span>';
            $colom = $colom.'</div>';
            //$colom = $colom.'<button type="button" class="btn btn-success sendcar" data-prod="'.$value->prod_id.'" data-cats="'.$value->cat_prod_nombre.'" data-precio="'.$value->prod_precio.'" data-name="'.$value->prod_nombre.'" data-image="'.$value->prod_imagen.'" > <i class="fa fa-cart-plus"></i> </button></div>';
            $colom = $colom.'<span class="product-description">';
            $colom = $colom.' <b>Descripción:</b> '.$value->prod_descripcion.' ';
            $colom = $colom.'</span>';
            $colom = $colom.'</div>';
            $colom = $colom.'</li>';

            echo $colom;
          }
          echo "</ul>";
        }
      }
      else
      {
        echo "Not found Search";
      }
    }

    public function create()
    {
      $response = array();
      if ($this->input->post('pos')==TRUE)
      {

        $carrito = $this->input->post('carrito');

        $carrito2 = json_decode($this->input->post('carrito'));
        $total = 0;
        $impuestos = 0;
        $descuento = 0;

        foreach ($carrito2 as  $car)
        {
          $subtotalx = ($car->cant*$car->price)-$car->desc;
          $total = $total+$subtotalx;
          $descuento = $descuento +$car->desc;
          $impuestos = $impuestos + ($subtotalx*($car->impt/100));
          //echo " item de carrito $car->name <br>";
        }
        $subtotla = $total-$impuestos;

        $param['pvi_cliente'] = $this->input->post('cliente');
        $param['pvi_fecha']   = date("Y-m-d");
        $param['pvi_hora']    = date("g:i A");
        $param['pvi_sede']      = $this->session->userdata('sede');
        $param['pvi_usuario']   = $this->session->userdata('iduser');
        $param['pvi_subtotal']  = $subtotla;
        $param['pvi_impuestos'] = $impuestos;
        $param['pvi_descuento'] = $descuento;
        $param['pvi_total']     = $total;
        $param['pvi_delete']    = 0;

        $idventa = $this->model_ventas->insert($param);

        foreach ($carrito2 as  $car)
        {
          $paramd['pvd_subt']  = ($car->cant*$car->price)-$car->desc;
          $paramd['pvd_cant']  = $car->cant;
          $paramd['pvd_desc']  = $car->desc;
          $paramd['pvd_prod']  = $car->prod;
          $paramd['pvd_venta'] = $idventa;

          $this->model_ventas->insert_details($paramd);
        }

        $response['message'] = "<center><button type='button' class='btn btn-info clean-pos' > Nuevo </button> <br> <h3><b>Ventas registrada </b> <br> <a href='".base_url('Pos/factura/'.$idventa)."' target='_blank'> <span class='label label-success' style='font-size:15px'> Imprimir factura #$idventa</a></h3></center>";
        $response['success'] = true;

        echo json_encode($response);

        //echo "<tr><td colspan='6' style='text-align:center;'><h1><a href='".base_url('Pos/index/')."'> <span class='label label-info' > Nueva venta.</a></h1></td></tr>";
      }
      else{
        echo "Noooo";
      }
    }


    function factura()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $datos = $this->model_ventas->read($id);
      $pedidos = $this->model_ventas->read_details($id);
      //$almacen = $this->model_almacen->load();
      ///CALCULO DE LA ALTURA DEL TICKET
      $contador = 110;

      $height = $contador;
      $pdf = new Pdf();
      $this->pdf->AddPage('P',array(55,$height));
      //$this->pdf->AddPage();
      $this->pdf->SetTitle("TICKET");
      $this->pdf->SetLeftMargin(4);
      $this->pdf->SetRightMargin(4);
      $this->pdf->SetFont('Arial', '', 8);


      foreach ($datos as $orden) {
        $this->pdf->Cell(0,04,'NIT.0000000000 ',0,1,'C');
        $this->pdf->Cell(0,04,'PANADERIA Y PASTELERIA ',0,1,'C');

      }

      foreach ($datos as $orden) {
        //echo $sede;
        $date1 =  date('d/m/Y', strtotime($orden->pvi_fecha));
        // Se imprimen los datos de cada alumno
        $this->pdf->Cell(0,04,'Factura #'.$orden->pvi_id,0,1,'C');
        $this->pdf->SetFont('Arial', '', 6);
        $this->pdf->Cell(0,04,'Fecha: '.$date1.' - Hora: '.$orden->pvi_hora,0,1,'C');
        //$this->pdf->SetFont('Arial', '', 8);
        $this->pdf->Cell(0,02,'Cliente: '.utf8_decode($orden->clt_nombre),0,1);
        //Direccion si hay domicilio

        //$this->pdf->Cell(0,01,' ',0,1);
        //$this->pdf->Cell(0,01,'===========================',0,1);
      }

      $this->pdf->SetFont('Arial', '', 8);
      $this->pdf->Cell(0,03,'-----------------------------------------------',0,1);

      $this->pdf->Cell(0,01,utf8_decode('INFORMACIÓN DE PRODUCTOS'),0,1);
      $this->pdf->Cell(0,02,'-----------------------------------------------',0,1);

      //$this->pdf->Cell(25,05,'Descripcion',0,0);
      //$this->pdf->Cell(10,05,'Cant.',0,0);
      //$this->pdf->Cell(10,05,'Desc.',0,0);
      //$this->pdf->Cell(11,05,'Subtotal',0,1);
      //$this->pdf->Cell(0,01,'-----------------------------------------------',0,1);

      foreach ($pedidos as $query)
      {
        // productos
        $this->pdf->SetFont('Arial', '', 6);
        //$this->pdf->Cell(0,02,utf8_decode($query->cat_nombre),0,1);
        $this->pdf->Cell(1,03,'',0,0);
        $this->pdf->Cell(5,03,$query->pvd_cant." x ",0,0, 'R');
        //$this->pdf->SetFont('Arial', '', 6);
        $this->pdf->Cell(30,03,utf8_decode($query->prod_nombre),0,0);

        //$this->pdf->Cell(10,03,$query->pvd_desc,0,0);
        $totalvar = ($query->pvd_subt+$query->pvd_desc)/$query->pvd_cant;
        $this->pdf->Cell(11,03,"$".number_format($totalvar),0,1);


        //$this->pdf->SetFont('Arial', '', 7);
        //$this->pdf->Cell(32,02,'Subtotal ',0,0,'R');
        //$this->pdf->Cell(13,02,' $ '.$query->pvd_subt,0,1);
        //$this->pdf->Cell(0,01,'',0,1);
        //$this->pdf->Cell(0,01,'------------------------------------------------------',0,1);
      }

      foreach ($datos as $orden) {

        $this->pdf->Cell(0,01,'---------------------------------------------------------------',0,1);

        $this->pdf->Cell(32,03,'Subtotal ',0,0);
        $this->pdf->Cell(13,03,'$ '.number_format($orden->pvi_subtotal),0,1);

        $this->pdf->Cell(32,03,'Descuento ',0,0);
        $this->pdf->Cell(13,03,'$ '.number_format($orden->pvi_descuento),0,1);

        $this->pdf->Cell(32,03,'Impuesto ',0,0);
        $this->pdf->Cell(13,03,'$ '.number_format($orden->pvi_impuestos),0,1);


        $this->pdf->SetFont('Arial','', 8);
        $this->pdf->Cell(32,03,'Total',0,0);
        $this->pdf->Cell(13,03,'$ '.number_format($orden->pvi_total),0,1);
        //$this->pdf->Cell(0,01,'------------------------------------------------------',0,1);
        $this->pdf->Cell(0,01,'------------------------------------------------',0,1);
        //$this->pdf->Cell(0,04,'Forma de Pago: '.$orden->mp_metodo,0,1);
        //$this->pdf->Cell(0,04,'Atendido Por: '.$this->session->userdata('usuario'),0,1);
      }
      $this->pdf->output("factura.pdf", 'I');
    }

    function factura_backup()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $datos = $this->model_ventas->read($id);
      $pedidos = $this->model_ventas->read_details($id);
      //$almacen = $this->model_almacen->load();
      ///CALCULO DE LA ALTURA DEL TICKET
      $contador = 110;

      $height = $contador;
      $pdf = new Pdf();
      $this->pdf->AddPage('P',array(55,$height));
      //$this->pdf->AddPage();
      $this->pdf->SetTitle("TICKET");
      $this->pdf->SetLeftMargin(4);
      $this->pdf->SetRightMargin(4);
      $this->pdf->SetFont('Arial', '', 8);


      foreach ($datos as $orden) {
        //echo $sede;
        $date1 =  date('d/m/Y', strtotime($orden->pvi_fecha));
        // Se imprimen los datos de cada alumno
        $this->pdf->Cell(0,04,'Pedido No. #'.$orden->pvi_id,0,1,'C');
        $this->pdf->Cell(0,04,'Fecha: '.$date1.' - Hora: '.$orden->pvi_hora,0,1);
        $this->pdf->Cell(0,04,'Cliente: '.utf8_decode($orden->clt_nombre),0,1);

        //Direccion si hay domicilio

        $this->pdf->Cell(0,01,' ',0,1);
        //$this->pdf->Cell(0,01,'===========================',0,1);
      }


      $this->pdf->Cell(0,03,'-----------------------------------------------',0,1);
      $this->pdf->SetFont('Arial', '', 8);
      $this->pdf->Cell(0,01,utf8_decode('INFORMACIÓN DE PRODUCTOS'),0,1);
      $this->pdf->Cell(0,02,'-----------------------------------------------',0,1);

      $this->pdf->Cell(25,05,'Descripcion',0,0);
      $this->pdf->Cell(10,05,'Cant.',0,0);
      //$this->pdf->Cell(10,05,'Desc.',0,0);
      $this->pdf->Cell(11,05,'Subtotal',0,1);
      $this->pdf->Cell(0,01,'-----------------------------------------------',0,1);

      foreach ($pedidos as $query)
      {
        // productos
        $this->pdf->SetFont('Arial', '', 5);
        //$this->pdf->Cell(0,02,utf8_decode($query->cat_nombre),0,1);
        $this->pdf->SetFont('Arial', '', 6);
        $this->pdf->Cell(29,03,utf8_decode($query->prod_nombre),0,0);
        $this->pdf->Cell(6,03,$query->pvd_cant,0,0);
        //$this->pdf->Cell(10,03,$query->pvd_desc,0,0);
        $this->pdf->Cell(11,03,($query->pvd_subt+$query->pvd_desc)/$query->pvd_cant,0,1);


        $this->pdf->SetFont('Arial', '', 7);
        $this->pdf->Cell(32,02,'Subtotal ',0,0,'R');
        $this->pdf->Cell(13,02,' $ '.$query->pvd_subt,0,1);
        $this->pdf->Cell(0,01,'',0,1);
        $this->pdf->Cell(0,01,'------------------------------------------------------',0,1);
      }

      foreach ($datos as $orden) {

        $this->pdf->Cell(32,03,'Subtotal: ',0,0,'R');
        $this->pdf->Cell(13,03,'$ '.$orden->pvi_subtotal,0,1);

        $this->pdf->Cell(32,03,'Descuento: ',0,0,'R');
        $this->pdf->Cell(13,03,'$ '.$orden->pvi_descuento,0,1);

        $this->pdf->Cell(32,03,'Impuestos: ',0,0,'R');
        $this->pdf->Cell(13,03,'$ '.$orden->pvi_impuestos,0,1);

        $this->pdf->Cell(32,03,'Total Final: ',0,0,'R');
        $this->pdf->Cell(13,03,'$ '.$orden->pvi_total,0,1);
        $this->pdf->Cell(0,01,'------------------------------------------------------',0,1);

        //$this->pdf->Cell(0,04,'Forma de Pago: '.$orden->mp_metodo,0,1);
        $this->pdf->Cell(0,04,'Atendido Por: '.$this->session->userdata('usuario'),0,1);
       }
      $this->pdf->output("factura.pdf", 'I');
    }

    function very_session()
  	{
  		if (!$this->session->userdata('logged')==TRUE) {
  			header("Location:".base_url('Welcome/login'));
  		}
  	}

    public function json_detail_venta()
    {
      if ($this->input->post('call')==true) {
        $id = $this->uri->segment(3);
        $data['datos'] = $this->model_ventas->read($id);
        $data['details'] = $this->model_ventas->read_details($id);
        $data['success'] = true;
        echo json_encode($data);
      }
      else{
        $data['datos'] = [];
        $data['details'] =[];
        $data['success'] = false;
        echo json_encode($data);
      }

    }

    public function json_detail_ventax()
    {
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_ventas->read($id);
      $data['details'] = $this->model_ventas->read_details($id);
      echo json_encode($data);
    }



  }
 ?>
