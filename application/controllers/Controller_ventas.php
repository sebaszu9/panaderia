<?php

  /**
   *
   */
  class Controller_ventas extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->library('session');
      $this->load->model('model_productos');
      $this->load->model('model_usuario');
      $this->load->model('model_ventas');
      header('Access-Control-Allow-Origin: *');
    }

    public function createx()
    {
      echo "envio exitosamente";
    }

    public function index()
    {
      $this->very_session();
      $data['datos'] = $this->model_ventas->load_per_day();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('ventas/vi_ventas',$data);
      $this->load->view('layout/footer.php');
    }

    public function details()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_ventas->read($id);
      $data['details'] = $this->model_ventas->read_details($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('ventas/vi_details',$data);
      $this->load->view('layout/footer.php');
    }

    public function delete_ventas()
    {
      if ($this->input->post('delete')==true)
      {
        $id = $this->input->post('posicion');
        $this->model_ventas->delete($id);
        $response['message'] = "elimino exitosamente";
        $response['success'] = true;
        echo json_encode($response);
      }
      else
      {
        $response['message'] = "Safety BlackTurpial";
        $response['success'] = false;
        echo json_encode($response);
      }
    }

    public function delete_item()
    {
      if ($this->input->post('delete')==true)
      {
        $id = $this->input->post('posicion');
        $this->model_ventas->delete_details($id);

        //falta actualizar de cantidad de productos
        $response['message'] = "elimino exitosamente";
        $response['success'] = true;
        echo json_encode($response);
      }
      else
      {
        $response['message'] = "Safety BlackTurpial";
        $response['success'] = false;
        echo json_encode($response);
      }
    }

    //vasura
    public function create()
    {
      if ($this->session->userdata('carrito'))
      {
        $carritop = $this->session->userdata('carrito');
        $carritototal = 0;
        // sumar total de todos los carritos_jso
        foreach ($carritop as $value)
        {
          $carritototal = $carritototal + ($value['cantidad']*$value['valor']);
        }

        // datos de pedido
        $param ['vent_cliente']  = $this->input->post('cliente');
        $param ['vent_fecha']    = date("Y-m-d");
        $param ['vent_total']    = $carritototal;
        $param ['vent_hora']     = date("g:i A");
        $param ['vent_sede']     = $this->session->userdata('sede');
        $param ['vent_vendedor'] = $this->input->post('vendedor');
        // inserta pedidos
        $idpedido = $this->model_ventas->insert($param);
        // obtiene variable de pedidos
        //$idpedido = $this->model_pedidos->get_order_latest($param['cliente']);
        $i = 1;

        foreach ($carritop as $value)
        {
          $paramd ['dtv_venta']   = $idpedido;
          $paramd ['dtv_producto'] = $value['producto'];
          $paramd ['dtv_unitario'] = $value['valor'];
          $paramd ['dtv_cantidad'] = $value['cantidad'];
          $paramd ['dtv_impuesto'] = 0;
          $paramd ['dtv_subtotal']    = $value['cantidad']*$value['valor'];
          $this->model_ventas->insert_details($paramd);
          $i++;
        }// cierre de ciclo carrito

        echo "<tr><td colspan='6' style='text-align:center;'><h1><b>Ventas registrada ... </b> Imprimir factura <a href='".base_url('Pedidos/pdf/'.$idpedido)."' target='_blank'> <span class='label label-success' style='font-size:15px'> #$idpedido</a></h1></td></tr>";
        echo "<tr><td colspan='6' style='text-align:center;'><h1><a href='".base_url('Pos/index/')."'> <span class='label label-info' > Nueva venta.</a></h1></td></tr>";
        unset($_SESSION["carrito"]);
      }
      else
      {
        echo "<tr><td colspan='6'>Error, No hay carrito de compras. 404 </td></tr>";
        echo "<tr><td colspan='6' style='text-align:center;'><h3><a href='".base_url('Pos/index/')."'> <span class='label label-info' > Nueva venta.</a></h3></td></tr>";
      }
    }

    function very_session()
    {
      if (!$this->session->userdata('logged')==TRUE) {
        redirect(base_url('Welcome/login'));
      }
    }

  }

?>
