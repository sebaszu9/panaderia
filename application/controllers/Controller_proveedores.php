<?php

  /**
   *
   */
  class Controller_proveedores extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_proveedores');
      $this->load->library ('form_validation');
      $this->load->library('session');
    }

    function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['datos'] = $this->model_proveedores->display();
      $this->load->view('proveedores/vi_proveedores',$data);
      $this->load->view('layout/footer.php');
    }


    function form ()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('proveedores/vc_proveedores');
      $this->load->view('layout/footer.php');
    }

    function create ()  
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');
      $this->form_validation->set_rules('nit', 'Nit/Cedula', 'required');
      $this->form_validation->set_rules('direccion', 'direccion', 'required');


      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      else {

        $param ['prov_nombre'] = $this->input->post('nombre');
        $param ['prov_nit'] = $this->input->post('nit');
        $param ['prov_dir'] = $this->input->post('direccion');
        $param ['prov_tel1'] = $this->input->post('telefono1');
        $param ['prov_tel2'] = $this->input->post('telefono2');
        $param ['prov_fax'] = $this->input->post('fax');
        $param ['prov_email'] = $this->input->post('email');
        $param ['prov_observaciones'] = $this->input->post('descripcion');
        $param ['prov_cel'] = $this->input->post('celular');
        $param ['prov_cuenta_banco1'] = $this->input->post('banco1');
        $param ['prov_cuenta_num1'] = $this->input->post('cuenta1');
        $param ['prov_cuenta_tipo1'] = $this->input->post('tipo1');
        $param ['prov_cuenta_banco2'] = $this->input->post('banco2');
        $param ['prov_cuenta_num2'] = $this->input->post('cuenta2');
        $param ['prov_cuenta_tipo2'] = $this->input->post('tipo2');
        $param ['prov_cont_nombre'] = $this->input->post('contacto');
        $param ['prov_cont_email'] = $this->input->post('celcontacto');
        $param ['prov_cont_celular'] = $this->input->post('emailcontacto');

        $this->model_proveedores->insert($param);

        $this->index();
        }
    }

    function get ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_proveedores->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('proveedores/vd_proveedores',$data);
      $this->load->view('layout/footer.php');
    }

    function view ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_proveedores->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('proveedores/vv_proveedores',$data);
      $this->load->view('layout/footer.php');
    }

    function delete ()
    {
      $id = $this->uri->segment(3);
      $this->model_proveedores->delete($id);
      $this->index();
    }

    function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_proveedores->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('proveedores/ve_proveedores',$data);
      $this->load->view('layout/footer.php');
    }
    function update()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');
      $this->form_validation->set_rules('nit', 'Nit/Cedula', 'required');
      $this->form_validation->set_rules('direccion', 'direccion', 'required');


      if ($this->form_validation->run() == FALSE )
      {
        $this->edit();
      }
      else {

        $param ['prov_id'] = $this->input->post('id');
        $param ['prov_nombre'] = $this->input->post('nombre');
        $param ['prov_nit'] = $this->input->post('nit');
        $param ['prov_dir'] = $this->input->post('direccion');
        $param ['prov_tel1'] = $this->input->post('telefono1');
        $param ['prov_tel2'] = $this->input->post('telefono2');
        $param ['prov_fax'] = $this->input->post('fax');
        $param ['prov_email'] = $this->input->post('email');
        $param ['prov_observaciones'] = $this->input->post('descripcion');
        $param ['prov_cel'] = $this->input->post('celular');
        $param ['prov_cuenta_banco1'] = $this->input->post('banco1');
        $param ['prov_cuenta_num1'] = $this->input->post('cuenta1');
        $param ['prov_cuenta_tipo1'] = $this->input->post('tipo1');
        $param ['prov_cuenta_banco2'] = $this->input->post('banco2');
        $param ['prov_cuenta_num2'] = $this->input->post('cuenta2');
        $param ['prov_cuenta_tipo2'] = $this->input->post('tipo2');
        $param ['prov_cont_nombre'] = $this->input->post('contacto');
        $param ['prov_cont_email'] = $this->input->post('celcontacto');
        $param ['prov_cont_celular'] = $this->input->post('emailcontacto');

        $this->model_proveedores->update($param);

        $this->index();
        }
      }

      function very_session()
      {
        //echo $this->session->userdata('usuario');
        if (!$this->session->userdata('usuario')==1) {
          //echo "Fordibben";
          redirect(base_url());
        }
      }
  }


 ?>
