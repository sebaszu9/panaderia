<?php

  /**
   *
   */
  class Controller_reportes extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_panaderia');
      $this->load->model('model_materias');
      $this->load->model('model_productos');
      $this->load->model('model_mov_mat');
      $this->load->model('model_mov_prod');
      $this->load->model('model_movimiento');
      $this->load->model('model_conceptos_rec');
      $this->load->library('session');
      $this->load->library('form_validation');
    }

    public function form_materias()
    {
      $date = date('Y-m-d');
      $data['sede'] = $this->model_panaderia->load($date);
      $data['materias'] = $this->model_materias->display($date);
      $data['concepto'] = $this->model_mov_prod->load_concept();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('reportes/form_materias.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function search_materias()
    {
      $data['unidad'] = 0;
      if ($this->input->post('search'))
      {

        $this->form_validation->set_rules('fecha1', 'Fecha Inicial', 'required');
        $this->form_validation->set_rules('fecha2', 'Fecha Final',   'required');

        if ($this->form_validation->run() == FALSE)
        {
          $this->form_materias();
        }
        else
        {
          $tipo = $this->input->post('concepto');
          $data['info'] = $this->model_mov_mat->reporte($tipo);
          $data['uni_empaque'] = 0;

          // parte donde trabaja por ciclo
          $date1 = date('Y-m-d', strtotime($this->input->post('fecha1')));
          $date2 = date('Y-m-d', strtotime($this->input->post('fecha2')));
          $info = $data['info'];
          $rawdata = array();
          $i = 0;
          // ciclo de fecha hasta final
          while ($date1 <= $date2):
            $a = array($i => $date1);
            $b = array($i => 0);
            $textdate = $this->convert_text_date($date1);
            $c = array($i => $textdate);
            $d = array($i => 0);
            foreach($info as $datos)
            {
              $totalmateria = 0;
              $fecha = $datos->cm_fecha;
              // si la fecha es igual al dia de movimiento
              if ($fecha == $date1)
              {
                $consult['movimiento'] = $datos->cm_id;
                $consult['materias']   = $this->input->post('materia');
                // consultar por materia y movimiento
                $materia = $this->model_mov_mat->read_mov_mat_materia($consult);


                //echo "Entro $fecha -->".$consult['movimiento']." -mat: ".$consult['materias']."<br>";

                foreach($materia as $datico) {
                  $data['uni_empaque'] = $datico->unidad_escala;
                  $cant_dia = ($datico->mm_cant/$datico->unidad_escala);
                  $totalmateria = $totalmateria +  $cant_dia;
                  $data['unidad'] = $datico->mat_ue;
                  //echo " $totalmateria + ($datico->mm_cant/$datico->unidad_escala) = $res2134<br>";
                }
                $b[$i] = $b[$i]+$totalmateria;
                $d[$i] = $d[$i]+1;
              }

              $rawdata[$i] =  array(
                  "fecha" =>$a[$i],
                  "total" =>$b[$i],
                  "mes"   =>$c[$i],
                  "cant"  =>$d[$i],
                );
            }
            $i++;
            $date1 =  date('Y-m-d', strtotime($date1."+ 1 days"));
            //echo "=======================<br>";
          endwhile;
          echo "unidad".$data['unidad'];
          //echo json_encode($rawdata);
          $data['ventas'] = $rawdata;

          $this->load->view('layout/header.php');
          $this->load->view('layout/sidebar.php');
          $this->load->view('reportes/graphic_materias.php',$data);
          $this->load->view('layout/footer.php');
          //echo "buscando";
        }

      }
      else
      {
        echo "error not found";
      }
    }

    public function search_materias_informe()
    {
      $this->form_validation->set_rules('fecha1', 'Fecha Inicial', 'required');
      $this->form_validation->set_rules('fecha2', 'Fecha Final', 'required');
      $this->form_validation->set_rules('concepto', 'Concepto', 'required');

      if ($this->form_validation->run() == FALSE)
      {
        $this->form_materias();
      }
      else
      {
        // parte donde trabaja por ciclo
        $search['fecha_1']  = date('Y-m-d', strtotime($this->input->post('fecha1')));
        $search['fecha_2']  = date('Y-m-d', strtotime($this->input->post('fecha2')));
        $search['concepto'] = $this->input->post('concepto');
        $search['sede']     = $this->input->post('sede');
        $data['info']     = $this->model_mov_mat->read_mov_mat_by_mov_advanced($search);
        $data['materias'] = $this->model_materias->display();

        //concepto
        $daconcepto = $this->model_conceptos_rec->read_concept($search['concepto']);

        $data['concepto'] = "";

        foreach ($daconcepto as $concepto) {
          $data['concepto'] = $concepto->c_nombre;
        }


        $textdate1 = $this->convert_text_date($search['fecha_1']);
        $dia1 = date('d', strtotime($search['fecha_1']));
        $data['fecha1'] = "$dia1 de $textdate1";
        $textdate2 = $this->convert_text_date($search['fecha_2']);
        $dia2 = date('d', strtotime($search['fecha_2']));
        $data['fecha2'] = "$dia2 de $textdate2";


        foreach($data['materias'] as $materia){

          $cantidad = 0;
          $materianame = $materia->mat_nombre;

          foreach($data['info'] as $movmat){

            if($materia->mat_id==$movmat->mm_id_mat)
            {
              $cantidad = $cantidad + $movmat->mm_cant;
            }

          }

        }

        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('reportes/inform_materias',$data);
        $this->load->view('layout/footer.php');


      }
    }

    public function search_produccion_informe()
    {
      $this->form_validation->set_rules('fecha1', 'Fecha Inicial', 'required');
      $this->form_validation->set_rules('fecha2', 'Fecha Final', 'required');
      $this->form_validation->set_rules('concepto', 'Concepto', 'required');

      if ($this->form_validation->run() == FALSE)
      {
        $this->form_materias();
      }
      else
      {
        //echo "entro aca";
        // parte donde trabaja por ciclo
        $search['fecha_1']  = date('Y-m-d', strtotime($this->input->post('fecha1')));
        $search['fecha_2']  = date('Y-m-d', strtotime($this->input->post('fecha2')));
        $search['concepto'] = $this->input->post('concepto');
        $search['sede']     = $this->input->post('sede');
        $data['info']     = $this->model_mov_prod->read_mov_prod_by_mov_advanced($search);
        $data['productos'] = $this->model_productos->display();

        $textdate1 = $this->convert_text_date($search['fecha_1']);
        $dia1 = date('d', strtotime($search['fecha_1']));
        $data['fecha1'] = "$dia1 de $textdate1";
        $textdate2 = $this->convert_text_date($search['fecha_2']);
        $dia2 = date('d', strtotime($search['fecha_2']));
        $data['fecha2'] = "$dia2 de $textdate2";

        //concepto
        $daconcepto = $this->model_conceptos_rec->read_concept($search['concepto']);

        $data['concepto'] = "";

        foreach ($daconcepto as $concepto) {
          $data['concepto'] = $concepto->c_nombre;
        }


        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('reportes/inform_productos',$data);
        $this->load->view('layout/footer.php');


      }
    }

    public function search_produccion()
    {
      //echo "aqui es";
      if ($this->input->post('search'))
      {
        $this->form_validation->set_rules('fecha1', 'Fecha Inicial', 'required');
        $this->form_validation->set_rules('fecha2', 'Fecha Final', 'required');

        if ($this->form_validation->run() == FALSE)
        {
          $this->form_produccion();
        }
        else
        {
          $tipo = $this->input->post('tipo');
          $data['info'] = $this->model_mov_prod->reporte($tipo);
          // parte donde trabaja por ciclo
          $date1 = date('Y-m-d', strtotime($this->input->post('fecha1')));
          $date2 = date('Y-m-d', strtotime($this->input->post('fecha2')));


          $info = $data['info'];

          $rawdata = array();
          $i = 0;
          while ($date1 <= $date2):

            $a = array($i => $date1);
            $b = array($i => 0);
            $textdate = $this->convert_text_date($date1);
            $c = array($i => $textdate);
            $d = array($i => 0);

            foreach($info as $datos)
            {
              $totalmateria = 0;

              $fecha = $datos->cm_fecha;
              if ($fecha == $date1)
              {
                $consult['movimiento'] = $datos->cm_id;
                $consult['producto']   = $this->input->post('producto');
                $materia = $this->model_mov_prod->read_mov_prod_produccion($consult);

                foreach($materia as $datico) {
                  $totalmateria = $totalmateria + $datico->mp_cant ;
                  //echo "<tr><td> $data->mm_id </td> <td> $data->mm_cant </td> <td> $data->mm_id_concepto </td><tr>";
                }

                $b[$i] = $b[$i]+$totalmateria;
                $d[$i] = $d[$i]+1;
              }


              $rawdata[$i] =  array(
                  "fecha" =>$a[$i],
                  "total" =>$b[$i],
                  "mes"   =>$c[$i],
                  "cant"  =>$d[$i],
                );
            }
            $i++;
            $date1 =  date('Y-m-d', strtotime($date1."+ 1 days"));
          endwhile;
          //echo json_encode($rawdata);
          $data['ventas'] = $rawdata;

          $this->load->view('layout/header.php');
          $this->load->view('layout/sidebar.php');
          $this->load->view('reportes/graphic_productos.php',$data);
          $this->load->view('layout/footer.php');
          //echo "buscando";
        }

      }
      else
      {
        echo "error not found";
      }
    }

    public function empaque()
    {
      $this->form_validation->set_rules('fecha1', 'Fecha Inicial', 'required');
      $this->form_validation->set_rules('fecha2', 'Fecha Final', 'required');

      if ($this->form_validation->run() == FALSE)
      {
        $this->form_materias();
      }
      else
      {
        //echo "entro aca";
        // parte donde trabaja por ciclo
        $search['fecha_1']  = date('Y-m-d', strtotime($this->input->post('fecha1')));
        $search['fecha_2']  = date('Y-m-d', strtotime($this->input->post('fecha2')));
        $search['sede']     = $this->input->post('sede');
        $data['info']     = $this->model_mov_prod->read_empaque_by_date($search);
        $data['productos'] = $this->model_productos->display();

        $textdate1 = $this->convert_text_date($search['fecha_1']);
        $dia1 = date('d', strtotime($search['fecha_1']));
        $data['fecha1'] = "$dia1 de $textdate1";
        $textdate2 = $this->convert_text_date($search['fecha_2']);
        $dia2 = date('d', strtotime($search['fecha_2']));
        $data['fecha2'] = "$dia2 de $textdate2";

        //concepto
        $daconcepto = $this->model_conceptos_rec->read_concept('2');

        $data['concepto'] = "";

        foreach ($daconcepto as $concepto) {
          $data['concepto'] = $concepto->c_nombre;
        }


        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('reportes/inform_empaque',$data);
        $this->load->view('layout/footer.php');


      }
    }

    // produccion
    public function form_produccion()
    {
      $date = date('Y-m-d');
      $data['sede'] = $this->model_panaderia->load($date);
      $data['productos'] = $this->model_productos->display();
      $data['concepto'] = $this->model_mov_prod->load_concept();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('reportes/form_produccion.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function convert_text_date($date)
    {
      //$data = "2018-05-02";
      $mes = date('m', strtotime($date));
      //echo  $mes;
      if ($mes==1) {
        return  "Enero";
      }
      elseif ($mes==2) {
        return  "Febrero";
      }
      elseif ($mes==3) {
        return  "Marzo";
      }
      elseif ($mes==4) {
        return  "Abril";
      }
      elseif ($mes==5) {
        return  "Mayo";
      }
      elseif ($mes==6) {
        return  "Junio";
      }
      elseif ($mes==7) {
        return  "Julio";
      }
      elseif ($mes==8) {
        return  "Agosto";
      }
      elseif ($mes==9) {
        return  "Septiembre";
      }
      elseif ($mes==10) {
        return  "Octubre";
      }
      elseif ($mes==11) {
        return  "Noviembre";
      }
      elseif ($mes==12) {
        return  "Diciembre";
      }
      else {
        return  "Error";
      }
    }

  }


 ?>
