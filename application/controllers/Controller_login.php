<?php

/**
*
*/
class Controller_login extends CI_Controller
{

  function __construct()
  {
    parent::__construct ();
    $this->load->helper('url');
    $this->load->library('session');
    $this->load->model('model_usuario');
    $this->load->library('form_validation');
    $this->load->library('encrypt');
  }

  public function index(){
    $this->load->view('component/header.php');
    $this->load->view('component/sidebar.php');
    $data['info'] = $this->model_usuario->load();
    $this->load->view('usuarios/vi_usuario',$data);
    $this->load->view('component/footer.php');
  }

  public function login()
  {
    $this->load->view('login.php');
  }

  public function autenticar()
  {
    if ($this->input->post('correo')&&$this->input->post('password'))
    {

      $param ['correo']   = $this->input->post('correo');
      $param ['password'] =  $this->input->post('password');
      //$param ['tipo']     =  1;

      $valid  = $this->model_usuario->valid_email($param);
      $autner = $this->model_usuario->autenticar_login($param);

      if ($valid==false){
        $data['mensaje'] = "Correo no existe.";
        $data['alert']   = "alert-danger";
        $this->load->view('login.php',$data);
      }
      elseif ($autner==false)
      {
        $data['mensaje'] = " Contraseña mal ingresado.";
        $data['alert']   = "alert-danger";
        $this->load->view('login.php',$data);
      }
      else
      {
        //$info = $this->model_usuario->read_login($param);

        foreach($autner->result() as $datos) {
          $response["iduser"] = $datos->usr_id;
          $response["nombre"] = $datos->usr_nombre;
          $response["correo"] = $datos->usr_correo;
          $response["tipo"]   = $datos->usr_tipo_usuario;
          $response["sede"]   = $datos->usr_sede;
        }

        $newdata = array(
              'usuario' => $response["nombre"],
              'correo'  => $response["correo"],
              'iduser'  => $response["iduser"],
              'sede'    => $response["sede"],
              'role'    => $response["tipo"],
              'panaderia' => "gogo",
              'logged'  => TRUE
        );

        if ($response["tipo"]==1)
        {
          //echo "Logeo exitosamente";
          $this->session->set_userdata($newdata);
          redirect(base_url('Welcome/home'));
        }
        elseif ($response["tipo"]==2) {
          //echo "Panadero";
          $this->session->set_userdata($newdata);
          redirect(base_url('Calculos/home'));
        }

        elseif ($response["tipo"]==3) {
          //echo "Panadero";
          $this->session->set_userdata($newdata);
          redirect(base_url('Calculos/domicilios'));
        }

        //redirect(base_url('index.jsp'));
        //header("Location:".base_url('index.jsp'));
      }
    }
    else {
      $this->load->view('forbidden.php');
    }


  }

  public function logout(){
    $this->session->sess_destroy();
    redirect(base_url('login'));
  }





}

?>
