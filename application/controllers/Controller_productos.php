<?php

  /**
   *
   */
  class Controller_productos extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_productos');
      $this->load->model('model_mov_prod');
      $this->load->model('model_cat_prod');
      $this->load->model('model_impuestos');
      $this->load->model('model_stock_prod');
      $this->load->model('model_recetas');
      $this->load->model('model_materias');
      $this->load->library('pdf');
      $this->load->library ('form_validation');
      $this->load->library('session');
    }

    public function load_prod_table()
    {
      if ($this->input->post('safety')==true)
      {
        $catg = $this->input->post('categoria');
        $data = $this->model_productos->load_per_catg($catg);

        foreach($data as $datos) {

          echo "<tr>";
          echo "<td> $datos->prod_nombre <input type='hidden' id='nombre".$datos->prod_id."' value='".$datos->prod_nombre."' ></td>";
          echo '<td> <input type="number" id="cant'.$datos->prod_id.'" value="1" class="form-control"> </td>';
          echo '<td><div class="input-group"><span class="input-group-addon"><i class="fa fa-dollar"></i></span><input type="number" id="costo'.$datos->prod_id.'" class="form-control"> </div></td>';
          echo '<td> <button value="'.$datos->prod_id.'" type="button" class="btn btn-success addcar"><i class="fa fa-cart-plus"></i></button></td>';
          echo "</tr>";
        }
      }
      else {
        echo "data not found";
      }
    }

    public function excel()
    {
      $this->very_session();
      if ($this->input->post('excel'))
      //if (true)
      {
        $sede = $this->input->post('data');
        $productos = $this->model_productos->display();

        $waka['fields'][0] = array('name' =>'Codigo' );
        $waka['fields'][1] = array('name' =>'Nombre' );
        $waka['fields'][2] = array('name' =>'Costo' );
        $waka['fields'][3] = array('name' =>'Precio Venta' );

        $headers = '';
        $filarow = '';

        foreach ($waka['fields'] as $field) {
           $headers .= $field['name'] ."\t";
        }

        foreach ($productos as $value) {
           $row = '';
           $row .= $value->prod_id  ."\t";
           $row .= $value->prod_nombre  ."\t";
           $row .= $value->prod_precio  ."\t";
           $row .= $value->prod_precio  ."\t";
           $filarow .= trim($row)."\n";
        }

        $filarow = str_replace("\r","",$filarow);

        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=Reporte de Productos.xls");
        echo mb_convert_encoding("$headers\n$filarow",'utf-16','utf-8');

      }
    }

    public function pdf()
    {
      $this->very_session();
      //if ($this->input->post('excel'))
      if(true)
      {
        $sede = $this->input->post('data');
        //$productos = $this->model_stock_mat->load_per_bakery($sede);
        $productos = $this->model_productos->display();

        $pdf = new Pdf();
        $this->pdf->AddPage('L','Letter');

        $this->pdf->SetTitle("REPORTE");
        $this->pdf->SetLeftMargin(4);
        $this->pdf->SetRightMargin(4);
        $this->pdf->SetFont('Arial', '', 14);

        $this->pdf->Cell(25,06,'',0,0);
        $this->pdf->Cell(25,06,'Listado de Productos ',0,1);
        $this->pdf->Cell(25,06,'',0,1);

        $this->pdf->Cell(10,06,'',0,0);
        $this->pdf->Cell(25,06,'Codigo',1,0);
        $this->pdf->Cell(120,06,'Nombre',1,0);
        $this->pdf->Cell(25,06,'Costo',1,0);
        $this->pdf->Cell(25,06,'P. venta.',1,1);

        foreach ($productos as $value) {

          $this->pdf->Cell(10,06,'',0,0);
          $this->pdf->Cell(25,06,$value->prod_id,1,0);
          $this->pdf->Cell(120,06,utf8_decode($value->prod_nombre),1,0);
          $this->pdf->Cell(25,06,$value->prod_precio,1,0);
          $this->pdf->Cell(25,06,$value->prod_precio,1,1);

        }
        $this->pdf->output('Inventario.pdf', 'I');
      }

    }

    // agrega carrito de pedido proveedor
    public function addcart_prod()
    {
      if ($this->input->post('safety')==true)
      {
        if (!$this->session->userdata('pedido_producto')==TRUE) {

          $carritop = array();

          $item =  array(
                      "producto" =>$this->input->post('producto'),
                      "nombre" =>$this->input->post('nombre'),
                      "cantidad" =>$this->input->post('cantidad'),
                      "costo"    =>$this->input->post('costo')
                    );

          array_push($carritop,$item);

          $dataseciom['pedido_producto'] = $carritop;
          $this->session->set_userdata($dataseciom);

          $this->load_cart_table();
        }
        else
        {
          $carritop = $this->session->userdata('pedido_producto');

          $item =  array(
                      "producto" =>$this->input->post('producto'),
                      "nombre" =>$this->input->post('nombre'),
                      "cantidad" =>$this->input->post('cantidad'),
                      "costo"    =>$this->input->post('costo')
                    );

          array_push($carritop,$item);
          $dataseciom['pedido_producto'] = $carritop;
          $this->session->set_userdata($dataseciom);

          $this->load_cart_table();
        }
      }
      else
      {
        echo "data not found";
      }
    }

    // carga el carrito de la tabla producto
    private function load_cart_table(){

      $i = 1;
      $carritop = $this->session->userdata('pedido_producto');

      foreach ($carritop as $value) {
        $posicion = $i-1;
        echo "<tr>";
        echo "<td> ".$i." </td>";
        echo "<td> ".$value['nombre']." </td>";
        echo "<td> ".$value['cantidad']." </td>";
        echo "<td> ".number_format($value['costo'])." </td>";
        echo '<td> <button type="button" class="btn btn-info deletet" value="'.$posicion.'"><i class="fa fa-trash"></i></button></td>';
        echo "<tr>";
        $i++;
      }
    }

    // eliminar posicion de carrito
    public function delete_array(){

      if ($this->input->post('safety')==true)
      {
        // obtiene parametro de posicion
        $item = $this->input->post('posicion');

        // obtiene carrito en el Session
        $carritop = $this->session->userdata('pedido_producto');
        // elimina una posicion de array en el carrito
        unset($carritop[$item]);
        $array = array_values($carritop);
        // declara variable
        $dataseciom['pedido_producto'] = $array;
        // y enviar session
        $this->session->set_userdata($dataseciom);
        // cargar carrito de table
        $this->load_cart_table();

      }
      else
      {
        echo "No lo hay.";
      }
    }

    function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->model_productos->display();
      $this->load->view('productos/vi_productos',$data);
      $this->load->view('layout/footer.php');
    }


    function form ()
    {
      $this->very_session();
      $data['categorias'] = $this->model_cat_prod->display();
      $data['impuestos'] = $this->model_impuestos->display();
      $data['error'] = '';
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/vc_productos',$data);
      $this->load->view('layout/footer.php');
    }

    function create ()
    {

      $this->form_validation->set_rules('nombre', 'Nombre', 'required');
      $this->form_validation->set_rules('precio', 'Precio', 'required|numeric');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      else
      {
        $param ['prod_codigo'] = $this->input->post('id');
        $param ['prod_nombre'] = $this->input->post('nombre');
        $param ['prod_precio'] = $this->input->post('precio');
        $param ['prod_cat'] = $this->input->post('categoria');
        $param ['prod_barcode'] = $this->input->post('barras');
        $param ['prod_imp'] = $this->input->post('impuesto');
        $param ['prod_descripcion'] = $this->input->post('descripcion');
        $param ['prod_inv'] = $this->input->post('inventario');
        $param ['prod_vendaje'] = $this->input->post('vendaje');
        $param ['prod_tipo'] = $this->input->post('tipo');

          if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {

              $config['upload_path']          = './uploads/productos/';
              $config['allowed_types']        = 'jpg|gif|png|jpeg|JPG|PNG';
              $config['max_size']             = 2000;
              $config['max_width']            = 2024;
              $config['max_height']           = 2768;
              $config['overwrite']            = true;
              $config['file_name'] = "producto".$this->input->post('prod_id');

              $this->load->library('upload', $config);

              if (! $this->upload->do_upload('userfile')){
                $data['categorias'] = $this->model_cat_prod->display();
                $data['impuestos'] = $this->model_impuestos->display();
                $data['error'] = '';
                $this->load->view('layout/header.php');
                $this->load->view('layout/sidebar.php');
                $this->load->view('productos/vc_productos',$data);
                $this->load->view('layout/footer.php');
              }
              else {
                $id_prod = $this->model_productos->insert($param);
                echo "<script type=text/javascript>alert('Se ha guardado exitosamente el nuevo Producto');</script>";
                if ($param ['prod_tipo'] == 0) {
                  redirect('/Productos/index/');
                }
                elseif ($param ['prod_tipo'] == 2)
                {
                  $paramrec['rp_producto'] = $id_prod;
                  $this->model_productos->insert_recet($paramrec);
                  redirect('/Productos/edit/'.$id_prod);
                }
                else {
                  redirect('/Productos/edit/'.$id_prod);
                }
              }
          }
          else
          {

            $id_prod = $this->model_productos->insert($param);

            echo "<script type=text/javascript>alert('Se ha guardado exitosamente el nuevo Producto');</script>";

            if ($param ['prod_tipo'] == 0)
            {
              redirect('/Productos/index/');
            }
            elseif ($param ['prod_tipo'] == 2)
            {
              $paramrec['rp_producto'] = $id_prod;
              $this->model_productos->insert_recet($paramrec);
              redirect('/Productos/edit/'.$id_prod);
            }
            else {
              redirect('/Productos/edit/'.$id_prod);
            }
          }
        }


      }

    function get ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_productos->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/vd_productos',$data);
      $this->load->view('layout/footer.php');
    }

    function delete ()
    {
      $id = $this->uri->segment(3);
      $this->model_productos->delete($id);
      echo "<script type=text/javascript>alert('Se ha eliminado exitosamente el  Producto');</script>";
      redirect('/Productos/index/');
    }

    function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['productos'] = $this->model_productos->display();
      $data['datos'] = $this->model_productos->read($id);
      $data['combo'] = $this->model_productos->read_combo($id);
      $data['receta'] = $this->model_productos->read_recet_prod($id);
      $data['categorias'] = $this->model_cat_prod->display();
      $data['impuestos'] = $this->model_impuestos->display();

      //recetas_prod
      $data['materias'] = $this->model_materias->display();
      $data['conceptos'] = $this->model_recetas->display_concepto();

      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/ve_productos',$data);
      $this->load->view('layout/footer.php');
    }
    function update()
    {
      $this->form_validation->set_rules('id', 'id', 'required');
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');
      $this->form_validation->set_rules('precio', 'Precio', 'required|numeric');

      if ($this->form_validation->run() == FALSE )
      {
        $this->edit();
      }
      else {
      $param ['prod_id'] = $this->input->post('id');
      $param ['prod_codigo'] = $this->input->post('code');
      $param ['prod_nombre'] = $this->input->post('nombre');
      $param ['prod_precio'] = $this->input->post('precio');
      $param ['prod_costo'] = $this->input->post('costo');
      $param ['prod_cat'] = $this->input->post('categoria');
      $param ['prod_barcode'] = $this->input->post('barras');
      $param ['prod_imp'] = $this->input->post('impuesto');
      $param ['prod_descripcion'] = $this->input->post('descripcion');
      $param ['prod_inv'] = $this->input->post('inventario');
      $param ['prod_vendaje'] = $this->input->post('vendaje');
      $param ['prod_tipo'] = $this->input->post('tipo');

      $this->model_productos->update($param);

      redirect('/Productos/index/');
      }
    }

    public function details()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $var = $this->model_stock_prod->display_stock($id);
      $data['datos'] = $this->model_productos->read($id);
      $data['stock'] = $this->model_stock_prod->display_stock($id);
      /// parametros movimientos prod
      $param['date2'] = date('Y-m-d');
      $date = date('Y-m-d');
      $param['date1'] =  date('Y-m-d',(strtotime ( '-7 day' , strtotime ( $date) ) ));
      $param['prod']=$id;
      $param['concepto']='2';
      $data['movimientos']=$this->model_mov_prod->prom_mov($param);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');

      if ($var == FALSE) {
        $this->load->view('productos/vv2_productos',$data);
      }
      else {
        $this->load->view('productos/vv_productos',$data);
      }
      $this->load->view('layout/footer.php');
    }

    public function add_combo()
    {
      $data = array(
        'comb_padre' => $this->input->post('padre'),
        'comb_hijo' => $this->input->post('hijo'),
        'comb_cant' => $this->input->post('cantidad'),
      );
      $this->model_productos->insert_combo($data);
      echo json_encode(array("status" => TRUE));
    }

    public function delete_combo()
    {
      $id = $this->uri->segment(3);
      $this->model_productos->delete_combo($id);
      echo json_encode(array("status" => TRUE));
    }

    function very_session()
  	{
  		//echo $this->session->userdata('usuario');
  		if (!$this->session->userdata('usuario')==1) {
  			//echo "Fordibben";
        redirect(base_url());
  		}
  	}
  }
 ?>
