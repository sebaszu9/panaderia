<?php

  /**
   *
   */
  class Controller_clientes extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('Model_clientes');
      $this->load->model('Model_usuario');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    public function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->Model_clientes->display();
      $this->load->view('clientes/vi_clientes',$data);
      $this->load->view('layout/footer.php');
    }

    public function form ()
    {
      $this->very_session();
      $data['vendedores'] = $this->Model_usuario->load_seller();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('clientes/vc_clientes.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function create ()
    {
      $this->very_session();
      $this->form_validation->set_rules('name', 'Nombre deL Cliente', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      else
      {
        $param ['clt_cedula']      = $this->input->post('nit');
        $param ['clt_nombre'] = $this->input->post('name');
        $param ['clt_telefono']      = $this->input->post('tel');
        $param ['clt_telefono2']      = $this->input->post('tel2');
        $param ['clt_direccion']      = $this->input->post('dir');
        $param['clt_ruta'] =$this->input->post('vendedor');
        $res =  $this->Model_clientes->create($param);
        $data['mensaje'] = "Guardo exitosamente. ";
        $data['alert']   = "alert-success";
        redirect('/Clientes/index/');
      }
    }

    public function check_id ($id)
    {
      $check= $this->Model_cat_mat->check_id($id);

      if($check)
      {
        $this->form_validation->set_message('check_id','El Codigo '.$id.' Ya se encuentra registrado con otra categoria');
        return FALSE;
      }
      else {
        return TRUE;
      }
    }

    public function get ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->Model_clientes->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('clientes/vd_clientes.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function delete ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $param ['clt_id']      = $this->input->post('id');
      $param ['clt_del']      = 1;
      $this->Model_clientes->update($param);
      redirect('/Clientes/index/');
    }

    public function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->Model_clientes->read($id);
      $data['vendedores'] = $this->Model_usuario->load_seller();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('clientes/ve_clientes.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function update()
    {
      $this->very_session();
      $this->form_validation->set_rules('name', 'Nombre', 'required');
      // $this->form_validation->set_rules('nit', 'Nombre', 'required');
      if ($this->form_validation->run() == FALSE )
      {
        $this->edit();
      }
      else
      {
      $param ['clt_id']      = $this->input->post('id');
      $param ['clt_cedula']      = $this->input->post('nit');
      $param ['clt_nombre'] = $this->input->post('name');
      $param ['clt_telefono']      = $this->input->post('tel');
      $param ['clt_telefono2']      = $this->input->post('tel2');
      $param ['clt_direccion']      = $this->input->post('dir');
      $param['clt_ruta'] =$this->input->post('vendedor');

      $this->Model_clientes->update($param);

      redirect('/Clientes/index/');
      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url().'forbidden');
      }
    }

  }


 ?>
