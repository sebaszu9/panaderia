<?php

  /**
   *
   */
  class Controller_cat_mat extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_cat_mat');
      $this->load->library('session');

    }

    function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['datos'] = $this->model_cat_mat->display();
      $this->load->view('categorias_mat/vi_categorias_mat',$data);
      $this->load->view('layout/footer.php');
    }


    function form ()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_mat/vc_categorias_mat');
      $this->load->view('layout/footer.php');
    }
    function create ()
    {
      $this->very_session();
      $param ['id_categoria'] = $this->input->post('id_categoria');
      $param ['nombre'] = $this->input->post('nombre');
      $param ['descripcion'] = $this->input->post('descripcion');

      $this->model_categorias_mat->create($param);


      echo "<script type=text/javascript>alert('Se ha guardado exitosamente la nueva categoria');</script>";
      $this->load_create();
    }

    function display ()
    {
      $this->very_session();
      $data['datos'] = $this->model_categorias_mat->display();
      $this->load->view('categorias_mat/vi_categorias_mat',$data);

    }

    function load_delete ($id)
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_categorias_mat->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_mat/vld_categorias_mat',$data);
      $this->load->view('layout/footer.php');
    }

    function delete ()
    {
      $id = $this->uri->segment(3);
      $this->model_categorias_mat->delete($id);
      $data['datos'] = $this->model_categorias_mat->display();
      $this->index();
    }

    function load_update ()
    {
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_categorias_mat->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_mat/vu_categorias_mat',$data);
      $this->load->view('layout/footer.php');
    }
    function update()
    {
      $param ['id_categoria'] = $this->input->post('id_categoria');
      $param ['nombre'] = $this->input->post('nombre');
      $param ['descripcion'] = $this->input->post('descripcion');

      $this->model_categorias_mat->update($param);

      $this->index();
    }

    function very_session()
  	{
  		//echo $this->session->userdata('usuario');
  		if (!$this->session->userdata('usuario')==1) {
  			//echo "Fordibben";
        redirect(base_url());
  		}
  	}
  }


 ?>
