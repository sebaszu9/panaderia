<?php

  class Controller_mov_prod extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_productos');
      $this->load->model('model_panaderia');
      $this->load->model('model_proveedores');
      $this->load->model('model_cat_prod');
      $this->load->model('model_mov_prod');
      $this->load->library('session');
      $this->load->model('Model_stock_prod');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    public function index_input()
    {
      $this->very_session();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/entrada/vi_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function search_input()
    {
      $this->very_session();
      echo "Consultando...";
      $search = $this->model_mov_mat->search();
      $this->session->set_flashdata('search',$search);
      redirect(base_url('Movimiento/producto/input/index'), 'refresh');
    }

    public function detail_input()
    {
      $this->very_session();
      $id = $this->uri->segment(5);
      $data['entrada'] = $this->model_mov_prod->read_cmp($id);
      $data['productos'] = $this->model_mov_prod->read_mov_prod($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/entrada/vd_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function edit_input()
    {
      $this->very_session();
      $id = $this->uri->segment(5);
      $data['proveedor'] = $this->model_proveedores->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['entrada'] = $this->model_mov_mat->read_cmm($id);
      $data['productos'] = $this->model_mov_mat->read_mov_mat($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/entrada/ve_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function update_input()
    {
      $this->very_session();
      $param ['cm_id']   = $this->input->post('concepto');
      $param ['cm_obs']   =  $this->input->post('descripcion');
      $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
      // inserta movimiento concepto
      $this->model_mov_mat->update_cmm($param);

      $relacion ['rp_prov'] = $this->input->post('proveedor');
      $relacion ['rp_cm'] = $this->input->post('concepto');
      $this->model_mov_mat->update_rp($relacion);

      redirect('Movimiento/producto/input/edit/'.$param['cm_id']);
    }

     public function form_input()
    {
      $this->very_session();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['categoria'] = $this->model_cat_prod->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/entrada/vc_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

    // actualiza el item de entrada
    public function update_product_mov()
    {
      //echo "actualizo exitosamente";
      $concepto = $this->input->post('concepto');
      $cantidad_ant = $this->input->post('cantdold');
      $param ['id'] = $this->input->post('id');
      $param ['cantd'] = $this->input->post('cantd');
      $param ['price'] = $this->input->post('price');
      $this->model_mov_mat->update_mov_mat($param);

      $cantidad = $param ['cantd'] - $cantidad_ant;
      // modificar inventario
      $mat['id'] = $this->input->post('producto');
      $mat['tipo'] = 1;
      $mat['costo'] = $this->input->post('price');
      $mat['cant'] = $cantidad*$key->mat_inv;
      $mat['sede'] = $this->input->post('panaderia');

      $this->update_inventory($mat);

      $this->session->set_flashdata('mensaje_item','Actualizado.');
      redirect('Movimiento/producto/input/edit/'.$concepto);
    }

    //********************************************************
    //***************** MOVIMIENTO DE SALIDA *****************
    //********************************************************

    public function form_output()
    {
      $this->very_session();
      $data['concepto'] = $this->model_mov_prod->load_concept();
      $data['categoria'] = $this->model_cat_prod->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/salida/vc_salida.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function index_output()
    {
      $this->very_session();
      $data['concepto'] = $this->model_mov_prod->load_concept();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/salida/vi_salida.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function search_output()
    {
      $this->very_session();
      echo "Consultando...";
      $search = $this->model_mov_mat->search_output();
      $this->session->set_flashdata('search',$search);
      redirect(base_url('Movimiento/producto/output/index'), 'refresh');
    }

    public function detail_output()
    {
      $this->very_session();
      $id = $this->uri->segment(5);

      $data['entrada'] = $this->model_mov_mat->read_cmm_output($id);
      $data['productos'] = $this->model_mov_mat->read_mov_mat($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/salida/vd_salida.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function edit_output()
    {
      $this->very_session();
      $id = $this->uri->segment(5);
      $data['concepto'] = $this->model_mov_mat->load_concept();
      $data['panaderias'] = $this->model_panaderia->load();
      $data['entrada'] = $this->model_mov_mat->read_cmm_output($id);
      $data['productos'] = $this->model_mov_mat->read_mov_mat($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/salida/ve_salida.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function update_output()
    {
      $this->very_session();
      $param ['cm_id']   = $this->input->post('id');
      $param ['cm_concepto']   = $this->input->post('concepto');
      $param ['cm_obs']   =  $this->input->post('descripcion');
      $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
      // inserta movimiento concepto
      $this->model_mov_mat->update_cmm($param);

      $relacion ['rp_prov'] = $this->input->post('proveedor');
      $relacion ['rp_cm'] = $this->input->post('concepto');
      $this->model_mov_mat->update_rp($relacion);

      redirect('Movimiento/producto/output/edit/'.$param['cm_id']);
    }

    public function create_output()
    {
      $this->form_validation->set_rules('panaderia', 'Panaderia', 'required');
      $this->form_validation->set_rules('concepto', 'Concepto', 'required');
      $this->form_validation->set_rules('fecha', 'Fecha', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form_output();
      }
      elseif ($this->session->userdata('pedido_producto')==null||count($this->session->userdata('pedido_producto'))==0) {
        $this->session->set_flashdata('mensaje','Error, No hay producto agregado.');
        redirect('Movimiento/producto/output/form');
      }
      else
      {
        $param ['cm_tipo'] = 0;
        $param ['cm_concepto'] = $this->input->post('concepto_cod');
        //$param ['cm_concepto'] = 1;
        $param ['cm_obs'] = $this->input->post('descripcion');
        $param ['cm_ubicacion'] = $this->input->post('panaderia');
        $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
        //$param ['cm_usuario'] = $this->session->userdata('usr_id');
        $param ['cm_usuario'] = 2;
        // inserta movimiento concepto
        $concepto =  $this->model_mov_prod->insert_cmp($param);

        $carritop = $this->session->userdata('pedido_producto');

        foreach ($carritop as $value) {

          $data = array(
                 'mp_cant'        => $value['cantidad'],
                 'mp_id_concepto' => $concepto,
                 'mp_id_prod'      => $value['producto'],
                 'mp_costo'       => $value['costo'],
             );

          $this->model_mov_mat->insert_mov($data);

          $mat['id'] = $value['producto'];
          $mat['tipo'] = 0;
          $mat['costo'] = $value['costo'];
          $mat['cant'] = $value['cantidad'];
          $mat['sede'] = $this->input->post('panaderia');

          $this->update_inventory($mat);
        }
        //session_destroy();
        $this->session->set_flashdata('mensaje','Guardado exitosamente.');
        redirect('Movimiento/producto/output/details/'.$concepto);
      }
    }

    public function create_input(){

      $this->form_validation->set_rules('panaderia', 'Panaderia', 'required');
      $this->form_validation->set_rules('proveedor', 'Proveedor', 'required');
      $this->form_validation->set_rules('fecha', 'Fecha', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form_pedidos();
      }
      elseif ($this->session->userdata('pedido_producto')==null||count($this->session->userdata('pedido_producto'))==0) {
        $this->session->set_flashdata('mensaje','Error, No hay producto agregado.');
        redirect('Movimiento/producto/input/form');
      }
      else
      {
        $param ['cm_tipo'] = 1;
        $param ['cm_concepto'] = 1;
        $param ['cm_obs'] = $this->input->post('descripcion');
        $param ['cm_ubicacion'] = $this->input->post('panaderia');
        $param ['cm_fecha'] = date('Y-m-d', strtotime($this->input->post('fecha')));
        //$param ['cm_usuario'] = $this->session->userdata('usr_id');
        $param ['cm_usuario'] = 2;
        // inserta movimiento concepto
        $concepto =  $this->model_mov_prod->insert_cmp($param);

        $relacion ['rp_prov'] = $this->input->post('proveedor');
        $relacion ['rp_cm']  = $concepto;
        $this->model_mov_prod->insert_rp($relacion);


        $carritop = $this->session->userdata('pedido_producto');

        foreach ($carritop as $value) {

          $data = array(
                 'mp_cant'        => $value['cantidad'],
                 'mp_id_concepto' => $concepto,
                 'mp_id_prod'      => $value['producto'],
                 'mp_costo'       => $value['costo'],
             );

          $this->model_mov_prod->insert_mov($data);

          $mat['id'] = $value['producto'];
          $mat['tipo'] = 1;
          $mat['cant'] = $value['cantidad'];
          $mat['sede'] = $this->input->post('panaderia');

          $this->update_inventory($mat);
          $costo['prod_id'] = $value['producto'];
          $costo['prod_costo'] = $value['costo'];
          $this->model_productos->update($costo);
        }
        //session_destroy();
        $this->session->set_flashdata('mensaje','Guardado exitosamente.');
        redirect('Movimiento/producto/input/details/'.$concepto);
      }

    }
    public function form_pedidos()
    {
      $this->very_session();
      $data['proveedor'] = $this->model_proveedores->display();
      $data['categoria'] = $this->model_cat_prod->display();
      $data['panaderias'] = $this->model_panaderia->load();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('productos/entrada/vc_entrada.php',$data);
      $this->load->view('layout/footer.php');
    }

      public function delete_mov_in()
      {
        $id = $this->uri->segment(3);
        $mov = $this->model_mov_mat->read_mov($id);

        foreach ($mov as $key)
        {
          $mat['id'] = $key->mm_id_mat;
          $mat['tipo'] = 0;
          $mat['cant'] = $key->mm_cant*$key->mat_inv;
          $mat['sede'] = $key->cm_ubicacion;
        }
        $this->update_inventory($mat);
        $this->model_mov_mat->delete_mov($id);
        echo json_encode(array("status" => TRUE));
     }

     public function delete_mov_out()
     {
       $id = $this->uri->segment(3);
       $mov = $this->model_mov_mat->read_mov($id);

       foreach ($mov as $key)
       {
         $mat['id'] = $key->mm_id_mat;
         $mat['tipo'] = 1;
         $mat['cant'] = $key->mm_cant*$key->mat_inv;
         $mat['sede'] = $key->cm_ubicacion;
       }
       $this->update_inventory($mat);
       $this->model_mov_mat->delete_mov($id);
       echo json_encode(array("status" => TRUE));
    }

    public function delete_input()
    {
      $id = $this->uri->segment(5);
      $mov = $this->model_mov_mat->read_mov_mat($id);
      foreach ($mov as $key)
      {
        $mat['id'] = $key->mm_id_mat;
        $mat['tipo'] = 0;
        $mat['cant'] = $key->mm_cant*$key->mat_inv;
        $mat['sede'] = $key->cm_ubicacion;
        $id_mov = $key->mm_id;
        echo 'entro';
        $this->update_inventory($mat);
      }
      $this->model_mov_mat->delete_mov_per_cmm($id);
      $this->model_mov_mat->delete_relation_prov_per_cmm($id);
      $this->model_mov_mat->delete_cmm($id);
      echo "eliminado exitosamente";
      redirect('Movimiento/producto/input/index');
    }

    public function delete_output()
    {
      $id = $this->uri->segment(5);
      $mov = $this->model_mov_mat->read_mov_mat($id);
      foreach ($mov as $key)
      {
        $mat['id'] = $key->mm_id_mat;
        $mat['tipo'] = 1;
        $mat['cant'] = $key->mm_cant*$key->mat_inv;
        $mat['sede'] = $key->cm_ubicacion;
        $id_mov = $key->mm_id;
        echo 'entro';
        $this->update_inventory($mat);
      }
      $this->model_mov_mat->delete_mov_per_cmm($id);
      $this->model_mov_mat->delete_cmm($id);
      echo "eliminado exitosamente";

      redirect('Movimiento/producto/output/index');
    }


    public function update_inventory($prod)
    {
      $this->very_session();
      $param = $this->Model_stock_prod->get_stock($prod);

      if ($param==FALSE)
      {
        $number2 = $prod['cant'];
        $param ['sp_cant'] = $number2;
        $param ['sp_prod'] = $prod['id'];
        $param ['sp_location'] = $prod['sede'];
        // inserta movimiento concepto
        $concepto =  $this->Model_stock_prod->insert_stock($param);
      }
      else {
        # code...
        foreach ($param->result() as $value) {
          ///Obtiene el valor actual, con un ciclo
          $number1 = $value->sp_cant;
        }
        //La cantidad que se va a sumar o a restar vienenen la funcion
        $number2 = $prod['cant'];

        if ($prod['tipo'] == 1) {
          $stock = $number1 + $number2;
        }
        elseif ($prod['tipo'] == 0) {
          $stock = $number1 - $number2;
        }
        $data2 = array(
               'sp_cant' => $stock,
               'sp_prod' => $prod['id'],
               'sp_location' => $prod['sede'],
           );
        $this->Model_stock_prod->update_stock($data2);
      }
    }

    function very_session()
  	{
  		//echo $this->session->userdata('usuario');
  		if (!$this->session->userdata('usuario')==1) {
  			//echo "Fordibben";
        redirect(base_url());
  		}
  	}

  }

 ?>
