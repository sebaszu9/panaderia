<?php

  /**
   *
   */
  class Controller_repartidores extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('Model_clientes');
      $this->load->model('Model_usuario');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    public function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->Model_usuario->load_seller();
      $this->load->view('repartidores/vi_rutas',$data);
      $this->load->view('layout/footer.php');
    }

    public function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->Model_usuario->load_seller_id($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('repartidores/ve_rutas.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function update()
    {
      $this->very_session();
      $this->form_validation->set_rules('name', 'Nombre', 'required');
      // $this->form_validation->set_rules('nit', 'Nombre', 'required');
      if ($this->form_validation->run() == FALSE )
      {
        $this->edit();
      }
      else
      {
      $param ['id']      = $this->input->post('id');
      $param ['name']      = $this->input->post('ruta');
      $param ['nombre_repartidor']      = $this->input->post('name');
      $param ['celular_repartidor'] = $this->input->post('cel');
      $this->Model_usuario->update_repartidor($param);
      redirect('/Rutas/index/');
      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url().'forbidden');
      }
    }

  }


 ?>
