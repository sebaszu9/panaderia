<?php

  /**
   *
   */
  class Controller_conceptos_rec extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_conceptos_rec');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->model_conceptos_rec->display();
      $this->load->view('conceptos_rec/vi_conceptos_rec',$data);
      $this->load->view('layout/footer.php');
    }

    function form ()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('conceptos_rec/vc_conceptos_rec.php');
      $this->load->view('layout/footer.php');
    }

    function create ()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|alpha');

      if ($this->form_validation->run() == FALSE )
      {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('conceptos_rec/vc_conceptos_rec.php');
        $this->load->view('layout/footer.php');

      }
      else {
        $param ['cr_nombre'] = $this->input->post('nombre');
        $param ['cr_descripcion'] = $this->input->post('descripcion');

        $this->model_conceptos_rec->insert($param);
        $this->index();
      }
    }

    function get ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_conceptos_rec->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('conceptos_rec/vd_conceptos_rec',$data);
      $this->load->view('layout/footer.php');
    }

    function delete ()
    {
      $id = $this->uri->segment(3);
      $val = $this->model_conceptos_rec->val_exist($id);
      if ($val == TRUE) {
        echo "<script type=text/javascript>alert('No se ha podido eliminar porque tiene recetas asociadas.');</script>";
        redirect('/Concept_ing/index/');
      }
      else {
        $this->model_conceptos_rec->delete($id);
        redirect('/Concept_ing/index/');
      }

    }

    function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_conceptos_rec->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('conceptos_rec/ve_conceptos_rec',$data);
      $this->load->view('layout/footer.php');
    }

    function update()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('conceptos_rec/vc_conceptos_rec.php');
        $this->load->view('layout/footer.php');

      }
      else {
        $param ['cr_id'] = $this->input->post('id');
        $param ['cr_nombre'] = $this->input->post('nombre');
        $param ['cr_descripcion'] = $this->input->post('descripcion');

        $this->model_conceptos_rec->update($param);
        redirect('/Concept_ing/index/');

      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url());
      }
    }
  }


 ?>
