<?php

    class Controller_mesas extends CI_Controller{

        function __construct()
        {
            parent::__construct ();
            $this->load->helper('url');
            $this->load->model('Model_mesas');
            $this->load->library('form_validation');
            $this->load->library('session');
        }


        // ZONAS

        function zonas()

        {
            $this->very_session();
            $this->load->view('layout/header.php');
            $this->load->view('layout/sidebar.php');
            $data['zonas'] = $this->Model_mesas->display_zonas();
            $this->load->view('layout/footer.php');
            $this->load->view('mesas/zonas',$data);
        }



        function form_zonas ()
        {
            $this->very_session();
            $this->load->view('layout/header.php');
            $this->load->view('layout/sidebar.php');
            $this->load->view('layout/footer.php');
            $this->load->view('mesas/form_zonas');
        }



        function create_zonas ()
        {
          $this->form_validation->set_rules('nombre', 'Nombre', 'required');


          if ($this->form_validation->run() == FALSE )
          {
            $this->load->view('layout/header.php');
            $this->load->view('layout/sidebar.php');
            $this->load->view('mesas/form_zonas');
            $this->load->view('layout/footer.php');

          }
          else {
            $data_form = $this->input->post();
            $data_query["zona_nombre"]=$data_form["nombre"];
            $data_query["zona_descripcion"]=$data_form["descripcion"];
            $data_query["zona_delete"]=0;

            $this->Model_mesas->insert_zonas($data_query);
            redirect("Mesas/zonas/form");
          }

        }

        function remove_zonas($id=NULL){

            if ($id!=NULL) {
                $data_query["zona_delete"]=1;
                $data_query["zona_id"]=$id;
                $this->Model_mesas->delete_zonas($data_query);
                redirect("Mesas/zonas/form");
              }


        }

        function edit_zonas($id=NULL){
            $this->very_session();

            if ($id!=NULL) {
                $data["data_zona"]=$this->Model_mesas->edit_zonas($id);
                $this->load->view('layout/header.php');
                $this->load->view('layout/sidebar.php');
                $this->load->view('layout/footer.php');
                $this->load->view("mesas/edit_zonas",$data);
              }
        }


        function update_zonas(){

          $this->form_validation->set_rules('nombre', 'Nombre', 'required');


          if ($this->form_validation->run() == FALSE )
          {
            $this->load->view('layout/header.php');
            $this->load->view('layout/sidebar.php');
            $this->load->view('mesas/edit_zonas');
            $this->load->view('layout/footer.php');

          }
          else {
            $data_form = $this->input->post();
            $data_query["zona_nombre"]=$data_form["nombre"];
            $data_query["zona_descripcion"]=$data_form["descripcion"];
            $data_query["zona_id"]=$data_form["id"];


            $this->Model_mesas->update_zonas($data_query);
            redirect("Mesas/zonas/form");
          }

        }

        // MESASMesas/zonas/form

        function mesas()

        {
            $this->very_session();
            $this->load->view('layout/header.php');
            $this->load->view('layout/sidebar.php');
            $this->load->view('layout/footer.php');
            $data['mesas'] = $this->Model_mesas->display_mesas();
            $this->load->view('mesas/mesas',$data);
        }

        function form_mesas()

        {
            $this->very_session();
            $this->load->view('layout/header.php');
            $this->load->view('layout/sidebar.php');
            $this->load->view('layout/footer.php');
            $data["nombre_zona"]=$this->Model_mesas->display_zonas();
            $this->load->view('mesas/form_mesas',$data);
        }

        function create_mesas(){

          $this->form_validation->set_rules('codigo', 'Codigo', 'required');
          // $this->form_validation->set_rules('zona', 'Zona', 'required|callback_check_default');
          // $this->form_validation->set_message('check_default', 'Debe seleccionar la zona a la cual pertenece la mesa');

          if ($this->form_validation->run() == FALSE )
          {
            $this->load->view('layout/header.php');
            $this->load->view('layout/sidebar.php');
            $data["nombre_zona"]=$this->Model_mesas->display_zonas();
            $this->load->view('mesas/form_mesas',$data);
            $this->load->view('layout/footer.php');

          }
          else {
            $data_form = $this->input->post();


            $data_query["mesas_codigo"]=$data_form["codigo"];
            $data_query["mesas_zona"]=$data_form["zona"];
            $data_query["mesas_descripcion"]=$data_form["descripcion"];
            $data_query["mesas_delete"]=0;
            // $data["test"]=$data_query;

            $id = $this->Model_mesas->insert_mesas($data_query);
            //////SE CREA LAS CUENTAS PARA LAS MESAS POS
            /// QUE ES DONDE SE ALMACENARÁN INFO DE LAS MESAS EN POS
            ///recupero el id de la mesa en el insert
            $data_cuenta["cuenta_mesa"]=$id;
            $data_cuenta["cuenta_activo"]=0;
            $id =$this->Model_mesas->insert_info_mesas($data_cuenta);
            redirect("Mesas/mesas/form");
            // $this->load->view("mesas/mesas",$data);
          }


        }

        function remove_mesas($id=NULL){

          if ($id!=NULL) {
              $data_query["mesas_delete"]=1;
              $data_query["mesas_id"]=$id;
              $this->Model_mesas->delete_mesas($data_query);
              redirect("Mesas/mesas/form");
            }



        }

        function edit_mesas($id=NULL){
            $this->very_session();

            if ($id!=NULL) {
                $data["nombre_zona"]=$this->Model_mesas->display_zonas();
                $data["data_mesas"]=$this->Model_mesas->edit_mesas($id);
                $this->load->view('layout/header.php');
                $this->load->view('layout/sidebar.php');
                $this->load->view('layout/footer.php');
                $this->load->view("mesas/edit_mesas",$data);
              }
        }

        function update_mesas(){

            $this->form_validation->set_rules('codigo', 'Codigo', 'required');


            if ($this->form_validation->run() == FALSE )
            {
                $this->load->view('layout/header.php');
                $this->load->view('layout/sidebar.php');
                // $data["nombre_zona"]=$this->Model_mesas->display_zonas();
                $this->load->view('mesas/edit_mesas');
                $this->load->view('layout/footer.php');

            }
            else {
              $data_form = $this->input->post();
              $data_query["mesas_codigo"]=$data_form["codigo"];
              $data_query["mesas_zona"]=$data_form["zona"];
              $data_query["mesas_descripcion"]=$data_form["descripcion"];
              $data_query["mesas_id"]=$data_form["id"];


              $this->Model_mesas->update_mesas($data_query);
              redirect("Mesas/mesas/form");
            }

          }


          function very_session()
          {
            //echo $this->session->userdata('usuario');
            if (!$this->session->userdata('usuario')==1) {
              //echo "Fordibben";
              redirect(base_url());
            }
          }






    }

?>
