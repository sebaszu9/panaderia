<?php

  /**
   *
   */
  class Controller_cat_prod extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('Model_cat_prod');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    public function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->Model_cat_prod->display();
      $this->load->view('categorias_prod/vi_categorias_prod',$data);
      $this->load->view('layout/footer.php');
    }


    public function form () {
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_prod/vc_categorias_prod.php');
      $this->load->view('layout/footer.php');
      $this->very_session();
    }

    public function create ()
    {
//      $this->form_validation->set_rules('id', 'Codigo de la categoria', 'required|callback_check_id');
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      else
      {
        $param ['cat_prod_code']      = $this->input->post('id');
        $param ['cat_prod_nombre'] = $this->input->post('nombre');
        $param ['cat_prod_dsc'] = $this->input->post('descripcion');
        $res =  $this->Model_cat_prod->insert($param);
        $data['mensaje'] = "Guardo exitosamente. ";
        $data['alert']   = "alert-success";
        redirect('Categoriasprod/index');
      }
    }

    public function check_id ($id)
    {
      $check= $this->Model_cat_prod->check_id($id);

      if($check)
      {
        $this->form_validation->set_message('check_id','El Código '.$id.' Ya se encuentra registrado con otra categoria');
        return FALSE;
      }
      else {
        return TRUE;
      }

    }

    public function get ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->Model_cat_prod->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_prod/vd_categorias_prod.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function delete ()
    {
      $id = $this->uri->segment(3);
      $this->Model_cat_prod->delete($id);
      redirect('/Categoriasprod/index/');
    }

    public function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->Model_cat_prod->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_prod/ve_categorias_prod.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function update()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->edit();
      }
      else
      {
      $param ['cat_prod_id'] = $this->input->post('cat_id');
      $param ['cat_prod_code'] = $this->input->post('code');
      $param ['cat_prod_nombre'] = $this->input->post('nombre');
      $param ['cat_prod_dsc'] = $this->input->post('descripcion');

      $this->Model_cat_prod->update($param);

      redirect('/Categoriasprod/index/');
      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url().'forbidden');
      }
    }

  }




 ?>
