<?php

  /**
   *
   */
  class Controller_impuestos extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_impuestos');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->model_impuestos->display();
      $this->load->view('impuestos/vi_impuestos',$data);
      $this->load->view('layout/footer.php');
    }

    function form ()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('impuestos/vc_impuestos.php');
      $this->load->view('layout/footer.php');
    }

    function create ()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|alpha');
      $this->form_validation->set_rules('porcentaje', 'Porcentaje', 'required|numeric|max_length[2]');

      if ($this->form_validation->run() == FALSE )
      {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('impuestos/vc_impuestos.php');
        $this->load->view('layout/footer.php');

      }
      else {
        $param ['imp_nombre'] = $this->input->post('nombre');
        $param ['imp_porcentaje'] = $this->input->post('porcentaje');

        $this->model_impuestos->insert($param);
        $this->index();
      }
    }

    function get ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_impuestos->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('impuestos/vd_impuestos',$data);
      $this->load->view('layout/footer.php');
    }

    function delete ()
    {
      $id = $this->uri->segment(3);
      $return = $this->model_impuestos->delete_pipe($id);

      if ($return==true) {
        redirect('/Impuestos/index/');
      }

    }

    function delete_susp()
    {
      $id = $this->uri->segment(3);
      $val = $this->model_impuestos->val_exist($id);
      if ($val == TRUE)
      {
        //echo "<script type=text/javascript>alert('No se ha podido eliminar porque tiene productos asociados.');</script>";
        $this->model_impuestos->delete_pipe($id);
      //  echo "no se elimino error...";
        redirect('/Impuestos/index/');
      }
      else
      {
        $this->model_impuestos->delete_pipe($id);
        redirect('/Impuestos/index/');
      }

    }

    function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->model_impuestos->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('impuestos/ve_impuestos',$data);
      $this->load->view('layout/footer.php');
    }

    function update()
    {
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');
      $this->form_validation->set_rules('porcentaje', 'Porcentaje', 'required|numeric|max_length[2]');

      if ($this->form_validation->run() == FALSE )
      {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('impuestos/vc_impuestos.php');
        $this->load->view('layout/footer.php');

      }
      else {
        $param ['imp_id'] = $this->input->post('id');
        $param ['imp_nombre'] = $this->input->post('nombre');
        $param ['imp_porcentaje'] = $this->input->post('porcentaje');

        $this->model_impuestos->update($param);
        redirect('/Impuestos/index/');

      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url());
      }
    }
  }


 ?>
