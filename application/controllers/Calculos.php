<?php

  /**
   *
   */
  class Calculos extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('model_recetas');
      $this->load->model('model_usuario');
      $this->load->model('model_materias');
      $this->load->model('model_unidades');
      $this->load->model('model_productos');
      $this->load->model('Model_pedidos');
      $this->load->model('model_mov_mat');
      $this->load->model('Model_orden_prod');
      $this->load->model('model_mov_prod');
      $this->load->library('session');
      $this->load->library('form_validation');
      $this->load->model('Model_orden_prod');
      $this->load->model('model_parametrizacion');
      $this->load->model('model_movimiento');
    }

    public function migracion()
    {
      $data = $this->model_movimiento->list_migration();
      $i=0;
      foreach ($data as $key) {
        $i=$i+1;
        if ($key->mm_id<=100000) {
          echo $key->mm_id;
          echo "migracion exitosa<br>";
          $var = $key->mm_id;
          $datos['mm_id']=$key->mm_id+100000;
          $this->model_movimiento->migration($datos,$var);
        }
        else {
          //
          echo $key->mm_id;
          echo "mayor que 100k<br>";
        }

        ///////////////////////
        // $var = $key->cm_id+100000;
        // echo $datos['cm_id']."<br>";
        if ($i>108000) {
          echo "ROMPIO-".$i."-O-";
          break;
        }
      }
      // echo "migracion exitosa";
    }
    public function mensaje()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['mensaje'] = $this->model_parametrizacion->read();
      $this->load->view('pedidos/mensaje',$data);
      $this->load->view('layout/footer.php');
    }

    public function update_mensaje()
    {
      $this->very_session();
      $data['para_texto']=$this->input->post('texto');
      $data['para_int']=$this->input->post('id');
      $data['para_cel']=$this->input->post('cel');
      $data['para_fijo']=$this->input->post('fijo');
      $this->model_parametrizacion->update($data);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['mensaje'] = $this->model_parametrizacion->read();
      $this->load->view('pedidos/mensaje',$data);
      $this->load->view('layout/footer.php');
    }

    public function pruebata()
    {
      $this->very_session();
      $this->load->view('calculos/indexs.php');
    }

    public function home()
    {
      $this->very_session();
      $this->load->view('layout/header-pad.php');
      $this->load->view('calculos/home.php');
      $this->load->view('layout/footer-pad.php');
    }

    public function produccion()
    {
      $this->very_session();
      $datos['date'] =date('Y-m-d');
      $datos['ubicacion'] = $this->session->userdata('sede');
      $datos['panadero'] = '100';
      $data['productos'] = $this->model_mov_prod->produccion($datos);
      $data['materias'] = $this->model_mov_mat->produccion($datos);

      $this->load->view('layout/header-pad.php');
      $this->load->view('calculos/produccion.php',$data);
      $this->load->view('layout/footer-pad.php');
    }

    public function empaque()
    {
      $this->very_session();
      $datos['date'] =date('Y-m-d');
      $datos['ubicacion'] = $this->session->userdata('sede');
      $datos['panadero'] = '100';
      $data['productos'] = $this->model_mov_prod->display_empaque($datos);

      $this->load->view('layout/header-pad.php');
      $this->load->view('calculos/empaque.php',$data);
      $this->load->view('layout/footer-pad.php');
    }
    public function empaque1()
    {
      $this->very_session();
      $datos['date'] = date('Y-m-d');
      $datos['date'] =  date('Y-m-d', strtotime($datos['date']."- 1 days"));
      $datos['ubicacion'] = $this->session->userdata('sede');
      $datos['panadero'] = '100';
      $data['productos'] = $this->model_mov_prod->display_empaque($datos);

      $this->load->view('layout/header-pad.php');
      $this->load->view('calculos/empaque.php',$data);
      $this->load->view('layout/footer-pad.php');
    }

    public function empaque2()
    {
      $this->very_session();
      $datos['date'] = date('Y-m-d');
      $datos['date'] =  date('Y-m-d', strtotime($datos['date']."- 2 days"));
      $datos['ubicacion'] = $this->session->userdata('sede');
      $datos['panadero'] = '100';
      $data['productos'] = $this->model_mov_prod->display_empaque($datos);

      $this->load->view('layout/header-pad.php');
      $this->load->view('calculos/empaque.php',$data);
      $this->load->view('layout/footer-pad.php');
    }

    public function empaque_update()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $empaque = array(
        'emp_id' =>$id,
        'emp_cant' => $this->input->post('cantidad'),
      );
      $this->model_mov_prod->update_empaque($empaque);
      echo json_encode(array("status" => TRUE));
    }

    public function produccion_details()
    {
      $this->very_session();
      $in = $this->uri->segment(3);
      $out = $this->uri->segment(4);
      $data['produccion'] = $this->model_mov_mat->read_produccion($out);
      $data['ing'] = $this->model_mov_mat->read_mov_mat($out);
      $data['producto'] = $this->model_mov_prod->read_mov_prod($in);
      $data['materias'] = $this->model_mov_mat->read_mov_mat($in);

      if ($this->session->userdata('role')==1)
      {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('produccion/vv_produccion.php',$data);
        $this->load->view('layout/footer.php');
      }
      elseif ($this->session->userdata('role')==2)
      {
        $this->load->view('layout/header-pad.php');
        $this->load->view('calculos/details_produccion.php',$data);
        $this->load->view('layout/footer-pad.php');
      }
    }

    public function index()
    {
      $this->very_session();
      if ($this->session->userdata('role')==1)
      {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('calculos/buscar.php');
        $this->load->view('layout/footer.php');
      }
      elseif ($this->session->userdata('role')==2)
      {
        $this->load->view('layout/header-pad.php');
        $this->load->view('calculos/buscar.php');
        $this->load->view('layout/footer-pad.php');
      }
    }

    public function recetas()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $calc = $this->uri->segment(4);
      $data['calculo'] = $calc;
      $data['panadero'] = $this->model_usuario->read_panaderos();
      $data['receta'] = $this->model_recetas->read($id);
      $data['ing'] = $this->model_recetas->display_ing($id);
      $data['ingredientes_prod'] = $this->model_recetas->display_ing_prod($id);
      $data['productos'] = $this->model_recetas->display_prod_calc($calc);
      $data['productos_rec'] = $this->model_recetas->display_prod($id);
      $data['materias'] = $this->model_recetas->display_mat_calc($calc);

      if ($this->session->userdata('role')==1) {
        $this->load->view('layout/header.php');
        $this->load->view('layout/sidebar.php');
        $this->load->view('calculos/recetas2.php',$data);
        $this->load->view('layout/footer.php');
      }
      elseif ($this->session->userdata('role')==2) {
        $this->load->view('layout/header-pad.php');
        $this->load->view('calculos/recetas.php',$data);
        $this->load->view('layout/footer-pad.php');
      }
    }

    public function search()
    {
      header('Access-Control-Allow-Origin: *');
      if ($this->input->post('safety')) {

        $data = $this->input->post('data');
        $query = $this->model_recetas->search($data);

        if ($query==FALSE) {
          echo '<div class="col-xs-6"><h2>No hay recetas.</h2></div>';
        }
        else{
          foreach ($query->result() as $value) {
            //$box = '<a href="'.base_url('Recetas/seleccionar_ing/'.$value->rec_id).'">';
            $box = '';
            $box = $box.'<a href="'.base_url('Recetas/seleccionar_ing/'.$value->rec_id).'"class="col-md-6 col-lg-6 col-xs-12"> ';
            $box = $box.'<div class="small-box bg-yellow">';
            $box = $box. '<div class="inner">';
            $box = $box.  '<h3>'.$value->rec_nombre.'</h3>';
            $box = $box.    '<h4>- '.$value->rec_obs.'</h4>';
            $box = $box.  '</div>';
            $box = $box.'<div class="icon">';
            $box = $box.'<i class="ion ion-ios-cloud"></i>';
            $box = $box.'</div>';
            $box = $box.'<div href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i> </div>';
            $box = $box.'</div>';
            $box = $box.'</a>';
            //$box = $box.'</a>';
            echo $box;
          }

        }
      }
    }

    public function pedidos()
    {
      $this->very_session();
      $sede = $this->session->userdata('sede');
      $data['orden'] = $this->Model_orden_prod->load_per_sede_day($sede);
      $this->load->view('layout/header-pad.php');
      $this->load->view('calculos/pedidos.php',$data);
      $this->load->view('layout/footer-pad.php');
    }

    public function domicilios()
    {
      $this->very_session();
      $sede = $this->session->userdata('sede');
      $datos['pedidos'] = $this->Model_pedidos->load_today();
      $this->load->view('layout/header-pad.php');
      $this->load->view('calculos/domicilios.php',$datos);
      $this->load->view('layout/footer-pad.php');
    }

    // public function domicilios_admin()
    // {
    //   $this->very_session();
    //   $sede = $this->session->userdata('sede');
    //   $datos['orden'] = $this->Model_pedidos->load_today();
    //   $this->load->view('layout/header.php');
    //   $this->load->view('calculos/domicilios-admin.php',$datos);
    //   $this->load->view('layout/footer.php');
    // }


    //Calculos/order/
    public function produccion_details_order()
    {
        if ($this->uri->segment(4))
        {
          $this->very_session();
          $order = $this->uri->segment(4);
          $data['orden']    = $this->Model_orden_prod->read($order);
          $pan = $this->uri->segment(5);
          $data['producto'] = $this->Model_orden_prod->read_prod_producto_id($pan);

          $this->load->view('layout/header-pad.php');
          $this->load->view('calculos/details_produccion_order.php',$data);
          $this->load->view('layout/footer-pad.php');
        }
        else
        {
          echo "Not Found";
        }
    }

    function very_session()
  	{
  		//echo $this->session->userdata('usuario');
  		if (!$this->session->userdata('usuario')==1) {
  			//echo "Fordibben";
        redirect(base_url());
  		}
  	}

    public function prueba()
    {
      $this->load->library('validacion_panaderia');
      $result = $this->validacion_panaderia->check_panaderia();
      if ($result==true) {
        echo "Bloquea panaderia";
      }

    }
  }


 ?>
