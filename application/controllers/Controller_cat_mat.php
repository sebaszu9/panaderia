<?php

  /**
   *
   */
  class Controller_cat_mat extends CI_Controller
  {

    function __construct()
    {
      parent::__construct ();
      $this->load->helper('url');
      $this->load->model('Model_cat_mat');
      $this->load->library('form_validation');
      $this->load->library('session');
    }

    public function index()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $data['info'] = $this->Model_cat_mat->display();
      $this->load->view('categorias_mat/vi_categorias_mat',$data);
      $this->load->view('layout/footer.php');
    }


    public function form ()
    {
      $this->very_session();
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_mat/vc_categorias_mat.php');
      $this->load->view('layout/footer.php');
    }

    public function create ()
    {
      $this->very_session();
      $this->form_validation->set_rules('nombre', 'Nombre de la categoria', 'required');

      if ($this->form_validation->run() == FALSE )
      {
        $this->form();
      }
      else
      {
        $param ['cat_mat_code']      = $this->input->post('id');
        $param ['cat_mat_nombre'] = $this->input->post('nombre');
        $param ['cat_mat_dsc']      = $this->input->post('descripcion');
        $res =  $this->Model_cat_mat->insert($param);
        $data['mensaje'] = "Guardo exitosamente. ";
        $data['alert']   = "alert-success";
        redirect('/Categoriasmat/index/');
      }
    }

    public function check_id ($id)
    {
      $check= $this->Model_cat_mat->check_id($id);

      if($check)
      {
        $this->form_validation->set_message('check_id','El Codigo '.$id.' Ya se encuentra registrado con otra categoria');
        return FALSE;
      }
      else {
        return TRUE;
      }
    }

    public function get ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->Model_cat_mat->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_mat/vd_categorias_mat.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function delete ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $this->Model_cat_mat->delete($id);
      redirect('/Categoriasmat/index/');
    }

    public function edit ()
    {
      $this->very_session();
      $id = $this->uri->segment(3);
      $data['datos'] = $this->Model_cat_mat->read($id);
      $this->load->view('layout/header.php');
      $this->load->view('layout/sidebar.php');
      $this->load->view('categorias_mat/ve_categorias_mat.php',$data);
      $this->load->view('layout/footer.php');
    }

    public function update()
    {
      $this->very_session();
      $this->form_validation->set_rules('nombre', 'Nombre', 'required');
      if ($this->form_validation->run() == FALSE )
      {
        $this->edit();
      }
      else
      {
      $param ['cat_mat_id'] = $this->input->post('id');
      $param ['cat_mat_code'] = $this->input->post('code');
      $param ['cat_mat_nombre'] = $this->input->post('nombre');
      $param ['cat_mat_dsc'] = $this->input->post('descripcion');

      $this->Model_cat_mat->update($param);

      redirect('/Categoriasmat/index/');
      }
    }

    function very_session()
    {
      //echo $this->session->userdata('usuario');
      if (!$this->session->userdata('usuario')==1) {
        //echo "Fordibben";
        redirect(base_url().'forbidden');
      }
    }

  }


 ?>
